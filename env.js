module.exports = {
    PORT: 3133,
    API_VERSION : '1.0.0',
    VERSION_HISTORY : {
        '1.0.0' : 'v1'
    },
    secretAccessKey: "",
    accessKeyId: "",
    region: '', // region of your bucket
}

