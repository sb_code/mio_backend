//=================================================
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const Conference = require("../models/conference");
const Guest = require("../models/guest");
const User = require("../models/user");
const Invitation = require("../models/invitation");
// const bcrypt = require("bcrypt");
const SALT_WORK_FACTOR = 10;
const Mailercontroller = require("../../../services/mailer");

const errorMsgJSON = require("../../../services/errors.json");
const config = require("../config/globals/keyfile");
const AutogenerateIdcontroller = require('../../../services/AutogenerateId');
const Base64ImgUploadmodule = require('../../../services/Base64ImgUploadmodule');
const UserBusiness = require("../BusinessLogic/userBusiness");

var fileupload = require('express-fileupload');

const fs = require('fs');
const AWS = require('aws-sdk');


//=================================================
var OpenTok = require('opentok'),
  opentok = new OpenTok(config.tokbox.apiKey, config.tokbox.apiSecret);

//================================
// Import events module
var events = require('events');

//===========================================

module.exports = {

  /*
      This function is used to get all the list of the api on user type
  */
 changeUserStatus: (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";

    try {

      // Here, we are receiving the user Id
      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Id",
      });
      
      let status = req.body.status ? req.body.status : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - status",
      });

      // Here, Circular json avoided
      if (!req.body.userId || !req.body.status) { return; }

      User.updateOne(
          { "userId" : userId }, 
          { $set: { isActive: status == 'ACTIVE' ? true : false } }, 
          (error, response) => {
          if (error) {
              res.json({
                isError: true,
                message: "Status Was Not Modified",
                statuscode: 404,
                details: null
              });
              return;
          } else {
              res.json({
                isError: false,
                message: "Status Modified Successfully",
                statuscode: 200,
                details: null
              });
              return;
          }
      });

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }

  },


  /*
      This function is used to get all the list of the api on user type
  */
  getUserUsertypeBasis: (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";

    try {

      // Here, we are receiving the Company Id
      let companyId = req.body.companyId ? req.body.companyId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Company Id",
      });

      // Here, we are receiving the User id of those user who created those user
      let createdBy = req.body.createdBy ? req.body.createdBy : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Created By",
      });

      // Here, we are receiving the User Type
      let userType = req.body.userType ? req.body.userType : "COMPANY_OPERATOR";

      // Here, Circular json avoided
      if (!req.body.companyId || !req.body.createdBy) { return; }

      let aggrQuery = [
        {
          $match: {
            'companyId': mongoose.Types.ObjectId(companyId),
            'createdBy': createdBy,
            'userType': userType
          }
        },
        {
          $project: {
            "password": 0
          }
        },
      ];

      User.aggregate(aggrQuery, function (err, response) {

        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;
        }

        if (response.length == 0) {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["204"],
            statuscode: 204,
            details: null
          });

          return;
        }

        if (response.length > 0) {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: {
              Number: response.length,
              response: response
            }
          });

          return;
        }

      });


    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }

  },

  /* 
      This function is written get the user on name basis
  */
  searchUserNameBasis: (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      console.log("..ok");

      // Here, we are receiving the User Name
      let userName = req.body.userName ? req.body.userName : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Name",
      });

      // Here, we are receiving the Company Id
      let companyId = req.body.companyId ? req.body.companyId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Company Id",
      });

      // Here, we are receiving the User Id of the user who created thises users
      // let createdBy = req.body.createdBy ? req.body.createdBy : res.json({
      //   isError: true,
      //   message: errorMsgJSON[lang]["303"] + " - Created By",
      // });

      // Here, we are receiving the User Type
      let userType = req.body.userType ? req.body.userType : "NA";


      // Here, Circular json avoided
      if (!req.body.userName || !req.body.companyId) { return; }

      let findMatch = { 'companyId': mongoose.Types.ObjectId(companyId) };
      if (userType && userType !== "NA") findMatch.userType = userType;
      if (req.body.createdBy) findMatch.createdBy = req.body.createdBy;

      let aggrQuery = [
        {
          $match: findMatch
        },
        {
          $project: {
            "password": 0
          }
        },
        {
          $addFields: {
            fullname: { $concat: ["$fname", " ", "$lname"] }
          }
        },
        { $match: { fullname: { $regex: userName, $options: 'i' } } },
        {
          $project: {
            "password": 0
          }
        },


      ];

      User.aggregate(aggrQuery, function (err, response) {
        res.json({
          response: response
        });
      });

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
      This function is written to delete the user on the basis of given user id
  */
  deleteUserIdBasis: (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      // Here, we are receiving the User Id
      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Id",
      });

      // Here, Circular json avoided
      if (!req.body.userId) { return; }

      // Create an eventEmitter object
      var eventEmitter = new events.EventEmitter();

      // Create an event handler to delete user
      // This event is written to delete the user if user exist
      var delete_user = function delete_user() {

        let deleteQuery = {
          "userId": userId
        }

        User.deleteOne(deleteQuery, function (err) {
          if (err) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            });

            return;
          } else {
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: null
            });
          }

        });
      } // End of delete user event  

      // Bind the delete user event with the handler
      eventEmitter.addListener('delete_user', delete_user);

      // Create an event handler to fetch_user
      // This Event is to check whether the user exist
      var fetch_user = function fetch_user() {

        let aggrQuery = [
          {
            $match: {
              'userId': userId,
            }
          },
          {
            $project: {
              "password": 0
            }
          },
        ];

        User.aggregate(aggrQuery, function (err, response) {

          if (err) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            });

            return;
          }

          if (response.length == 0) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["204"],
              statuscode: 204,
              details: null
            });

            return;
          }

          if (response.length > 0) {
            // Fire the fetch_conference event 
            eventEmitter.emit('delete_user');
          }
        });

      } // End of fetch_user Event Handeler 

      // Bind the fetch_user event with the handler
      eventEmitter.addListener('fetch_user', fetch_user);

      // Fire the fetch_user event 
      eventEmitter.emit('fetch_user');

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },
  /*
      This function is used to fetch all the user on company basis
  */
  getUserCompanyBasis: (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";

    try {
      // Here, we are receiving the Company Id
      let companyId = req.body.companyId ? req.body.companyId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Company Id",
      });

      // Here, we are receiving the User id of those user who created those user
      // let createdBy = req.body.createdBy ? req.body.createdBy : res.json({
      //   isError: true,
      //   message: errorMsgJSON[lang]["303"] + " - Created By",
      // });

      // Here, Circular json avoided
      if (!req.body.companyId) { return; }

      let match = { 'companyId': mongoose.Types.ObjectId(companyId), };
      if (req.body.createdBy) match.createdBy = req.body.createdBy;

      let aggrQuery = [
        {
          $match: match
        },
        {
          $project: {
            "password": 0
          }
        },
      ];

      User.aggregate(aggrQuery, function (err, response) {

        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;
        }

        if (response.length == 0) {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["204"],
            statuscode: 204,
            details: null
          });

          return;
        }

        if (response.length > 0) {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: {
              Number: response.length,
              response: response
            }
          });

          return;
        }

      });

    } catch (error) {

      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        // message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },
  /*
     This function is written to update the user on the basis of user Id
  */
  editUser: (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      // Here, we are receiving the User Id
      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Id",
      });

      // Here, we are receiving the first Name
      let fname = req.body.fname ? req.body.fname : "NA";

      // Here, we are receiving the last Name
      let lname = req.body.lname ? req.body.lname : "NA";

      // Here, we are receiving the User Type
      let userType = req.body.userType ? req.body.userType : "NA";

      // Here, we are receiving the Company Id
      let countryCode = req.body.countryCode ? req.body.countryCode : "NA";

      // Here, we are receiving the email of user
      let email = req.body.email ? req.body.email : "NA";

      // Here, we are receiving the mobile number of the user
      let mobile = req.body.mobile ? req.body.mobile : "NA";

      // Here, we are receiving the service assigned to user in a company 
      let service = req.body.service ? req.body.service : "NA";

      // Here, we are receiving the availablity of user in a company 
      let availablity = req.body.availablity ? req.body.availablity : "NA";

      // Here, we are receiving the isActive of user in a company 
      let isActive = typeof req.body.isActive === "boolean" ? req.body.isActive : 'NA';

      // Here, we are receiving the pricePerMinute 
      let pricePerMinute = req.body.pricePerMinute ? req.body.pricePerMinute : 'NA';

      // Here, Circular json avoided          
      if (!req.body.userId) { return; }

      // var element = {};
      //  element.push({ id: id, quantity: quantity });



      let aggrQuery = [
        {
          $match: {
            'userId': userId,
          }
        },
        {
          $project: {
            '_id': 0,
          }
        },

      ];

      // Here, we are going to search wheather the user exist or not 
      User.aggregate(aggrQuery, function (err, response) {

        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["405"],
            statuscode: 405,
            details: null
          })

          return;
        }

        if (response.length < 1) {
          res.json({
            isError: true,
            reason: "User On this id not exist in the system",
            message: errorMsgJSON[lang]["204"],
            statuscode: 204,
            details: null
          })

          return;

        }

        let updateWith = {};

        if (fname != 'NA') {
          updateWith.fname = fname;
        }

        if (lname != 'NA') {
          updateWith.lname = lname;
        }

        if (userType != 'NA') {
          updateWith.userType = userType;
        }

        if (countryCode != 'NA') {
          updateWith.countryCode = countryCode;
        }

        if (email != 'NA') {
          updateWith.email = email;
        }

        if (mobile != 'NA') {
          updateWith.mobile = mobile;
        }

        if (service != 'NA') {
          updateWith.service = service;
        }

        if (availablity != 'NA') {
          updateWith.availablity = availablity;
        }

        if (isActive != 'NA') {
          updateWith.isActive = isActive;
        }

        if (pricePerMinute != 'NA') {
          updateWith.pricePerMinute = pricePerMinute;
        }



        let updateWhere = {
          "userId": userId,
        }

        User.updateOne(updateWhere, updateWith,
          function (updatederror, updatedresponse) {
            if (updatederror) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["405"],
                statuscode: 405,
                details: null
              })
            } else {
              if (updatedresponse.nModified == 1) {
                res.json({
                  isError: false,
                  message: errorMsgJSON[lang]["200"],
                  statuscode: 200,
                  details: null
                });
              }
              else {
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["405"],
                  statuscode: 405,
                  details: null
                })
              }
            }
          }) // END User.updateOne(updateWhere,updateWith, 

      }) // END User.aggregate(aggrQuery,function(err,response){   

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },
  /* 
      This function is written to add user 
  */
  addNewUser: async (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      // Here, we are receiving the name of the admin
      let createdBy = req.body.createdBy ? req.body.createdBy : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Created By",
      });

      // Here, we are receiving the first Name
      let fname = req.body.fname ? req.body.fname : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - First Name",
      });

      // Here, we are receiving the last Name
      let lname = req.body.lname ? req.body.lname : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Last Name",
      });

      // Here, we are receiving the User Type
      let userType = req.body.userType ? req.body.userType : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Type",
      });

      // Here, we are receiving the Company Id
      let companyId = req.body.companyId ? req.body.companyId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Company Id",
      });

      // Here, we are receiving the Company Type
      let companyType = req.body.companyType ? req.body.companyType : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Company Type",
      });

      // Here, we are receiving the email of user
      let email = req.body.email ? req.body.email : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - email",
      });

      // Here, we are receiving the mobile number of the user
      let mobile = req.body.mobile ? req.body.mobile : 'NA';

      // Here, we are receiving the country Code of the user
      let countryCode = req.body.countryCode ? req.body.countryCode : 'NA';

      // Here, we are receiving the service assigned to user in a company 
      let service = req.body.service ? req.body.service : "NA";

      // Here, we are receiving the availablity of user in a company 
      let availablity = req.body.availablity ? req.body.availablity : "NA";

      // Here, we are receiving the availablity of user in a company 
      let pricePerMinute = req.body.pricePerMinute ? req.body.pricePerMinute : 0;

      // Here, Circular json avoided
      if (!req.body.fname || !req.body.lname || !req.body.userType || !req.body.companyId || !req.body.companyType || !req.body.email || !req.body.createdBy) { return; }

      // This is the tempory password assigned to the user
      let password = Math.floor(100000 + Math.random() * 900000);

      let usrTypeBlock = 'MUSR'
      let userId = AutogenerateIdcontroller.autogenerateId(usrTypeBlock);

      
      // When mobile number was not sent then we have to fetch the mobile and 
      // country code of the company 
      let fetchCompanyDetailStatus = '';
      if (mobile == 'NA') {
          // Here, We are going to fetch the details of conference duration table
          fetchCompanyDetailStatus = await UserBusiness.fetchCompanyDetail(companyId);
          if (fetchCompanyDetailStatus.isError == true) {
              res.json({
                isError: true,
                message: "Error Occur While fetching the company Details on the basis of its Id",
                statuscode: 404,
                details: null
              });
              return;
          }

          if (fetchCompanyDetailStatus.CompanyExist == false) {
              res.json({
                isError: true,
                message: "Either the company does not exist or the company is not active",
                statuscode: 204,
                details: null
              });
              return;
          }
          
          mobile = fetchCompanyDetailStatus.companyDetail[0].mobile ? fetchCompanyDetailStatus.companyDetail[0].mobile : '';
          countryCode = fetchCompanyDetailStatus.companyDetail[0].countryCode ? fetchCompanyDetailStatus.companyDetail[0].countryCode : '';

      } // END if (mobile == 'NA') {

      var newUser = new User({
        fname: fname,
        lname: lname,
        email: email,
        userType: userType,
        mobile: mobile,
        password: password,
        companyId: companyId,
        countryCode: countryCode,
        service: service,
        availablity: availablity,
        createdBy: createdBy,
        pricePerMinute: pricePerMinute,
        userId: userId
      });

      newUser.save(function (err, item) {

        if (item) {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: {
              userType: item.userType,
              isActive: item.isActive,
              isApproved: item.isApproved,
              companyId: item.companyId,
              countryCode: item.countryCode,
              password: password,
              fname: item.fname,
              lname: item.lname,
              email: item.email,
              mobile: item.mobile,
              service: item.service,
              availablity: item.availablity,
              profileCreatedAt: item.profileCreatedAt,
              userId: item.userId,
              createdBy: item.createdBy,
              _id: item._id,

            }

          });
        }

        if (err) {

          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: err["message"]
          });

          return;
        }

      });


      console.log("..ok");

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }

  },






}