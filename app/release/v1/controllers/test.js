const pdfCreator = require('../../../services/pdfCreator');
const errorMsgJSON = require("../../../services/errors.json");

module.exports = {
  uploadFileS3: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";

    let file = req.files.file ? req.files.file : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - file",
    });

    let bucketName = req.body.bucketName ? req.body.bucketName : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - bucketName",
    });

    if (!req.body.bucketName || !req.files.file) return;

    try {
      let url = await s3FileUploader.upload(file, bucketName);
      res.json({
        isError: false,
        message: "",
        statuscode: 200,
        details: url
      });
      return;
    } catch (error) {
      res.json({
        isError: true,
        message: error,
        statuscode: 403,
        details: null
      });
      return;
    }

  },

  pdfGenerate: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";

    let html = req.body.html ? req.body.html : `
    <html>
      <head>
        <style>
          
        </style>
      </head>
      <body>

        <div id='port'>
          <div className="elemClass page">
            <div>Hello!</div>
          </div>
        </div>

      </body>
    </html>
  `;
    let fileName = req.body.fileName ? req.body.fileName : 'invioce_0001.pdf';

    if (!req.body.fileName || !req.body.html) return;
    try {

      let url = await pdfCreator.generatePdf(html, fileName);
      res.json({
        isError: false,
        message: "",
        statuscode: 200,
        details: url
      });
      return;
    } catch (error) {
      res.json({
        isError: true,
        message: error,
        statuscode: 403,
        details: null
      });
      return;
    }
  },

  startSipVezeti: async (req, res, next) => {

  },

  stopSipVezeti: async (req, res, next) => { },
}