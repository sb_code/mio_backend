const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const User = require("../models/user");
const CompanyDetail = require("../models/companyDetail");
const UpgradeRequest = require("../models/upgradeRequest");

const bcrypt = require("bcrypt");
const SALT_WORK_FACTOR = 10;
const Mailercontroller = require("../../../services/mailer");
const errorMsgJSON = require("../../../services/errors.json");
const config = require("../config/globals/keyfile");
const AutogenerateIdcontroller = require('../../../services/AutogenerateId');

const UserBusiness = require("../BusinessLogic/userBusiness");





module.exports = {
  /** ******************************* AUTH-RELATED FEATURES start__******************************** */

  changeCompanyStatus: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";

    try {

      let companyId = req.body.companyId ? req.body.companyId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - company Id",
      });

      let status = req.body.status ? req.body.status : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " -status",
      });

      // avoid concurrent response
      if (!req.body.companyId || !req.body.status) { return; }

      let query = { _id: companyId };

      let updatevalueswith = { $set: { status } };

      CompanyDetail.updateOne(query, updatevalueswith, function (err, response) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;
        } else if (response.nModified == 1) {
          User.updateOne({ companyId }, { $set: { isActive: status == 'IN_ACTIVE' ? false : true } }, (error, response) => {
            if (error) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["404"],
                statuscode: 404,
                details: null
              });
              return;
            } else {
              res.json({
                isError: false,
                message: errorMsgJSON[lang]["200"],
                statuscode: 200,
                details: null
              });
              return;
            }
          });
        } else {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: response
          });
        }
      }); // END User.updateOne(query, updatevalueswith, function(err, ressponse)            


    } catch (error) {

      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: null
      });
    }
  },


  /*
      Here, This function is written to update the company details for a particular user
  */
  updateCompanyDetails: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";

    try {

      let companyId = req.body.companyId ? req.body.companyId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Id",
      });

      let companyName = req.body.companyName ? req.body.companyName : "";

      let companyType = req.body.companyType ? req.body.companyType : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Company Type",
      });

      let walletBalence = req.body.walletBalence ? req.body.walletBalence : '';

      let sipProvider = req.body.sipProvider ? req.body.sipProvider : '';

      let paymentGateway = req.body.paymentGateway ? req.body.paymentGateway : '';


      // avoid concurrent response
      if (!req.body.companyId) { return; }

      let query = { _id: companyId };
      let set = {};
      if (req.body.companyName) set.companyName = req.body.companyName;
      if (req.body.companyType) set.companyType = req.body.companyType;
      if (req.body.noOfSeats) set.noOfSeats = !isNaN(req.body.noOfSeats) ? Number(req.body.noOfSeats) : 0;
      if (req.body.pricePerSeat) set.pricePerSeat = !isNaN(req.body.pricePerSeat) ? Number(req.body.pricePerSeat) : 0;
      if (req.body.trialPeriod) set.trialPeriod = !isNaN(req.body.trialPeriod) ? Number(req.body.trialPeriod) : 0;
      if (req.body.currency) set.currency = req.body.currency ? req.body.currency : 'GBP';
      if (req.body.vat) set.vat = req.body.vat ? req.body.vat : 0;
      if (req.body.services && req.body.services.length > 0) set.services = req.body.services;
      if (walletBalence) set.walletBalence = walletBalence;
      if (sipProvider) set.sipProvider = sipProvider;
      if (paymentGateway) set.paymentGateway = paymentGateway;


      let updatevalueswith = { $set: set };

      CompanyDetail.updateOne(query, updatevalueswith, function (err, response) {
        if (err) {

          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;
        }

        if (response.nModified == 1) {

          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: null
          });

          return;
        }

        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          statuscode: 404,
          details: response
        });

      }); // END User.updateOne(query, updatevalueswith, function(err, ressponse)            


    } catch (error) {
      console.log('error', error);

      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: null
      });
    }

  },

  companyUpgradeRequest: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";

    try {

      let companyId = req.body.companyId ? req.body.companyId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Id",
      });

      let companyName = req.body.companyName ? req.body.companyName : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - companyName",
      });

      let requestedBy = req.body.requestedBy ? req.body.requestedBy : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - requestedBy",
      });

      let currentCompanyType = req.body.currentCompanyType ? req.body.currentCompanyType : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - currentCompanyType",
      });

      let changeToCompanyType = req.body.changeToCompanyType ? req.body.changeToCompanyType : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - changeToCompanyType",
      });

      let changeType = req.body.changeType ? req.body.changeType : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - changeType",
      });

      // avoid concurrent response
      if (!req.body.companyId || !req.body.companyName || !req.body.requestedBy || !req.body.currentCompanyType || !req.body.changeToCompanyType || !req.body.changeType) { return; }
      let newUpgradeRequest = new UpgradeRequest({
        companyId,
        companyName,
        requestedBy,
        currentCompanyType,
        changeToCompanyType,
        changeType, // ['UPGRADE', 'DOWNGRADE']
        status: 'APPLIED',
      });

      newUpgradeRequest.save((error, upgradeRequest) => {
        if (error) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
          return;
        } else {
          CompanyDetail.updateOne({ _id: companyId }, { $set: { upgradeRequestId: upgradeRequest._id } }, (err, response) => {
            if (err) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["404"],
                statuscode: 404,
                details: null
              });
              return;
            } else {
              res.json({
                isError: false,
                message: errorMsgJSON[lang]["200"],
                statuscode: 200,
                details: null
              });
              return;
            }
          });
        }
      });
    } catch (error) {
      console.log('error', error);
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: null
      });
    }
  },

  upgradeRequestList: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";

    try {

      let companyId = req.body.companyId ? req.body.companyId : null;

      UpgradeRequest.find({}, (err, upgradeRequests) => {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
          return;
        } else {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: upgradeRequests
          });
          return;
        }
      });

    } catch (error) {
      console.log('error', error);
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: null
      });
    }
  },

  upgradeRequestAction: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";

    try {

      let companyId = req.body.companyId ? req.body.companyId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - companyId",
      });

      let status = req.body.status ? req.body.status : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - status",
      });

      let requestId = req.body.requestId ? req.body.requestId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - requestId",
      });

      if (!req.body.companyId || !req.body.status || !req.body.requestId) return;
      UpgradeRequest.updateOne({ _id: requestId }, { status }, (err, response) => {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
          return;
        } else {
          CompanyDetail.updateOne({ _id: companyId }, { upgradeRequestId: '' }, (err, response) => {
            if (err) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["404"],
                statuscode: 404,
                details: null
              });
              return;
            } else {
              res.json({
                isError: false,
                message: errorMsgJSON[lang]["200"],
                statuscode: 200,
                details: null
              });
              return;
            }
          })
        }
      });

    } catch (error) {
      console.log('error', error);
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: null
      });
    }
  },
  /**
   * get list of company
   */

  getCompanies: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      // let aggrQuery = [
      //   {
      //     $match: {
      //       "userType": "COMPANY_ADMIN",
      //     }
      //   },
      //   {
      //     $project: {
      //       '_id': 0,
      //       'password': 0
      //     }
      //   },
      //   {
      //     $lookup:
      //     {
      //       from: "companydetails",
      //       localField: "companyId",
      //       foreignField: "_id",
      //       as: "details"
      //     }
      //   }
      // ]

      // User.aggregate(aggrQuery, function (err, response) {
      //   if (err) {
      //     res.json({
      //       isError: true,
      //       message: errorMsgJSON[lang]["404"],
      //       statuscode: 404,
      //       details: null
      //     });
      //   }
      //   else {
      //     res.json({
      //       isError: false,
      //       message: errorMsgJSON[lang]["200"],
      //       statuscode: 200,
      //       details: response
      //     });
      //   }
      // })

      CompanyDetail.find({}, (err, response) => {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
        } else {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: response
          });
        }
      });

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: error
      });
    }
  },

  getCompanyById: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let companyId = req.body.companyId ? req.body.companyId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - companyId",
    });

    if (!req.body.companyId) return;

    try {
      CompanyDetail.find({ _id: companyId }, (err, response) => {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
        } else {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: response && response.length > 0 ? response[0] : {}
          });
        }
      });

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: error
      });
    }
  },
  /**
   * get list of company
   */

  searchCompanies: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      let match = {};
      if (req.body.companyType) match.companyType = req.body.companyType;
      if (req.body.companyName) match.companyName = { $regex: '.*' + req.body.companyName + '.*', "$options": "i" };
      // searchName: { $regex: '.*' + searchName + '.*', "$options": "i" } 

      let aggrQuery = [
        {
          $match: match
        },

      ]

      CompanyDetail.aggregate(aggrQuery, function (err, response) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
        }
        else {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: response
          });
        }
      })

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: error
      });
    }
  },

  /*
     Here, in this function we are retrieving the details of contact
     on the basis of email
  */
  searchContactDetailsByEmail: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Id",
      });

      let emailAddress = req.body.emailAddress ? req.body.emailAddress : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Email Id",
      });

      // avoid concurrent response
      if (!req.body.userId || !req.body.emailAddress) { return; }

      let aggrQuery = [
        {
          $match: {
            'userId': userId,
            'addressBook.emailAddress': emailAddress
          }
        },
        {
          $project: {
            '_id': 0,
            "addressBook": 1
          }
        },
        { $unwind: "$addressBook" },
      ];

      User.aggregate(aggrQuery, function (err, response) {

        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
        }
        else {
          if (response.length == 0) { // when no data found
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["204"],
              statuscode: 204,
              details: null
            });
          } else {

            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: response[0]
            });

          }
        }

      })



    } catch (error) {

      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: null
      });
    }
  },

  /**
   * @description :  // common sign-up process for any type of user (SUPER-ADMIN, CITY-ADMIN, USER)
   */
  searchContactDetails: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Id",
      });
      let page = req.body.page_no ? (parseInt(req.body.page_no == 0 ? 0 : parseInt(req.body.page_no) - 1)) : 0;
      let perPage = req.body.per_page_item ? parseInt(req.body.per_page_item) : 5;
      let serachText = req.body.searchText;
      console.log('.......', typeof serachText)

      let aggrQuery = [
        {
          $match: {
            'userId': userId,
          }
        },
        {
          $project: {
            '_id': 0,
            "addressBook": 1
          }
        },
        { $unwind: "$addressBook" },
        // {
        //   '$match': { 'addressBook.contactName': { "$regex": serachText, "$options": "i" } }
        // },
        {
          '$match': { 'addressBook.name': { "$regex": serachText, "$options": "i" } }
        },


        {
          $group: {
            _id: null,
            total: {
              $sum: 1
            },
            results: {
              $push: '$$ROOT.addressBook'
            }
          }
        },
        {
          $project: {
            '_id': 0,
            'total': 1,
            'noofpage': { $ceil: { $divide: ["$total", perPage] } },
            'results': {
              $slice: [
                '$results', page * perPage, perPage
              ]
            }
          }
        }

      ];
      User.aggregate(aggrQuery, function (err, response) {

        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
        }
        else {
          if (response.length == 0) { // when no data found
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["204"],
              statuscode: 204,
              details: null
            });
          } else {

            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: response[0]
            });

          }
        }

      })

    }
    catch (error) {

      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: null
      });
    }
  },

  // this function is written to delete the particular address book details on the basis of address book id 
  deleteContactDetails: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Id",
      });
      let addressBookId = req.body.addressBookId ? req.body.addressBookId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Address Book Id",
      });
      if (!req.body.userId || !req.body.addressBookId) { return; }
      //=================================================

      let aggrQuery = [
        {
          $match: {
            'userId': userId,
          }
        },
        {
          $project: {
            '_id': 0,
            "addressBook": 1
          }
        },
        { $unwind: "$addressBook" },
        {
          '$match': { 'addressBook._id': mongoose.Types.ObjectId(addressBookId) }
        }
      ];


      // Here, we are going to search wheather any contact exists on this addressBookId for this user 
      User.aggregate(aggrQuery, function (err, response) {

        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
        }
        else {
          // no contact exist
          if (response.length == 0) {
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["204"],
              statuscode: 204,
              details: response[0]
            });
          } else { // contact exist
            // Here, we are deleteing the particular address book on the basis of addressBookId
            User.updateOne({ "userId": userId }, { "$pull": { "addressBook": { "_id": addressBookId } } }, { safe: true, multi: true }, function (updatederror, updatedresponse) {
              if (updatederror) {
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["405"],
                  statuscode: 405,
                  details: null
                })
              }
              else {
                if (updatedresponse.nModified == 1) {
                  res.json({
                    isError: false,
                    message: errorMsgJSON[lang]["200"],
                    statuscode: 200,
                    details: null
                  });
                }
                else {
                  res.json({
                    isError: true,
                    message: errorMsgJSON[lang]["405"],
                    statuscode: 405,
                    details: null
                  })

                }
              }
            })
          }

        }
      })

    }
    catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: null
      });
    }
  },

  updateContactDetails: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Id",
      });
      let addressBookId = req.body.addressBookId ? req.body.addressBookId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Address Book Id",
      });

      let name = req.body.name ? req.body.name : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Contact Name",
      });
      let phoneNumber = req.body.phoneNumber ? req.body.phoneNumber : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Contact Number",
      });
      let emailAddress = req.body.emailAddress ? req.body.emailAddress : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Email Address",
      });
      let countryCode = req.body.countryCode ? req.body.countryCode : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Country Code",
      });
      let userType = req.body.userType ? req.body.userType : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Type",
      });

      //if (!req.body.userId || !req.body.addressBookId || !req.body.contactName || !req.body.contactNumber || !req.body.contactEmailAddress || || !req.body.countryCode ) { return; }

      if (!req.body.userId || !req.body.addressBookId || !req.body.name || !req.body.phoneNumber || !req.body.emailAddress || !req.body.countryCode || !req.body.userType) { return; }

      let updateWith = {
        "addressBook.$.name": name,
        "addressBook.$.phoneNumber": phoneNumber,
        "addressBook.$.emailAddress": emailAddress,
        "addressBook.$.countryCode": countryCode,
        "addressBook.$.userType": userType,
      }

      let updateWhere = {
        "userId": userId,
        "addressBook._id": addressBookId,
      }

      User.updateOne(updateWhere, updateWith, function (updatederror, updatedresponse) {
        if (updatederror) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["405"],
            statuscode: 405,
            details: null
          })
        }
        else {
          if (updatedresponse.nModified == 1) {
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: null
            });
          }
          else {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["405"],
              statuscode: 405,
              details: null
            })

          }
        }
      })
    }
    catch (error) {
      //console.log(error)
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: null
      });
    }
  },

  getContactDetailsById: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Id",
      });
      let addressBookId = req.body.addressBookId ? req.body.addressBookId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Address Book Id",
      });
      if (!req.body.userId || !req.body.addressBookId) { return; }
      let aggrQuery = [
        {
          $match: {
            'userId': userId,
          }
        },
        {
          $project: {
            '_id': 0,
            "addressBook": 1
          }
        },
        { $unwind: "$addressBook" },
        {
          '$match': { 'addressBook.addressBookId': addressBookId }
        }

      ]
      User.aggregate(aggrQuery, function (err, response) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
        }
        else {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: response[0]
          });
        }

      })


    }
    catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: null
      });
    }
  },
  
  getContactList: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      let page = req.body.pageno ? (parseInt(req.body.pageno == 0 ? 0 : parseInt(req.body.pageno) - 1)) : 0;
      let perPage = req.body.perpage ? parseInt(req.body.perpage) : 10;
      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Id",
      });
      if (!req.body.userId) { return; }
      let aggrQuery = [
        {
          $match: {
            'userId': userId,

          }
        },
        {
          $project: {
            '_id': 0,
            "addressBook": 1
          }
        },
        { $unwind: "$addressBook" },
        {
          $group: {
            _id: null,
            total: {
              $sum: 1
            },
            results: {
              $push: '$$ROOT.addressBook'
            }
          }
        },
        {
          $project: {
            '_id': 0,
            'total': 1,
            'noofpage': { $ceil: { $divide: ["$total", perPage] } },
            'results': {
              $slice: [
                '$results', page * perPage, perPage
              ]
            }
          }
        }

      ];
      User.aggregate(aggrQuery, function (err, response) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
        }
        else {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: response[0]
          });
        }

      })
    }
    catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: null
      });
    }
  },

  //================================================================

  /*
      This function is written to multiple address book for a particular user
  */
  addAddressBookMultiple: async (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      // let type = "ADBK";
      // let autouserid;
      // AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
      //     autouserid = data;
      // })
      // Here, we are receiving the user id for which address book has to be added 
      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Id",
      });

      // Here, we are receiving the list of address books to be inserted
      let arrAddressBook = [];
      arrAddressBook = req.body.details ? req.body.details : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Contacts",
      });

      // Avoid circular json
      if (!req.body.userId || !req.body.details) { return; }

      // let insertquery={
      //   "name":arrAddressBook.name,
      //   "phoneNumber":arrAddressBook.phoneNumber,
      //   "emailAddress":arrAddressBook.emailAddress,
      //   "userType":arrAddressBook.userType,
      //   "countryCode":arrAddressBook.countryCode,
      //   "contactUserId":arrAddressBook.contactUserId
      // }

      let name;
      let phoneNumber;
      let emailAddress;
      let userType;
      let countryCode;
      let contactUserId;

      let checkExistanceAddressBookStatus;
      let addAddressBookToUserStatus;
      let insertContactStatus = [];

      for (i = 0; i < arrAddressBook.length; i++) {

        name = arrAddressBook[i].emailAddress ? arrAddressBook[i].emailAddress : "";
        emailAddress = arrAddressBook[i].emailAddress ? arrAddressBook[i].emailAddress : "";
        emailAddress = arrAddressBook[i].emailAddress ? arrAddressBook[i].emailAddress : "";
        emailAddress = arrAddressBook[i].emailAddress ? arrAddressBook[i].emailAddress : "";
        emailAddress = arrAddressBook[i].emailAddress ? arrAddressBook[i].emailAddress : "";
        emailAddress = arrAddressBook[i].emailAddress ? arrAddressBook[i].emailAddress : "";

        // Here, wea checking 
        checkExistanceAddressBookStatus = await UserBusiness.checkExistanceAddressBook(emailAddress, userId);

        if (checkExistanceAddressBookStatus.ErrorStatus == true) {
          let insertContact = {
            "operation": "Some Error Occur",
            "message": errorMsgJSON[lang]["404"],
            "statuscode": 404,
            "emailAddress": emailAddress
          }

          insertContactStatus.push(insertContact);
        } else {
          if (checkExistanceAddressBookStatus.ContactExist == true) {
            let insertContact = {
              "operation": "Address Book Already Inserted",
              "message": errorMsgJSON[lang]["205"],
              "statuscode": 205,
              "emailAddress": emailAddress
            }

            insertContactStatus.push(insertContact);
          } else { // END if (checkExistanceAddressBookStatus.ContactExist == true) 

            addAddressBookToUserStatus = await UserBusiness.addAddressBookToUser(arrAddressBook[i], userId);

            if (addAddressBookToUserStatus.ErrorStatus == true) {
              let insertContact = {
                "operation": "Some Error Occur",
                "message": errorMsgJSON[lang]["404"],
                "statuscode": 404,
                "emailAddress": emailAddress
              }

              insertContactStatus.push(insertContact);
            } else {  // END if (addAddressBookToUserStatus.ErrorStatus == true) {

              if (addAddressBookToUserStatus.isInserted == false) {
                let insertContact = {
                  "operation": "Some Error Occur",
                  "message": errorMsgJSON[lang]["404"],
                  "statuscode": 404,
                  "emailAddress": emailAddress
                }

                insertContactStatus.push(insertContact);

              } else { // END  if (addAddressBookToUserStatus.isInserted == false)
                let insertContact = {
                  "operation": "Address Book Added Successfully",
                  "message": errorMsgJSON[lang]["200"],
                  "statuscode": 200,
                  "emailAddress": emailAddress
                }

                insertContactStatus.push(insertContact);

              } // END OF ELSE of if (addAddressBookToUserStatus.isInserted == false) {

            } // END of if (addAddressBookToUserStatus.ErrorStatus == true) {
          } // END of if (checkExistanceAddressBookStatus.ContactExist == true) {
        }

      } //END for (i = 0; i < arrAddressBook.length; i++) {

      res.json({
        userId: userId,
        details: {
          "insertContactStatus": insertContactStatus
        }
      });

    }
    catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: null
      });
    }
  },


  //================================================================


  addAddressBook: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      let type = "ADBK";
      let autouserid;
      AutogenerateIdcontroller.autogenerateId(type, (err, data) => {
        autouserid = data;
      })

      let name = req.body.name ? req.body.name : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Contact Name",
      });
      let phoneNumber = req.body.phoneNumber ? req.body.phoneNumber : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Phone Number",
      });
      let emailAddress = req.body.emailAddress ? req.body.emailAddress : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Email Address",
      });
      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Id",
      });
      let countryCode = req.body.countryCode ? req.body.countryCode : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Country Code",
      });

      let userType = req.body.userType ? req.body.userType : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Type",
      });

      let contactUserId = req.body.contactUserId ? req.body.contactUserId : "";


      if (!req.body.name || !req.body.phoneNumber || !req.body.emailAddress || !req.body.userId || !req.body.countryCode || !req.body.userType) { return; }

      let aggrQuery = [
        {
          '$match': {
            'userId': userId,
            // 'addressBook.emailAddress': emailAddress 
          }
        },

        {
          $project: {
            '_id': 0,
            "addressBook": 1
          }
        },
        { $unwind: "$addressBook" },
        {
          $match: { 'addressBook.emailAddress': emailAddress }
        }
      ];

      User.aggregate(aggrQuery, async function (fetcherr, fetchresponse) { // start of fetch function
        if (fetcherr) { // When some error occur
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;
        }



        if (fetchresponse) { // successful fetch occur
          if (fetchresponse.length > 0) { // Address book with this email already exist
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["205"],
              statuscode: 205,
              details: fetchresponse
            });
            return;
          }

          if (fetchresponse.length == 0) { // Address book with this email does not exist

            if (userType == config.userTypeForGuest) { // 

              let checkGuestExistanceStatus = await UserBusiness.checkGuestExistance(emailAddress);

              if (checkGuestExistanceStatus.isError == true) { // When Error Occur
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["404"],
                  statuscode: 404,
                  details: null
                });
                return;
              }

              if (checkGuestExistanceStatus.isGuestExist == false) { // GUEST with this email not exist
                // Here, we are inserting the user in guest table
                let insertGuestUserStatus = await UserBusiness.insertGuestUser(userType, name, emailAddress, phoneNumber, countryCode);
                contactUserId = insertGuestUserStatus.guest.userId

                if (insertGuestUserStatus.isError == true) {
                  res.json({
                    isError: true,
                    message: errorMsgJSON[lang]["404"],
                    statuscode: 404,
                    details: null
                  });
                  return;
                }
              }
            } // END if (userType == config.userType) {

            let insertquery = {
              "addressBookId": autouserid,
              "name": name,
              "phoneNumber": phoneNumber,
              "emailAddress": emailAddress,
              "userType": userType,
              "countryCode": countryCode,
              "contactUserId": contactUserId,
            }

            User.updateOne({ 'userId': userId },
              { $push: { "addressBook": insertquery } },
              function (updatederror, updatedresponse) {

                if (updatederror) {
                  res.json({
                    isError: true,
                    message: errorMsgJSON[lang]["405"],
                    statuscode: 405,
                    details: null
                  })
                }
                else {
                  if (updatedresponse.nModified == 1) {
                    res.json({
                      isError: false,
                      message: errorMsgJSON[lang]["200"],
                      statuscode: 200,
                      details: null
                    });
                  }
                  else {
                    res.json({
                      isError: true,
                      message: errorMsgJSON[lang]["405"],
                      statuscode: 405,
                      details: null
                    })

                  }
                }
              })  // end of fetch query
          }
        }

      }); // end of fetch function

    }
    catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: null
      });
    }
  },


  // Here, we are going to fetch the details of the user on the basis of mail
  getUserByemail: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      let email = req.body.email ? req.body.email : res.json({
        message: errorMsgJSON[lang]["303"] + " Email Id"
      });
      if (!req.body.email) { return; }

      let aggrQuery = [
        {
          $match: {
            'email': email
          }
        },
        {
          $project: {
            '_id': 0,
            'password': 0
          }
        }
      ]

      User.aggregate(aggrQuery, function (err, response) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
        }
        else {
          if (response.length == 0) {
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["204"],
              statuscode: 204,
              details: null
            });
          }
          else {
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: response[0]
            });
          }
        }
      })

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: error
      });
    }
  },


  getUserById: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      let userId = req.body.userId ? req.body.userId : res.json({
        message: errorMsgJSON[lang]["303"] + " User Id"
      });
      if (!req.body.userId) { return; }
      let aggrQuery = [
        {
          $match: {
            'userId': userId
          }
        },
        {
          $project: {
            '_id': 0,
            'password': 0
          }
        }
      ]
      User.aggregate(aggrQuery, function (err, response) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
        }
        else {
          if (response.length == 0) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["504"],
              statuscode: 504,
              details: null
            });
          }
          else {
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: response[0]
            });

          }
        }
      })

    }
    catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: null
      });
    }
  },

  searchUserDetails: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      let page = req.body.page_no ? (parseInt(req.body.page_no == 0 ? 0 : parseInt(req.body.page_no) - 1)) : 0;
      let perPage = req.body.per_page_item ? parseInt(req.body.per_page_item) : 5;
      let serachText = req.body.searchText;
      console.log('.......', typeof serachText)
      let aggrQuery = [
        {
          $match: {
          }
        },
        {
          $project: {
            '_id': 0,
          }
        },
        {
          '$match': { 'fname': { "$regex": serachText, "$options": "i" } }
        },
        {
          $group: {
            _id: null,
            total: {
              $sum: 1
            },
            results: {
              $push: '$$ROOT'
            }
          }
        },
        {
          $project: {
            '_id': 0,
            'total': 1,
            'noofpage': { $ceil: { $divide: ["$total", perPage] } },
            'results': {
              $slice: [
                '$results', page * perPage, perPage
              ]
            }
          }
        }
      ]
      User.aggregate(aggrQuery, function (err, response) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: err
          });
        }
        else {
          if (response.length == 0) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["504"],
              statuscode: 504,
              details: null
            });
          }
          else {
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: response[0]
            });

          }
        }
      })
    }
    catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: error
      });
    }
  },
  updateUserDetails: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      let fname = req.body.fname ? req.body.fname : '';
      let lname = req.body.lname ? req.body.lname : '';
      let email = req.body.email ? req.body.email : '';
      let mobile = req.body.mobile ? req.body.mobile : '';
      let userId = req.body.userId ? req.body.userId : '';
      let detailsOne = req.body.detailsOne ? req.body.detailsOne : '';
      let detailsTwo = req.body.detailsTwo ? req.body.detailsTwo : '';
      if (!req.body.userId) { return; }
      let updateWith = {
        "fname": fname,
        "lname": lname,
        "email": email,
        "mobile": mobile,
        "detailsOne": detailsOne,
        "detailsTwo": detailsTwo
      }
      User.updateOne({ 'userId': userId }, updateWith, function (updatederror, updatedresponse) {
        if (updatederror) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["405"],
            statuscode: 405,
            details: null
          })
        }
        else {
          if (updatedresponse.nModified == 1) {
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: null
            });
          }
          else {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["405"],
              statuscode: 405,
              details: null
            })
          }
        }
      })
    }
    catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: null
      });
    }
  },
  signUp: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    var fname = req.body.fname
      ? req.body.fname
      : res.json({
        isError: true,
        statuscode: 303,
        details: null,
        message: errorMsgJSON[lang]["303"] + " first name"
      });
    var lname = req.body.lname
      ? req.body.lname
      : res.json({
        isError: true,
        statuscode: 303,
        details: null,
        message: errorMsgJSON[lang]["303"] + " last name"
      });

    let companyName = fname + " " + lname;
    var email = req.body.email
      ? req.body.email
      : res.json({
        isError: true,
        statuscode: 303,
        details: null,
        message: errorMsgJSON[lang]["303"] + " email"
      });
    var mobile = req.body.mobile
      ? req.body.mobile
      : res.json({
        isError: true,
        statuscode: 303,
        details: null,
        message: errorMsgJSON[lang]["303"] + " mobile"
      });
    var password = req.body.password
      ? req.body.password
      : res.json({
        isError: true,
        statuscode: 303,
        details: null,
        message: errorMsgJSON[lang]["303"] + " password"
      });

    var countryCode = req.body.countryCode
      ? req.body.countryCode
      : res.json({
        isError: true,
        statuscode: 303,
        details: null,
        message: errorMsgJSON[lang]["303"] + " Country Code"
      });

    let walletBalence = req.body.walletBalence ? req.body.walletBalence : '';

    let sipProvider = req.body.sipProvider ? req.body.sipProvider : '';

    let paymentGateway = req.body.paymentGateway ? req.body.paymentGateway : '';

    if (!req.body.fname || !req.body.lname || !req.body.email || !req.body.mobile || !req.body.password || !req.body.countryCode) return;

    let usrTypeBlock = 'MUSR'
    let userId = AutogenerateIdcontroller.autogenerateId(usrTypeBlock);

    var userType = req.body.userType ? req.body.userType : "COMPANY_ADMIN";


    try {

      var newCompanyDetail = new CompanyDetail({
        companyName: companyName,
        createdBy: userId,
        mobile: mobile,
        countryCode: countryCode,

        walletBalence: walletBalence,
        sipProvider: sipProvider,
        paymentGateway: paymentGateway,

        companyType: ''
      });

      newCompanyDetail.save(function (err, company) {
        if (err) {

          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: err["message"]
          });

          return;
        }

        let companyId = company._id;


        let newUser = new User({
          userId: userId,
          companyId: companyId,
          fname: fname,
          lname: lname,
          email: email,
          mobile: mobile,
          countryCode: countryCode,
          password: password,
          userType: userType,
          createdBy: userId,
        });

        newUser.save(function (err, item) {
          if (item) {

            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: item
            });

          }
          if (err) {
            console.log(err);
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: err["message"]
            });
          }
        });  // END newUser.save(function(err, item) {


      }); // END newCompanyDetail.save(function(err, item) 


    } catch (e) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 400,
        details: e
      });
    }
  },

  /**
   * @description :  sign in process using email and password
   */
  signIn: (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";
    var email = req.body.email
      ? req.body.email
      : res.json({
        isError: true,
        statuscode: 303,
        details: null,
        message: errorMsgJSON[lang]["303"] + "Please provide email"
      });
    var password = req.body.password
      ? req.body.password
      : res.json({
        isError: true,
        statuscode: 303,
        details: null,
        message: errorMsgJSON[lang]["303"] + "Please provide password"
      });

    // Here, Circular json avoided
    if (!req.body.email || !req.body.password) { return; }

    let findUserQry = {
      isActive: true,
      email: email
    };

    User.findOne(findUserQry, function (err, item) {

      if (err) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["400"],
          statuscode: 400,
          details: null
        });
      } else {
        if (!item) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["400"],
            statuscode: 400,
            details: null
          });
        } else {
          item.comparePassword(password, async function (err, isMatch) {
            if (isMatch && !err) {
              try {
                user = JSON.parse(JSON.stringify(item));
                delete user.password;
                var token = await jwt.sign(user, config.secret.d, {
                  expiresIn: "12h"
                });
                //Update user isLoggedIn status to true

                User.updateOne(
                  findUserQry,
                  {
                    isLoggedIn: true
                  },
                  function (err, item) {
                    if (err) {
                      res.json({
                        isError: true,
                        message: errorMsgJSON[lang]["400"],
                        statuscode: 400,
                        details: null
                      });
                    } else {

                      CompanyDetail.findOne({ _id: user.companyId }, function (err, companyDetail) {

                        if (err) {
                          res.json({
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statuscode: 200,
                            details: {
                              token: "JWT " + token,
                              data: user
                            }
                          });

                          return;

                        }
                        user.companyName = companyDetail.companyName;
                        user.companyType = companyDetail.companyType;

                        res.json({
                          isError: false,
                          message: errorMsgJSON[lang]["200"],
                          statuscode: 200,
                          details: {
                            token: "JWT " + token,
                            data: user
                          }
                        });

                      })
                    }
                  }
                );
              } catch (error) {
                return res.json({
                  isError: true,
                  message: "Auth failed." + JSON.stringify(error),
                  statuscode: 400,
                  data: null
                });
              }
            } else {
              return res.json({
                isError: true,
                message: "Auth failed.",
                statuscode: 400,
                data: []
              });
            }
          });
        }
      }
    });


    //==========
  },

  /**
   * @description :   forget password feature, will send email to registered email with OTP
   */
  forgotPassword: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let email = req.body.email
      ? req.body.email
      : res.json({
        isError: true,
        statuscode: 303,
        details: null,
        message: errorMsgJSON[lang]["303"] + " - email "
      });
    let query = { email: email };

    User.find(query, function (err, item) {

      if (err) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["400"],
          statuscode: 400,
          details: null
        });
      } else {



        if (item.length == 0) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["204"],
            statuscode: 204,
            details: null
          });
        } else {
          let randomNumber =
            Math.floor(Math.random() * (999999 - 111111 + 1)) + 111111;
          var otpMailDetails = {
            receiver: email,
            subject: "OTP",
            message: "Hi, Your OTP for the verification is " + randomNumber
          };

          Mailercontroller.viaGmail(otpMailDetails, (err, data) => {

            if (err) {
              // res.json({
              //   isError: true,
              //   message: errorMsgJSON[lang]["401"],
              //   //otp: 401,
              //   statuscode: 401,
              //   details: null
              // });

              //================================
              //has to be removed
              var updatevalueswith = {
                $set: {
                  otp: randomNumber
                }
              };
              User.updateOne(query, updatevalueswith, function (err, res1) {
                if (err) {
                  res.json({
                    isError: true,
                    message: errorMsgJSON[lang]["400"],
                    statuscode: 400,
                    details: null
                  });
                }
                if (res1.nModified > 0) {
                  res.json({
                    isError: false,
                    message: errorMsgJSON[lang]["203"],
                    otp: randomNumber,
                    statuscode: 200,
                    details: data
                  });
                }
              });
              //======================
            } else {
              var updatevalueswith = {
                $set: {
                  otp: randomNumber
                }
              };
              User.updateOne(query, updatevalueswith, function (err, res1) {
                if (err) {
                  res.json({
                    isError: true,
                    message: errorMsgJSON[lang]["400"],
                    statuscode: 400,
                    details: null
                  });
                }
                if (res1.nModified > 0) {
                  res.json({
                    isError: false,
                    message: errorMsgJSON[lang]["203"],
                    statuscode: 200,
                    details: data
                  });
                }
              });
            }
          });
        }
      }
    });
  },

  /**
   * @description :   reset password feature, if OTP matches for the user then update password
   */
  resetPassword: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let email = req.body.email
      ? req.body.email
      : res.json({
        isError: true,
        statuscode: 303,
        details: null,
        message: errorMsgJSON[lang]["303"] + " - Email "
      });
    let otp = req.body.otp
      ? req.body.otp
      : res.json({
        isError: true,
        statuscode: 303,
        details: null,
        message: errorMsgJSON[lang]["303"] + " - otp "
      });
    let newPassword = req.body.new_password
      ? req.body.new_password
      : res.json({
        isError: true,
        statuscode: 303,
        details: null,
        message: errorMsgJSON[lang]["303"] + " - New Password "
      });

    var salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
    let hashedNewpassword = await bcrypt.hash(newPassword, salt);

    let query = { email: email };

    User.find(query, function (err, item) {
      if (err) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["400"],
          statuscode: 400,
          details: null
        });
      } else {
        if (item.length == 0) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["503"],
            statuscode: 503,
            details: null
          });
        } else {
          if (item[0].otp.toString().trim() === otp.toString().trim()) {
            var updatevalueswith = {
              $set: {
                password: hashedNewpassword
              }
            };
            User.updateOne(query, updatevalueswith, function (err, res1) {
              if (err) {
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["400"],
                  statuscode: 400,
                  details: null
                });
              }
              if (res1.nModified > 0) {
                res.json({
                  isError: false,
                  message:
                    errorMsgJSON[lang]["200"] +
                    " Password Changed Successfully",
                  statuscode: 200,
                  details: null
                });
              }
            });
          } else {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["503"],
              statuscode: 503,
              details: null
            });
          }
        }
      }
    });
  },

  /**
   * @description : sign out functionality
   */

  signOut: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    var email = req.body.email
      ? req.body.email
      : res.json({
        isError: true,
        statuscode: 303,
        details: null,
        message: errorMsgJSON[lang]["303"] + "Please provide email"
      });

    let findUserQry = {
      email: email
    };

    User.updateOne(
      findUserQry,
      {
        isLoggedIn: false
      },
      function (err, item) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["400"],
            statuscode: 400,
            details: null
          });
        } else {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: null
          });
        }
      }
    );
  }

  /** ******************************* AUTH-RELATED FEATURES __end ******************************** */
};
