const errorMsgJSON = require("../../../services/errors.json");
const ACL = require("../models/acl");

module.exports = {

    //Add new ACL for user type

    addACL: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        var userType = req.body.user_type
          ? req.body.user_type
          : res.json({
              isError: true,
              statuscode: 303,
              details: null,
              message: errorMsgJSON[lang]["303"] + " user type"
            });
        var pageAccessList = req.body.page_access_list
        ? req.body.page_access_list
        : [];

        var componentAccessList = req.body.component_access_list
        ? req.body.component_access_list
        : [];
        
        let newACL = {
            userType : userType,
            pageAccessList : JSON.parse(pageAccessList),
            componentAccessList : JSON.parse(componentAccessList)
        }
       ACL.find({userType : userType},function(err,foundItem){
           if(err){
             // send error message
           }
           else
           {
            if(foundItem)
            {
              // If item found send not allowed message
            }
            else
            { // No item found
                ACL.create(newACL,function(error, item){
                    if(error){
                        // send error response
                    }
                    else{
                        // send success response
                    }
                })
            }
           }   
       }) 
       
    },



    //Edit ACL for user type

    editACL: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        var userType = req.body.user_type
          ? req.body.user_type
          : res.json({
              isError: true,
              statuscode: 303,
              details: null,
              message: errorMsgJSON[lang]["303"] + " user type"
            });
        var pageAccessList = req.body.page_access_list
        ? req.body.page_access_list
        : [];

        var componentAccessList = req.body.component_access_list
        ? req.body.component_access_list
        : [];
        
        let editableACL = {
            userType : userType,
            pageAccessList : JSON.parse(pageAccessList),
            componentAccessList : JSON.parse(componentAccessList)
        }





    },



    //view ACL for user type

    viewACL: (req, res, next) => {
        let lang = req.headers.language ? req.headers.language : "EN";
        var userType = req.body.user_type
          ? req.body.user_type
          : res.json({
              isError: true,
              statuscode: 303,
              details: null,
              message: errorMsgJSON[lang]["303"] + " user type"
            });
        ACL.find({userType : userType},function(err,aclResponse)
        {
            if(err)
            {
                
            }
            else
            {
                if(aclResponse)
                {

                }
            }
        })
    }
};