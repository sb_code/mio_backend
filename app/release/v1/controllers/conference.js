//=================================================
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const Conference = require("../models/conference");
const SipCall = require("../models/sipCall");
const Guest = require("../models/guest");
const User = require("../models/user");
const Invitation = require("../models/invitation");
// const bcrypt = require("bcrypt");
const SALT_WORK_FACTOR = 10;
const MailServices = require("../../../services/mailer");
const logger = require("../../../services/logger");

const errorMsgJSON = require("../../../services/errors.json");
const config = require("../config/globals/keyfile");
const AutogenerateIdcontroller = require('../../../services/AutogenerateId');
const Base64ImgUploadmodule = require('../../../services/Base64ImgUploadmodule');

const ConferenceBusiness = require("../BusinessLogic/conferenceBusiness");
const sipUriHandler = require("../BusinessLogic/sipUriHandler");
const Sip_dialBusiness = require("../BusinessLogic/sip_dialBusiness");
const PriceManagementBusiness = require("../BusinessLogic/priceManagementBusiness");
const Archives = require("../models/archives");


var fileupload = require('express-fileupload');


const fs = require('fs');
const AWS = require('aws-sdk');

// const Nexmo = require('nexmo');
// const nexmo = new Nexmo({
//   apiKey: config.nexmo.apiKey,
//   apiSecret: config.nexmo.apiSecret,
//   applicationId: config.nexmo.applicationId,
//   privateKey: config.nexmo.privateKey,
// }, {});

//=================================================
var OpenTok = require('opentok'),
  opentok = new OpenTok(config.tokbox.apiKey, config.tokbox.apiSecret);

//================================
// Import events module
var events = require('events');

//===========================================

module.exports = {

  /*
     Here, we are fetching the Conference for normal participants,
     here, only those conference are fetched which are active
  */
  fetchConferenceOfParticipants: (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      // Here, we are receiving the userId of the host user
      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " -User Id",
      });

      // Here, Circular json avoided
      if (!req.body.userId) { return; }

      let aggrQuery = [
        {
          $match:
          {
            'participants.userId': userId,
            'isActive': true
          }
        },
      ]

      Conference.aggregate(aggrQuery,
        function (err, response) {
          if (err) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              message: "Error",
              statuscode: 404,
              details: null
            });

            return;
          }

          if (response.length > 0) {
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: response
            });

            return;
          }

          if (response.length < 1) {
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["204"],
              statuscode: 204,
              details: response
            });

            return;
          }
        })


    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
      Fetch Conference for host user,
      here, only those conference are fetched which are not active
  */
  fetchConferenceForHostUser: (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the userId of the host user
      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " -User Id",
      });

      // Here, Circular json avoided
      if (!req.body.userId) { return; }

      let aggrQuery = [
        {
          $match:
          {
            'hostUserId': userId,
            'isActive': false
          }
        },
      ]

      Conference.aggregate(aggrQuery,
        function (err, response) {
          if (err) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              message: "Error",
              statuscode: 404,
              details: null
            });

            return;
          }

          if (response.length > 0) {
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: response
            });

            return;
          }

          if (response.length < 1) {
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["204"],
              statuscode: 204,
              details: response
            });

            return;
          }
        })
      // return;
    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
      This function is written to fetch all the details of conference on the basis of Id
  */
  fetchConferenceById: (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the conference Id for particular conference
      let conferenceId = req.body.conferenceId ? req.body.conferenceId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Conference Id",
      });

      // Here, Circular json avoided
      if (!req.body.conferenceId) { return; }

      let aggrQuery = [
        { $match: { '_id': mongoose.Types.ObjectId(conferenceId) } },
      ]

      Conference.aggregate(aggrQuery,
        function (err, response) {
          if (err) { // When Error Occur
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              message: "Error",
              statuscode: 404,
              details: null
            });

            return;
          }

          if (response.length < 1) { // When No conference exist with this Id
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["204"],
              statuscode: 204,
              details: null
            });

            return;
          }

          if (response.length > 0) { // When conference exist with this Id
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: response
            });

            return;
          }
        }); // End Conference.aggregate      

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },
  /*
      This function is wrritten to fetch the details of a particular 
      participant on the basis of conference Id and userId
  */
  getParticipantDetails: (req, res, next) => {
    console.log(req.body);
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the conference Id for particular conference
      let conferenceId = req.body.conferenceId ? req.body.conferenceId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Conference Id",
      });

      // Here, we are receiving the user Id for particular conference
      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " -User Id",
      });

      // Here, Circular json avoided
      if (!req.body.conferenceId || !req.body.userId) { return; }

      let aggrQuery = [
        { $match: { '_id': mongoose.Types.ObjectId(conferenceId) } },
        {
          $project: {
            '_id': 0,
            "participants": 1
          }
        },
        { $unwind: "$participants" },
        {
          '$match': { 'participants._id': mongoose.Types.ObjectId(userId) }
        }
      ]

      Conference.aggregate(aggrQuery,
        function (err, response) {

          if (err) { // When error Occur
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            });

            return;
          }

          if (response.length == 0) {  // When Participant does not exist
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["204"],
              statuscode: 204,
              details: null
            });

            return;
          }

          if (response.length > 0) { // When Participant Exist
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: response[0].participants
            });
          }



        });

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
      This function is written to update the status of the call 
      generated in the conference
  */
  sipDialStatus: (req, res, next) => {
    logger.info('==================');
    logger.info('Sip dial Status hitted');
    logger.info(req.body);

    //   {
    //  "timestamp":1578990029485,
    //  "call":{
    //       "connectionId":"3f3d4b5f-921f-4a84-8f95-8138e5a33569",
    //       "createdAt":1578989974560,
    //       "id":"1fb9adbd-3c5c-48e1-a385-a8c3fee200d0"
    //   },
    //   "projectId":"46425072",
    //   "event":"callDestroyed",
    //   "reason_message":"Normal Clearing",
    //   "sessionId":"1_MX40NjQyNTA3Mn5-MTU3ODkxMjY1ODY1Mn5uUDc4bS8wK3NYNTZKRmI5TS9RRjJJTXN-fg",
    //   "reason_code":700
    // }


    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      // Here, we are receiving the Session Id for particular conference
      let sessionId = req.body.sessionId ? req.body.sessionId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Session Id",
      });

      // Here, we are receiving the Connection Id for 
      // particular participant for particular conference
      let connectionId = req.body.call.connectionId ? req.body.call.connectionId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Connection Id",
      });

      // Here, we are receiving the Event for particular conference
      let event = req.body.event ? req.body.event : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Event",
      });

      // Here, Circular json avoided
      if (!req.body.sessionId || !req.body.call.connectionId || !req.body.event) {
        res.json({
          isError: false,
          message: errorMsgJSON[lang]["200"],
          statuscode: 200,
          details: null
        });
        return;
      }


      //   {
      //  "timestamp":1578990029485,
      //  "call":{
      //       "connectionId":"3f3d4b5f-921f-4a84-8f95-8138e5a33569",
      //       "createdAt":1578989974560,
      //       "id":"1fb9adbd-3c5c-48e1-a385-a8c3fee200d0"
      //   },
      //   "projectId":"46425072",
      //   "event":"callDestroyed",
      //   "reason_message":"Normal Clearing",
      //   "sessionId":"1_MX40NjQyNTA3Mn5-MTU3ODkxMjY1ODY1Mn5uUDc4bS8wK3NYNTZKRmI5TS9RRjJJTXN-fg",
      //   "reason_code":700
      // }


      let updateWith;

      if (event == "callDestroyed") { // When get terminated



        // Here, we are receiving the reason code for particular call
        let reason_code = req.body.reason_code ? req.body.reason_code : res.json({
          isError: true,
          message: errorMsgJSON[lang]["303"] + " - reason code",
        });

        // Here, we are receiving the reason of disconnection for particular call
        let reason_message = req.body.reason_message ? req.body.reason_message : res.json({
          isError: true,
          message: errorMsgJSON[lang]["303"] + " - reason message",
        });

        // Here, Circular json avoided
        if (!req.body.reason_code || !req.body.reason_message) { return; }

        updateWith = {
          "participants.$.reason_code": reason_code,
          "participants.$.reason_message": reason_message,
          "participants.$.event": event,
        }
      }

      // res.json({
      //       event: event,
      //       updateWith: updateWith
      //   });
      //   return;


      if (event == "callUpdated") { // When call activity is happening

        // Here, we are receiving the state for particular call
        let state = req.body.state ? req.body.state : res.json({
          isError: true,
          message: errorMsgJSON[lang]["303"] + " - reason code",
        });

        // Here, Circular json avoided
        if (!req.body.state) { return; }

        updateWith = {
          "participants.$.state": state,
          "participants.$.event": event
        }
      }

      if (event == "callCreated") { // When call is created

        updateWith = {
          "participants.$.event": event
        }

      }

      let updateWhere = {
        "sessionId": sessionId,
        "participants.connectionId": connectionId,
      }

      Conference.updateOne(updateWhere, updateWith,
        function (updatederror, updatedresponse) {

          if (updatederror) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            });

            return;
          }

          logger.info('In sip dial Status conference is updated');
          logger.info(req.body.event);

          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: null
          });

        });




    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /* 
      This function is written to end the phonic conference for 
      a particular participant
  */
  sipDialEnd: async (req, res, next) => {
    // exit;
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the Conference Id
      let conferenceId = req.body.conferenceId ? req.body.conferenceId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Conference Id",
      });

      // Here, we are receiving the receiver contact number
      let toMobile = req.body.toMobile ? req.body.toMobile : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Receiver Contact Number",
      });

      // Here, we are receiving the receiver country code
      let countryCode = req.body.countryCode ? req.body.countryCode : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Receiver country Code",
      });

      // Here, Circular json avoided
      if (!req.body.conferenceId || !req.body.toMobile) { return; }

      // this function is used to check whether the mobile number exist in the participant 
      async function checkParticipantInConference() {  // start of checkParticipantInConference    
        return new Promise(function (resolve, reject) {
          let aggrQuery = [
            { '$match': { '_id': mongoose.Types.ObjectId(conferenceId) } },
            {
              $project: {
                "conferenceTitle": 1,
                "sessionId": 1,
                "participants": 1
              }
            },

            { $unwind: "$participants" },
            {
              '$match': {
                'participants.contactNumber': toMobile.toString(),
                'participants.countryCode': countryCode.toString(),
              }
            },
          ];

          // Here, we are fetching conference details 
          Conference.aggregate(aggrQuery, function (fetchConferenceErr, fetchConferenceResponse) { // start fetch conference
            if (fetchConferenceErr) { // When some error occur
              resolve({
                "isError": true,
              });
            }


            if (fetchConferenceResponse) { // START fetchConferenceResponse

              if (fetchConferenceResponse.length == 0) { // When no participant available on this number
                resolve({
                  "isError": false,
                  "isParticipantExist": false,
                });
              } // END fetchConferenceResponse.length == 0

              if (fetchConferenceResponse.length > 0) { // When participant available on this number
                let sessionId = fetchConferenceResponse[0].sessionId ? fetchConferenceResponse[0].sessionId : "";
                let connectionId = fetchConferenceResponse[0].participants.connectionId ? fetchConferenceResponse[0].participants.connectionId : "";

                resolve({
                  "isError": false,
                  "isParticipantExist": true,
                  "sessionId": sessionId,
                  "connectionId": connectionId,
                });

              } // END fetchConferenceResponse.length > 0  
            } // END fetchConferenceResponse
          })
        })
      } // start of checkParticipantInConference

      //*****************************************************************************

      // this function is used to disconnect the call
      async function disconnectCall(sessionId, connectionId) {  // start of checkUser     
        return new Promise(function (resolve, reject) {
          opentok.forceDisconnect(sessionId, connectionId, function (err) {
            if (err) { // When some error occur
              resolve({
                "isError": true,
              });
            } else {
              resolve({
                "isError": false
              });
            }
          })
        }) // END  Promise   
      } // start of disconnectCall

      //************************************************************************************

      // this function is used to used to update the field updated at sipCall collection
      async function updateSipCall(sessionId, connectionId) {  // start of updateSipCall     
        return new Promise(function (resolve, reject) {
          let updatedAt = Date.now();
          let updateWith = {
            "updatedAt": updatedAt.toString(),
          }

          let updateWhere = {
            "sessionId": sessionId,
            "connectionId": connectionId,
          }

          SipCall.updateOne(updateWhere, updateWith,
            function (updatederror, updatedresponse) {

              if (updatederror) {
                resolve({
                  "isError": true,
                });
              } else {
                resolve({
                  "isError": false,
                });
              }

            })
        }) // END  Promise   
      } // start of updateSipCall

      //************************************************************************************

      // start executing the operation for each number
      let checkParticipantInConferenceStatus = await checkParticipantInConference();

      if (checkParticipantInConferenceStatus.isError == true) { // When Error in fetching the participant
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          statuscode: 404,
          details: null
        })

        return;

      }

      if (checkParticipantInConferenceStatus.isParticipantExist == false) {
        res.json({
          isError: false,
          message: errorMsgJSON[lang]["204"],
          statuscode: 204,
          details: null
        })

        return;
      }

      let sessionId = checkParticipantInConferenceStatus.sessionId;
      let connectionId = checkParticipantInConferenceStatus.connectionId;

      // Here, we are disconnecting the call
      let disconnectCallStatus = await disconnectCall(sessionId, connectionId);

      if (disconnectCallStatus.isError == true) { // Some error in disconnecting the call
        res.json({
          isError: true,
          message: "Either the call was already disconnected or some error occur",
          statuscode: 404,
          details: null
        })

        return;
      }

      // Here, we are update the updateAt status in sipcall connection
      let updateSipCallStatus = await updateSipCall(sessionId, connectionId);

      if (updateSipCallStatus.isError == true) {
        res.json({
          isError: true,
          message: "Some error occur while updating the sip call",
          statuscode: 404,
          details: null
        })

        return;
      }

      res.json({
        isError: false,
        message: "call Disconnected",
        statuscode: 412,
        details: null
      })

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }

  },


  /* 
      This function is used to upload partcipant's file on amazon s3 bucket
  */
  uploadParticipantFile: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // console.log(req.files);
      // Here, we are receiving the file to be uploaded
      let file = req.files.file ? req.files.file : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - File",
      });

      // Here, Circular json avoided
      if (!req.files.file) { return; }

      let AWS_ACCESS_KEY = config.aws_s3.accessKeyId;
      let AWS_SECRET_ACCESS_KEY = config.aws_s3.secretAccessKey;
      let bucketName = config.aws_s3.bucketName;
      // name of the file to be uploaded
      let fileNameOriginal = file.name;

      // deriving a file name which is to be uploaded
      var arrfileName = fileNameOriginal.split(".");
      var fileExtension = arrfileName.reverse()[0];
      arrfileName.reverse(); // reversing an array
      arrfileName.pop(); // deleting an array from an array
      var date = new Date();
      var timestamp = date.getTime();

      var fileName = arrfileName.join();
      fileName = fileName + '-' + timestamp + '.' + fileExtension;


      // local location where the file has to be uploaded
      let fileToUpload = __dirname + '/uploads/' + fileName;
      let fileUrl = "https://test-subendu-bucket.s3.us-east-2.amazonaws.com/" + fileName;
      //let fileUrl = config.aws_s3.aws_url+bucketName+"/"+fileName;

      const s3 = new AWS.S3({
        accessKeyId: AWS_ACCESS_KEY,
        secretAccessKey: AWS_SECRET_ACCESS_KEY
      });

      // Import events module
      var events = require('events');

      // Create an eventEmitter object
      var eventEmitter = new events.EventEmitter();

      // Create an event handler as follows
      var upload_file_s3_bucket = function upload_file_s3_bucket() { // Start of creation of event handeler
        const fileContent22 = fs.readFileSync(fileToUpload);

        const params = {
          Bucket: bucketName, // pass your bucket name
          Key: fileName,
          ACL: 'public-read',
          Body: fileContent22
        };
        s3.upload(params, function (err, data) {
          if (err) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            })
          }

          if (data) {
            fs.unlink(fileToUpload, function (err, response) {
              if (err) {
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["411"],
                  statuscode: 411,
                  details: null
                })

                return;
              }

              res.json({
                isError: false,
                message: "File Uploaded",
                statuscode: 200,
                detail: {
                  url: fileUrl
                }
              })

            });

          }

        });

      } // Start of creation of event handeler

      // Bind the connection event with the handler
      eventEmitter.on('upload_file_s3_bucket', upload_file_s3_bucket);

      // uploading the files
      file.mv(fileToUpload, function (err, response) { // Start of File upload in local storage
        if (err) { // When Error Occur
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["411"],
            statuscode: 411,
            details: null
          })

          return;
        }

        // Fire the data_received event 
        eventEmitter.emit('upload_file_s3_bucket');

      }); // End of File upload in local storage        

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
       This function is used to add participant to the conference
  */
  addParticipant: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the Conference Id
      let conferenceId = req.body.conferenceId ? req.body.conferenceId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Conference Id",
      });

      // here we are assigning the users going to join the conference


      var arrParticipant = new Array();
      var arrParticipantOld = new Array();
      arrParticipantOld = req.body.participants;

      for (var i in arrParticipantOld) {
        if (arrParticipantOld[i].userId != "") { // when the user is not given
          arrParticipant.push(arrParticipantOld[i]);
        }
      }

      // number of participants going to join the conference
      let noOfParticipants = req.body.participants ? arrParticipant.length : 0;

      var participants_details = [];


      for (var i in arrParticipant) {  // start of for loop

        let userId = arrParticipant[i].userId;
        let userType = arrParticipant[i].userType ? arrParticipant[i].userType : "USER";

        let aggrQuery = [
          { '$match': { '_id': mongoose.Types.ObjectId(conferenceId) } },
          {
            $project: {
              "conferenceTitle": 1,
              "participants": 1
            }
          },

          { $unwind: "$participants" },
          {
            '$match': {
              'participants.userId': userId
            }
          },
        ];

        // Here, we are fetching conference details 
        Conference.aggregate(aggrQuery, function (fetchConferenceErr, fetchConferenceResponse) { // start fetch conference
          if (fetchConferenceErr) { // When some error occur
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            })

            return;
          }




          if (fetchConferenceResponse) { // On successful fetch

            if (fetchConferenceResponse.length > 0) { // User is already a participant of this conference

              let data = { // details of each paricipants inserted here
                "userId": userId,
                // "userType": userType,
                "userType": fetchConferenceResponse[0].participants.userType,
                "message": "Already Exist"
              };

              participants_details.push(data);// inserting details of each user in this array

              if (participants_details.length == noOfParticipants) { // when last participant get processed
                res.json({
                  isError: false,
                  message: errorMsgJSON[lang]["200"],
                  statuscode: 200,
                  participants: participants_details
                })
              }

            }

            if (fetchConferenceResponse.length == 0) { // we have to join the user as a participant of the conference

              if (userType != "GUEST") { // when the user type is User then partipant will be user

                let aggrQuery = [
                  {
                    '$match': {
                      'userId': userId
                    }
                  },
                ];

                // Here, we are fetching user details 
                User.aggregate(aggrQuery, function (fetchUserErr, fetchUserResponse) { // start fetch user
                  if (fetchUserErr) { // When error occur

                    res.json({
                      isError: false,
                      message: errorMsgJSON[lang]["404"],
                      statuscode: 404,
                      details: null
                    })

                    return;
                  }



                  if (fetchUserResponse) { // No error occur
                    if (fetchUserResponse.length == 0) { // When no user exist with this email id
                      let data = { // details of each paricipants inserted here
                        "userId": userId,
                        "userType": userType,
                        "message": "User not exist in the system"
                      };

                      participants_details.push(data);// inserting details of each user in this array

                      if (participants_details.length == noOfParticipants) { // when last participant get processed
                        res.json({
                          isError: false,
                          message: errorMsgJSON[lang]["200"],
                          statuscode: 200,
                          participants: participants_details
                        })
                      }
                    }

                    if (fetchUserResponse.length > 0) { // When user exist, we have to add him as a participant
                      let fname = fetchUserResponse[0].fname ? fetchUserResponse[0].fname : "";
                      let lname = fetchUserResponse[0].lname ? fetchUserResponse[0].lname : "";
                      let name = fname + " " + lname;

                      let companyId = fetchUserResponse[0].company ? fetchUserResponse[0].company : "";
                      let countryCode = fetchUserResponse[0].countryCode ? fetchUserResponse[0].countryCode : "";
                      let contactNumber = fetchUserResponse[0].mobile ? fetchUserResponse[0].mobile : "";
                      let userId = fetchUserResponse[0].userId ? fetchUserResponse[0].userId : "";
                      let userType = fetchUserResponse[0].userType ? fetchUserResponse[0].userType : "";

                      //console.log(fetchUserResponse);
                      let insertquery = {
                        "name": name,
                        "companyId": companyId,
                        "countryCode": countryCode,
                        "contactNumber": contactNumber,
                        "userId": userId,
                        "userType": userType,
                      }

                      Conference.updateOne({ '_id': conferenceId },
                        { $push: { "participants": insertquery } },
                        function (updatederror, updatedresponse) {

                          if (updatederror) {

                            res.json({
                              isError: false,
                              message: errorMsgJSON[lang]["404"],
                              statuscode: 404,
                              details: null
                            })

                          }
                          else {
                            if (updatedresponse.nModified == 1) {
                              let data = { // details of each paricipants inserted here
                                "userId": userId,
                                "userType": userType,
                                "message": "user added to the conference"
                              };

                              participants_details.push(data);// inserting details of each user in this array

                              if (participants_details.length == noOfParticipants) { // when last participant get processed
                                res.json({
                                  isError: false,
                                  message: errorMsgJSON[lang]["200"],
                                  statuscode: 200,
                                  participants: participants_details
                                })
                              }
                            }
                            else {
                              let data = { // details of each paricipants inserted here
                                "userId": userId,
                                "userType": userType,
                                "message": "user was added to the conference"
                              };

                              participants_details.push(data);// inserting details of each user in this array

                              if (participants_details.length == noOfParticipants) { // when last participant get processed
                                res.json({
                                  isError: false,
                                  message: errorMsgJSON[lang]["200"],
                                  statuscode: 200,
                                  participants: participants_details
                                })
                              }
                            }
                          }
                        })  // end of update query

                    }

                  }  // end of No error if block  

                }); // End of fetch user function         
              } // end of fetch user if block as user if (userType != "GUEST") { 

              if (userType == "GUEST") { // when the user type is User then partipant will be user


                let aggrQuery = [
                  {
                    '$match': {
                      'userId': userId
                    }
                  },
                ];

                // Here, we are fetching guest details 
                Guest.aggregate(aggrQuery, function (fetchGuestErr, fetchGuestResponse) { // start fetch user
                  if (fetchGuestErr) { // When error occur

                    res.json({
                      isError: false,
                      message: errorMsgJSON[lang]["404"],
                      statuscode: 404,
                      details: null
                    })

                    return;
                  }



                  if (fetchGuestResponse) { // No error occur
                    if (fetchGuestResponse.length == 0) { // When no Guest exist with this email id
                      let data = { // details of each paricipants inserted here
                        "userId": userId,
                        "userType": userType,
                        "message": "User not exist in the system"
                      };

                      participants_details.push(data);// inserting details of each user in this array

                      if (participants_details.length == noOfParticipants) { // when last participant get processed
                        res.json({
                          isError: false,
                          message: errorMsgJSON[lang]["200"],
                          statuscode: 200,
                          participants: participants_details
                        })
                      }
                    }





                    if (fetchGuestResponse.length > 0) { // When user exist, we have to add him as a participant
                      //                   "_id": "5e303a1f8bb00c45421ae535",
                      // "userType": "GUEST",
                      // "firstName": "qwqw",
                      // "lastName": "",
                      // "email": "qwqw@pkweb.in",
                      // "mobile": "1234568790",
                      // "countryCode": "1",
                      // "userId": "MGUSR1580218911830",
                      // "profileCreatedAt": "2020-01-28T13:41:51.831Z",
                      // "createdAt": "2020-01-28T13:41:51.833Z",
                      // "updatedAt": "2020-01-28T13:41:51.833Z"

                      let fname = fetchGuestResponse[0].firstName ? fetchGuestResponse[0].firstName : "";
                      let lname = fetchGuestResponse[0].lastName ? fetchGuestResponse[0].lastName : "";
                      let name = fname + " " + lname;

                      let companyId = fetchGuestResponse[0].company ? fetchGuestResponse[0].company : "";
                      let countryCode = fetchGuestResponse[0].countryCode ? fetchGuestResponse[0].countryCode : "";
                      let contactNumber = fetchGuestResponse[0].mobile ? fetchGuestResponse[0].mobile : "";
                      let userId = fetchGuestResponse[0].userId ? fetchGuestResponse[0].userId : "";
                      let userType = fetchGuestResponse[0].userType ? fetchGuestResponse[0].userType : "";

                      //console.log(fetchUserResponse);
                      let insertquery = {
                        "name": name,
                        "companyId": companyId,
                        "countryCode": countryCode,
                        "contactNumber": contactNumber,
                        "userId": userId,
                        "userType": userType,
                      }


                      // res.json({
                      //   fetchGuestResponse: fetchGuestResponse,
                      //   participants_details: participants_details,
                      //   insertquery: insertquery
                      // });
                      // return;



                      Conference.updateOne({ '_id': conferenceId },
                        { $push: { "participants": insertquery } },
                        function (updatederror, updatedresponse) {

                          if (updatederror) {

                            res.json({
                              isError: false,
                              message: errorMsgJSON[lang]["404"],
                              statuscode: 404,
                              details: null
                            })

                          }
                          else {
                            if (updatedresponse.nModified == 1) {
                              let data = { // details of each paricipants inserted here
                                "userId": userId,
                                "userType": userType,
                                "message": "Guest added to the conference"
                              };

                              participants_details.push(data);// inserting details of each user in this array

                              if (participants_details.length == noOfParticipants) { // when last participant get processed
                                res.json({
                                  isError: false,
                                  message: errorMsgJSON[lang]["200"],
                                  statuscode: 200,
                                  participants: participants_details
                                })
                              }
                            }
                            else {
                              let data = { // details of each paricipants inserted here
                                "userId": userId,
                                "userType": userType,
                                "message": "Guest was added to the conference"
                              };

                              participants_details.push(data);// inserting details of each user in this array

                              if (participants_details.length == noOfParticipants) { // when last participant get processed
                                res.json({
                                  isError: false,
                                  message: errorMsgJSON[lang]["200"],
                                  statuscode: 200,
                                  participants: participants_details
                                })
                              }
                            }
                          }
                        })  // end of update query

                    }

                  }  // end of No error if block  

                }); // End of fetch user function           

              } //  end of if block When user is guest if (userType == "GUEST")

            } // END if (fetchConferenceResponse.length == 0) {
          } // END if (fetchConferenceResponse) {
        }); // End fetch Conference

        console.log(arrParticipant[i]);
      } // End of for loop

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }

    //------------------------------------------------------------------------

  },  // end of fetch confe
  /*
      This function is used to end the conference for a particular user
  */
  endConference: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";

    try {
      // Here, we are receiviing the session id
      let conferenceId = req.body.conferenceId ? req.body.conferenceId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - conference Id",
      });

      // Here, we are receiving the user Id of the participant
      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Id",
      });

      // Here, we are receiving the session Id of the participant
      let sessionId = req.body.sessionId ? req.body.sessionId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Session Id",
      });

      // Here, we are receiving the session Id of the participant
      let connectionIds = req.body.connectionId ? req.body.connectionId : [];

      // Here, Circular json avoided
      if (!req.body.conferenceId || !req.body.userId || !req.body.sessionId) { return; }

      // Here, We are going to check that if conference is active
      let checkConferenceActiveStatus = await ConferenceBusiness.checkConferenceActive(conferenceId);


      if (checkConferenceActiveStatus.isError == true) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          statuscode: 404,
          details: null
        });

        return;
      }

      if (checkConferenceActiveStatus.isConferenceExist == false) {
        res.json({
          isError: false,
          message: errorMsgJSON[lang]["204"],
          statuscode: 204,
          details: null
        });

        return;
      }

      if (checkConferenceActiveStatus.conferenceDetail[0].isActive == false) {
        res.json({
          isError: false,
          message: "Conference already Deactivated",
          statuscode: 204,
          details: null
        });

        return;
      }

      // Here, we are checking whether the user user is active in the conferecnce in the conference
      let checkUserExistanceStatus = await ConferenceBusiness.checkUserExistance(conferenceId);

      if (checkUserExistanceStatus.isError == true) { // When Error Occur
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          statuscode: 404,
          details: null
        });

        return;
      }

      if (checkUserExistanceStatus.response[0].isActive == false) {
        res.json({
          isError: true,
          message: "Conference Not Active",
          statuscode: 204,
          details: null
        });

        return;
      }

      if (checkUserExistanceStatus.response.length < 1) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["204"],
          statuscode: 204,
          details: null
        });

        return;
      }

      let hostUserId = checkUserExistanceStatus.response[0].hostUserId;

      // This array contain only active user in the conference
      let arrUsers = new Array();
      arrUsers = checkUserExistanceStatus.response;

      let i;
      let userIsStarted = false;
      let updateConferenceStatus // In this variable we are keeping the whole status after deactivating the user from conference
      let deactivateConferenceStatus // this will take the status after deactivating the conference
      let sipDialEndForEndConfStatus // this will take the status after deactivating the conference

      //==================================================================
      if (hostUserId == userId) {  // When the user is host user

        // Here, We are going to fetch the details of conference duration table
        let fetchConferenceDurationDetail = await PriceManagementBusiness.fetchConferenceDurationForSipCall(sessionId);
        if (fetchConferenceDurationDetail.isError == true) { // some error occur
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;
        }

        let conferenceDurationId = fetchConferenceDurationDetail.confDuration[0]._id;

        // Here, We are going to update the conferenence Duration
        let updateSipCallStatus = await PriceManagementBusiness.updateSipCallForConf(conferenceDurationId);

        if (updateSipCallStatus.isError == true) { // some error occur
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;
        }

        // Here, We are going to update the conferenence Duration
        let updateWebCallStatus = await PriceManagementBusiness.updateWebCall(conferenceId, conferenceDurationId);

        if (updateWebCallStatus.isError == true) { // some error occur
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;
        }

        // Here, We are going to update the conferenence Duration
        let updateConferenceDurationStatus = await PriceManagementBusiness.updateConferenceDuration(conferenceId, conferenceDurationId);

        if (updateConferenceDurationStatus.isError == true) { // some error occur
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;
        }



        // forceDisconnect --start
        // Here, We are deactivating all the web conference
        connectionIds.map(function (connectionId) {
          console.log('connectionId ======>', connectionId);
          opentok.forceDisconnect(sessionId, connectionId, function (error) {
            if (error) return console.log("error:", error);
          });
        });
        // forceDisconnect --end


        let userUpdateConferenceStatus = [];
        let dialedUserUpdateConferenceStatus = [];

        for (i = 0; i < arrUsers.length; i++) { // Start First For Loop

          if (arrUsers[i].participants.userType == 'DIALEDUSER') {

            // Here, We are disconnecting the call
            sipDialEndForEndConfStatus = await Sip_dialBusiness.sipDialEndForEndConf(conferenceId, arrUsers[i].participants.contactNumber);

            // Here, we are deactivating the user from conference
            updateConferenceStatus = await ConferenceBusiness.updateConferenceForDialedUser(conferenceId, arrUsers[i].participants.contactNumber);

            if (updateConferenceStatus.isError == true) { // When Some Error Occur
              let dataForOperation = {
                "callDisconnectMessage": sipDialEndForEndConfStatus.message,
                "operation": "User Was not Deactivated",
                "message": errorMsgJSON[lang]["404"],
                "statuscode": 404,
                "isError": true,
                "contactNumber": arrUsers[i].participants.contactNumber
              }

              dialedUserUpdateConferenceStatus.push(dataForOperation);
            }

            if (updateConferenceStatus.isError == false) { // When No error Occur
              if (updateConferenceStatus.isModified == false) {
                let dataForOperation = {
                  "callDisconnectMessage": sipDialEndForEndConfStatus.message,
                  "operation": "User Was not Deactivated",
                  "message": errorMsgJSON[lang]["404"],
                  "statuscode": 404,
                  "isError": true,
                  "contactNumber": arrUsers[i].participants.contactNumber
                }

                dialedUserUpdateConferenceStatus.push(dataForOperation);
              }

              if (updateConferenceStatus.isModified == true) {
                let dataForOperation = {
                  "callDisconnectMessage": sipDialEndForEndConfStatus.message,
                  "operation": "User Was Deactivated",
                  "message": errorMsgJSON[lang]["200"],
                  "statuscode": 200,
                  "isError": false,
                  "contactNumber": arrUsers[i].participants.contactNumber
                }

                dialedUserUpdateConferenceStatus.push(dataForOperation);
              }
            }

          } else {  // END if (arrUsers[i].participants.userId == 'DIALEDUSER') 

            // Here, we are deactivating the user from conference
            updateConferenceStatus = await ConferenceBusiness.updateConference(conferenceId, arrUsers[i].participants.userId);

            if (updateConferenceStatus.isError == true) { // When Some Error Occur
              let dataForOperation = {
                "operation": "User Was not Deactivated",
                "message": errorMsgJSON[lang]["404"],
                "statuscode": 404,
                "isError": true,
                "userId": arrUsers[i].participants.userId
              }

              userUpdateConferenceStatus.push(dataForOperation);
            }

            if (updateConferenceStatus.isError == false) { // When No error Occur
              if (updateConferenceStatus.isModified == false) {
                let dataForOperation = {
                  "operation": "User Was not Deactivated",
                  "message": errorMsgJSON[lang]["404"],
                  "statuscode": 404,
                  "isError": true,
                  "userId": arrUsers[i].participants.userId
                }

                userUpdateConferenceStatus.push(dataForOperation);
              }

              if (updateConferenceStatus.isModified == true) {
                let dataForOperation = {
                  "operation": "User Was Deactivated",
                  "message": errorMsgJSON[lang]["200"],
                  "statuscode": 200,
                  "isError": false,
                  "userId": arrUsers[i].participants.userId
                }

                userUpdateConferenceStatus.push(dataForOperation);
              }
            }

          } // END ELSE OF if (arrUsers[i].participants.userId == 'DIALEDUSER') 

        } // END First For Loop

        // Here, we are deactivatin the conference 
        deactivateConferenceStatus = await ConferenceBusiness.deactivateConference(conferenceId);

        res.json({
          conferenceId: conferenceId,
          hostUserId: hostUserId,
          detail: {
            "userUpdateConferenceStatus": userUpdateConferenceStatus,
            "dialedUserUpdateConferenceStatus": dialedUserUpdateConferenceStatus,
            "deactivateConferenceStatus": deactivateConferenceStatus
          }
        });

        return;

      } // END OF if (hostUserId == userId) {  // When the user is host user

      //================================================================== 
      for (i = 0; i < arrUsers.length; i++) {
        if (arrUsers[i].participants.userId == userId) {
          userIsStarted = true;
        }
      }

      if (userIsStarted == false) { // user is not active in this conference
        res.json({
          isError: false,
          message: errorMsgJSON[lang]["204"],
          statuscode: 204,
          details: null
        });

        return;
      }

      if (checkUserExistanceStatus.response.length == 1) { // When only last user exist the conference need to be deactivated
        deactivateConferenceStatus = await ConferenceBusiness.deactivateConference(conferenceId);

        if (deactivateConferenceStatus.isError == true) { // some error occur
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;
        }
      }


      // console.log(updateConferenceDurationStatus);

      // Here, we are deactivating the user from conference
      updateConferenceStatus = await ConferenceBusiness.updateConference(conferenceId, userId);

      if (updateConferenceStatus.isError == true) { // When Some Error Occur
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          statuscode: 404,
          details: null
        });

        return;
      }

      if (updateConferenceStatus.isError == false) { // When No error Occur
        if (updateConferenceStatus.isModified == false) {
          res.json({
            isError: true,
            message: "User Could not get removed from conference",
            statuscode: 404,
            details: null
          });

          return;
        }

        if (updateConferenceStatus.isModified == true) {

          res.json({
            isError: true,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: null
          });

          return;
        }
      }



    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }

  },


  /*
      This function is written to start the conference for the user
  */
  startConference: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      // Here, we are receiviing the session id
      let conferenceId = req.body.conferenceId ? req.body.conferenceId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Session Id",
      });

      // Here, we are receiving the user Id of the participant
      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Id",
      });

      // Here, we are receiving the stream Id of the participant
      let streamId = req.body.connections.streamId ? req.body.connections.streamId : '';

      // Here, we are receiving the stream Id of the participant
      let connectionId = req.body.connections.connectionId ? req.body.connections.connectionId : '';

      // Here, Circular json avoided
      if (!req.body.conferenceId || !req.body.userId) { return; }

      // Here, We are going to check that if conference is active
      let checkConferenceActiveStatus = await ConferenceBusiness.checkConferenceActive(conferenceId);


      if (checkConferenceActiveStatus.isError == true) { // Error occur while fetching conference
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["405"],
          statuscode: 405,
          details: null
        })

        return;
      }

      if (checkConferenceActiveStatus.isConferenceExist == false) { // Either the conference not exist or not active
        res.json({
          isError: false,
          message: errorMsgJSON[lang]["204"],
          statuscode: 204,
          details: null
        })

        return;
      }

      // conference already Started
      if (checkConferenceActiveStatus.conferenceDetail[0].isActive == true && checkConferenceActiveStatus.conferenceDetail[0].hostUserId == userId) {
        res.json({
          isError: false,
          message: "Conference already Started",
          statuscode: 204,
          details: null
        })

        return;
      }

      // conference already Started
      if (checkConferenceActiveStatus.conferenceDetail[0].isActive == false && checkConferenceActiveStatus.conferenceDetail[0].hostUserId != userId) {
        res.json({
          isError: false,
          message: "Conference Not Started",
          statuscode: 204,
          details: null
        })

        return;
      }


      // Here, we are getting the session Id of the conference 
      let sessionId = checkConferenceActiveStatus.conferenceDetail[0].sessionId;
      let conferenceDurationId

      // Here, this is the conference Id For which the conference is initiated
      let companyId = checkConferenceActiveStatus.conferenceDetail[0].companyId;


      // Conference Duration should be entered whwn host user start the conference 
      if (checkConferenceActiveStatus.conferenceDetail[0].hostUserId == userId) {
        // Here, We are going to insert the details of conference duration table
        let insertConferenceDetailStatus = await PriceManagementBusiness.insertConferenceDetail(conferenceId, sessionId, companyId);

        if (insertConferenceDetailStatus.isError == true) { // Error occur while fetching conference
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["405"],
            statuscode: 405,
            details: null
          })

          return;
        }

        conferenceDurationId = insertConferenceDetailStatus.confDuration._id;


      } else {
        // Here, We are going to fetch the details of conference duration table
        let fetchConferenceDurationDetail = await PriceManagementBusiness.fetchConferenceDurationForSipCall(sessionId);

        conferenceDurationId = fetchConferenceDurationDetail.confDuration[0]._id;

      } //END if ( checkConferenceActiveStatus.conferenceDetail[0].hostUserId == userId      

      // Here, We are going to insert the details of webCall of Conference

      let insertWebCallDetailStatus = await PriceManagementBusiness.insertWebCallDetail(conferenceId, sessionId, conferenceDurationId, streamId, connectionId, companyId, userId);


      // res.json({
      //    checkConferenceActiveStatus:checkConferenceActiveStatus,
      //    conferenceDurationId: conferenceDurationId,
      //    insertWebCallDetailStatus: insertWebCallDetailStatus
      // });
      // return; 
      // Create an eventEmitter object
      var eventEmitter = new events.EventEmitter();

      /*
          Create an event handler to update_conference for user
          this event is created to join the user in the conference
      */
      var update_conference = function update_conference() {

        let updateWith = {
          "participants.$.isStarted": true,
          "isActive": true
        }

        let updateWhere = {

          "_id": conferenceId,
          "participants.userId": userId,
        }

        Conference.updateOne(updateWhere, updateWith, function (updatederror, updatedresponse) {
          if (updatederror) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["405"],
              statuscode: 405,
              details: null
            })
          }
          else {
            if (updatedresponse.nModified == 1) {
              res.json({
                isError: false,
                message: errorMsgJSON[lang]["200"],
                statuscode: 200,
                details: {
                  "isStarted": true
                }
              });
            }
            else {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["405"],
                statuscode: 405,
                details: null
              })

            }
          }
        })

      } // end of update conference event  

      // Bind the update_conference event with the handler
      eventEmitter.addListener('update_conference', update_conference);

      /* 
          this event checked whether host user logged in or not
      */
      var check_host_user_login_status = function check_host_user_login_status(givenUserId) {

        let aggrQuery = [
          {

            '$match': { '_id': mongoose.Types.ObjectId(conferenceId) }
          },
          {
            $project: {
              '_id': 0,
              "hostUserId": 1,
              "participants": 1
            }
          },
          { $unwind: "$participants" },

          {
            '$match': { 'participants.userId': givenUserId }
          },
        ];

        Conference.aggregate(aggrQuery, function (err, response) {

          if (err) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            });

            return;
          }

          if (response.length == 0) { // when no data found
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["204"],
              statuscode: 204,
              details: null
            });

            return;
          }

          if (response.length > 0) {

            // When the host user did not joined the conference
            if (response[0].participants.isStarted == false) {
              res.json({
                isError: false,
                message: errorMsgJSON[lang]["408"],
                statuscode: 408,
                details: null
              });

              return;
            } else { // when the host user joined the conference

              eventEmitter.emit('update_conference');

              return;
            }
          }

        })

      } // end of event host user login status 

      // Bind the update_conference event with the handler
      eventEmitter.addListener('check_host_user_login_status', check_host_user_login_status);


      /*
          Create an event handler to update_conference for user
          this event is created to join the user in the conference
          turn = 1 for first attempt
          turn = 2 for 2nd attempt
      */
      var check_host_user = function check_host_user() {

        let aggrQuery = [
          {

            '$match': { '_id': mongoose.Types.ObjectId(conferenceId) }
          },
          {
            $project: {
              '_id': 0,
              "hostUserId": 1,
              "participants": 1
            }
          },
          { $unwind: "$participants" },

          {
            '$match': { 'participants.userId': userId }
          },
        ];

        Conference.aggregate(aggrQuery, function (err, response) {

          if (err) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            });

            return;
          }

          if (response.length == 0) { // when no data found

            res.json({
              isError: false,
              message: errorMsgJSON[lang]["204"],
              statuscode: 204,
              details: null
            });

            return;
          }

          if (response.length > 0) {

            // When the user already joined the conference
            if ((response[0].participants.isStarted == true) && (response[0].hostUserId != response[0].participants.userId)) {
              res.json({
                isError: false,
                message: errorMsgJSON[lang]["200"],
                statuscode: 200,
                details: {
                  isStarted: response[0].participants.isStarted
                }
              });

              return;
            }

            // When the user is host user
            // when turn == 1 means orginal user is the host user
            if (response[0].hostUserId == response[0].participants.userId) {

              if (response[0].participants.isStarted == true) {
                res.json({
                  isError: false,
                  message: errorMsgJSON[lang]["200"],
                  statuscode: 200,
                  details: {
                    isStarted: response[0].participants.isStarted
                  }
                });

                return;

              } else {

                // Fire the update_conference event 
                eventEmitter.emit('update_conference');

                return;
              }

            }

            // When the user is host user
            if (response[0].hostUserId != response[0].participants.userId) {

              eventEmitter.emit('check_host_user_login_status', response[0].hostUserId);

              return;
            }

          }

        })

      } // end of check host user

      // Bind the update_conference event with the handler
      eventEmitter.addListener('check_host_user', check_host_user);
      // Fire the check_host_user event 
      eventEmitter.emit('check_host_user');

    } catch (error) {
      console.log(error);

      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
       This function for placing call to participant for the conference    
  */
  sipDialOut: async (req, res, next) => {
    console.log('here=======');

    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiviing the session id
      let sessionId = req.body.sessionId ? req.body.sessionId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Session Id",
      });

      // Here, we are receiviing the sender's mobile number
      let toMobile = req.body.toMobile ? req.body.toMobile : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Mobile",
      });

      // Here, we are receiviing the sender's mobile number
      let countryCode = req.body.countryCode ? req.body.countryCode : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - countryCode",
      });

      // Here, we are receiving the receiver's mobile number
      let fromMobile = req.body.fromMobile ? req.body.fromMobile : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Mobile",
      });
      let contactNumber = '' + countryCode + toMobile;

      // let sipUri = 'sip:+' + contactNumber + '@31.13.156.183';
      // let senderUri = '14155550101@example.com';
      let sipConfig = sipUriHandler.getOutboundConfig({
        toCountryCode: countryCode,
        toMobile: toMobile,
        fromCountryCode: '1',
        fromMobile: '4155550101',
      });



      // Here, Circular json avoided
      if (!req.body.sessionId || !req.body.fromMobile || !req.body.toMobile || !req.body.sessionId) { return; }

      //============================================
      // Here, we are checking whether user exist in this conference or not
      let checkConferenceIsActiveStatus = await Sip_dialBusiness.checkConferenceIsActive(sessionId);


      if (checkConferenceIsActiveStatus.ErrorStatus == true) { // Error occur while checking wheathe user exist in the conference
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          statuscode: 404,
          details: null
        });

        return;
      }

      if (checkConferenceIsActiveStatus.isConferenceExist == false) { // Conference Does Not Exist
        res.json({
          isError: false,
          message: "Conference Not Exist",
          statuscode: 204,
          details: null
        });

        return;
      }

      if (checkConferenceIsActiveStatus.responseConference[0].isActive == false) { // Conference Is Not Active
        res.json({
          isError: false,
          message: errorMsgJSON[lang]["206"],
          statuscode: 206,
          details: null
        });

        return;
      }
      //===================================   
      // Here, we are generating the token for dialing call  
      let tokenRole = await ConferenceBusiness.getOpentokTokenRole(sessionId, "", "sip");
      var sipToken = opentok.generateToken(sessionId, {
        role: tokenRole,
        // data: `phoneNumber:${toMobile}`
        data: `phoneNumber:${contactNumber}`
      });

      // This function is written to aial sip call
      async function dialOpentok() {
        return new Promise(function (resolve, reject) {
          opentok.dial(sessionId, sipToken, sipConfig.sipUri, sipConfig.option, function (err, sipCall) {// when error occur

            if (err) {  // When Error Occur
              resolve({
                "ErrorStatus": true,
              });
            } else {
              resolve({
                "ErrorStatus": false,
                "sipCall": sipCall
              });
            }
          }) // End dialOpentok Function  
        }) // End of Promise 
      } // End dialOpentok 


      // This function is written add the details of sip call in database
      async function addSipCallDetail(SipCall) {
        return new Promise(function (resolve, reject) {
          SipCall.save(function (err, item) {
            if (err) {  // When Error Occur
              resolve({
                "ErrorStatus": true,
              });
            } else {
              resolve({
                "ErrorStatus": false,
                "sipCall": item
              });
            }
          });
        }) // End of Promise 
      } // End addSipCallDetail  



      // This function is written add the details of sip call in database
      async function checkParticipantExist(SipCall) {
        return new Promise(function (resolve, reject) {

          let aggrQuery = [
            {
              $match: {
                'sessionId': sessionId,
              }
            },
            {
              $project: {
                "hostUserId": 1,
                "participants": 1
              }
            },
            { $unwind: "$participants" },

            {
              '$match': {
                'participants.contactNumber': toMobile.toString(),
                'participants.countryCode': countryCode.toString()
              }
            },
          ];

          Conference.aggregate(aggrQuery, function (err, responseConference) {
            if (err) {  // When Error Occur
              resolve({
                "ErrorStatus": true,
              });
            } else {

              if (responseConference.length > 0) { // When User Exist With THis Number
                resolve({
                  "ErrorStatus": false,
                  "isUserExist": true,
                  "responseConference": responseConference
                });
              }

              if (responseConference.length == 0) { // When User Exist With THis Number
                resolve({
                  "ErrorStatus": false,
                  "isUserExist": false,
                });
              }

            }


          });

        }) // End of Promise 
      } // End addSipCallDetail 

      // This function is written update the details of participant in conference
      async function updateParticipantDetail(updateWith, updateWhere) {
        return new Promise(function (resolve, reject) {
          console.log(updateWith);
          Conference.updateOne(updateWhere,
            { "$set": updateWith },
            function (updatederror, updatedresponse) {
              if (updatederror) {
                resolve({
                  "ErrorStatus": true,
                });
              }

              if (updatedresponse) {
                resolve({
                  "ErrorStatus": false,
                  "updatedresponse": updatedresponse
                });
              }
            })
        }) // End of Promise 
      } // End addSipCallDetail  


      // This function is written fetch the details of participant in conference
      async function fetchParticipantDetail(aggrQuery) {
        return new Promise(function (resolve, reject) {

          Conference.aggregate(aggrQuery, function (err, response) {
            if (err) {
              resolve({
                "ErrorStatus": true,
              });
            } else {
              resolve({
                "ErrorStatus": false,
                "response": response
              });
            }
          })
        }) // End of Promise 
      } // End fetchParticipantDetail 

      // This function is written fetch the details of participant in conference
      async function addParticipantDetail(insertquery) {
        return new Promise(function (resolve, reject) {

          Conference.findOneAndUpdate({ 'sessionId': sessionId },
            { $push: { "participants": insertquery } },
            function (updatederror, updatedresponse) {
              if (updatederror) {
                resolve({
                  "ErrorStatus": true,
                });
              } else {
                resolve({
                  "ErrorStatus": false,
                });
              }



            })
        }) // End of Promise 
      } // End addParticipantDetail 



      // Here, we are dialing a call 
      let dialOpentokStatus = await dialOpentok();

      if (dialOpentokStatus.ErrorStatus == true) { // When the dial was not successfull
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          statuscode: 404,
          details: null
        });

        return;
      }

      if (dialOpentokStatus.ErrorStatus == false) { // When the dial was successfull  //==Start First IF

        // Here, we are fetching the conference duration Id
        let fetchConferenceDurationForSipCallStatus = await PriceManagementBusiness.fetchConferenceDurationForSipCall(sessionId);
        let conferenceDurationId
        if (fetchConferenceDurationForSipCallStatus.isError == true || fetchConferenceDurationForSipCallStatus.isConfDurationExist == false) {
          conferenceDurationId = 'NOT FOUND';
        } else {
          conferenceDurationId = fetchConferenceDurationForSipCallStatus.confDuration[0]._id;
        }

        let sipCallNew = new SipCall({
          toMobile: toMobile,
          countryCode: countryCode,
          conferenceDurationId: mongoose.Types.ObjectId(conferenceDurationId),
          fromMobile: fromMobile,
          companyId: checkConferenceIsActiveStatus.responseConference[0].companyId,
          sipCallId: dialOpentokStatus.sipCall.id,
          sessionId: dialOpentokStatus.sipCall.sessionId,
          projectId: dialOpentokStatus.sipCall.projectId,
          connectionId: dialOpentokStatus.sipCall.connectionId,
          streamId: dialOpentokStatus.sipCall.streamId,
          createdAt: dialOpentokStatus.sipCall.createdAt,
          updatedAt: dialOpentokStatus.sipCall.updatedAt
        });

        // Here, we are inserting the the details of sip call
        let addSipCallDetailStatus = await addSipCallDetail(sipCallNew);

        if (addSipCallDetailStatus.ErrorStatus == true) { // Error Occur While inserting sip call details
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;
        }

        if (addSipCallDetailStatus.ErrorStatus == false) { // Sip Call Added Successfully //== Start 2nd IF

          // Here, we are checking whether user exist in this conference or not
          let checkParticipantExistStatus = await checkParticipantExist();

          if (checkParticipantExistStatus.ErrorStatus == true) { // Error occur while checking wheathe user exist in the conference
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            });

            return;
          }

          if (checkParticipantExistStatus.ErrorStatus == false) { // When checking of the existance of user is done successfully
            // Start of 3rd If 
            if (checkParticipantExistStatus.isUserExist == true) { // When User Exist in the conference // START 4th IF
              let updateWith = {
                "participants.$.connectionId": dialOpentokStatus.sipCall.connectionId,
                "participants.$.streamId": dialOpentokStatus.sipCall.streamId,
                "participants.$.createdAt": dialOpentokStatus.sipCall.createdAt,
                "participants.$.isStarted": true
              }

              let updateWhere = {
                "sessionId": sessionId,
                "participants.contactNumber": toMobile.toString(),
                //"participants.countryCode":countryCode,

              }

              console.log(updateWhere);

              // Here, we are inserting the the details of sip call
              let updateParticipantDetailStatus = await updateParticipantDetail(updateWith, updateWhere);

              // The participant Detail was not Updated Successfully
              if (updateParticipantDetailStatus.ErrorStatus == true) {
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["404"],
                  statuscode: 404,
                  details: null
                });

                return;
              }

              // The participant Detail was Updated Successfully
              if (updateParticipantDetailStatus.ErrorStatus == false) { // START 4th IF 
                // Here, we are fetching the the details of participant in a conference
                let aggrQuery = [
                  {
                    $match: {
                      'sessionId': sessionId,
                    }
                  },
                ];

                let fetchParticipantDetailStatus = await fetchParticipantDetail(aggrQuery);

                if (fetchParticipantDetailStatus.ErrorStatus == true) {
                  res.json({
                    isError: true,
                    message: errorMsgJSON[lang]["404"],
                    statuscode: 404,
                    details: null
                  });

                  return;
                }

                if (fetchParticipantDetailStatus.ErrorStatus == false) {

                  res.json({
                    isError: false,
                    message: errorMsgJSON[lang]["200"],
                    statuscode: 200,
                    details: fetchParticipantDetailStatus.response
                  });

                  return;
                }
              } // END 6th IF 
            } // END 4th IF  

            // When the user does not exist in the conference, Now we have to perform insert operation
            if (checkParticipantExistStatus.isUserExist == false) { // START 5th IF
              //let contactNumber = toMobile;
              let userType = "DIALEDUSER";

              let insertquery = {
                "contactNumber": toMobile,
                "countryCode": countryCode,
                "isStarted": true,
                "userType": userType,
                "connectionId": dialOpentokStatus.sipCall.connectionId,
                "streamId": dialOpentokStatus.sipCall.streamId,
                "createdAt": dialOpentokStatus.sipCall.createdAt,
              }

              let addParticipantDetailStatus = await addParticipantDetail(insertquery);

              if (addParticipantDetailStatus.ErrorStatus == true) {
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["404"],
                  statuscode: 404,
                  details: null
                });

                return;
              }

              // When Participant Added successfully  
              if (addParticipantDetailStatus.ErrorStatus == false) { // START 7th IF
                let aggrQuery = [
                  {
                    $match: {
                      'sessionId': sessionId,
                    }
                  },
                ];

                let fetchParticipantDetailStatus = await fetchParticipantDetail(aggrQuery);

                if (fetchParticipantDetailStatus.ErrorStatus == true) {
                  res.json({
                    isError: true,
                    message: errorMsgJSON[lang]["404"],
                    statuscode: 404,
                    details: null
                  });

                  return;
                }

                if (fetchParticipantDetailStatus.ErrorStatus == false) {

                  res.json({
                    isError: false,
                    message: errorMsgJSON[lang]["200"],
                    statuscode: 200,
                    details: fetchParticipantDetailStatus.response
                  });

                  return;
                }

              } // END 7th IF
            } // END 5th IF                                                 
          } // End Of 3rd If


        }  // == End 2nd If

      } //==END First IF

    } catch (error) {
      console.log('fff');
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: null
      });
    }
  },

  /*
       This function is written to insert the detail of the guest user in 
       the system if not exist and if exist it will return the whole details 
       of the user as a response
  */
  loginAsGuest: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the first name of the guest user
      let firstName = req.body.firstName ? req.body.firstName : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - First Name",
      });

      // Here, we are receiving the last name of guest user
      let lastName = req.body.lastName ? req.body.lastName : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Last Name",
      });

      // Here, we are receiving the Email of guest user
      let email = req.body.email ? req.body.email : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Email",
      });

      // Here, we are receiving the mobile of guest user
      let mobile = req.body.mobile ? req.body.mobile : "";

      // Here, we are receiving the user type of guest user
      let userType = req.body.userType ? req.body.userType : "GUEST";

      let userId = AutogenerateIdcontroller.autogenerateId("MGUSR");

      // Here, Circular json avoided
      if (!req.body.firstName || !req.body.lastName || !req.body.email) { return; }


      // Create an eventEmitter object
      var eventEmitter = new events.EventEmitter();

      //----------------------------------------------------
      // Create an event handler to add guest
      var save_guest = function save_guest() {

        var guest = new Guest({
          firstName: firstName,
          lastName: lastName,
          email: email,
          mobile: mobile,
          userType: userType,
          userId: userId
        });

        // Here, we are adding guest
        guest.save(function (err, guestResponse) {
          if (guestResponse) {

            let userfinal = {
              "user": guestResponse
            }

            var token = jwt.sign(userfinal, config.secret.d, {
              expiresIn: "12h"
            });

            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: {
                token: "JWT " + token,
                data: guestResponse
              }
            });

            return;
          }
          if (err) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 400,
              details: err["message"]
            });

            return;
          }
        }); // end guest addition  
      } // end of save_guest

      // Bind the save_guest event with the handler
      eventEmitter.addListener('save_guest', save_guest);
      //----------------------------------------------------

      //----------------------------------------------------
      var check_guest = function check_guest() {

        let findGuestQry = {
          email: email
        };

        Guest.find(findGuestQry, function (err, responseGuest) { // fetching guest details

          if (err) { // when some error occur
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["400"],
              statuscode: 400
              //details: err["message"]
            });

            return;
          }

          if (responseGuest) { // successfully fetched guest details

            if (responseGuest.length == 0) { // no guest available on this mail
              // Fire the save_guest event 
              eventEmitter.emit('save_guest');

              return;
            }

            if (responseGuest.length > 0) { // when guest exist on this mail

              let userfinal = {
                "user": responseGuest
              }

              var token = jwt.sign(userfinal, config.secret.d, {
                expiresIn: "12h"
              });

              res.json({
                isError: false,
                message: errorMsgJSON[lang]["205"],
                statuscode: 205,
                details: {
                  token: "JWT " + token,
                  data: responseGuest[0]
                }
              });

              return;
            }
          }
        }); // end of fetch guest  
      } // end of check_guest event

      // Bind the check_guest event with the handler
      eventEmitter.addListener('check_guest', check_guest);
      //----------------------------------------------------

      // Fire the save_guest event 
      eventEmitter.emit('check_guest');

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
     This function is written to allow a user to join the conference 
     with invitation code
  */
  joinConferenceWithCode: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the Conference Id
      let invitationCode = req.body.invitationCode ? req.body.invitationCode : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Invitation Code",
      });

      // Here, we are receiving the User Id, who need to join the conference
      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Id",
      });

      let userType = req.body.userType ? req.body.userType : "USER";

      // Here, Circular json avoided
      if (!req.body.invitationCode || !req.body.userId) { return; }

      // Create an eventEmitter object
      var eventEmitter = new events.EventEmitter();

      //-----------------------------------------------------------------
      // Create an event handler to fetch user
      var update_conference = function update_conference(name, companyId, countryCode, contactNumber, userId, userType) {

        let insertquery = {
          "name": name,
          "companyId": companyId,
          "countryCode": countryCode,
          "contactNumber": contactNumber,
          "userId": userId,
          "userType": userType,
        }

        Conference.updateOne({ 'invitationCode': invitationCode },
          { $push: { "participants": insertquery } },
          function (updatederror, updatedresponse) {

            if (updatederror) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["405"],
                statuscode: 405,
                details: null
              })
            }
            else {
              if (updatedresponse.nModified == 1) {

                Conference.find(
                  { 'invitationCode': invitationCode },
                  function (err, companyResponse) {

                    if (err) {
                      res.json({
                        isError: true,
                        message: errorMsgJSON[lang]["405"],
                        statuscode: 405,
                        details: null
                      })

                      return;
                    }

                    if (companyResponse) {
                      res.json({
                        isError: false,
                        message: errorMsgJSON[lang]["200"],
                        statuscode: 200,
                        details: companyResponse
                      });
                    }

                  });

              }
              else {
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["405"],
                  statuscode: 405,
                  details: null
                })

              }
            }
          })  // end of update query
      }

      // Bind the update_conference event with the handler
      eventEmitter.addListener('update_conference', update_conference);
      //------------------------------------------------------------------
      // Create an event handler to fetch guest
      var fetch_guest = function fetch_guest() {
        let aggrQuery = [
          {
            '$match': {
              'userId': userId
            }
          },
        ];


        // Here, we are fetching guest details 
        Guest.aggregate(aggrQuery, function (fetchGuestErr, fetchGuestResponse) { // start fetch user
          if (fetchGuestErr) { // When error occur
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            });

            return;
          }

          if (fetchGuestResponse) { // No error occur
            if (fetchGuestResponse.length == 0) { // When no guest exist with this email id
              res.json({
                isError: false,
                message: errorMsgJSON[lang]["204"],
                statuscode: 204,
                details: null
              });

              return;
            }

            if (fetchGuestResponse.length > 0) { // When user exist, we have to add him as a participant

              let fname = fetchGuestResponse[0].firstName ? fetchGuestResponse[0].firstName : "";
              let lname = fetchGuestResponse[0].lastName ? fetchGuestResponse[0].lastName : "";
              let name = fname + " " + lname;

              let companyId = fetchGuestResponse[0].company ? fetchGuestResponse[0].company : "";
              let countryCode = fetchGuestResponse[0].countryCode ? fetchGuestResponse[0].countryCode : "";
              let contactNumber = fetchGuestResponse[0].mobile ? fetchGuestResponse[0].mobile : "";
              let userId = fetchGuestResponse[0].userId ? fetchGuestResponse[0].userId : "";
              let userType = "GUEST";

              //Fire the update_conference event 
              eventEmitter.emit('update_conference', name, companyId, countryCode, contactNumber, userId, userType);
            }
          }
        }); // End of fetch user 

      }

      // Bind the fetch_user event with the handler
      eventEmitter.addListener('fetch_guest', fetch_guest);
      //------------------------------------------------------------------
      // Create an event handler to fetch user
      var fetch_user = function fetch_user() {

        let aggrQuery = [
          {
            '$match': {
              'userId': userId
            }
          },
        ];

        // Here, we are fetching user details 
        User.aggregate(aggrQuery, function (fetchUserErr, fetchUserResponse) { // start fetch user
          if (fetchUserErr) { // When error occur
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            });

            return;
          }

          if (fetchUserResponse) { // No error occur
            if (fetchUserResponse.length == 0) { // When no user exist with this email id
              res.json({
                isError: false,
                message: errorMsgJSON[lang]["204"],
                statuscode: 204,
                details: null
              });

              return;
            }

            if (fetchUserResponse.length > 0) { // When user exist, we have to add him as a participant
              let fname = fetchUserResponse[0].fname ? fetchUserResponse[0].fname : "";
              let lname = fetchUserResponse[0].lname ? fetchUserResponse[0].lname : "";
              let name = fname + " " + lname;

              let companyId = fetchUserResponse[0].company ? fetchUserResponse[0].company : "";
              let countryCode = fetchUserResponse[0].countryCode ? fetchUserResponse[0].countryCode : "";
              let contactNumber = fetchUserResponse[0].mobile ? fetchUserResponse[0].mobile : "";
              let userId = fetchUserResponse[0].userId ? fetchUserResponse[0].userId : "";
              let userType = fetchUserResponse[0].userType ? fetchUserResponse[0].userType : "";

              // Fire the update_conference event 
              eventEmitter.emit('update_conference', name, companyId, countryCode, contactNumber, userId, userType);
            }
          }
        }); // End of fetch user 
      }

      // Bind the fetch_user event with the handler
      eventEmitter.addListener('fetch_user', fetch_user);
      //-----------------------------------------------------------------

      // Create an event handler to fetch conference
      var fetch_conference = function fetch_conference() {

        let aggrQuery = [
          {
            '$match': {
              'invitationCode': invitationCode,
            }
          },
          {
            $project: {
              //"_id": 0,
              "conferenceTitle": 1,
              "participants": 1
            }
          },

          { $unwind: "$participants" },
          {
            '$match': {
              'participants.userId': userId
            }
          },
        ];

        // Here, we are fetching conference details 
        Conference.aggregate(aggrQuery, function (fetchConferenceErr, fetchConferenceResponse) { // start fetch conference

          if (fetchConferenceErr) { // When some error occur
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            });

            return;
          }

          if (fetchConferenceResponse) { // On successful fetch
            if (fetchConferenceResponse.length > 0) { // User is already a participant of this conference
              res.json({
                isError: false,
                message: errorMsgJSON[lang]["205"],
                statuscode: 205,
                details: fetchConferenceResponse
              });

              return;
            }

            if (fetchConferenceResponse.length < 1) { // we have to join the user as a participant of the conference


              if (userType == "GUEST") { // when the user type is User then partipant will be user
                // Fire the fetch_user event 
                eventEmitter.emit('fetch_guest');

                return;
              } else {  // when the user type is User then partipant will be user
                // Fire the fetch_user event 
                eventEmitter.emit('fetch_user');

                return;

              }

            }
          }
        }); // End fetch Conference
      } // end of fetch conference event

      // Bind the fetch_conference event with the handler
      eventEmitter.addListener('fetch_conference', fetch_conference);
      //----------------------------------------------------------------

      // Fire the fetch_conference event 
      eventEmitter.emit('fetch_conference');

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
     This code is used to generate invitation link 
     for a particular conference againest a particular conference
  */
  getInvitationLink: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the Conference Id
      let conferenceId = req.body.conferenceId ? req.body.conferenceId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Conference Id",
      });

      // Here, we are receiving the email Id of the participant
      let email = req.body.email ? req.body.email : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Email Id",
      });

      // Here, we are generating unique code  
      var currentTime = Math.floor(Date.now() / 1000);
      var randThreeDigit = Math.floor(Math.random() * (999 - 100 + 1) + 100);
      var uniqueCode = "uniq" + currentTime + randThreeDigit;


      // Here, Circular json avoided
      if (!req.body.conferenceId || !req.body.email) { return; }

      // condition for fetch
      let aggrQuery = [
        {
          '$match': { '_id': mongoose.Types.ObjectId(conferenceId) }
        },
        {
          $project: {
            "conferenceTitle": 1,
            "invitationCode": 1
          }
        },
      ];

      // Here, we are checking wheather any conference exist or not 
      Conference.aggregate(aggrQuery, function (err, response) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
        } else {
          if (response.length == 0) { // conference not exist
            res.json({
              isError: true,
              message: "No data Found",
              statuscode: 404,
              details: null
            });
          } else { // conference exist

            let aggrQuery = [
              {
                '$match': {
                  'conferenceId': conferenceId,
                  'email': email
                }
              },

            ];

            // Here, We are checking Wheather the link was sent ot not
            Invitation.aggregate(aggrQuery, function (err, responseFetchInvitation) {

              if (responseFetchInvitation) { // successfull response
                if (responseFetchInvitation.length == 0) { // link needs to be generated
                  let conferenceTitle = response[0].conferenceTitle;
                  let invitationCode = response[0].invitationCode;
                  let generatedLink = "http://mioconf/" + invitationCode + "/" + uniqueCode;

                  var invitation = new Invitation({
                    conferenceTitle: conferenceTitle,
                    conferenceId: conferenceId,
                    invitationCode: invitationCode,
                    uniqueCode: uniqueCode,
                    generatedLink: generatedLink,
                    email: email
                  });

                  // Here, we are generating link
                  invitation.save(function (err, invitationResponse) {
                    if (invitationResponse) {
                      res.json({
                        isError: false,
                        message: errorMsgJSON[lang]["200"],
                        statuscode: 200,
                        details: {
                          "Invitation Link": invitationResponse.generatedLink
                        }
                      });
                    }
                    if (err) {
                      res.json({
                        isError: true,
                        message: errorMsgJSON[lang]["404"],
                        statuscode: 400,
                        details: err["message"]
                      });
                    }
                  }); // end of link generation
                } // end for responseFetchInvitation.length == 0 

                if (responseFetchInvitation.length > 0) { // When link was sent
                  res.json({
                    isError: true,
                    message: "Invitation was already sent",
                    statuscode: 400,
                    details: {
                      "Invitation Link": responseFetchInvitation[0].generatedLink
                    }
                  });
                } // end for responseFetchInvitation.length > 0
              } // end og if for successfull response  

              if (err) { // when error occurr while fetching the response
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["404"],
                  statuscode: 404,
                  details: null
                });
              } // end of error
            });
          }
        }
      }); // end of findConference function
      //***********************************

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
     This function is written to delete the conference
  */
  deleteConference: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the Conference Id
      let conferenceId = req.body.conferenceId ? req.body.conferenceId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Conference Id",
      });

      // Here, Circular json avoided
      if (!req.body.conferenceId) { return; }

      let aggrQuery = [
        {
          '$match': {
            '_id': mongoose.Types.ObjectId(conferenceId),
          }
        },
      ];

      /*
          Here, we are checking wheather the conference exist or not 
      */
      Conference.aggregate(aggrQuery, function (fetcherr, fetchresponse) {  // start of fetch function
        if (fetcherr) { // when error occur
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["405"],
            statuscode: 405,
            details: null
          })
        }

        if (fetchresponse) { // successfully fetched
          if (fetchresponse.length == 0) { // conference not exist
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["204"],
              statuscode: 204,
              details: null
            });
          } else { // conference exist
            Conference.deleteOne(  // start of delete function
              { _id: conferenceId },
              function (err, response) {
                if (err) {
                  res.json({
                    isError: true,
                    message: errorMsgJSON[lang]["405"],
                    statuscode: 405,
                    details: null
                  })
                } else {
                  res.json({
                    isError: false,
                    message: errorMsgJSON[lang]["200"],
                    statuscode: 200,
                    details: null
                  });
                }
              }); // end of delete function       
          }
        }

      }); //end of start function

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
   This function is written to remove a participant from a particular conference
  */
  removeParticipant: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the Conference Id
      let conferenceId = req.body.conferenceId ? req.body.conferenceId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Conference Id",
      });

      // Here, we are receiving the User Id
      let participantId = req.body.participantId ? req.body.participantId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Participant Id",
      });

      // Here, Circular json avoided
      if (!req.body.conferenceId || !req.body.participantId) { return; }

      let aggrQuery = [
        {
          '$match': {
            '_id': mongoose.Types.ObjectId(conferenceId),
          }
        },
        {
          $project: {
            '_id': 0,
            "participants": 1
          }
        },
        { $unwind: "$participants" },
        {
          '$match': { 'participants._id': mongoose.Types.ObjectId(participantId) }
        }
      ];

      /*
          Here, we are checking wheather the user 
          associated with this conference exist or not 
      */
      Conference.aggregate(aggrQuery, function (fetcherr, fetchresponse) {  // start of fetch function
        if (fetcherr) { // when error occur
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["405"],
            statuscode: 405,
            details: null
          })
        }

        if (fetchresponse) { // when the fetch is successfull

          if (fetchresponse.length == 0) { // this user is not associated with this conference
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["204"],
              statuscode: 204,
              details: null
            });
          } else { // this user is associated with this conference

            Conference.updateOne(
              { "_id": conferenceId },
              { "$pull": { "participants": { "_id": participantId } } },
              { safe: true, multi: true }, function (updatederror, updatedresponse) {


                if (updatederror) {
                  res.json({
                    isError: true,
                    message: errorMsgJSON[lang]["405"],
                    statuscode: 405,
                    details: null
                  })
                } else {
                  if (updatedresponse.nModified == 1) {
                    res.json({
                      isError: false,
                      message: errorMsgJSON[lang]["200"],
                      statuscode: 200,
                      details: null
                    });
                  } else {
                    res.json({
                      isError: true,
                      message: errorMsgJSON[lang]["405"],
                      statuscode: 405,
                      details: null
                    })
                  }
                }
              }) // end of update function       
          }
        }

      }); // End of fetch function 


    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /* 
   This function is written to fetch the list of all conference 
   hosted by a particular company
  */
  listConferenceCompany: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the Conference Id
      let companyId = req.body.companyId ? req.body.companyId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Company Id",
      });

      // Here, Circular json avoided
      if (!req.body.companyId) { return; }

      // condition for fetch
      let aggrQuery = [
        {
          '$match': { 'companyId': companyId }
        },
      ];

      Conference.aggregate(aggrQuery, function (err, response) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
        }
        else {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: response
          });
        }
      }); // end of findConference function

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  // This function is used to create the conference
  createConference: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the Conference Title
      let conferenceTitle = req.body.conferenceTitle ? req.body.conferenceTitle : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Conference Title",
      });

      // Here, we are receiving the host User Id
      let hostUserId = req.body.hostUserId ? req.body.hostUserId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Host User Id",
      });

      // Here, we are receiving the Company Id
      let companyId = req.body.companyId ? req.body.companyId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Company Id",
      });

      // Here, we are receiving the initiator of conference
      let callInitiateBy = req.body.callInitiateBy ? req.body.callInitiateBy : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Call Initiated by whom",
      });

      // Here, we are generating invitation code  
      var currentTime = Math.floor(Date.now() / 1000);
      var randThreeDigit = Math.floor(Math.random() * (999 - 100 + 1) + 100);
      var invitationCode = "inv" + currentTime + randThreeDigit;

      // Here, we are getting the broadcast Id
      let broadcastId = req.body.broadcastId ? req.body.broadcastId : "";

      // Here, we duration of the conference
      let timeOut_hours = req.body.timeOut ? req.body.timeOut.hours : 0;

      let timeOut_minutes = req.body.timeOut ? req.body.timeOut.minutes : 0;

      let timeOut_seconds = req.body.timeOut ? req.body.timeOut.seconds : 0;

      let timeOut = {
        "hours": timeOut_hours,
        "minutes": timeOut_minutes,
        "seconds": timeOut_seconds
      };

      // Here, we duration of the conference
      let duration_hours = req.body.duration ? req.body.duration.hours : 0;

      let duration_minutes = req.body.duration ? req.body.duration.minutes : 0;

      let duration_seconds = req.body.duration ? req.body.duration.seconds : 0;

      let durationDetail = {
        "hours": duration_hours,
        "minutes": duration_minutes,
        "seconds": duration_seconds
      };


      //=======================================================================

      let callType = req.body.callType ? req.body.callType : 'CONFERENCE';

      let recordingPreference = req.body.recordingPreference ? req.body.recordingPreference : 'NOT_ALLOWED';

      let fileShare = req.body.fileShare ? req.body.fileShare : false;

      let allowParticipantToShareScreen = req.body.allowParticipantToShareScreen ? req.body.allowParticipantToShareScreen : true;

      let acceptInboundCall = req.body.acceptInboundCall ? req.body.acceptInboundCall : false;

      //=======================================================================



      let maxNoOfParticipants = req.body.maxNoOfParticipants ? req.body.maxNoOfParticipants : 0;
      let participants = req.body.participants ? req.body.participants : "";
      let noOfParticipants = req.body.participants ? participants.length : 0;

      if (noOfParticipants == 0) {
        res.json({
          isError: true,
          message: "Participant details was not given"
        });
      }

      if (!req.body.conferenceTitle || maxNoOfParticipants == 0 || !req.body.callInitiateBy || !req.body.hostUserId || !req.body.companyId) { return; }

      var participants_details = []; // details of all paricipants will be inserted in this array in the for loop


      // This function is used to generate a six digit OTP
      async function getdtmf() {
        let dtmfDetail = await AutogenerateIdcontroller.autogenerateOTP();

        if (dtmfDetail.ErrorStatus == true) {
          getdtmf();
        } else {
          if (dtmfDetail.isOTPExist == true) {
            getdtmf();
          } else {
            return dtmfDetail.sixDigitOTP;
          }

        }
      }

      // This is the OTP Genereted for the user 
      let dtmf = await getdtmf();

      // This function is written whether user exist or not
      async function checkUserExist(userId) {  // Start
        return new Promise(function (resolve, reject) {
          let searchQry = {
            userId: userId
          };
          User.find(searchQry, function (err, user) { // fetching participants details

            if (err) { // When Error Occur
              resolve({ "isUserExists": false });
            } else {

              if (user.length > 0) { // When User Exist

                if (user[0].isActive == true) { // User is active

                  resolve({
                    "isUserExists": true,
                    "user": user[0]
                  });

                } else { // User is not active
                  resolve({ "isUserExists": false });
                }
              } else { // When User not Exist
                resolve({ "isUserExists": false });
              }
            }
          });
        })
      } // End checkUserExist

      // This function is written whether user exist or not
      async function createSession() {  // Start
        return new Promise(function (resolve, reject) {
          opentok.createSession(
            { mediaMode: "routed" },
            function (err, session) {

              if (err) {
                resolve({ "isError": true });
              } else {
                resolve({
                  "isError": false,
                  "session": session,
                });
              }
            })
        })
      } // End createSession

      // Here, We are saving conference
      async function saveConference(conference) {
        return new Promise(function (resolve, reject) {
          conference.save(function (err, conferenceDetail) {
            if (err) {
              resolve({ "isError": true });
            } else {
              resolve({
                "isError": false,
                "conferenceDetail": conferenceDetail,
              });
            }

          })
        })
      } // End Save Conference

      async function getCompanyAdminUserId(companyId) {
        return new Promise((resolve, reject) => {
          User.find({ "userType": "COMPANY_ADMIN", 'companyId': companyId }, (err, response) => {
            if (err) {
              console.log('err', err);
              reject(err);
            } else {
              let details = response && response.length > 0 ? response[0] : null;
              if (details) {
                resolve(details.userId);
              } else {
                reject();
              }
            }
          });
        });
      }

      let userExistStatus // In this variable we receive the status sent by checkUserExist function 
      let user // In this variable we receive the user
      let sessionStatus = await createSession();
      let sessionId // in this variable we keep the session Id after creating the session
      let hostUserToken // in this variaable we are keeping hte token of host user

      if (sessionStatus.isError == true) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          message: "Seesion not created",
          statuscode: 404,
          details: null
        });
      }

      if (sessionStatus.isError == false) {

        sessionId = sessionStatus.session.sessionId;
        let tokenRole = await ConferenceBusiness.getOpentokTokenRole(sessionId, hostUserId, "web");
        hostUserToken = opentok.generateToken(sessionId, {
          role: tokenRole,
          data: `userId:${hostUserId}`,
          initialLayoutClassList: ['focus']
        });
        logger.info('This is in: Create Conference');
        logger.info('dtmf Code: ' + dtmf);
        logger.info('Session Id: ' + sessionId);
      }

      // add company admin as a paricipant.--START
      try {
        let companyAdminUserId = await getCompanyAdminUserId(companyId);
        if (companyAdminUserId && !participants.includes(companyAdminUserId)) {
          participants.push(companyAdminUserId);
        }
      } catch (error) {
      }
      // add company admin as a paricipant.--END

      for (let userId of participants) { // for loop started 
        userExistStatus = await checkUserExist(userId);

        if (userExistStatus.isUserExists == true) { // When User Exist

          user = userExistStatus.user;
          let fname = user.fname ? user.fname : "";
          let lname = user.lname ? user.lname : "";

          let name = user.fname + " " + user.lname;
          let email = user.email ? user.email : "";
          let countryCode = user.countryCode ? user.countryCode : "";
          let contactNumber = user.mobile ? user.mobile : "";
          let token = "";
          let userId = user.userId;
          let participantCompanyId = user.company ? user.company : "NA";

          if (userId == hostUserId) {
            token = hostUserToken
          }

          let data = { // details of each paricipants inserted here
            "name": name,
            "email": email,
            "companyId": participantCompanyId,
            "countryCode": countryCode,
            "contactNumber": contactNumber,
            "token": token,
            "userId": userId
          };

          participants_details.push(data);
        }
      } // End For Loop 

      let noOfInsertedParticipants = participants_details.length;

      if (participants_details.length > 0) {


        let conference = new Conference({
          conferenceTitle: conferenceTitle,
          sessionId: sessionId,
          dtmf: dtmf,
          broadcastId: broadcastId,
          hostUserId: hostUserId,
          callInitiateBy: callInitiateBy,
          companyId: companyId,
          companyId: companyId,
          invitationCode: invitationCode,
          duration: durationDetail,
          status: config.tokbox.CREATED_STATUS,
          maxNoOfParticipants: maxNoOfParticipants,
          participants: participants_details,

          callType: callType,
          timeOut: timeOut,
          recordingPreference: recordingPreference,
          fileShare: fileShare,
          allowParticipantToShareScreen: allowParticipantToShareScreen,
          acceptInboundCall: acceptInboundCall
        });


        let saveConferenceStatus = await saveConference(conference);

        if (saveConferenceStatus.isError == true) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;

        } else {

          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: saveConferenceStatus.conferenceDetail
          });

        }

      } else {
        res.json({
          isError: true,
          message: "Participant Not Exist",
          statuscode: 404,
          details: null
        });
      }

    } catch (error) {

      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  //==========================================================







  //========================================================



  /*
     This functionis used to assign the token for a particular user 
     involved in the particular conference
  */
  joinConference: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the Conference Id
      let conferenceId = req.body.conferenceId ? req.body.conferenceId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Conference Id",
      });

      // Here, we are receiving the Id of a particular participants
      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Id",
      });

      // Here, we are receiving the Conference Title
      let sessionId = req.body.sessionId ? req.body.sessionId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Session Id",
      });

      // Here, Circular json avoided
      if (!req.body.conferenceId || !req.body.userId || !req.body.sessionId) { return; }

      // Here, we are generating the token for particular participant
      let tokenRole = await ConferenceBusiness.getOpentokTokenRole(sessionId, userId, "web");
      var token = opentok.generateToken(sessionId, {
        role: tokenRole,
        data: `userId:${userId}`
      });
      let updateWith = {
        "participants.$.token": token,
      }

      let updateWhere = {
        "_id": conferenceId,
        "participants.userId": userId,
      }

      Conference.updateOne(updateWhere, updateWith, function (updatederror, updatedresponse) {
        if (updatederror) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["405"],
            statuscode: 405,
            details: null
          })
        }
        else {
          if (updatedresponse.nModified == 1) {
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: {
                "token": token
              }
            });
          }
          else {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["405"],
              statuscode: 405,
              details: null
            })

          }
        }
      })

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },


  /*
     This function is used to fetch the details of 
     all the conference of a particular user
  */
  //================================================
  listConference: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the user id of the user who hosted the conference
      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Host User Id",
      });

      // Circular json avoided
      if (!req.body.userId) { return; }
      let aggrQuery = [
        {
          '$match': { 'participants.userId': userId }
        },
      ];
      Conference.aggregate(aggrQuery, function (err, response) {

        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
        }
        else {

          if (response.length == 0) {
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["204"],
              statuscode: 204,
              details: response
            });

            return;
          }

          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: response
          });
        }

      })

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  // ===========================================

  testMail: (req, res, next) => {
    var mailData = {
      receiver: 'subhendu.sett@pkweb.in',
      subject: "nexmoEvent",
      message: "Hello!"
    };

    MailServices.viaGmail(mailData, (err, data) => {
      if (err) {
        res.json(err);
      } else {
        res.status(200).send();
      }
    });

    // logger.info('this is test info');
    // res.status(200).send();
  },

  // Nexmo api --start
  dialOutForDialin: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      let mailData = {
        receiver: 'subhendu.sett@pkweb.in',
        subject: "dialOutForDialin",
        message: "Hello!"
      };
      logger.info('THIS IS IN ->> dialOutForDialin');
      const { sessionId } = req.body; // sessionid
      const conferenceNumber = config.nexmo.conferenceNumber;
      const sipTokenData = `{"sip":true, "role":"client", "name":"'${conferenceNumber}'"}`;
      // const sessionId = app.get(roomId);
      // get role of the token whether this is 'publisher' or 'subsriber'
      let tokenRole = await ConferenceBusiness.getOpentokTokenRole(sessionId, "", "sip");
      const token = opentok.generateToken(sessionId, {
        role: tokenRole,
        data: sipTokenData,
      });
      const options = {
        auth: {
          username: config.nexmo.sip.username,
          password: config.nexmo.sip.password,
        },
        secure: false,
      };
      const sipUri = `sip:${conferenceNumber}@sip.nexmo.com;transport=tls`;
      opentok.dial(sessionId, token, sipUri, options, (error, sipCall) => {
        if (error) {
          // mailData.subject = 'dialOutForDialin - There was an error dialing out'
          // mailData.message = JSON.stringify(error)
          // MailServices.viaGmail(mailData, (err, data) => {
          //   res.status(500).send('There was an error dialing out');
          // });
          console.log('There was an error dialing out');
          res.status(500).send('There was an error dialing out');
        } else {
          // mailData.subject = 'dialOutForDialin - Success'
          // mailData.message = JSON.stringify(sipCall);
          // MailServices.viaGmail(mailData, (err, data) => {
          //   res.json(sipCall);
          // });
          console.log(sipCall.connectionId);
          let updateWith = {
            "inboundConnectionId": sipCall.connectionId,
          }

          let updateWhere = {
            "sessionId": sessionId,
          }

          Conference.updateOne(updateWhere, updateWith,
            function (updatederror, updatedresponse) {
              if (updatederror) {
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["404"],
                  message: "Error",
                  statuscode: 404,
                  details: null
                });

                return;
              } else {
                // console.log('sipCall', sipCall);
                res.json({
                  isError: false,
                  message: errorMsgJSON[lang]["200"],
                  statuscode: 200,
                  details: sipCall
                });
                // res.json(sipCall);
              }
            })
        }
      });

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }

  },

  dialOutHangUp: (req, res, next) => {
    logger.info('THIS IS IN ->> dialOutHangUp');
    let mailData = {
      receiver: 'subhendu.sett@pkweb.in',
      subject: "dialOutHangUp",
      message: "dialOutHangUp!"
    };
    // MailServices.viaGmail(mailData, (err, data) => {
    //   res.status(200).send();
    // });
    const { sessionId } = req.body; // sessionid 2_MX40NjQyNTA3Mn5-MTU3NTYyNzk4MTU3Nn5jQmtHRk5Dc3gzTUJkR2tWdlVyWHc2SGV-fg
    const { connectionId } = req.body; // connectionId e17e0d9b-6c3f-454a-9bef-4e3e92d44783
    opentok.forceDisconnect(sessionId, connectionId, (error) => {
      if (error) {
        console.log('There was an error hanging up');
        res.status(500).send('There was an error hanging up');
      } else {
        console.log('ok');
        res.status(200).send('Ok');
      }
    });
  },

  nexmoEvent: (req, res, next) => {
    logger.info('THIS IS IN ->> nexmoEvent');
    logger.info(req.body);
    var mailData = {
      receiver: 'subhendu.sett@pkweb.in',
      subject: "nexmoEvent",
      message: "Hello!"
    };

    // MailServices.viaGmail(mailData, (err, data) => {
    //   res.status(200).send();
    // });
    // console.log('nexmoEvent', req);
    logger.info('nexmoEvent ->> ' + JSON.stringify(req));

    res.status(200).send();

  },

  nexmoAnswer: (req, res, next) => {
    logger.info('THIS IS IN ->> nexmoAnswer');

    let = sipHeader = req.query['SipHeader_X-OpenTok-SessionId'];
    logger.info('nexmoAnswer ->> ' + sipHeader);

    console.log('nexmo-answer ==== ', sipHeader);
    // var mailData = {
    //   receiver: 'subhendu.sett@pkweb.in',
    //   subject: "nexmoAnswer",
    //   message: req.query['SipHeader_X-OpenTok-SessionId']
    // };
    // MailServices.viaGmail(mailData, (err, data) => { });

    const { serverUrl } = config.nexmo;
    const ncco = [];
    if (sipHeader) {
      ncco.push({
        action: 'conversation',
        name: sipHeader,
      });
    } else {
      ncco.push(
        {
          action: 'talk',
          text: 'Please Please enter a a pin code to join the session'
        },
        {
          action: 'input',
          eventUrl: [`${serverUrl}nexmo-dtmf`],
          submitOnHash: true,
          timeOut: 10,
          maxDigits: 10
        }
      )
    }

    logger.info('nexmoAnswer ->> ' + JSON.stringify(ncco));
    res.json(ncco);
  },

  nexmoDtmf: (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      logger.info('THIS IS IN ->> nexmoDtmf');
      // const { dtmf } = req.body;
      // Here, We are receiving the dtmf a six digit code
      let dtmf = req.body.dtmf ? req.body.dtmf : "";
      dtmf = dtmf.toString();

      logger.info(req.body);

      let aggrQuery = [
        {
          $match: {
            'dtmf': dtmf,
            'isDeleted': false
          }
        },
        {
          $project: {
            '_id': 0,
            "sessionId": 1,
            "conferenceTitle": 1
          }
        },
      ];

      Conference.aggregate(aggrQuery, function (err, responseConference) {
        console.log(responseConference);
        if (err) {
          let ncco = [{
            action: 'talk',
            text: 'Invalid pin'
          }];

          res.json(ncco);

        } else {
          if (responseConference.length > 0) {

            let ncco = [{
              action: 'conversation',
              name: responseConference[0].sessionId
            }];

            res.json(ncco);

          } else {
            let ncco = [{
              action: 'talk',
              text: 'Invalid pin'
            }];

            res.json(ncco);
          }
        }
      })

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  nexmoFallback: (req, res, next) => {
    logger.info('THIS IS IN ->> nexmoFallback');
    logger.info('nexmoFallback ->> ' + JSON.stringify(req));

    // var mailData = {
    //   receiver: 'subhendu.sett@pkweb.in',
    //   subject: "nexmoFallback",
    //   message: JSON.stringify(req)
    // };
    // MailServices.viaGmail(mailData, (err, data) => {
    //   res.status(200).send();
    // });
    res.status(200).send();
  },

  // Nexmo api --end

  hostConference: (req, res, next) => {
    // After creating a conference we need to host the conference by creating a token:
    /*var sessionId = app.get('sessionId');
    // generate a fresh token for this client
    var token = opentok.generateToken(sessionId, {
      role: 'publisher',
      initialLayoutClassList: ['focus']
    });

    res.render('host.ejs', {
      apiKey: apiKey,
      sessionId: sessionId,
      token: token,
      initialBroadcastId: app.get('broadcastId'),
      focusStreamId: app.get('focusStreamId') || '',
      initialLayout: app.get('layout')
    });*/
  },


  startBroadcast: (req, res, next) => {
    // Start Broadcast by the user.
    /*var broadcastOptions = {
    maxDuration: Number(req.param('maxDuration')) || undefined,
    resolution: req.param('resolution'),
    layout: req.param('layout'),
    outputs: {
      hls: {}
    }
  };
  opentok.startBroadcast(app.get('sessionId'), broadcastOptions, function (err, broadcast) {
    if (err) {
      return res.send(500, err.message);
    }
    app.set('broadcastId', broadcast.id);
    return res.json(broadcast);
  });*/
  },


  stopBroadcast: (req, res, next) => {
    /*var broadcastId = req.param('broadcastId');
    opentok.stopBroadcast(broadcastId, function (err, broadcast) {
      if (err) {
        return res.send(500, 'Error = ' + err.message);
      }
      app.set('broadcastId', null);
      return res.json(broadcast);
    });*/
  },
  //======

  startArchive: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let sessionId = req.body.sessionId ? req.body.sessionId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - sessionId",
    });
    let conferenceId = req.body.conferenceId ? req.body.conferenceId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - conferenceId",
    });
    if (!req.body.sessionId || !req.body.conferenceId) { return; }

    // Here, We are going to fetch the details of conference duration table
    let fetchConferenceDurationDetail = await PriceManagementBusiness.fetchConferenceDurationForSipCall(sessionId);
    if (fetchConferenceDurationDetail.isError == true) { // some error occur
      res.json({
        isError: true,
        message: "Some error Occur While Fetching the Conference Duration Detail",
        statuscode: 404,
        details: null
      });

      return;
    }

    if (fetchConferenceDurationDetail.isConfDurationExist == false) { // some error occur
      res.json({
        isError: false,
        message: "Either the conference not started or conference doesnot exist",
        statuscode: 204,
        details: null
      });

      return;
    }

    let companyId = fetchConferenceDurationDetail.confDuration[0].companyId;
    let conferenceDurationId = fetchConferenceDurationDetail.confDuration[0]._id;


    var archiveOptions = {
      name: 'Important Presentation',
      // outputMode: 'individual'
    };
    opentok.startArchive(sessionId, archiveOptions, function (err, archive) {
      if (err) {
        console.log(err);
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          error: err,
          statuscode: 404,
          details: null
        });
      } else {
        // save into a database

        /*archive:{
        id: '6ed487af-0f94-4267-b0fd-07e4085280ed',
        status: 'started',
        name: 'Important Presentation',
        reason: '',
        sessionId: '1_MX40NjQyNTA3Mn5-MTU3OTE3MTUzNTQyNX4zemJRYjZrejRDVk1ZY0dMbjdRZkFLOXd-fg',
        createdAt: 1579704365000,
        size: 0,
        duration: 0,
        outputMode: 'individual',
        hasAudio: true,
        hasVideo: true,
        updatedAt: 1579704366000,
        url: null
      }*/
        let newArchive = new Archives({
          archiveId: archive.id,
          status: archive.status,
          name: 'Important Presentation',
          reason: archive.reason,
          sessionId: archive.sessionId,
          startedAt: archive.createdAt,
          size: archive.size,
          duration: archive.duration,
          outputMode: archive.outputMode,
          hasAudio: archive.hasAudio,
          hasVideo: archive.hasVideo,
          lastUpdate: archive.lastUpdate,
          event: archive.event,
          url: `https://callarchives.s3.us-east-2.amazonaws.com/46425072/${archive.id}/archive.mp4`,
          conferenceId: conferenceId,
          companyId: companyId,
          conferenceDurationId: mongoose.Types.ObjectId(conferenceDurationId),
        });
        console.log("new archive:" + archive.id);
        newArchive.save(function (err, archiveDetail) {
          if (err) {
            console.log('err ==== ', err);

            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: archive
            });
          } else {
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: archive
            });
          }
        })
      }
    });
  },

  stopArchive: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let archiveId = req.body.archiveId ? req.body.archiveId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - Archive Id",
    });
    if (!req.body.archiveId) { return; }
    opentok.stopArchive(archiveId, function (err, archive) {
      if (err) {
        console.log(err);
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          error: err,
          statuscode: 404,
          details: null
        });
      } else {
        console.log("Stopped archive:" + archive.id);
        // Save in DB
        /*Archive {
          id: '6ed487af-0f94-4267-b0fd-07e4085280ed',
          status: 'available',
          reason: 'user initiated',
          sessionId: '1_MX40NjQyNTA3Mn5-MTU3OTE3MTUzNTQyNX4zemJRYjZrejRDVk1ZY0dMbjdRZkFLOXd-fg',
          createdAt: 1579704365000,
          size: 707488,
          duration: 210,
          outputMode: 'individual',
          hasAudio: true,
          hasVideo: true,
          updatedAt: 1579704585000,
          event: 'archive',
          url: 'https://s3.amazonaws.com/tokbox.com.archive2/46425072/6ed487af-0f94-4267-b0fd-07e4085280ed/archive.zip?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20200122T145005Z&X-Amz-SignedHeaders=host&X-Amz-Expires=600&X-Amz-Credential=AKIAT5VIDVNM7GIYBDFL%2F20200122%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=af871bf71543e3f201c7743ab7e2502fb0ac76b264e3551adb80ae67beca0ae9',
          }*/
        console.log(archive);
        let updateWith = {
          archiveId: archive.id,
          status: archive.status,
          name: 'Important Presentation',
          reason: archive.reason,
          sessionId: archive.sessionId,
          startedAt: archive.createdAt,
          size: archive.size,
          duration: archive.duration,
          outputMode: archive.outputMode,
          hasAudio: archive.hasAudio,
          hasVideo: archive.hasVideo,
          lastUpdate: archive.lastUpdate,
          event: archive.event,
          url: `https://callarchives.s3.us-east-2.amazonaws.com/46425072/${archive.id}/archive.mp4`,
        }

        let updateWhere = {
          "archiveId": archiveId,
        }

        Archives.updateOne(updateWhere, updateWith,
          function (updatederror, updatedresponse) {

            if (updatederror) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["404"],
                statuscode: 404,
                details: null
              });
              return;
            } else {
              res.json({
                isError: false,
                message: errorMsgJSON[lang]["200"],
                statuscode: 200,
                details: archive
              });
              return;
            }
          });
      }
    });
  },

  getArchives: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let archiveId = req.body.archiveId ? req.body.archiveId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - Archive Id",
    });
    if (!req.body.archiveId) { return; }
    opentok.getArchive(archiveId, function (err, archive) {
      if (err) {
        console.log(err);
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          error: err,
          statuscode: 404,
          details: null
        });
      } else {
        console.log(archive);
        // Save in DB
        /*Archive {
          id: '6ed487af-0f94-4267-b0fd-07e4085280ed',
          status: 'available',
          reason: 'user initiated',
          sessionId: '1_MX40NjQyNTA3Mn5-MTU3OTE3MTUzNTQyNX4zemJRYjZrejRDVk1ZY0dMbjdRZkFLOXd-fg',
          createdAt: 1579704365000,
          size: 707488,
          duration: 210,
          outputMode: 'individual',
          hasAudio: true,
          hasVideo: true,
          updatedAt: 1579704585000,
          event: 'archive',
          url: 'https://s3.amazonaws.com/tokbox.com.archive2/46425072/6ed487af-0f94-4267-b0fd-07e4085280ed/archive.zip?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20200122T145005Z&X-Amz-SignedHeaders=host&X-Amz-Expires=600&X-Amz-Credential=AKIAT5VIDVNM7GIYBDFL%2F20200122%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=af871bf71543e3f201c7743ab7e2502fb0ac76b264e3551adb80ae67beca0ae9',
          }*/
        let updateWith = {
          archiveId: archive.id,
          status: archive.status,
          name: 'Important Presentation',
          reason: archive.reason,
          sessionId: archive.sessionId,
          startedAt: archive.createdAt,
          size: archive.size,
          duration: archive.duration,
          outputMode: archive.outputMode,
          hasAudio: archive.hasAudio,
          hasVideo: archive.hasVideo,
          lastUpdate: archive.lastUpdate,
          event: archive.event,
          url: archive.url,
        }

        let updateWhere = {
          "archiveId": archiveId,
        }

        Archives.updateOne(updateWhere, updateWith,
          function (updatederror, updatedresponse) {

            if (updatederror) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["404"],
                statuscode: 404,
                details: null
              });
              return;
            } else {
              res.json({
                isError: false,
                message: errorMsgJSON[lang]["200"],
                statuscode: 200,
                details: archive
              });
              return;
            }
          });
      }
    });
  },

  deleteArchive: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let archiveId = req.body.archiveId ? req.body.archiveId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - Archive Id",
    });
    if (!req.body.archiveId) { return; }
    opentok.deleteArchive(archiveId, function (err) {
      if (err) {
        console.log(err);
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          error: err,
          statuscode: 404,
          details: null
        });
      } else {
        let updateWith = {
          status: 'deleted',
        }

        let updateWhere = {
          "archiveId": archiveId,
        }

        Archives.updateOne(updateWhere, updateWith,
          function (updatederror, updatedresponse) {

            if (updatederror) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["404"],
                statuscode: 404,
                details: null
              });
              return;
            } else {
              res.json({
                isError: false,
                message: errorMsgJSON[lang]["200"],
                statuscode: 200,
                details: 'Archive deleted.'
              });
              return;
            }
          });
      }
    });
  },

  getArchiveList: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let conferenceId = req.body.conferenceId ? req.body.conferenceId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - conference Id",
    });
    if (!req.body.conferenceId) { return; }
    let findQry = { conferenceId };
    Archives.find(findQry, function (err, responseArchive) {
      if (err) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["405"],
          statuscode: 405,
          details: null
        })
        return;
      } else {
        res.json({
          isError: false,
          message: errorMsgJSON[lang]["200"],
          statuscode: 200,
          details: responseArchive
        });
        return;
      }
    });
  },

  isHostJoin: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let conferenceId = req.body.conferenceId ? req.body.conferenceId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - conference Id",
    });

    let findQry = { '_id': conferenceId, isActive: true };
    Conference.find(findQry, function (err, responseConference) {
      if (err) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["405"],
          statuscode: 405,
          details: null
        })
        return;
      } else {
        console.log('responseConference', responseConference);
        if (responseConference && responseConference.length > 0) {
          let conferecnce = responseConference[0];
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: true
          });
          return;
        } else {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: false
          });
          return;
        }
      }
    });

  },

  //===================
  //  For getting the time durations/ which users were live in an web call for how long --
  webCallDuration: async (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";

    try {

      let lang = req.headers.language ? req.headers.language : "EN";
      let operatorId = req.body.operatorId ? req.body.operatorId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - operator Id",
      });
      let service = req.body.service ? req.body.service : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - service",
      });
      let year = req.body.year ? req.body.year : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - year",
      });
      let companyId = req.body.companyId;

      if (!req.body.operatorId || !req.body.service || !req.body.year) { return; }

      let dt = new Date()
      let months = [];
      let currentMonth = dt.getMonth() + 1;

      let k;

      for (k = 1; k <= currentMonth; k++) {
        months.push(k);
      }



      let fetchWebCallDetailForUserStatus = await ConferenceBusiness.fetchWebCallDetailForUser(operatorId, months, year, companyId);


      if (fetchWebCallDetailForUserStatus.isError == true) {
        res.json({
          isError: true,
          message: "Some Error Occur While Fetching The details of Web Call",
          statuscode: 404,
          details: null
        });

        return;
      }

      if (fetchWebCallDetailForUserStatus.isUserExist == false) {
        res.json({
          isError: false,
          message: "No Data Found",
          statuscode: 204,
          details: null
        });

        return;
      }

      let webCallDetails = fetchWebCallDetailForUserStatus.webCallDetails;
      let startedAt;
      let endedAt;
      let responseMonth;

      let differenceInTime;
      let differenceInDays;
      let differenceInHours;
      let differenceInMinutes;
      let differenceInSeconds;

      let totalDays = 0;
      let totalHours = 0;
      let totalMinutes = 0;
      let totalSeconds = 0;
      let webCallDurationDetail = [];
      let webCallDurationDetailSingle = {};

      /*
          "response": {
          "_id": "5e54da6e72a68f3c85f05d96",
          "conferenceId": "5e4e6049d7a2c02d9fb94214",
          "sessionId": "2_MX40NjQyNTA3Mn5-MTU4MjE5NDc2MDg1MH5za2FoYzVzN3BpYjlKNUxCU0dCdUFhd2h-fg",
          "streamId": "4ce61e6f-1d2a-42af-b744-37f7e1d51b83",
          "connectionId": "f59fb8ad-3aa0-4466-96c9-326fcbcbb931",
          "startedAt": "1582619246228",
          "endedAt": "1582619350518",
          "conferenceStatus": "ENDED",
          "createdAt": "2020-02-25T08:27:26.231Z"
        },
        "year": 2020,
        "month": 2
      },

      */


      let j;
      for (j = 0; j < months.length; j++) {

        totalDays = 0;
        totalHours = 0;
        totalSeconds = 0;

        let i;
        for (i = 0; i < webCallDetails.length; i++) {
          startedAt = webCallDetails[i].response.startedAt;
          endedAt = webCallDetails[i].response.endedAt;
          responseMonth = webCallDetails[i].month;
          console.log("in2loop=>" + responseMonth)



          if (responseMonth == months[j]) {

            differenceInTime = parseInt(endedAt) - parseInt(startedAt);

            // calculating the number of days  
            differenceInDays = differenceInTime / (1000 * 3600 * 24);
            totalDays = totalDays + differenceInDays;

            // calculating the number of Hours  
            differenceInHours = differenceInTime / (1000 * 3600);
            totalHours = totalHours + differenceInHours;

            // calculating the number of Minutes  
            differenceInMinutes = differenceInTime / (1000 * 60);
            totalMinutes = totalMinutes + differenceInMinutes;

            // calculating the number of Seconds  
            differenceInSeconds = differenceInTime / (1000);
            totalSeconds = totalSeconds + differenceInSeconds;

          }

        } // End for (i = 0; i < webCallDetails.length; i++) {
        webCallDurationDetailSingle = {
          "totalDays": totalDays,
          "totalHours": totalHours,
          "totalSeconds": totalSeconds,
          "month": months[j]
        }

        webCallDurationDetail.push(webCallDurationDetailSingle);
      } // for (j = 0; j < months.length; j++) {  



      res.json({
        isError: false,
        message: errorMsgJSON[lang]["200"],
        statuscode: 200,
        details: {
          webCallDurationDetail: webCallDurationDetail
        }
      });

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }



  },


}