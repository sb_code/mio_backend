//=================================================
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const Conference = require("../models/conference");
const SipCall = require("../models/sipCall");
const Guest = require("../models/guest");
const User = require("../models/user");
const Invitation = require("../models/invitation");
// const bcrypt = require("bcrypt");
const SALT_WORK_FACTOR = 10;
const Mailercontroller = require("../../../services/mailer");

const errorMsgJSON = require("../../../services/errors.json");
const config = require("../config/globals/keyfile");
const AutogenerateIdcontroller = require('../../../services/AutogenerateId');
const Base64ImgUploadmodule = require('../../../services/Base64ImgUploadmodule');

const Sip_dialBusiness = require("../BusinessLogic/sip_dialBusiness");
const PriceManagementBusiness = require("../BusinessLogic/priceManagementBusiness");
const sipUriHandler = require("../BusinessLogic/sipUriHandler");
const ConferenceBusiness = require("../BusinessLogic/conferenceBusiness");


var fileupload = require('express-fileupload');

const fs = require('fs');
const AWS = require('aws-sdk');


//=================================================
var OpenTok = require('opentok'),
  opentok = new OpenTok(config.tokbox.apiKey, config.tokbox.apiSecret);

//================================
// Import events module
var events = require('events');

//===========================================

module.exports = {

  /*
      This function for placing call to participant for the conference    
 */
  sipDialOutMultiple: async (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiviing the session id
      let sessionId = req.body.sessionId ? req.body.sessionId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Session Id",
      });

      // // Here, we are receiviing the session id
      // let countryCode = req.body.countryCode ? req.body.countryCode : res.json({
      //   isError: true,
      //   message: errorMsgJSON[lang]["303"] + " - Country Code",
      // });

      // Here, we are receiving the receiver's mobile number
      let fromMobile = req.body.fromMobile ? req.body.fromMobile : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Mobile",
      });

      // Here, Circular json avoided
      if (!req.body.sessionId || !req.body.fromMobile || !req.body.mioUsers) { return; }

      let contactNumber;

      let senderUri = '14155550101@example.com';


      let toUser;
      var statusReport = [];

      // Here, we are checking whether user exist in this conference or not
      let checkConferenceIsActiveStatus = await Sip_dialBusiness.checkConferenceIsActive(sessionId);

      if (checkConferenceIsActiveStatus.ErrorStatus == true) { // Error occur while checking wheathe user exist in the conference
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          statuscode: 404,
          details: null
        });

        return;
      }

      if (checkConferenceIsActiveStatus.isConferenceExist == false) { // Conference Does Not Exist
        res.json({
          isError: false,
          message: "Conference Not Exist",
          statuscode: 204,
          details: null
        });

        return;
      }

      if (checkConferenceIsActiveStatus.responseConference[0].isActive == false) { // Conference Is Not Active
        res.json({
          isError: false,
          message: errorMsgJSON[lang]["206"],
          statuscode: 206,
          details: null
        });

        return;
      }

      // Here, we are fetching the conference duration Id
      let fetchConferenceDurationForSipCallStatus = await PriceManagementBusiness.fetchConferenceDurationForSipCall(sessionId);
      let conferenceDurationId
      let companyId
      if (fetchConferenceDurationForSipCallStatus.isError == true || fetchConferenceDurationForSipCallStatus.isConfDurationExist == false) {
        conferenceDurationId = 'NOT FOUND';
        companyId = 'NOT FOUND';
      } else {
        conferenceDurationId = fetchConferenceDurationForSipCallStatus.confDuration[0]._id;
        companyId = fetchConferenceDurationForSipCallStatus.confDuration[0].companyId;
      }


      // this function is used to check whether user exist or not
      async function checkUser(toUser) {  // start of checkUser

        return new Promise(function (resolve, reject) {
          let searchQry = {
            mobile: toUser.toMobile.toString(),
            countryCode: toUser.countryCode.toString()
          };
          User.find(searchQry, function (err, user) {
            if (err) {
              resolve({ "isUserExists": false });
            } else {

              if (user.length > 0) {
                resolve({
                  "isUserExists": true,
                  "userDetail": user
                });
              } else {
                resolve({
                  "isUserExists": false,
                });
              }
            }

          });
        })
      } // start of checkUser


      // This Function is used to place a call to a mobile
      async function dialToUser(toUser) { // Start dialToUser

        contactNumber = '' + toUser.countryCode + toUser.toMobile;


        let sipUri = 'sip:+' + contactNumber + '@31.13.156.183';

        let sipConfig = sipUriHandler.getOutboundConfig({
          toCountryCode: toUser.countryCode,
          toMobile: toUser.toMobile,
          fromCountryCode: '1',
          fromMobile: '4155550101',
        });
        // Here, we are generating the token for dialing call  
        let tokenRole = await ConferenceBusiness.getOpentokTokenRole(sessionId, "", "sip");
        var sipToken = opentok.generateToken(sessionId, {
          role: tokenRole,
          data: `phoneNumber:${contactNumber}`
        });


        return new Promise(function (resolve, reject) {
          opentok.dial(sessionId, sipToken, sipConfig.sipUri, sipConfig.option, function (err, sipCall) {// when error occur
            //console.log(sipCall);
            if (err) {
              resolve({ "isDialed": false });
            } else {
              resolve({
                "isDialed": true,
                "sipToken": sipToken,
                "sipCall": sipCall
              });
            }

          })
        })
      } // End dialToUser

      // This function is written add the details of sip call in database
      async function addSipCallDetail(SipCall) {
        return new Promise(function (resolve, reject) {
          SipCall.save(function (err, item) {
            if (err) {  // When Error Occur
              resolve({
                "ErrorStatus": true,
              });
            } else {
              resolve({
                "ErrorStatus": false,
                "sipCall": item
              });
            }
          });
        }) // End of Promise 
      } // End addSipCallDetail 

      // This function is used to check whether the user exist as a participant in the conference
      async function checkParticipant(toUser) {

        return new Promise(function (resolve, reject) {

          let aggrQuery = [
            {
              $match: {
                'sessionId': sessionId,
              }
            },
            {
              $project: {
                "hostUserId": 1,
                "participants": 1
              }
            },
            { $unwind: "$participants" },
            {
              '$match': {
                'participants.contactNumber': toUser.toMobile.toString(),
                'participants.countryCode': toUser.countryCode.toString()
              }
            },
          ];

          Conference.aggregate(aggrQuery, function (err, responseConference) {


            if (err) {
              resolve({
                "isParticipantExist": false,
                "ErrorStatus": true,
                "toUser": toUser
              });
            } else {
              if (responseConference.length > 0) {
                resolve({
                  "isParticipantExist": true,
                  "ErrorStatus": false,
                  "toUser": toUser,
                  "participantDetail": responseConference
                });
              } else {
                resolve({
                  "isParticipantExist": false,
                  "ErrorStatus": false,
                  "toUser": toUser,
                  "participantDetail": null
                });
              }

            }

          })

        })
      } // End checkParticipant

      // This function is used to update the details of the participant
      async function updateParticipant(participantExistStatus, data, toUser) {

        let updateWith = {
          "participants.$.connectionId": data.sipCall.connectionId,
          "participants.$.streamId": data.sipCall.streamId,
          "participants.$.createdAt": data.sipCall.createdAt,
          "participants.$.isStarted": true
        }

        let updateWhere = {
          "sessionId": sessionId,
          "participants.contactNumber": toUser.toMobile.toString(),
        }

        return new Promise(function (resolve, reject) {
          Conference.updateOne(updateWhere, updateWith,
            function (updatederror, updatedresponse) {

              if (updatederror) {
                resolve({
                  "ErrorStatus": true,
                });

              } else {

                resolve({
                  "ErrorStatus": false,
                });

              }

            })
        })

      } // End updateParticipant

      // This function is used to add the user as a participant in the conference
      async function insertParticipant(insertquery, toUser) {

        return new Promise(function (resolve, reject) {

          Conference.updateOne({ 'sessionId': sessionId },
            { $push: { "participants": insertquery } },
            function (updatederror, updatedresponse) {
              if (updatederror) {
                resolve({
                  "ErrorStatus": true,
                });

              } else {

                resolve({
                  "ErrorStatus": false,
                });
              }

            })

        })
      } // End insertParticipant 


      // This the main function form where all the function are called 
      async function mainOperation(toUser) {


        // Here, Placing a call to a number 
        let dailingStatus = await dialToUser(toUser);
        if (dailingStatus.isDialed == false) { // When Dial Was not Successfull
          var dataForOperation = {
            "operation": "Dial Was not Successfull",
            "message": errorMsgJSON[lang]["404"],
            "statuscode": 404,
            "isError": false,
            "toUser": toUser.toMobile,
            "data": null,
          }

          return dataForOperation;

        }


        let sipCallNew = new SipCall({
          toMobile: toUser.toMobile,
          countryCode: toUser.countryCode,
          companyId: companyId,
          conferenceDurationId: mongoose.Types.ObjectId(conferenceDurationId),
          fromMobile: fromMobile,
          sipCallId: dailingStatus.sipCall.id,
          sessionId: dailingStatus.sipCall.sessionId,
          projectId: dailingStatus.sipCall.projectId,
          connectionId: dailingStatus.sipCall.connectionId,
          streamId: dailingStatus.sipCall.streamId,
          createdAt: dailingStatus.sipCall.createdAt,
          updatedAt: dailingStatus.sipCall.updatedAt
        });

        // Here, we are inserting the the details of sip call
        let addSipCallDetailStatus = await addSipCallDetail(sipCallNew);

        if (addSipCallDetailStatus.ErrorStatus == true) { // When sip call Can not be registered
          var dataForOperation = {
            "operation": "Sip Call Was not added Successfull",
            "message": errorMsgJSON[lang]["404"],
            "statuscode": 404,
            "isError": false,
            "toUser": toUser.toMobile,
            "data": null,
          }

          return dataForOperation;
        }

        // Here, we check whether the user exist as participant in the conference
        let participantExistStatus = await checkParticipant(toUser);

        if (participantExistStatus.ErrorStatus == true) { // Error occur while checking the participant
          var dataForOperation = {
            "operation": "Error Occur in checking the participant",
            "message": errorMsgJSON[lang]["404"],
            "statuscode": 404,
            "isError": true,
            "toUser": toUser.toMobile,
            "data": null,
          }

          return dataForOperation;
        }

        var data = {
          "toMobile": toUser.toMobile,
          "sipToken": dailingStatus.sipToken,
          "sipCall": dailingStatus.sipCall
        }

        if (participantExistStatus.isParticipantExist == true) { // When User Exist as participant // START 4th IF

          // Updating the details of participant  
          let participantUpdateStatus = await updateParticipant(participantExistStatus, data, toUser);

          if (participantUpdateStatus.ErrorStatus == false) { // When Update occur successfully

            var dataForOperation = {
              "operation": "updated existing participant",
              "message": errorMsgJSON[lang]["200"],
              "statuscode": 200,
              "isError": false,
              "toUser": toUser.toMobile,
              "data": data,

            }

            return dataForOperation;
          } else { // updation failed

            var dataForOperation = {
              "operation": "Can not update existing participant",
              "message": errorMsgJSON[lang]["405"],
              "statuscode": 405,
              "isError": true,
              "toUser": toUser.toMobile,
              "data": data,

            }

            return dataForOperation;
          }

        } else { // When User does Exist as participant // ELSE FOR 4th IF

          // Here, we are checking whether user with this mobile exists in our system 
          let userExistStatus = await checkUser(toUser);
          console.log(userExistStatus.isUserExists);

          if (userExistStatus.isUserExists == false) { // When User not exist

            let insertquery = {
              "contactNumber": toUser.toMobile,
              "isStarted": true,
              "countryCode": toUser.countryCode,
              "userType": 'DIALEDUSER',
              "connectionId": dailingStatus.sipCall.connectionId,
              "streamId": dailingStatus.sipCall.streamId,
              "createdAt": dailingStatus.sipCall.createdAt,
            }

            // add the user as a participant in the conference 
            let participantInsertStatus = await insertParticipant(insertquery, toUser);


            if (participantInsertStatus.ErrorStatus == false) { // When Update occur successfully

              var dataForOperation = {
                "operation": "Participant Inserted",
                "message": errorMsgJSON[lang]["200"],
                "statuscode": 200,
                "isError": false,
                "toUser": toUser.toMobile,
                "data": data,

              }

              return dataForOperation;
            } else { // updation failed

              var dataForOperation = {
                "operation": "Participant was not inserted",
                "message": errorMsgJSON[lang]["405"],
                "statuscode": 405,
                "isError": true,
                "toUser": toUser,
                "data": data,

              }

              return dataForOperation;
            }

          }

          if (userExistStatus.isUserExists == true) { // When User exist IF Block

            let insertquery = {
              "contactNumber": toUser,
              "countryCode": countryCode,
              "userType": userExistStatus.userDetail[0].userType,
              "connectionId": dailingStatus.sipCall.connectionId,
              "streamId": dailingStatus.sipCall.streamId,
              "createdAt": dailingStatus.sipCall.createdAt,
              "userId": userExistStatus.userDetail[0].userId,
            }

            // add the user as a participant in the conference 
            let participantInsertStatus = await insertParticipant(insertquery, toUser);

            if (participantInsertStatus.ErrorStatus == false) { // When Update occur successfully

              var dataForOperation = {
                "operation": "Participant Inserted",
                "message": errorMsgJSON[lang]["200"],
                "statuscode": 200,
                "isError": false,
                "toUser": toUser,
                "data": data,
              }

              return dataForOperation;
            } else { // updation failed

              var dataForOperation = {
                "operation": "Participant was not inserted",
                "message": errorMsgJSON[lang]["405"],
                "statuscode": 405,
                "isError": true,
                "toUser": toUser,
                "data": data,

              }

              return dataForOperation;
            }

          } // END User exist IF Block






        } // END 4th ELSE

      } // End mainOperation

      // From Here, We Started our execution
      for (i = 0; i < req.body.mioUsers.length; i++) {

        // here we are assigning the mobile number  
        toUser = req.body.mioUsers[i];

        // start executing the operation for each number
        let operationStatus = await mainOperation(toUser);

        statusReport.push(operationStatus);


        if (i == (req.body.mioUsers.length - 1)) {
          res.json({
            sessionId: sessionId,
            fromMobile: fromMobile,
            details: statusReport
          });
        }

      } // End For Loop

    } catch (error) {
      console.log(error);
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: null
      });
    }
  },



  //======================*********************************************************************
  /*
       This function for placing call to participant for the conference    
  */
  sipDialOut: async (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiviing the session id
      let sessionId = req.body.sessionId ? req.body.sessionId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Session Id",
      });

      // Here, we are receiviing the sender's mobile number
      let toMobile = req.body.toMobile ? req.body.toMobile : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Mobile",
      });

      // Here, we are receiving the receiver's mobile number
      let fromMobile = req.body.fromMobile ? req.body.fromMobile : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Mobile",
      });

      let sipUri = 'sip:+' + toMobile + '@31.13.156.183';

      let senderUri = '14155550101@example.com';

      // Here, Circular json avoided
      if (!req.body.sessionId || !req.body.fromMobile || !req.body.toMobile) { return; }

      // Here, we are generating the token for dialing call  
      let tokenRole = await ConferenceBusiness.getOpentokTokenRole(sessionId, "", "sip");
      var sipToken = opentok.generateToken(sessionId, {
        role: tokenRole,
        data: `phoneNumber:${toMobile}`
      });

      // Here, we are checking whether user exist in this conference or not
      let checkConferenceIsActiveStatus = await Sip_dialBusiness.checkConferenceIsActive(sessionId);

      if (checkConferenceIsActiveStatus.ErrorStatus == true) { // Error occur while checking wheathe user exist in the conference
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          statuscode: 404,
          details: null
        });

        return;
      }

      if (checkConferenceIsActiveStatus.isConferenceExist == false) { // Conference Does Not Exist
        res.json({
          isError: false,
          message: "Conference Not Exist",
          statuscode: 204,
          details: null
        });

        return;
      }

      if (checkConferenceIsActiveStatus.responseConference[0].isActive == false) { // Conference Is Not Active
        res.json({
          isError: false,
          message: errorMsgJSON[lang]["206"],
          statuscode: 206,
          details: null
        });

        return;
      }

      // Here, we are dialing a call 
      let dialOpentokStatus = await Sip_dialBusiness.dialOpentok(sessionId, sipToken, sipUri, senderUri);

      if (dialOpentokStatus.ErrorStatus == true) { // When the dial was not successfull
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          statuscode: 404,
          details: null
        });

        return;
      }


      // Here, we are inserting the the details of sip call
      let addSipCallDetailStatus = await Sip_dialBusiness.addSipCallDetail(toMobile, fromMobile, dialOpentokStatus);

      if (addSipCallDetailStatus.ErrorStatus == true) { // Error Occur While inserting sip call details
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          statuscode: 404,
          details: null
        });

        return;
      }

      // Here, we are checking whether user exist in this conference or not
      let checkParticipantExistStatus = await Sip_dialBusiness.checkParticipantExist(sessionId, toMobile);

      if (checkParticipantExistStatus.ErrorStatus == true) { // Error occur while checking wheathe user exist in the conference
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          statuscode: 404,
          details: null
        });

        return;
      }


      if (checkParticipantExistStatus.isUserExist == true) { // When User Exist in the conference // START 4th IF
        let updateWith = {
          "participants.$.connectionId": dialOpentokStatus.sipCall.connectionId,
          "participants.$.streamId": dialOpentokStatus.sipCall.streamId,
          "participants.$.isStarted": true,
          "participants.$.createdAt": dialOpentokStatus.sipCall.createdAt,
        }

        let updateWhere = {
          "sessionId": sessionId,
          "participants.contactNumber": toMobile.toString(),
        }

        // Here, we are inserting the the details of sip call
        let updateParticipantDetailStatus = await Sip_dialBusiness.updateParticipantDetail(updateWith, updateWhere);

        // The participant Detail was not Updated Successfully
        if (updateParticipantDetailStatus.ErrorStatus == true) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;
        }

        // The participant Detail was Updated Successfully
        if (updateParticipantDetailStatus.ErrorStatus == false) { // START 4th IF 
          // Here, we are fetching the the details of participant in a conference
          let aggrQuery = [
            {
              $match: {
                'sessionId': sessionId,
              }
            },
          ];

          let fetchParticipantDetailStatus = await Sip_dialBusiness.fetchParticipantDetail(aggrQuery);

          if (fetchParticipantDetailStatus.ErrorStatus == true) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            });

            return;
          }

          if (fetchParticipantDetailStatus.ErrorStatus == false) {

            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: fetchParticipantDetailStatus.response
            });

            return;
          }
        } // END 6th IF 
      } // END 4th IF  

      // When the user does not exist in the conference, Now we have to perform insert operation
      if (checkParticipantExistStatus.isUserExist == false) { // START 5th IF
        let contactNumber = toMobile;
        let userType = "DIALEDUSER";

        let insertquery = {
          "contactNumber": contactNumber,
          "userType": userType,
          "connectionId": dialOpentokStatus.sipCall.connectionId,
          "isStarted": true,
          "streamId": dialOpentokStatus.sipCall.streamId,
          "createdAt": dialOpentokStatus.sipCall.createdAt,
        }

        let addParticipantDetailStatus = await Sip_dialBusiness.addParticipantDetail(insertquery, sessionId);

        if (addParticipantDetailStatus.ErrorStatus == true) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;
        }

        // When Participant Added successfully  
        if (addParticipantDetailStatus.ErrorStatus == false) { // START 7th IF
          let aggrQuery = [
            {
              $match: {
                'sessionId': sessionId,
              }
            },
          ];

          let fetchParticipantDetailStatus = await Sip_dialBusiness.fetchParticipantDetail(aggrQuery);

          if (fetchParticipantDetailStatus.ErrorStatus == true) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            });

            return;
          }

          if (fetchParticipantDetailStatus.ErrorStatus == false) {

            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: fetchParticipantDetailStatus.response
            });

            return;
          }

        } // END 7th IF
      } // END 5th IF                                                 


    } catch (error) {
      console.log('fff');
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 404,
        details: null
      });
    }
  },

  //==================================================================================================
  /* 
      This function is written to end the phonic conference for 
      a particular participant
  */
  sipDialEnd: async (req, res, next) => {
    // exit;
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the Conference Id
      let conferenceId = req.body.conferenceId ? req.body.conferenceId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Conference Id",
      });

      // Here, we are receiving the receiver contact number
      let toMobile = req.body.toMobile ? req.body.toMobile : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Receiver Contact Number",
      });

      // Here, Circular json avoided
      if (!req.body.conferenceId || !req.body.toMobile) { return; }

      // start executing the operation for each number
      let checkParticipantInConferenceStatus = await Sip_dialBusiness.checkParticipantInConference(conferenceId, toMobile);

      if (checkParticipantInConferenceStatus.isError == true) { // When Error in fetching the participant
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          statuscode: 404,
          details: null
        })

        return;

      }

      if (checkParticipantInConferenceStatus.isParticipantExist == false) {
        res.json({
          isError: false,
          message: errorMsgJSON[lang]["204"],
          statuscode: 204,
          details: null
        })

        return;
      }

      let sessionId = checkParticipantInConferenceStatus.sessionId;
      let connectionId = checkParticipantInConferenceStatus.connectionId;

      // Here, we are disconnecting the call
      let disconnectCallStatus = await Sip_dialBusiness.disconnectCall(sessionId, connectionId);

      if (disconnectCallStatus.isError == true) { // Some error in disconnecting the call
        res.json({
          isError: true,
          message: "Either the call was already disconnected or some error occur",
          statuscode: 404,
          details: null
        })

        return;
      }

      // Here, we are update the updateAt status in sipcall connection
      let updateSipCallStatus = await Sip_dialBusiness.updateSipCall(sessionId, connectionId);

      if (updateSipCallStatus.isError == true) {
        res.json({
          isError: true,
          message: "Some error occur while updating the sip call",
          statuscode: 404,
          details: null
        })

        return;
      }

      res.json({
        isError: false,
        message: "call Disconnected",
        statuscode: 412,
        details: null
      })

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }

  },


  //=============================================================================================================================================
  //==================================================================================================
  /* 
      This function is written to end the phonic conference for 
      a particular participant
  */
  // sipDialEndForEndConf: async (conferenceId, toMobile) => {

  //     return new Promise(function(resolve, reject){
  //           // start executing the operation for each number
  //           let checkParticipantInConferenceStatus = await Sip_dialBusiness.checkParticipantInConference(conferenceId, toMobile);

  //           if (checkParticipantInConferenceStatus.isError == true) { // When Error in fetching the participant
  //               resolve({
  //                 "isError": true,
  //               });

  //               return;
  //           }

  //           if (checkParticipantInConferenceStatus.isParticipantExist == false) {
  //               resolve({
  //                 "isError": false,
  //                 "isParticipantExist": false
  //               });

  //               return;
  //           }

  //           let sessionId = checkParticipantInConferenceStatus.sessionId;
  //           let connectionId = checkParticipantInConferenceStatus.connectionId;

  //           // Here, we are disconnecting the call
  //           let disconnectCallStatus = await Sip_dialBusiness.disconnectCall(sessionId, connectionId);

  //           if (disconnectCallStatus.isError == true) { // Some error in disconnecting the call
  //               resolve({
  //                 "isError": true,
  //               });

  //               return;
  //           }

  //           // Here, we are update the updateAt status in sipcall connection
  //           let updateSipCallStatus = await Sip_dialBusiness.updateSipCall(sessionId, connectionId);

  //           if (updateSipCallStatus.isError == true) {
  //               resolve({
  //                 "isError": true,
  //               });

  //               return;
  //           }

  //           resolve({
  //             "isError": true,
  //             "isParticipantExist": false,
  //             "message": "Call Disconnected Successfully"
  //           });

  //     }) // END Promise        


  // },


  //============================================================



}