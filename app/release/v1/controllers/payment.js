const errorMsgJSON = require("../../../services/errors.json");
const paymentBusiness = require("../BusinessLogic/paymentBusiness");

const config = require("../config/globals/keyfile");

const paystack = require("paystack")(config.PAYSTACK.secretKey);
const stripe = require("stripe")(config.STRIPE.secretKey);


module.exports = {
  createCustomer: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";

    let fname = req.body.fname ? req.body.fname : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - First Name",
    });

    let lname = req.body.lname ? req.body.lname : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - Last Name",
    });

    let email = req.body.email ? req.body.email : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - email",
    });

    let userId = req.body.userId ? req.body.userId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - userId",
    });

    let companyId = req.body.companyId ? req.body.companyId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - companyId",
    });

    if (!req.body.fname || !req.body.lname || !req.body.email || !req.body.userId || !req.body.companyId) { return; }

    // create customer and save in db
    try {
      await paymentBusiness.createCustomerPaystack(fname, lname, email, userId, companyId);
    } catch (error) { }
    try {
      await paymentBusiness.createCustomerStripe(email, userId, companyId);
    } catch (error) { }
    res.json({
      isError: false,
      message: errorMsgJSON[lang]["200"],
      statuscode: 200,
      details: null
    });
  },

  stripePayment: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";

    let description = req.body.description ? req.body.description : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - description",
    });

    let source = req.body.source ? req.body.source : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - source",
    });

    let currency = req.body.currency ? req.body.currency : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - currency",
    });

    let amount = req.body.amount && !isNaN(req.body.amount) ? Number(req.body.amount) : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - amount",
    });

    let customerId = req.body.customerId ? req.body.customerId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - customerId",
    });

    let userId = req.body.userId ? req.body.userId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - userId",
    });

    let companyId = req.body.companyId ? req.body.companyId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - companyId",
    });

    let companyType = req.body.companyType ? req.body.companyType : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - companyType",
    });

    // in case of invoice referenceCode = invoiceId
    let referenceCode = req.body.referenceCode ? req.body.referenceCode : "";

    if (!req.body.description || !req.body.source || !req.body.currency || !req.body.amount || !req.body.customerId || !req.body.userId || !req.body.companyId || !req.body.companyType) { return; }

    try {
      let charge = await paymentBusiness.makeStripePayment(description, source, currency, amount, customerId);
      let transaction = await paymentBusiness.saveTransaction({ userId, companyId, companyType, referenceCode, amount, currency, paymentGateway: 'STRIPE', paymentGatewayData: charge, paymentMode: "ONLINE" })
      if (companyType == "ENTERPRISE") {
        await paymentBusiness.paidInvoice({ invoiceId: referenceCode, transactionId: transaction._id, paymentMode: "ONLINE" });// invoiceId
      } else {
        await paymentBusiness.recgargeWallet(companyId, amount);
      }

      res.json({
        isError: false,
        message: errorMsgJSON[lang]["200"],
        statuscode: 200,
        details: null
      });
    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["403"],
        statuscode: 200,
        details: null
      });
    }

  },

  paystackPayment: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";

    let description = req.body.description ? req.body.description : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - description",
    });

    let currency = req.body.currency ? req.body.currency : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - currency",
    });

    let amount = req.body.amount && !isNaN(req.body.amount) ? Number(req.body.amount) : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - amount",
    });

    let userId = req.body.userId ? req.body.userId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - userId",
    });

    let companyId = req.body.companyId ? req.body.companyId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - companyId",
    });

    let companyType = req.body.companyType ? req.body.companyType : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - companyType",
    });

    let referenceCode = req.body.referenceCode ? req.body.referenceCode : "";

    let paymentGatewayData = req.body.data ? req.body.data : {};

    if (!req.body.description || !req.body.currency || !req.body.amount || !req.body.userId || !req.body.companyId || !req.body.companyType || !req.body.referenceCode) { return; }

    try {
      let transaction = await paymentBusiness.saveTransaction({ userId, companyId, companyType, referenceCode, amount, currency, paymentGateway: 'PAYSTACK', paymentGatewayData, paymentMode: "ONLINE" })
      console.log('transaction === ', transaction);

      if (companyType == "ENTERPRISE") {
        await paymentBusiness.paidInvoice({ invoiceId: referenceCode, transactionId: transaction._id, paymentMode: "ONLINE" });// invoiceId
      } else {
        await paymentBusiness.recgargeWallet(companyId, amount);
      }
      res.json({
        isError: false,
        message: errorMsgJSON[lang]["200"],
        statuscode: 200,
        details: null
      });
    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["403"],
        statuscode: 200,
        details: null
      });
    }

  },

  paypalPayment: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";

    let description = req.body.description ? req.body.description : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - description",
    });

    let currency = req.body.currency ? req.body.currency : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - currency",
    });

    let amount = req.body.amount && !isNaN(req.body.amount) ? Number(req.body.amount) : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - amount",
    });

    let userId = req.body.userId ? req.body.userId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - userId",
    });

    let companyId = req.body.companyId ? req.body.companyId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - companyId",
    });

    let companyType = req.body.companyType ? req.body.companyType : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - companyType",
    });

    let referenceCode = req.body.referenceCode ? req.body.referenceCode : "";

    let paymentGatewayData = req.body.data ? req.body.data : {};

    if (!req.body.description || !req.body.currency || !req.body.amount || !req.body.userId || !req.body.companyId || !req.body.companyType || !req.body.referenceCode) { return; }

    try {
      let transaction = await paymentBusiness.saveTransaction({ userId, companyId, companyType, referenceCode, amount, currency, paymentGateway: 'PAYPAL', paymentGatewayData, paymentMode: "ONLINE" })

      if (companyType == "ENTERPRISE") {
        await paymentBusiness.paidInvoice({ invoiceId: referenceCode, transactionId: transaction._id, paymentMode: "ONLINE" });// invoiceId
      } else {
        await paymentBusiness.recgargeWallet(companyId, amount);
      }
      res.json({
        isError: false,
        message: errorMsgJSON[lang]["200"],
        statuscode: 200,
        details: null
      });
    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["403"],
        statuscode: 200,
        details: null
      });
    }

  },

  manualPayment: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";

    let description = req.body.description ? req.body.description : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - description",
    });

    let currency = req.body.currency ? req.body.currency : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - currency",
    });

    let amount = req.body.amount && !isNaN(req.body.amount) ? Number(req.body.amount) : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - amount",
    });

    let userId = req.body.userId ? req.body.userId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - userId",
    });

    let companyId = req.body.companyId ? req.body.companyId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - companyId",
    });

    let companyType = "ENTERPRISE";

    // in case of invoice referenceCode = invoiceId
    let referenceCode = req.body.referenceCode ? req.body.referenceCode : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - referenceCode",
    });

    if (!req.body.description || !req.body.currency || !req.body.amount || !req.body.userId || !req.body.companyId || !req.body.referenceCode) { return; }

    try {
      let transaction = await paymentBusiness.saveTransaction({ userId, companyId, companyType, referenceCode, amount, currency, description, paymentGateway: 'MANUAL', paymentGatewayData: {}, paymentMode: "OFFLINE" })
      if (companyType == "ENTERPRISE") {
        await paymentBusiness.paidInvoice({ invoiceId: referenceCode, transactionId: transaction._id, paymentMode: "OFFLINE" });// invoiceId
      } else {
        // await paymentBusiness.recgargeWallet(companyId, amount);
      }

      res.json({
        isError: false,
        message: errorMsgJSON[lang]["200"],
        statuscode: 200,
        details: null
      });
    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["403"],
        statuscode: 200,
        details: null
      });
    }

  }


}