//=================================================
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const Conference = require("../models/conference");
const Guest = require("../models/guest");
const User = require("../models/user");
const Invitation = require("../models/invitation");
const Invoice = require("../models/invoice");
const Transaction = require("../models/transaction");

const ConferenceDuration = require("../models/conferenceDuration");
// const bcrypt = require("bcrypt");
const SALT_WORK_FACTOR = 10;
const Mailercontroller = require("../../../services/mailer");

const errorMsgJSON = require("../../../services/errors.json");
const config = require("../config/globals/keyfile");
const AutogenerateIdcontroller = require('../../../services/AutogenerateId');
const Base64ImgUploadmodule = require('../../../services/Base64ImgUploadmodule');
const PriceManagementBusiness = require("../BusinessLogic/priceManagementBusiness");
const convertCurrency = require('nodejs-currency-converter');

var fileupload = require('express-fileupload');

const fs = require('fs');
const AWS = require('aws-sdk');


//=================================================
var OpenTok = require('opentok'),
  opentok = new OpenTok(config.tokbox.apiKey, config.tokbox.apiSecret);

//================================
// Import events module
var events = require('events');

//===========================================

module.exports = {

  /* 
      this function is written to fetch all details of call of conference
  */
  fetchCallUsageDetailOfConference: async (req, res, next) => {
      let lang = req.headers.language ? req.headers.language : "EN";
      try {
  
          let companyId = req.body.companyId ? req.body.companyId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Company Id"
          });

          // Here, we are getting the month and year
          let dt = new Date();
          let year = req.body.year ? req.body.year : dt.getFullYear();
          let months = [];
          let month = req.body.month ? req.body.month : dt.getMonth() + 1;
          
          months.push(month);
        
          // Here, Circular json avoided
          if (!req.body.companyId) { return; }

          // Here, We are going to fetch the all the services of a particular company
          let fetchCompanyDetailStatus = await PriceManagementBusiness.fetchCompanyDetail(companyId);

        
          if (fetchCompanyDetailStatus.isError == true) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["404"],
                statuscode: 404,
                details: null
              });

              return;
          }

          if (fetchCompanyDetailStatus.iscompanyExist == false) {
              res.json({
                isError: true,
                message: "Company Does not exist",
                statuscode: 204,
                details: null
              });

              return;
          }

          let currencyTo = fetchCompanyDetailStatus.companyDetail[0].currency;
          let vatRate = fetchCompanyDetailStatus.companyDetail[0].vat;

          // Here, We are going to fetch the currency rate with respect to USD
          let getExchangeRateStatus = await PriceManagementBusiness.getExchangeRate(currencyTo);

          

          if (fetchCompanyDetailStatus.isError == true) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["404"],
                statuscode: 404,
                details: null
              });

              return;
          }



          let str = getExchangeRateStatus.response;
          let obj = JSON.parse(str);
          let exchangeRate;

          if (currencyTo == "NGN") {
              exchangeRate = obj.quotes['USDNGN'];
              exchangeRate = parseFloat(exchangeRate).toFixed(2);
          }

          if (currencyTo != "NGN") {
              exchangeRate = obj.rates[currencyTo];
              exchangeRate = parseFloat(exchangeRate).toFixed(2);
          }

          let costInUSD = 0;
          let realCost = 0;


          let serviceList = fetchCompanyDetailStatus.companyDetail[0].services;
         
          let  DIALIN_COST = 0;
          let  DIALOUT_COST = 0;
          let  WEB_COST = 0;
          let  ARCHIVE_COST = 0;

          for (tag1 = 0; tag1 < serviceList.length; tag1++) {
              
              if (serviceList[tag1]['serviceKey'] == 'DIALIN') {
                  DIALIN_COST = serviceList[tag1]['costPerMinute'];
              }

              if (serviceList[tag1]['serviceKey'] == 'DIALOUT') {
                  DIALOUT_COST = serviceList[tag1]['costPerMinute'];
              }

              if (serviceList[tag1]['serviceKey'] == 'WEB') {
                  WEB_COST = serviceList[tag1]['costPerMinute'];
              }

              if (serviceList[tag1]['serviceKey'] == 'ARCHIVE') {
                  ARCHIVE_COST = serviceList[tag1]['costPerMinute'];
              }
          } // END for (tag1 = 0; tag1 < serviceList.length; tag1++) {

         
          let humanReadableCompanyId = fetchCompanyDetailStatus.companyDetail[0].companyId;

          // Here, We are going to fetch the all the services of a particular company
          let fetchAllSipCallDetailForDetailStatus = await PriceManagementBusiness.fetchAllSipCallDetailForDetail(humanReadableCompanyId, months, year);

          if (fetchAllSipCallDetailForDetailStatus.isError == true) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["404"],
                statuscode: 404,
                details: null
              });

              return;
          }

          let differenceInTimeForConf;
          let differenceInMinutesForConf;
          let desccriptionForMonthForConf;
          let createdAtForConf;


          let totalSipCalDuration = 0;
          let totalSipCalCost = 0;
          let totalSipCall = 0;
          let sipCallList = [];

          if (fetchAllSipCallDetailForDetailStatus.isConfExist == true) {
             
              sipCallList = fetchAllSipCallDetailForDetailStatus.conferenceDetail;
              

              let differenceInTimeForSipCall;
              let differenceInMinutesForSipCall;
              let desccriptionForMonthForSipCall;
              let updatedAt;
              

              for (tag1 = 0; tag1 < sipCallList.length; tag1++) {

                  createdAtForConf = Date.parse(sipCallList[tag1].confDuration.createdAt);
                  differenceInTimeForConf = parseInt(sipCallList[tag1].confDuration.endedAt) - parseInt(createdAtForConf);
                  sipCallList[tag1].confDuration.duration = differenceInTimeForConf / (1000 * 60);
         
                  updatedAt = Date.parse(sipCallList[tag1].sipCallDetail.updatedAt);
                  differenceInTimeForSipCall = parseInt(updatedAt) - parseInt(sipCallList[tag1].sipCallDetail.createdAt);
                  sipCallList[tag1].sipCallDetail.duration = differenceInTimeForSipCall / (1000 * 60);
                  costInUSD = (differenceInTimeForSipCall / (1000 * 60)) * DIALOUT_COST;
                  realCost = costInUSD * exchangeRate;
                  realCost = parseFloat(realCost).toFixed(2);

                  // sipCallList[tag1].sipCallDetail.Cost = (differenceInTimeForSipCall / (1000 * 60)) * DIALOUT_COST;
                  sipCallList[tag1].sipCallDetail.costPerMinute = DIALOUT_COST * exchangeRate;
                  sipCallList[tag1].sipCallDetail.Cost = realCost;

                  totalSipCalDuration = totalSipCalDuration + sipCallList[tag1].sipCallDetail.duration;
                  totalSipCalCost = totalSipCalCost + parseFloat(sipCallList[tag1].sipCallDetail.Cost);
                  totalSipCall = totalSipCall + 1;
                 
              } // END for (tag1 = 0; tag1 < sipCallList.length; tag1++) {
              
          } // END if (fetchAllSipCallDetailForDetailStatus.isConfExist == true) {

          // Here, We are going to fetch the all the WebCall of a particular company
          let fetchAllWebCallDetailForDetailStatus = await PriceManagementBusiness.fetchAllWebCallDetailForDetail(humanReadableCompanyId, months, year);

          if (fetchAllWebCallDetailForDetailStatus.isError == true) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["404"],
                statuscode: 404,
                details: null
              });

              return;
          }

          let totalWebCalDuration = 0;
          let totalWebCalCost = 0; 
          let totalWebCall = 0; 
          let webCallList = [];

          if (fetchAllWebCallDetailForDetailStatus.isConfExist == true) {

             
              webCallList = fetchAllWebCallDetailForDetailStatus.conferenceDetail;
             
              let differenceInTimeForWebCall;
              let differenceInMinutesForWebCall;
              let createdAtForWebCall;
              let updatedAtForWebCall;

              for (tag1 = 0; tag1 < webCallList.length; tag1++) {
   
                  createdAtForConf = Date.parse(webCallList[tag1].confDuration.createdAt); 
                  differenceInTimeForConf = parseInt(webCallList[tag1].confDuration.endedAt) - parseInt(createdAtForConf);
                  webCallList[tag1].confDuration.duration = differenceInTimeForConf / (1000 * 60);
                  
                  createdAtForWebCall = Date.parse(webCallList[tag1].webCallDetail.createdAt);
                  updatedAtForWebCall = Date.parse(webCallList[tag1].webCallDetail.updatedAt);
                  differenceInTimeForWebCall = parseInt(updatedAtForWebCall) - parseInt(createdAtForWebCall);
                  webCallList[tag1].webCallDetail.duration = differenceInTimeForWebCall / (1000 * 60);

                  costInUSD = (differenceInTimeForWebCall / (1000 * 60)) * WEB_COST;
                  realCost = costInUSD * exchangeRate;
                  realCost = parseFloat(realCost).toFixed(2);

                  webCallList[tag1].webCallDetail.costPerMinute = WEB_COST * exchangeRate;
                  webCallList[tag1].webCallDetail.cost = realCost;


                  totalWebCalDuration = totalWebCalDuration + webCallList[tag1].webCallDetail.duration;
                  totalWebCalCost = totalWebCalCost + parseFloat(webCallList[tag1].webCallDetail.cost);
                  totalWebCall = totalWebCall + 1;
         
              } // END for (tag1 = 0; tag1 < sipCallList.length; tag1++) {

          } // END if (fetchAllWebCallDetailForDetailStatus.isConfExist == true) {  

          // Here, We are going to fetch the all the WebCall of a particular company
          let fetchAllArchiveDetailForDetailStatus = await PriceManagementBusiness.fetchAllArchiveDetailForDetail(humanReadableCompanyId, months, year);

          if (fetchAllArchiveDetailForDetailStatus.isError == true) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["404"],
                statuscode: 404,
                details: null
              });

              return;
          } 

          let totalArchiveDuration = 0;
          let totalArchiveCost = 0; 
          let totalArchive = 0;
          let archiveList = [];

          if (fetchAllArchiveDetailForDetailStatus.isConfExist == true) {

             
              archiveList = fetchAllArchiveDetailForDetailStatus.conferenceDetail;

              for (tag1 = 0; tag1 < archiveList.length; tag1++) {
   
                  createdAtForConf = Date.parse(archiveList[tag1].confDuration.createdAt); 
                  differenceInTimeForConf = parseInt(archiveList[tag1].confDuration.endedAt) - parseInt(createdAtForConf);
                  archiveList[tag1].confDuration.duration = differenceInTimeForConf / (1000 * 60);

                  costInUSD = archiveList[tag1].archivesDetail.duration * ARCHIVE_COST;
                  realCost = costInUSD * exchangeRate;
                  realCost = parseFloat(realCost).toFixed(2);

                  archiveList[tag1].archivesDetail.costPerMinute = ARCHIVE_COST * exchangeRate;
                  archiveList[tag1].archivesDetail.cost = realCost;

                  totalArchiveDuration = totalArchiveDuration + archiveList[tag1].archivesDetail.duration;
                  totalArchiveCost = totalArchiveCost + parseFloat(archiveList[tag1].archivesDetail.cost);
                  totalArchive = totalArchive + 1;
                               
              } // END for (tag1 = 0; tag1 < sipCallList.length; tag1++) {

          } // END if (fetchAllWebCallDetailForDetailStatus.isConfExist == true) {  

          // Here, we are going to get total conference 
          let fetchAllConferenceStatus = await PriceManagementBusiness.fetchAllConference(humanReadableCompanyId, months, year);

          if (fetchAllConferenceStatus.isError == true) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["404"],
                statuscode: 404,
                details: null
              });

              return;
          }

          let noOfConference = fetchAllConferenceStatus.conferenceDetail.length;

          // Here, we are going to get total conference Duration
          let fetchAllConferenceDurationStatus = await PriceManagementBusiness.fetchAllConferenceDuration(humanReadableCompanyId, months, year);

          if (fetchAllConferenceDurationStatus.isError == true) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["404"],
                statuscode: 404,
                details: null
              });

              return;
          }

          let noOfConferenceDuration = fetchAllConferenceDurationStatus.conferenceDurationDetail.length;
          let totalCostExcludingVat = totalArchiveCost + totalWebCalCost + totalSipCalCost;

          let vatAmt = totalCostExcludingVat * vatRate /100;
          let totalCostIncludingVat = totalCostExcludingVat + vatAmt;
          

          res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: {
                  "archiveList": archiveList,
                  "totalArchive": totalArchive,
                  "totalArchiveDuration": totalArchiveDuration,
                  "totalArchiveCost": totalArchiveCost,
                  "webCallList": webCallList,
                  "totalWebCall":totalWebCall,
                  "totalWebCalDuration":totalWebCalDuration,
                  "totalWebCalCost": totalWebCalCost,
                  "sipCallList": sipCallList,
                  "totalSipCall": totalSipCall,
                  "totalSipCalDuration": totalSipCalDuration,
                  "totalSipCalCost":totalSipCalCost,
                  "noOfConference": noOfConference,
                  "noOfConferenceDuration": noOfConferenceDuration,
                  "totalCostExcludingVat": totalCostExcludingVat,
                  "vatRate": vatRate,
                  "vatAmt" : vatAmt,
                  "totalCostIncludingVat": totalCostIncludingVat


              }
          }); 
           

      } catch (error) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              message: "Error",
              statuscode: 404,
              details: null
            });
      } 

  },    

  /*
      This function is written to fetch details of each conference 
  */
  fetchConferenceActivityDetails: async (req, res, next) => {

      let lang = req.headers.language ? req.headers.language : "EN";
      try {
          let companyId = req.body.companyId ? req.body.companyId : res.json({
                isError: true,
                message: errorMsgJSON['ResponseMsg']['303'] + " - Company Id"
          });

          // Here, we are getting the month and year
          let dt = new Date();
          let year = req.body.year ? req.body.year : dt.getFullYear();
          let months = [];
          let month = req.body.month ? req.body.month : dt.getMonth() + 1;
          
          months.push(month);

          // Here, Circular json avoided
          if (!req.body.companyId) { return; }

          // let currentMonth = dt.getMonth() + 1;

          // if (month >= currentMonth) {
          //     res.json({
          //       isError: true,
          //       message: "Given month must be lower than current month",
          //       statuscode: 404,
          //       details: null
          //     });

          //     return;
          // }
     
          // Here, We are going to fetch the all the services of a particular company
          let fetchCompanyDetailStatus = await PriceManagementBusiness.fetchCompanyDetail(companyId);

          if (fetchCompanyDetailStatus.isError == true) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["404"],
                statuscode: 404,
                details: null
              });

              return;
          }

          if (fetchCompanyDetailStatus.iscompanyExist == false) {
              res.json({
                isError: true,
                message: "Company Does not exist",
                statuscode: 204,
                details: null
              });

              return;
          }

          let serviceList = fetchCompanyDetailStatus.companyDetail[0].services;

          let tag1;
          let  DIALIN_COST = 0;
          let  DIALOUT_COST = 0;
          let  WEB_COST = 0;
          let  ARCHIVE_COST = 0;

          for (tag1 = 0; tag1 < serviceList.length; tag1++) {
              
              if (serviceList[tag1]['serviceKey'] == 'DIALIN') {
                  DIALIN_COST = serviceList[tag1]['costPerMinute'];
              }

              if (serviceList[tag1]['serviceKey'] == 'DIALOUT') {
                  DIALOUT_COST = serviceList[tag1]['costPerMinute'];
              }

              if (serviceList[tag1]['serviceKey'] == 'WEB') {
                  WEB_COST = serviceList[tag1]['costPerMinute'];
              }

              if (serviceList[tag1]['serviceKey'] == 'ARCHIVE') {
                  ARCHIVE_COST = serviceList[tag1]['costPerMinute'];
              }
          } // END for (tag1 = 0; tag1 < serviceList.length; tag1++) {

          // Here, We are going to fetch the of all Sip Call againest a particular company
          let humanReadableCompanyId = fetchCompanyDetailStatus.companyDetail[0].companyId;
          let fetchSipCallForCompanyOrderByConfDurationStatus = await PriceManagementBusiness.fetchSipCallForCompanyOrderByConfDuration(humanReadableCompanyId, months, year);
          
          if (fetchSipCallForCompanyOrderByConfDurationStatus.isError == true) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["404"],
                statuscode: 404,
                details: null
              });

              return;
          }

          let sipCallList;

          if (fetchSipCallForCompanyOrderByConfDurationStatus.isConfDurationExist == false) {
              sipCallList = "No SipCall Exist";
          } else {
              sipCallList = fetchSipCallForCompanyOrderByConfDurationStatus.sipCallList;

              let differenceInTimeForSipCall;
              let differenceInMinutesForSipCall;
              let desccriptionForMonthForSipCall;
              let updatedAt;

              for (tag2 = 0; tag2 < sipCallList.length; tag2++) {
                  
                  updatedAt = Date.parse(sipCallList[tag2].updatedAt);
                  differenceInTimeForSipCall = parseInt(updatedAt) - parseInt(sipCallList[tag2].createdAt);
                  
                  sipCallList[tag2].duration = differenceInTimeForSipCall / (1000 * 60);
                  sipCallList[tag2].callCost = (differenceInTimeForSipCall / (1000 * 60))*DIALIN_COST;
                  sipCallList[tag2].conferenceId = sipCallList[tag2].sipCallDetail[0]._id;
                  delete sipCallList[tag2].sipCallDetail;
              } // END for (tag2 = 0; tag2 < sipCallList.length; tag2++) {
          }

  
          // Here, We are fetching all Web Call againest a particular company
          let fetchWebCallForCompanyOrderByConfStatus = await PriceManagementBusiness.fetchWebCallForCompanyOrderByConf(humanReadableCompanyId, months, year);  

          if (fetchWebCallForCompanyOrderByConfStatus.isError == true) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["404"],
                statuscode: 404,
                details: null
              });

              return;
          }

          let WebCallList;

          if (fetchWebCallForCompanyOrderByConfStatus.isConfDurationExist == false) {
              WebCallList = "No WebCall List";
          } else {
              WebCallList = fetchWebCallForCompanyOrderByConfStatus.webCallList;

              let differenceInTimeForWebCall;
              let totalParticipantInConf = 1;
              let prevTag = 0;
              let arrtotalParticipantInConf = [];
              let objParticipantInConf = {};
              let conferenceDurationIdNow; 

              for (tag3 = 0; tag3 < WebCallList.length; tag3++) {
                  if (tag3 != 0) {
                      prevTag = tag3 - 1;
                  }
                  if ((WebCallList[prevTag].conferenceDurationId == WebCallList[tag3].conferenceDurationId) && (tag3 != 0)) {
                      totalParticipantInConf++;
                      
                      conferenceDurationIdNow = WebCallList[tag3].conferenceDurationId;
                      objParticipantInConf = {
                         "conferenceDurationId": WebCallList[tag3].conferenceDurationId,
                         "totalParticipantInConf": totalParticipantInConf
                      }
                      arrtotalParticipantInConf.pop();
                      arrtotalParticipantInConf.push(objParticipantInConf);   
                     


                  } else {
                      totalParticipantInConf = 1;
                      conferenceDurationIdNow = WebCallList[tag3].conferenceDurationId;
                      objParticipantInConf = {
                         "conferenceDurationId": WebCallList[tag3].conferenceDurationId,
                         "totalParticipantInConf": totalParticipantInConf
                      }

                      arrtotalParticipantInConf.push(objParticipantInConf);
                    
                  }

              } // END for (tag2 = 0; tag2 < sipCallList.length; tag2++) {

              for (tag4 = 0; tag4 < WebCallList.length; tag4++) {
                  differenceInTimeForWebCall = parseInt(WebCallList[tag4].endedAt) - parseInt(WebCallList[tag4].startedAt);
                  WebCallList[tag4].duration = differenceInTimeForWebCall / (1000 * 60);
              } // END for (tag2 = 0; tag2 < sipCallList.length; tag2++) {

                console.log(arrtotalParticipantInConf);
                res.json({
                   arrtotalParticipantInConf: arrtotalParticipantInConf
                });
          } // END if (fetchWebCallForCompanyOrderByConfStatus.isConfDurationExist == false) {

        
          res.json({
              // sipCallList: sipCallList,
             
              fetchWebCallForCompanyOrderByConfStatus: fetchWebCallForCompanyOrderByConfStatus.webCallList,
              year: year,
              month: month,
              DIALIN_COST: DIALIN_COST,
              DIALOUT_COST: DIALOUT_COST,
              WEB_COST: WEB_COST,
              ARCHIVE_COST: ARCHIVE_COST,
              humanReadableCompanyId: humanReadableCompanyId,
              // arrtotalParticipantInConf: arrtotalParticipantInConf

          });

         

         
          /*
           

          */





          



          //return;

      } catch (error) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          message: "Error",
          statuscode: 404,
          details: null
        });
      } 

  },


  /* 
      This function is written to get transaction list on the basis of 
      company id
  */

  getTransactionListByCompanyId: (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      let companyId = req.body.companyId ? req.body.companyId : res.json({
        isError: true,
        message: errorMsgJSON['ResponseMsg']['303'] + " - Company Id"
      });

      let page = req.body.page ? (parseInt(req.body.page == 0 ? 0 : parseInt(req.body.page) - 1)) : 0;
      let perPage = req.body.perPage ? parseInt(req.body.perPage) : 10;

      // Here, Circular json avoided
      if (!req.body.companyId) { return; }

      let aggrQuery = [
        { $sort: { _id: -1 } },
        {
          '$match': {
            'companyId': companyId,
          }
        },

        {
          $group: {
            _id: null,
            total: {
              $sum: 1
            },
            results: {
              $push: '$$ROOT'
            }
          }
        },
        {
          $project: {
            '_id': 0,
            'total': 1,
            'noofpage': { $ceil: { $divide: ["$total", perPage] } },
            'results': {
              $slice: [
                '$results', page * perPage, perPage
              ]
            }
          }
        }

      ];


      Transaction.aggregate(aggrQuery, function (err, response) {

        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;
        }

        if (response.length < 1) {
          res.json({
            isError: false,
            message: "Data Not Found",
            statuscode: 204,
            details: null
          });

          return;
        } else {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: response
          });

          return;
        }


      })

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }

  },

  /*
      This function is written to edit the invoice for a particular company
      on the basis of invoice Id
  */
  editInvoiceById: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the companyId 
      let invoiceId = req.body.invoiceId ? req.body.invoiceId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " -Invoice Id",
      });

      // Here, we are receiving the services 
      let services = req.body.services ? req.body.services : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " -Services Id",
      });

      let isPublished = typeof req.body.isPublished === "boolean" ? req.body.isPublished : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " -isPublished",
      });

      // Here, Circular json avoided
      if (!req.body.invoiceId || !req.body.services || !(typeof req.body.isPublished === "boolean")) { return; }

      let updateWhere = {
        '_id': invoiceId
      }

      Invoice.update(
        updateWhere,
        { $set: { services: services, isPublished: isPublished } },
        function (err, response) {

          if (err) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            });
          }
          else {
            if (response.nModified == 1) {
              res.json({
                isError: false,
                message: errorMsgJSON[lang]["200"],
                statuscode: 200,
                details: null
              });
            } else {
              res.json({
                isError: false,
                message: "Some problem occur during update",
                statuscode: 204,
                details: null
              });
            }
          }
        }) // END  Invoice.update(


    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },


  /*
     This function is written to fetch all the invoice for a particular company
     on the basis of year
 */
  fetchInvoiceById: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the companyId 
      let invoiceId = req.body.invoiceId ? req.body.invoiceId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " -Invoice Id",
      });

      // Here, Circular json avoided
      if (!req.body.invoiceId) { return; }

      let aggrQuery = [
        { $match: { '_id': mongoose.Types.ObjectId(invoiceId) } },
      ];

      Invoice.aggregate(aggrQuery, function (err, response) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
        } else {
          if (response.length < 1) {
            res.json({
              isError: false,
              message: 'No Data Found',
              statuscode: 204,
              details: null
            });
          } else {
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: response
            });
          }
        }
      });

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  sendInvoiceByMail: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the companyId 
      let invoiceId = req.body.invoiceId ? req.body.invoiceId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " -Invoice Id",
      });

      // Here, Circular json avoided
      if (!req.body.invoiceId) { return; }

      var otpMailDetails = {
        receiver: 'subhendu.sett@pkweb.in',
        subject: "Test",
        message: "PFTA",
        attachments: [
          {   // use URL as an attachment
            filename: 'invioce_0001.pdf',
            path: 'https://transactionpdfs.s3.us-east-2.amazonaws.com/invioce_0001.pdf'
          }
        ],
      };

      Mailercontroller.viaGmail(otpMailDetails, (err, data) => {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            message: "Error",
            statuscode: 404,
            details: null
          });
        } else {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: null
          });
        }
      })

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },



  /*
      This function is written to fetch all the invoice for a particular company
      on the basis of year
  */
  fetchInvoiceByCompanyId: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the companyId 
      let companyId = req.body.companyId ? req.body.companyId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " -Company Id",
      });

      // Here, we are getting the month and year
      let dt = new Date();
      let year = req.body.year ? req.body.year : dt.getFullYear();

      // Here, Circular json avoided
      if (!req.body.companyId) { return; }

      let aggrQuery = [
        {
          $match: {
            'companyId': companyId,
            'year': year.toString(),
          },
        },
      ];

      Invoice.aggregate(aggrQuery, function (err, response) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
        } else {
          if (response.length < 1) {
            res.json({
              isError: false,
              message: 'No Data Found',
              statuscode: 204,
              details: null
            });
          } else {
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: response
            });
          }
        }
      });

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },


  /* 
      This api is written to generate invoice for a particular company for the service 
      used by it for a particular month
  */
  createInvoice: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the companyId 
      let companyId = req.body.companyId ? req.body.companyId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " -Company Id",
      });

      // Here, we are getting the month and year
      let dt = new Date();
      let year = req.body.year ? req.body.year : dt.getFullYear();
      let months = [];
      let month = req.body.month ? req.body.month : dt.getMonth() + 1;

      // Here, Circular json avoided
      if (!req.body.companyId || !req.body.month || !req.body.year) { return; }

      let currentMonth = dt.getMonth() + 1;

      if (month >= currentMonth) {
        res.json({
          isError: true,
          message: "Given month must be lower than current month",
          statuscode: 404,
          details: null
        });

        return;

      }

      months.push(month);

      // Here, We are going to fetch the all the services of a particular company
      let fetchCompanyDetailStatus = await PriceManagementBusiness.fetchCompanyDetail(companyId);

      if (fetchCompanyDetailStatus.isError == true) {
        res.json({
          isError: true,
          message: "Some Error Occur While Fetching The details of services of a company",
          statuscode: 404,
          details: null
        });

        return;
      }


      if (fetchCompanyDetailStatus.iscompanyExist == false || fetchCompanyDetailStatus.companyDetail[0].services.length < 1) {
        res.json({
          isError: true,
          message: "Either the company is not valid or service details not exist",
          statuscode: 204,
          details: null
        });

        return;
      }

      let dialInService = [];
      let dialOutService = [];
      let webService = [];
      let archiveService = [];
      let serviceDetail = [];

      let services = fetchCompanyDetailStatus.companyDetail[0].services;

      let k;
      for (k = 0; k < services.length; k++) {

        if (services[k].serviceKey == 'DIALIN') {
          dialInService = services[k];
        }

        if (services[k].serviceKey == 'DIALOUT') {
          dialOutService = services[k];
        }

        if (services[k].serviceKey == 'WEB') {
          webService = services[k];
        }

        if (services[k].serviceKey == 'ARCHIVE') {
          archiveService = services[k];
        }
      }

      // Here, We are fetching all Web Call againest a particular company
      let fetchWebCallForCompanyStatus = await PriceManagementBusiness.fetchWebCallForCompany(companyId, months, year);

      if (fetchWebCallForCompanyStatus.isError == true) {
        res.json({
          isError: true,
          message: "Some Error Occur While Fetching The details of Web Call",
          statuscode: 404,
          details: null
        });

        return;
      }

      let conferenceDurationsForWebCall = fetchWebCallForCompanyStatus.confDuration;
      let differenceInTimeForWebCall;
      let differenceInMinutesForWebCall;
      let desccriptionForMonthForWebCall;

      let i; let j;
      if (fetchWebCallForCompanyStatus.isConfDurationExist == true) {
        let totalMinutesForWebCall = 0;
        for (i = 0; i < conferenceDurationsForWebCall.length; i++) {
          differenceInTimeForWebCall = parseInt(conferenceDurationsForWebCall[i].endedAt) - parseInt(conferenceDurationsForWebCall[i].startedAt);
          // calculating the number of Minutes  
          differenceInMinutesForWebCall = differenceInTimeForWebCall / (1000 * 60);
          totalMinutesForWebCall = totalMinutesForWebCall + differenceInMinutesForWebCall;
        } // END  for (i = 0; i < conferenceDurations.length; i++) {

        desccriptionForMonthForWebCall = {
          "serviceId": webService._id,
          "serviceKey": webService.serviceKey,
          "serviceName": webService.serviceName,
          "pricePerMinute": webService.pricePerMinute,
          "totalDuration": totalMinutesForWebCall,
          "totalAmount": totalMinutesForWebCall * webService.pricePerMinute
        }

        serviceDetail.push(desccriptionForMonthForWebCall);

      } // END if (fetchWebCallForCompanyStatus.isConfDurationExist == true) {         
      //====================================================================================================================
      // Here, We are going to fetch the of all Sip Call againest a particular company
      let fetchSipCallForCompanyStatus = await PriceManagementBusiness.fetchSipCallForCompany(companyId, months, year);

      if (fetchSipCallForCompanyStatus.isError == true) {
        res.json({
          isError: true,
          message: "Some Error Occur While Fetching The details of Sip Call",
          statuscode: 404,
          details: null
        });

        return;
      }

      let conferenceDurationsForSipCall = fetchSipCallForCompanyStatus.confDuration;
      let differenceInTimeForSipCall;
      let differenceInMinutesForSipCall;
      let desccriptionForMonthForSipCall;
      let updatedAt;

      if (fetchSipCallForCompanyStatus.isConfDurationExist == true) {
        let totalMinutesForSipCall = 0;
        for (i = 0; i < conferenceDurationsForSipCall.length; i++) {
          updatedAt = Date.parse(conferenceDurationsForSipCall[i].updatedAt);
          differenceInTimeForSipCall = parseInt(updatedAt) - parseInt(conferenceDurationsForSipCall[i].createdAt);
          // calculating the number of Minutes  
          differenceInTimeForSipCall = differenceInTimeForSipCall / (1000 * 60);
          totalMinutesForSipCall = totalMinutesForSipCall + differenceInTimeForSipCall;
        } // END  for (i = 0; i < conferenceDurations.length; i++) {

        desccriptionForMonthForSipCall = {
          "serviceId": dialOutService._id,
          "serviceKey": dialOutService.serviceKey,
          "serviceName": dialOutService.serviceName,
          "pricePerMinute": dialOutService.pricePerMinute,
          "totalDuration": totalMinutesForSipCall,
          "totalAmount": totalMinutesForSipCall * dialOutService.pricePerMinute
        }

        serviceDetail.push(desccriptionForMonthForSipCall);

      } // END if (fetchSipCallForCompanyStatus.isConfDurationExist == true) {  

      // Here, We are going to fetch the of all Archives againest a particular company
      let fetchArchivesForCompanyStatus = await PriceManagementBusiness.fetchArchivesForCompany(companyId, months, year);

      if (fetchArchivesForCompanyStatus.isError == true) {
        res.json({
          isError: true,
          message: "Some Error Occur While Fetching The details of Archives",
          statuscode: 404,
          details: null
        });

        return;
      }


      let conferenceDurationsForArchives = fetchArchivesForCompanyStatus.confDuration;
      let differenceInTimeForArchives;
      let minuteDescriptionForArchives = [];
      let desccriptionForMonthForArchives;

      if (fetchArchivesForCompanyStatus.isConfDurationExist == true) {
        let totalMinutesForArchives = 0;
        for (i = 0; i < conferenceDurationsForArchives.length; i++) {
          // calculating the number of Minutes  
          totalMinutesForArchives = totalMinutesForArchives + conferenceDurationsForArchives[i].duration;
        } // END  for (i = 0; i < conferenceDurations.length; i++) {

        desccriptionForMonthForArchives = {
          "serviceId": archiveService._id,
          "serviceKey": archiveService.serviceKey,
          "serviceName": archiveService.serviceName,
          "pricePerMinute": archiveService.pricePerMinute,
          "totalDuration": totalMinutesForArchives,
          "totalAmount": totalMinutesForArchives * archiveService.pricePerMinute
        }

        serviceDetail.push(desccriptionForMonthForArchives);
      } // END if (fetchArchivesForCompanyStatus.isConfDurationExist == true) { 

      // Here We are going to check whether the invoice exist or not.
      let fetchInvoiceDetailStatus = await PriceManagementBusiness.fetchInvoiceDetail(companyId, month, year);


      if (fetchInvoiceDetailStatus.isError == true) {
        res.json({
          isError: true,
          message: "Some Error Occur While Fetching the invoice detail",
          statuscode: 404,
          details: null
        });

        return;
      }


      if (fetchInvoiceDetailStatus.isInvoicedExist == true) {
        res.json({
          isError: false,
          message: "The Invoice was already generated",
          statuscode: 204,
          details: null
        });

        return;
      }

      // Here We are going to insert the invoice.
      let insertInvoiceDetailStatus = await PriceManagementBusiness.insertInvoiceDetail(companyId, month, year, serviceDetail);

      if (insertInvoiceDetailStatus.isError == true) {
        res.json({
          isError: true,
          message: "Some Error Occur While Fetching The details of Archives",
          statuscode: 404,
          details: null
        });

        return;
      }


      res.json({
        isError: false,
        message: errorMsgJSON[lang]["200"],
        statuscode: 200,
        details: insertInvoiceDetailStatus.response
      });
      return;

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
      This function is used to get all the list of the api on user type
  */

  calculatePriceForConference: async (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";

    try {

      // Here, we are receiving the conferenceId 
      let conferenceId = req.body.conferenceId ? req.body.conferenceId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " -Conference Id",
      });

      // Here, Circular json avoided
      if (!req.body.conferenceId) { return; }

      // Here, We are fetch the details of each duration of the conference
      let fetchConferenceDurationStatus = await PriceManagementBusiness.fetchConferenceDuration(conferenceId);

      if (fetchConferenceDurationStatus.isError == true) { // When Error Occur
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          statuscode: 404,
          details: null
        });

        return;
      }

      if (fetchConferenceDurationStatus.isConfDurationExist == false) { // When No duration exist
        res.json({
          isError: true,
          message: 'No Conference Duration exist',
          statuscode: 204,
          details: null
        });

        return;
      }


      let conferenceDurations = fetchConferenceDurationStatus.confDuration;
      let differenceInTime;
      let differenceInDays;
      let differenceInHours;
      let differenceInMinutes;
      let differenceInSeconds;

      let totalDays = 0;
      let totalHours = 0;
      let totalMinutes = 0;
      let totalSeconds = 0;


      let i;
      for (i = 0; i < conferenceDurations.length; i++) {
        differenceInTime = parseInt(conferenceDurations[i].endedAt) - parseInt(conferenceDurations[i].startedAt);

        // calculating the number of days  
        differenceInDays = differenceInTime / (1000 * 3600 * 24);
        totalDays = totalDays + differenceInDays;

        // calculating the number of Hours  
        differenceInHours = differenceInTime / (1000 * 3600);
        totalHours = totalHours + differenceInHours;

        // calculating the number of Minutes  
        differenceInMinutes = differenceInTime / (1000 * 60);
        totalMinutes = totalMinutes + differenceInMinutes;

        // calculating the number of Seconds  
        differenceInSeconds = differenceInTime / (1000);
        totalSeconds = totalSeconds + differenceInSeconds;
        console.log()
      }

      res.json({
        isError: false,
        message: errorMsgJSON[lang]["200"],
        statuscode: 200,
        details: {
          totalDays: totalDays,
          totalHours: totalHours,
          totalMinutes: totalMinutes,
          totalSeconds: totalSeconds
        }
      });

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }

  },


  /*
      This function is used to calculate the price for mobile
  */

  calculatePriceForCompany: async (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";

    try {

      // Here, we are receiving the conferenceId 
      let companyId = req.body.companyId ? req.body.companyId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " -Company Id",
      });

      let dt = new Date();
      let year = req.body.year ? req.body.year : dt.getFullYear();
      let months = [];
      let currentMonth = req.body.month ? req.body.month : dt.getMonth() + 1;

      let k;

      for (k = 1; k <= currentMonth; k++) {
        months.push(k);
      }

      // Here, Circular json avoided
      if (!req.body.companyId) { return; }

      // Here, We are fetching all Web Call againest a particular company
      let fetchWebCallForCompanyStatus = await PriceManagementBusiness.fetchWebCallForCompany(companyId, months, year);

      if (fetchWebCallForCompanyStatus.isError == true) {
        res.json({
          isError: true,
          message: "Some Error Occur While Fetching The details of Web Call",
          statuscode: 404,
          details: null
        });

        return;
      }

      let conferenceDurationsForWebCall = fetchWebCallForCompanyStatus.confDuration;
      let differenceInTimeForWebCall;
      let differenceInMinutesForWebCall;
      let minuteDescriptionForWebCall = [];
      let desccriptionForMonthForWebCall;

      let i; let j;
      if (fetchWebCallForCompanyStatus.isConfDurationExist == true) {
        // Here We are going to calculate the time and price of the Web Call on the basis of month
        for (j = 0; j < months.length; j++) {
          let totalMinutesForWebCall = 0;
          for (i = 0; i < conferenceDurationsForWebCall.length; i++) {
            if (months[j] == conferenceDurationsForWebCall[i].month) {
              differenceInTimeForWebCall = parseInt(conferenceDurationsForWebCall[i].endedAt) - parseInt(conferenceDurationsForWebCall[i].startedAt);
              // calculating the number of Minutes  
              differenceInMinutesForWebCall = differenceInTimeForWebCall / (1000 * 60);
              totalMinutesForWebCall = totalMinutesForWebCall + differenceInMinutesForWebCall;
            }
          } // END  for (i = 0; i < conferenceDurations.length; i++) {

          desccriptionForMonthForWebCall = {
            "totalMinutes": totalMinutesForWebCall,
            "month": months[j]
          }
          minuteDescriptionForWebCall.push(desccriptionForMonthForWebCall);
        } // END for (j = 0; j < months.length; j++) {   
      } // END if (fetchWebCallForCompanyStatus.isConfDurationExist == true) {         
      //====================================================================================================================

      // Here, We are going to fetch the of all Sip Call againest a particular company
      let fetchSipCallForCompanyStatus = await PriceManagementBusiness.fetchSipCallForCompany(companyId, months, year);

      if (fetchSipCallForCompanyStatus.isError == true) {
        res.json({
          isError: true,
          message: "Some Error Occur While Fetching The details of Sip Call",
          statuscode: 404,
          details: null
        });

        return;
      }

      let conferenceDurationsForSipCall = fetchSipCallForCompanyStatus.confDuration;
      let differenceInTimeForSipCall;
      let differenceInMinutesForSipCall;
      let minuteDescriptionForSipCall = [];
      let desccriptionForMonthForSipCall;
      let updatedAt;

      if (fetchSipCallForCompanyStatus.isConfDurationExist == true) {
        // Here We are going to calculate the time and price of the Sip Call on the basis of month
        for (j = 0; j < months.length; j++) {
          let totalMinutesForSipCall = 0;
          for (i = 0; i < conferenceDurationsForSipCall.length; i++) {
            if (months[j] == conferenceDurationsForSipCall[i].month) {
              updatedAt = Date.parse(conferenceDurationsForSipCall[i].updatedAt);
              differenceInTimeForSipCall = parseInt(updatedAt) - parseInt(conferenceDurationsForSipCall[i].createdAt);
              // calculating the number of Minutes  
              differenceInTimeForSipCall = differenceInTimeForSipCall / (1000 * 60);
              totalMinutesForSipCall = totalMinutesForSipCall + differenceInTimeForSipCall;
            }
          } // END  for (i = 0; i < conferenceDurations.length; i++) {
          // console.log(totalMinutesForSipCall);
          desccriptionForMonthForSipCall = {
            "totalMinutes": totalMinutesForSipCall,
            "month": months[j]
          }
          minuteDescriptionForSipCall.push(desccriptionForMonthForSipCall);
        } // END for (j = 0; j < months.length; j++) { 
      } // END if (fetchSipCallForCompanyStatus.isConfDurationExist == true) {  
      // ===========================================================================================================================

      // Here, We are going to fetch the of all Archives againest a particular company
      let fetchArchivesForCompanyStatus = await PriceManagementBusiness.fetchArchivesForCompany(companyId, months, year);

      if (fetchArchivesForCompanyStatus.isError == true) {
        res.json({
          isError: true,
          message: "Some Error Occur While Fetching The details of Archives",
          statuscode: 404,
          details: null
        });

        return;
      }

      let conferenceDurationsForArchives = fetchArchivesForCompanyStatus.confDuration;
      let differenceInTimeForArchives;
      let minuteDescriptionForArchives = [];
      let desccriptionForMonthForArchives;

      if (fetchArchivesForCompanyStatus.isConfDurationExist == true) {
        // Here We are going to calculate the time and price of the Archives on the basis of month
        for (j = 0; j < months.length; j++) {
          let totalMinutesForArchives = 0;
          for (i = 0; i < conferenceDurationsForArchives.length; i++) {
            if (months[j] == conferenceDurationsForArchives[i].month) {
              // calculating the number of Minutes  
              totalMinutesForArchives = totalMinutesForArchives + conferenceDurationsForArchives[i].duration;
              console.log(totalMinutesForArchives);
            }
          } // END  for (i = 0; i < conferenceDurations.length; i++) {

          desccriptionForMonthForArchives = {
            "totalMinutes": totalMinutesForArchives,
            "month": months[j]
          }
          minuteDescriptionForArchives.push(desccriptionForMonthForArchives);
        } // END for (j = 0; j < months.length; j++) { 
      } // END if (fetchArchivesForCompanyStatus.isConfDurationExist == true) {  

      res.json({
        isError: false,
        message: errorMsgJSON[lang]["200"],
        statuscode: 200,
        details: {
          descriptionForWebCall: minuteDescriptionForWebCall,
          descriptionForSipCall: minuteDescriptionForSipCall,
          descriptionForArchives: minuteDescriptionForArchives
        }
      });


    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }

  },













}