const mongoose = require("mongoose");
const User = require("../models/user");
const errorMsgJSON = require("../../../services/errors.json");

module.exports = {

  /**
   * @description :  // Get all user list
   */
  getUserList : (req,res,next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let page = req.body.page_no ? (parseInt(req.body.page_no == 0 ? 0 : parseInt(req.body.page_no) - 1)) : 0;
    let perPage = req.body.per_page_item ? parseInt(req.body.per_page_item) : 5;

    

    let aggrQry = [
      {
        $match : {
          userType : {$ne :'ADMIN'},
          isActive : true
        }
      },
      {
        $project :{
          "_id":0,
          "userId":1,
          "userType":1,
          "isActive": 1,
          "isApproved": 1,
          "isLoggedIn": 1,
          "fname": 1,
          "lname": 1,
          "email": 1,
          "mobile": 1,
          "company" : 1,
          "upc" : 1,
          "profileCreatedAt": 1,
        }
      },
      {
        $group: {
            _id: null,
            total: {
            $sum: 1
            },
            results: {
            $push : '$$ROOT'
            }
        }
    },
    {
        $project : {
            '_id': 0,
            'total': 1,
            'noofpage': { $ceil :{ $divide: [ "$total", perPage ] } },
            'results': {
                $slice: [
                    '$results', page * perPage , perPage
                ]
            }
        }
    }
    ]

    User.aggregate(aggrQry,function(err,response){
      console.log(response)
      if(err){
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          statuscode: 400,
          details: err
        });
      }
      else{
        if(response){
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: response[0]
          });
        }
      }
    })
  },

  /**
   * @description :  // Approve user account
   */
  approveUser: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    var userId = req.body.user_id
      ? req.body.user_id
      : res.json({
          isError: true, statuscode: 303, details :null,message: errorMsgJSON[lang]["303"] + "Please provide user_id"
        });

    let serachOption = { userId: userId };

    User.findOne(serachOption, function(err, item) {
      if (err) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          statuscode: 400,
          details: err
        });
      } else {
        if (item) {
          // console.log(item)
          let approvalType = item.isApproved?false:true
          let updateFieldsWith = {
            $set: {
              isApproved: approvalType
            }
          };

          User.updateOne(serachOption, updateFieldsWith, function(err, res1) {
            if (err) {
              res.json({
                isError: true,
                message: "Some error occured while updating.",
                statuscode: 404,
                details: null
              });
            } else {
              if (res1.nModified > 0) {
                res.json({
                  isError: false,
                  message: errorMsgJSON[lang]["200"],
                  statuscode: 200,
                  details: null
                });
              }
            }
          });
        } else {
          res.json({
            isError: false,
            message: "No record found",
            statuscode: 200,
            details: null
          });
        }
      }
    });
  },

  /**
   * @description :  // Remove user account
   */
  removeUser : (req,res,next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    var userId = req.body.user_id
    ? req.body.user_id
    : res.json({
        isError: true, statuscode: 303, details :null,message: errorMsgJSON[lang]["303"] + "Please provide user_id"
      });

    let serachOption = { userId: userId };

    let updatewith={
      "isActive":false
    }

    User.updateOne(serachOption, updatewith, function(err, res1) {
      if (err) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          statuscode: 400,
          details: err
        });
      } else {
        if (res1.nModified == 1) {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: null
          });
        } else {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["400"],
            statuscode: 400,
            details: null
          });
        }
      }
    });
  },

  /**
   * @description :  //Find user via search term
   */
  findUser: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    var searchTerm = req.body.search_term
      ? req.body.search_term
      : "";
    var pageNo = req.body.page_no ? req.body.page_no : 1;
    var perPageItem = req.body.per_page_item ? req.body.per_page_item : 10;

    var itemToSkip = (pageNo - 1) * perPageItem;
    
    // check wheather zip or name
    searchAggrQry = (searchTerm == "")?[
      {
        $match: {
          isActive : true
        }
      },
      {
        $skip: itemToSkip
      },
      {
        $limit: perPageItem
      },
      {
        $project : {
          _id : 0,
          password : 0
        }
      }
    ]:[
      {
        $match: { 
              $and : [
                {isActive : true},
                {
                  $or: [
                    {
                      fname: { $regex: searchTerm, $options: "g" }
                    },
                    {
                      lname: { $regex: searchTerm, $options: "g" }
                    },
                    {
                      email: { $regex: searchTerm, $options: "g" }
                    },
                    {
                      mobile: { $regex: searchTerm, $options: "g" }
                    },
                    {
                      upc:{ $regex: searchTerm, $options: "g" }
                    },
                    {
                      company:{ $regex: searchTerm, $options: "g" }
                    }
                  ]
                }
              ]     
            }
      },
      {
        $project : {
          _id : 0,
          password : 0
        }
      },
      {
        $skip: itemToSkip
      },
      {
        $limit: perPageItem
      }
    ];

    User.aggregate(searchAggrQry).exec((err, item) => {
      if (err) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          statuscode: 400,
          details: null
        });
      } else {
        res.json({
          isError: false,
          message: errorMsgJSON[lang]["200"],
          statuscode: 200,
          details: item
        });
      }
    });
  },

  /**
   * @description :  // Get details of a single user
   */
  getUserDetails : (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    var userId = req.body.user_id
      ? req.body.user_id
      : res.json({
        isError: true, statuscode: 303, details :null,message: errorMsgJSON[lang]["303"] + " Please provide user_id"
      });

      User.find({'userId':userId},function(err, response){
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 400,
            details: err
          });
        } else {
          user = JSON.parse(JSON.stringify(response[0]));
          delete user.password;
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: user
          });
        }
      })
  },


  /**
   * @description : add user from admin
   * 
   */
   addEditUser : (req,res,next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    var userid = req.body.userid
      ? req.body.userid
      : "";
    var fname = req.body.fname
      ? req.body.fname
      : res.json({
          isError: true,
          statuscode: 303,
          details: null,
          message: errorMsgJSON[lang]["303"] + " first name"
        });
    var lname = req.body.lname
      ? req.body.lname
      : res.json({
          isError: true,
          statuscode: 303,
          details: null,
          message: errorMsgJSON[lang]["303"] + " last name"
        });
    var email = req.body.email
      ? req.body.email
      : res.json({
          isError: true,
          statuscode: 303,
          details: null,
          message: errorMsgJSON[lang]["303"] + " email"
        });
    var mobile = req.body.mobile
      ? req.body.mobile
      : res.json({
          isError: true,
          statuscode: 303,
          details: null,
          message: errorMsgJSON[lang]["303"] + " mobile"
        });
    var company = req.body.company
      ? req.body.company
      : res.json({
          isError: true,
          statuscode: 303,
          details: null,
          message: errorMsgJSON[lang]["303"] + " company"
        });

    var upc = req.body.upc
      ? req.body.upc
      : res.json({
          isError: true,
          statuscode: 303,
          details: null,
          message: errorMsgJSON[lang]["303"] + " upc"
        });
    var mode = req.body.mode
      ? req.body.mode
      : res.json({
          isError: true,
          statuscode: 303,
          details: null,
          message: errorMsgJSON[lang]["303"] + " mode"
        });   
          
    if(mode == "ADD"){
      var newUser = new User({
        fname: fname,
        lname: lname,
        email: email,
        mobile: mobile,
        password: "123456",
        company : company,
        upc : upc
      });

      newUser.save(function(err, item) {
        if (item) {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["202"],
            statuscode: 200,
            details: null
          });
        }
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 400,
            details: err["message"]
          });
        }
      });
    }
    else{

      let querywith = {
        userId : userid
      }

      let updatewith = {
        fname: fname,
        lname: lname,
        email: email,
        mobile: mobile,
        company : company,
        upc : upc
      }

      User.updateOne(querywith, updatewith, function(err, res1) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 400,
            details: err
          });
        } else {
          if (res1.nModified == 1) {
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: null
            });
          } else {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["400"],
              statuscode: 400,
              details: null
            });
          }
        }
      });
    }

   }

};
