const BaseServices = require('../models/baseServices');
const errorMsgJSON = require("../../../services/errors.json");
const CompanyDetail = require("../models/companyDetail");

module.exports = {
  addNewServices: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let serviceName = req.body.serviceName ? req.body.serviceName : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - Service Name",
    });
    let costPerMinute = req.body.costPerMinute ? req.body.costPerMinute : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - CostPerMinute",
    });
    let pricePerMinute = req.body.pricePerMinute ? req.body.pricePerMinute : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - pricePerMinute",
    });
    let multiplier = req.body.multiplier ? req.body.multiplier : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - multiplier",
    });
    let createdBy = req.body.createdBy ? req.body.createdBy : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - Created By",
    });

    let serviceKey = req.body.serviceKey ? req.body.serviceKey : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - Service Key",
    });

    if (!req.body.serviceName || !req.body.costPerMinute || !req.body.pricePerMinute || !req.body.multiplier || !req.body.createdBy || !req.body.serviceKey) { return; }

    BaseServices.find({}, (error, baseServices) => {
      if (error) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["405"],
          statuscode: 405,
          details: null
        });
        return;
      } else {
        if (baseServices && baseServices.length > 0) {
          let baseService = baseServices[0];
          console.log('=====', baseService);
          let service = {
            serviceKey: serviceKey,
            serviceName: serviceName,
            costPerMinute: costPerMinute,
            pricePerMinute: pricePerMinute,
            multiplier: multiplier,
            createdBy: createdBy,
          };
          // update in exsisting services.
          BaseServices.updateOne({ '_id': baseService._id },
            { $push: { "services": service } },
            (updatederror, updatedresponse) => {
              if (updatederror) {
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["404"],
                  message: "Error",
                  statuscode: 404,
                  details: null
                });
                return;
              } else {
                // console.log('sipCall', sipCall);
                res.json({
                  isError: false,
                  message: errorMsgJSON[lang]["200"],
                  statuscode: 200,
                  details: {}
                });
                // res.json(sipCall);
              }
            });

        } else {
          let newService = new BaseServices({
            services: {
              serviceKey: serviceKey,
              serviceName: serviceName,
              costPerMinute: costPerMinute,
              pricePerMinute: pricePerMinute,
              multiplier: multiplier,
              createdBy: createdBy,
            }
          });
          newService.save((err, serviceResponses) => {
            if (err) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["405"],
                statuscode: 405,
                details: null
              });
              return;
            } else {
              res.json({
                isError: false,
                message: errorMsgJSON[lang]["200"],
                statuscode: 200,
                details: serviceResponses
              });
              return;
            }
          });
        }
      }
    });
  },

  addServicesConfig: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let companyType = req.body.companyType ? req.body.companyType : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - company type",
    });
    let trialPeriod = req.body.trialPeriod ? !isNaN(req.body.trialPeriod) ? Number(req.body.trialPeriod) : 0 : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - trial period",
    });
    let numberOfSeats = req.body.numberOfSeats ? !isNaN(req.body.numberOfSeats) ? Number(req.body.numberOfSeats) : 0 : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - number of seats",
    });
    let pricePerSeat = req.body.pricePerSeat ? !isNaN(req.body.pricePerSeat) ? Number(req.body.pricePerSeat) : 0 : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - price per seat",
    });
    let defaultCurrency = req.body.defaultCurrency ? req.body.defaultCurrency : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - default currency",
    });
    let defaultVat = req.body.defaultVat ? req.body.defaultVat : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - default VAT",
    });

    if (!req.body.companyType || !req.body.trialPeriod || !req.body.numberOfSeats || !req.body.defaultCurrency || !req.body.defaultVat) { return; }

    BaseServices.find({}, (error, baseServices) => {
      if (error) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["405"],
          statuscode: 405,
          details: null
        });
        return;
      } else {
        if (baseServices && baseServices.length > 0) {
          let baseService = baseServices[0];
          console.log('=====', baseService);
          let serviceConfig = {
            companyType: companyType,
            trialPeriod: trialPeriod,
            numberOfSeats: numberOfSeats,
            defaultCurrency: defaultCurrency,
            defaultVat: defaultVat,
            pricePerSeat: pricePerSeat,
          };
          // update in exsisting services.
          BaseServices.updateOne({ '_id': baseService._id },
            { $push: { "serviceConfig": serviceConfig } },
            (updatederror, updatedresponse) => {
              if (updatederror) {
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["404"],
                  message: "Error",
                  statuscode: 404,
                  details: null
                });
                return;
              } else {
                // console.log('sipCall', sipCall);
                res.json({
                  isError: false,
                  message: errorMsgJSON[lang]["200"],
                  statuscode: 200,
                  details: {}
                });
                // res.json(sipCall);
              }
            });

        } else {
          let newService = new BaseServices({
            serviceConfig: {
              companyType: companyType,
              trialPeriod: trialPeriod,
              numberOfSeats: numberOfSeats,
              defaultCurrency: defaultCurrency,
              defaultVat: defaultVat,
              pricePerSeat: pricePerSeat,
            }
          });
          newService.save((err, serviceResponses) => {
            if (err) {
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["405"],
                statuscode: 405,
                details: null
              });
              return;
            } else {
              res.json({
                isError: false,
                message: errorMsgJSON[lang]["200"],
                statuscode: 200,
                details: serviceResponses
              });
              return;
            }
          });
        }
      }
    });
  },

  getBaseServices: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    BaseServices.find({}, (err, baseServices) => {
      if (err) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["405"],
          statuscode: 405,
          details: null
        });
        return;
      } else {
        res.json({
          isError: false,
          message: errorMsgJSON[lang]["200"],
          statuscode: 200,
          details: baseServices
        });
        return;
      }
    });
  },

  editBaseService: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let serviceId = req.body.serviceId ? req.body.serviceId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - service Id",
    });

    let rowId = req.body.rowId ? req.body.rowId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - row Id",
    });

    if (!req.body.serviceId || !req.body.rowId) { return; }

    let updateWith = {}
    if (req.body.serviceName) updateWith['services.$.serviceName'] = req.body.serviceName;
    if (req.body.costPerMinute) updateWith['services.$.costPerMinute'] = req.body.costPerMinute;
    if (req.body.pricePerMinute) updateWith['services.$.pricePerMinute'] = req.body.pricePerMinute;
    if (req.body.multiplier) updateWith['services.$.multiplier'] = req.body.multiplier;
    if (req.body.createdBy) updateWith['services.$.createdBy'] = req.body.createdBy;

    BaseServices.updateOne(
      { _id: rowId, "services._id": serviceId },
      updateWith,
      function (updatederror, updatedresponse) {
        if (updatederror) {
          console.log('updatederror ---ooo ', updatederror);

          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            message: "Error",
            statuscode: 404,
            details: null
          });
          return;
        } else {
          // console.log('sipCall', sipCall);
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: {}
          });
          // res.json(sipCall);
        }
      });
  },

  editServiceConfig: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let configId = req.body.configId ? req.body.configId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - configId",
    });

    let rowId = req.body.rowId ? req.body.rowId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - row Id",
    });

    if (!req.body.configId || !req.body.rowId) { return; }

    let updateWith = {}
    if (req.body.trialPeriod) updateWith['serviceConfig.$.trialPeriod'] = !isNaN(req.body.trialPeriod) ? Number(req.body.trialPeriod) : 0;
    if (req.body.numberOfSeats) updateWith['serviceConfig.$.numberOfSeats'] = !isNaN(req.body.numberOfSeats) ? Number(req.body.numberOfSeats) : 0;
    if (req.body.defaultCurrency) updateWith['serviceConfig.$.defaultCurrency'] = req.body.defaultCurrency;
    if (req.body.pricePerSeat) updateWith['serviceConfig.$.pricePerSeat'] = !isNaN(req.body.pricePerSeat) ? Number(req.body.pricePerSeat) : 0;
    if (req.body.defaultVat) updateWith['serviceConfig.$.defaultVat'] = !isNaN(req.body.defaultVat)? Number(req.body.defaultVat): 0;

    BaseServices.updateOne(
      { _id: rowId, "serviceConfig._id": configId },
      updateWith,
      function (updatederror, updatedresponse) {
        if (updatederror) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            message: "Error",
            statuscode: 404,
            details: null
          });
          return;
        } else {
          // console.log('sipCall', sipCall);
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: {}
          });
          // res.json(sipCall);
        }
      });
  },

  deleteBaseServices: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let serviceId = req.body.serviceId ? req.body.serviceId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - service Id",
    });

    let rowId = req.body.rowId ? req.body.rowId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - row Id",
    });

    if (!req.body.serviceId) { return; }
    BaseServices.updateOne(  // start of delete function
      { "_id": rowId },
      { $pull: { services: { _id: serviceId } } },
      function (err, response) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["405"],
            statuscode: 405,
            details: null
          })
        } else {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: null
          });
        }
      });
  },

  deleteServicesConfig: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let configId = req.body.configId ? req.body.configId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - config Id",
    });

    let rowId = req.body.rowId ? req.body.rowId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - row Id",
    });

    if (!req.body.configId || !req.body.rowId) { return; }
    BaseServices.updateOne(  // start of delete function
      { "_id": rowId },
      { $pull: { serviceConfig: { _id: configId } } },
      function (err, response) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["405"],
            statuscode: 405,
            details: null
          })
        } else {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: null
          });
        }
      });
  },

  searchBaseServices: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let serviceName = req.body.serviceName ? req.body.serviceName : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - Search name",
    });

    if (!req.body.serviceName) { return; }
    BaseServices.find({ serviceName: { $regex: '.*' + serviceName + '.*', "$options": "i" } }, (err, response) => {
      if (err) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["405"],
          statuscode: 405,
          details: null
        })
      } else {
        res.json({
          isError: false,
          message: errorMsgJSON[lang]["200"],
          statuscode: 200,
          details: response
        });
      }
    })
  },

  addBaseServicesToCompany: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let companyId = req.body.companyId ? req.body.companyId : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - Search name",
    });

    let companyType = req.body.companyType ? req.body.companyType : res.json({
      isError: true,
      message: errorMsgJSON[lang]["303"] + " - Company Type",
    });

    if (!req.body.companyId || !req.body.companyType) { return; }

    BaseServices.find({}, (err, baseServices) => {
      if (err) {

        res.json({
          isError: true,
          message: errorMsgJSON[lang]["405"],
          statuscode: 405,
          details: null
        });
        return;
      } else {

        let updateWhere = {
          "_id": companyId,
        }
        let serviceConfig = {};
        baseServices[0].serviceConfig.map(config => {
          if (config.companyType == companyType) {
            serviceConfig = config;
          }
        })

        let updateWith = {
          "services": baseServices[0].services,
          "currency": serviceConfig.defaultCurrency,
          "pricePerSeat": serviceConfig.pricePerSeat,
          "noOfSeats": serviceConfig.numberOfSeats,
          "trialPeriod": serviceConfig.trialPeriod,
        }

        CompanyDetail.updateOne(updateWhere, updateWith, (updatederror, updatedresponse) => {
          if (updatederror) {

            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              message: "Error",
              statuscode: 404,
              details: null
            });
            return;
          } else {
            // console.log('sipCall', sipCall);
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: {}
            });
            return;
          }
        })
      }
    });
  },

  // getCompanyService: (req, res, next) => {
  //   let lang = req.headers.language ? req.headers.language : "EN";
  //   let companyId = req.body.companyId ? req.body.companyId : res.json({
  //     isError: true,
  //     message: errorMsgJSON[lang]["303"] + " - Search name",
  //   });
  //   if (!req.body.companyId) { return; }

  //   let findWhere = {
  //     "_id": companyId,
  //   }

  //   CompanyDetail.find(findWhere, (err, companyDetail) => {
  //     if (err) {
  //       res.json({
  //         isError: true,
  //         message: errorMsgJSON[lang]["404"],
  //         message: "Error",
  //         statuscode: 404,
  //         details: null
  //       });
  //       return;
  //     } else {
  //       // console.log('sipCall', sipCall);
  //       res.json({
  //         isError: false,
  //         message: errorMsgJSON[lang]["200"],
  //         statuscode: 200,
  //         details: companyDetail && companyDetail.length > 0 && companyDetail[0].services ? companyDetail[0].services : []
  //       });
  //       return;
  //     }
  //   })

  // },

  // editCompanyService: (req, res, next) => {
  //   let lang = req.headers.language ? req.headers.language : "EN";
  //   let companyId = req.body.companyId ? req.body.companyId : res.json({
  //     isError: true,
  //     message: errorMsgJSON[lang]["303"] + " - company Id",
  //   });
  //   let services = req.body.services ? req.body.services : res.json({
  //     isError: true,
  //     message: errorMsgJSON[lang]["303"] + " - services",
  //   });
  //   if (!req.body.companyId || !req.body.services) { return; }
  //   let updateWhere = { "_id": companyId, };
  //   let updateWith = { "services": baseServices, };
  //   CompanyDetail.updateOne(updateWhere, updateWith, (updatederror, updatedresponse) => {
  //     if (updatederror) {
  //       res.json({
  //         isError: true,
  //         message: errorMsgJSON[lang]["404"],
  //         message: "Error",
  //         statuscode: 404,
  //         details: null
  //       });
  //       return;
  //     } else {
  //       // console.log('sipCall', sipCall);
  //       res.json({
  //         isError: false,
  //         message: errorMsgJSON[lang]["200"],
  //         statuscode: 200,
  //         details: {}
  //       });
  //       return;
  //     }
  //   })
  // },

}