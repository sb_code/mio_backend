//=================================================
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const Conference = require("../models/conference");
const CompanyDetail = require("../models/companyDetail");


const Company = require("../models/company");
const Guest = require("../models/guest");
const User = require("../models/user");
const Invitation = require("../models/invitation");
// const bcrypt = require("bcrypt");
const SALT_WORK_FACTOR = 10;
const Mailercontroller = require("../../../services/mailer");

const errorMsgJSON = require("../../../services/errors.json");
const config = require("../config/globals/keyfile");
const AutogenerateIdcontroller = require('../../../services/AutogenerateId');
const Base64ImgUploadmodule = require('../../../services/Base64ImgUploadmodule');
const OperatorManagementBusiness = require("../BusinessLogic/operatorManagementBusiness");

var fileupload = require('express-fileupload');

const fs = require('fs');
const AWS = require('aws-sdk');


//=================================================
var OpenTok = require('opentok'),
  opentok = new OpenTok(config.tokbox.apiKey, config.tokbox.apiSecret);

//================================
// Import events module
var events = require('events');

//===========================================

module.exports = {

  /*
       This function is used to unassign the user from conference
  */ 
  unAssignOperatorFromCompany: async (req, res, next) => {
      let lang = req.headers.language ? req.headers.language : "EN";
      try {
            // Here, we are receiving the user Id 
          let userId = req.body.userId ? req.body.userId : res.json({
            isError: true,
            message: errorMsgJSON[lang]["303"] + " - user Id",
          });
          
          // Here, we are receiving the company Id 
          let companyId = req.body.companyId ? req.body.companyId : res.json({
            isError: true,
            message: errorMsgJSON[lang]["303"] + " - Company Id",
          });
          
          // Here, we are receiving the conference Id 
          let conferenceId = req.body.conferenceId ? req.body.conferenceId : res.json({
            isError: true,
            message: errorMsgJSON[lang]["303"] + " - conference Id",
          });

          // Here, Circular json avoided
          if (!req.body.companyId || !req.body.userId || !req.body.conferenceId) { return; }

          // Here, We are going to fetch the details of user
          let fetchUserDetailStatus = await OperatorManagementBusiness.fetchUserDetail(userId);

          if (fetchUserDetailStatus.isError == true) {
              res.json({
                isError: true,
                message: "Error occur while fetching the details of the user",
                statuscode: 404,
                details: null
              });
              
              return;
          }

          if (fetchUserDetailStatus.isUserExist == false) {
              res.json({
                isError: false,
                message: "Either the user not valid or not active",
                statuscode: 204,
                details: null
              });
              
              return;
          }

          let userDetail = fetchUserDetailStatus.userDetail[0];

          if (userDetail.isAssigned == false) {
              res.json({
                isError: false,
                message: "Operator Already Assigned to any Conference",
                statuscode: 204,
                details: null
              });
              
              return;
          } 

          if (userDetail.userType == 'ADMIN_OPERATOR') {
              // This is the company Id of the user, to which he belongs
                  let companyIdofUser = userDetail.companyId ? userDetail.companyId : '';
                  
                  if (companyIdofUser == '') {
                      res.json({
                        isError: false,
                        message: "User Does not not belong to any registered company",
                        statuscode: 204,
                        details: null
                      });
                      
                      return;
                  }  

                  // Here, We are going to UN Assign the operator to a company
                  let unAssignUserToCompanyStatus = await OperatorManagementBusiness.unAssignUserToCompany(userId, companyId, conferenceId);
                 
                  if (unAssignUserToCompanyStatus.isError == true) {
                      res.json({
                        isError: true,
                        message: "Some Error occur while un assigning the user from a company",
                        statuscode: 404,
                        details: null
                      });
                      
                      return;
                  } 

                  // Here, We are going to Assign the operator to a company
                  let updateAssignmentDetailToUserCompanyStatus = await OperatorManagementBusiness.updateAssignmentDetailToUserCompany(userId, companyId, conferenceId, companyIdofUser);
                  
                  if (updateAssignmentDetailToUserCompanyStatus.isError == true) {
                      res.json({
                        isError: true,
                        message: "Some Error occur while un assigning the user from a company",
                        statuscode: 404,
                        details: null
                      });
                      
                      return;
                  }   
          } // END f (userDetail.userType == 'ADMIN_OPERATOR') { 

          // Here, We are going to remove a operator in the conference
          let removeUserToConference = await OperatorManagementBusiness.removeUserToConference(userId, conferenceId); 
          
          if (removeUserToConference.isError == true) {
              res.json({
                isError: true,
                message: "Some Error occur while un assigning the user from a company",
                statuscode: 404,
                details: null
              });
              
              return;
          }

          // Here, We are going to update update the user assignment status
          let unsetUserAssignStatus = await OperatorManagementBusiness.unsetUserAssign(userId, conferenceId);   

          if (unsetUserAssignStatus.isError == true) {
              res.json({
                isError: true,
                message: "Some Error occur while un assigning the user from a company",
                statuscode: 404,
                details: null
              });
              
              return;
          } 

          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: null
          });

      } catch (error) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            message: "Error",
            statuscode: 404,
            details: null
          });
      }
  },

  /*
      This function is written to assign a operator to a particular company
  */
  assignOperatorToCompany: async (req, res, next) => {
      let lang = req.headers.language ? req.headers.language : "EN";
      try {
          // Here, we are receiving the user Id 
          let userId = req.body.userId ? req.body.userId : res.json({
            isError: true,
            message: errorMsgJSON[lang]["303"] + " - user Id",
          });
          
          // Here, we are receiving the company Id 
          let companyId = req.body.companyId ? req.body.companyId : res.json({
            isError: true,
            message: errorMsgJSON[lang]["303"] + " - Company Id",
          });
          
          // Here, we are receiving the conference Id 
          let conferenceId = req.body.conferenceId ? req.body.conferenceId : res.json({
            isError: true,
            message: errorMsgJSON[lang]["303"] + " - conference Id",
          });

          // Here, Circular json avoided
          if (!req.body.companyId || !req.body.userId || !req.body.conferenceId) { return; }

          // Here, We are going to fetch the details of user
          let fetchUserDetailStatus = await OperatorManagementBusiness.fetchUserDetail(userId);

          if (fetchUserDetailStatus.isError == true) {
              res.json({
                isError: true,
                message: "Error occur while fetching the details of the user",
                statuscode: 404,
                details: null
              });
              
              return;
          }

          if (fetchUserDetailStatus.isUserExist == false) {
              res.json({
                isError: false,
                message: "Either the user not valid or not active",
                statuscode: 204,
                details: null
              });
              
              return;
          }

          let userDetail = fetchUserDetailStatus.userDetail[0];

          if (userDetail.isAssigned == true) {
              res.json({
                isError: false,
                message: "Operator Already Assigned to a Conference",
                statuscode: 204,
                details: null
              });
              
              return;
          }  

          if (userDetail.userType == "ADMIN_OPERATOR") {

                  // This is the company Id of the user, to which he belongs
                  let companyIdofUser = fetchUserDetailStatus.userDetail[0].companyId ? fetchUserDetailStatus.userDetail[0].companyId : '';
                  
                  if (companyIdofUser == '') {
                      res.json({
                        isError: false,
                        message: "User Does not not belong to any registered company",
                        statuscode: 204,
                        details: null
                      });
                      
                      return;
                  }  
         
                  // Here, We are going to Assign the operator to a company
                  let assignUserToCompanyStatus = await OperatorManagementBusiness.assignUserToCompany(userId, companyId, conferenceId);
                  
                  if (assignUserToCompanyStatus.isError == true) {
                      res.json({
                        isError: true,
                        message: "Some Error occur while assigning the user to a company",
                        statuscode: 404,
                        details: null
                      });
                      
                      return;
                  } 

                  if (assignUserToCompanyStatus.idUpdated == false) {
                      res.json({
                        isError: false,
                        message: "Due to some technical problem user was not assigned to user",
                        statuscode: 204,
                        details: null
                      });
                      
                      return;
                  } 

                  // Here, We are going to Assign the operator to a company
                  let insertAssignmentDetailToUserCompanyStatus = await OperatorManagementBusiness.insertAssignmentDetailToUserCompany(userId, companyId, conferenceId, companyIdofUser);
                  
                  if (insertAssignmentDetailToUserCompanyStatus.isError == true) {
                      res.json({
                        isError: true,
                        message: errorMsgJSON[lang]["404"],
                        statuscode: 404,
                        details: null
                      });
                      
                      return;
                  }

                  if (insertAssignmentDetailToUserCompanyStatus.idUpdated == false) {
                      res.json({
                        isError: false,
                        message: "Some problem in updation",
                        statuscode: 204,
                        details: null
                      });
                      
                      return;
                  }

            } // END if (userDetail.userType == "ADMIN_OPERATOR") {   

            // Here, We are going to update update the user assignment status
            let setUserAssignStatus = await OperatorManagementBusiness.setUserAssign(userId, conferenceId);  
             
            if (setUserAssignStatus.isError == true) {
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["404"],
                  statuscode: 404,
                  details: null
                });
                
                return;
            }

            if (setUserAssignStatus.idUpdated == false) {
                res.json({
                  isError: false,
                  message: "Some problem in updation",
                  statuscode: 204,
                  details: null
                });
                
                return;
            }

            // Here, We are going to insert a operator in the conference
            let insertUserToConferenceStatus = await OperatorManagementBusiness.insertUserToConference(userId, conferenceId, userDetail); 

           
            if (insertUserToConferenceStatus.isError == true) {
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["404"],
                  statuscode: 404,
                  details: null
                });
                
                return;
            }

            if (insertUserToConferenceStatus.idUpdated == false) {
                res.json({
                  isError: false,
                  message: "Some problem in updation",
                  statuscode: 204,
                  details: null
                });
                
                return;
            }

            res.json({
              isError: false,
              message: "Operator is assigned to Conference",
              statuscode: 200,
              details: null
            });

           

      } catch (error) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            message: "Error",
            statuscode: 404,
            details: null
          });
      }
 
  },

  /*
      this function is used written to fetch all the Admin Operator
  */
  fetchAllAdminOperator: (req, res, next) => {

      let lang = req.headers.language ? req.headers.language : "EN";
      try {

            // Here, we are receiving the userName 
            let userName = req.body.userName ? req.body.userName : '';
            let aggrQuery = '';

            if (userName == '') {
                  aggrQuery = [
                    { 
                        '$match': { 
                          'userType': 'ADMIN_OPERATOR',
                        }
                    },
                    {
                        $project: {
                            '_id': 0,
                            "userType":1,     
                            "isActive":1,
                            "isApproved":1,
                            "isLoggedIn": 1,
                            "company": 1,
                            "upc": 1,
                            "email":1,     
                            "mobile":1,
                            "profileCreatedAt":1,
                            "createdAt": 1,
                            "updatedAt": 1,
                            "userId": 1,
                            "fname": 1,
                            "lname": 1,
                            'name': { $concat: [ "$fname", " ", "$lname" ] }
                        }
                    },

                  ];

            } else {

                  aggrQuery = [
                    { 
                        '$match': { 
                          'userType': 'ADMIN_OPERATOR',
                        }
                    },
                    {
                        $project: {
                            '_id': 0,
                            "userType":1,     
                            "isActive":1,
                            "isApproved":1,
                            "isLoggedIn": 1,
                            "company": 1,
                            "upc": 1,
                            "email":1,     
                            "mobile":1,
                            "profileCreatedAt":1,
                            "createdAt": 1,
                            "updatedAt": 1,
                            "userId": 1,
                            "fname": 1,
                            "lname": 1,
                            'name': { $concat: [ "$fname", " ", "$lname" ] }
                        }
                    },

                    { 
                        $match: { 
                          $and: [
                                  {
                                      name: { $regex: userName, $options: "i" }
                                  }
                          ]
                        }
                    },   
                ];
            }
            
            

            User.aggregate(aggrQuery, function (err, response) {

              if (err) {
                  res.json({
                    isError: true,
                    message: errorMsgJSON[lang]["404"],
                    statuscode: 404,
                    details: null
                  });
              }
              else {

                if (response.length < 1) {
                  res.json({
                    isError: false,
                    message: errorMsgJSON[lang]["204"],
                    statuscode: 204,
                    details: null
                  });
                }

                if (response.length > 0) {
                  res.json({
                    isError: false,
                    message: errorMsgJSON[lang]["200"],
                    statuscode: 200,
                    details: response
                  });
                }
                     
              }
            }) //END User.aggregate

      } catch (error) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            message: "Error",
            statuscode: 404,
            details: null
          });
      }

  },

  

  /*
      This function is written to assign a user a service
  */
  assignOperator: (req, res, next) => {
    console.log("=====  data === 111 = ", req.body);


    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the company Id 
      let companyId = req.body.companyId ? req.body.companyId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Company Id",
      });

      // Here, we are receiving the userId
      let userId = req.body.userId ? req.body.userId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Id",
      });

      // Here, we are receiving the operation to be performed
      let operation = req.body.operation ? req.body.operation : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - operation",
      });

      // Here, Circular json avoided
      if (!req.body.companyId || !req.body.userId || !req.body.operation) { return; }


      let current_time = Date.now();

      // Create an eventEmitter object
      var eventEmitter = new events.EventEmitter();

      /*
          Create an event handler to perform_operation 
          In this event a operator is assigned to a service for a 
          particular company and vice versa
      */
      var perform_operation = function perform_operation() {


        if (operation == 'UN_ASSIGN') { // Here, we have to remove the operator from the service

          let status = 3000;

          let updateWith = {
            "operator.$.status": status,
            "operator.$.endDate": current_time,
          }

          let setStatus = 'STARTED';
          let updateWhere = {
            $and: [
              { 'companyId': companyId },
              { 'operator.userId': userId },
              { 'operator.status': 2000 }
            ]

          }

          //Company.find(searchQuery, 
          Company.updateMany(updateWhere, { $set: updateWith },
            function (updatederror, updatedresponse) {


              if (updatederror) {
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["404"],
                  statuscode: 404,
                  details: updatedresponse
                });

                return;
              }

              if (updatedresponse) {
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["200"],
                  statuscode: 200,
                  details: updatedresponse
                });
              }
            });
        } // End of If block

        if (operation == 'ASSIGN') { // Here, we have to assign a operator to a service
          let status = 2000;

          let updateStatusQuery = {
            "userId": userId,
            "startDate": current_time,
            "endDate": current_time,
            "status": status
          }

          Company.updateOne({ 'companyId': companyId },
            { $push: { "operator": updateStatusQuery } },
            function (updatederror, updatedresponse) {

              if (updatederror) {
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["404"],
                  statuscode: 404,
                  details: null
                });

                return;
              }

              if (updatedresponse) {
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["200"],
                  statuscode: 200,
                  details: null
                });
              }
            });
        } // End of If block

      } // end of perform_operation event  

      // Bind the perform_operation event with the handler
      eventEmitter.addListener('perform_operation', perform_operation);

      /*
          Create an event handler to Check_Operator 
          In this event a WE Check Operator Service Service Status
      */
      var check_operator_status = function check_operator_status() { // Check Operator Service Service Status

        let aggrQuery = [
          { $match: { 'companyId': companyId } },
          {
            $project: {
              '_id': 0,
              "operator": 1
            }
          },
          { $unwind: "$operator" },

          {
            $match: { 'operator.userId': userId, 'operator.status': 2000 }
          },
        ];

        Company.aggregate(aggrQuery,
          function (err, response) {

            if (err) { // Some Error Occur
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["404"],
                statuscode: 404,
                details: null
              });

              return;
            }

            if (response.length > 0) { // When Operator is assigned to a service
              if (operation == 'ASSIGN') { // Here, we have to assign a operator to a service
                res.json({
                  isError: false,
                  message: errorMsgJSON[lang]["205"],
                  statuscode: 205,
                  details: null
                });

                return;
              }

              if (operation == 'UN_ASSIGN') {
                // Fire the perform_operation event 
                eventEmitter.emit('perform_operation');
              }
            }

            if (response.length == 0) { // When Operator is not assigned to a service
              // Fire the perform_operation event 
              eventEmitter.emit('perform_operation');
            }

          });
      } // End check_operator_status Event

      // Bind the check_operator_status event with the handler
      eventEmitter.addListener('check_operator_status', check_operator_status);




      /*
          Create an event handler to Check_Operator 
          In this event a operator exist in the system or not
      */
      var check_operator = function check_operator() {

        let aggrQuery = [
          { $match: { 'userId': userId, company: companyId } },
        ];

        User.aggregate(aggrQuery,
          function (err, response) {

            if (err) { // Some Error Occur
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["404"],
                statuscode: 404,
                details: null
              });

              return;
            }

            if (response.length == 0) { // When No company Exist
              res.json({
                isError: false,
                message: errorMsgJSON[lang]["204"],
                statuscode: 204,
                details: null
              });

              return;
            }

            if (response.length > 0) { // When Candidate Exist
              // Fire the perform_operation event 
              eventEmitter.emit('check_operator_status');
            }

          });
      } // End Of Check Operator event 

      // Bind the check_operator event with the handler
      eventEmitter.addListener('check_operator', check_operator);


      /*
          Create an event handler to perform_operation 
          In this event a operator is assigned to a service for a 
          particular company and vice versa
      */
      var check_company = function check_company() {

        let aggrQuery = [
          { $match: { companyId: companyId } },
        ];

        Company.aggregate(aggrQuery, function (err, response) {

          if (err) { // some error occur
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            });

            return;
          }

          if (response.length == 0) { // When No company Exist
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["204"],
              statuscode: 204,
              details: null
            });

            return;
          }

          if (response.length > 0) { // When company Exist
            // Fire the perform_operation event 
            eventEmitter.emit('check_operator');
          }
        });

      } // end of perform_operation event   

      // Bind the perform_operation event with the handler
      eventEmitter.addListener('check_company', check_company);
      // Fire the perform_operation event 
      eventEmitter.emit('check_company');

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }

  },

  /* 
      This function is written to add operator for particular company
  */
  addOperator: (req, res, next) => {
    console.log("..ok");
    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      // Here, we are receiving the first Name
      let fname = req.body.fname ? req.body.fname : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - First Name",
      });

      // Here, we are receiving the last Name
      let lname = req.body.lname ? req.body.lname : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Last Name",
      });

      // Here, we are receiving the User Type
      let userType = req.body.userType ? req.body.userType : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - User Type",
      });

      // Here, we are receiving the Company Id
      let company = req.body.companyId ? req.body.companyId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Company Id",
      });

      // Here, we are receiving the Company Type
      let companyType = req.body.companyType ? req.body.companyType : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Company Type",
      });

      // Here, we are receiving the email of user
      let email = req.body.email ? req.body.email : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - email",
      });

      // Here, we are receiving the mobile number of the user
      let mobile = req.body.mobile ? req.body.mobile : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Mobile",
      });

      // Here, we are receiving the type of service of the user
      let service = req.body.service ? req.body.service : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Service",
      });

      // Here, we are receiving the Availablity of the user
      let availablity = req.body.availablity ? req.body.availablity : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Availablity",
      });

      // Here, Circular json avoided
      if (!req.body.fname || !req.body.lname || !req.body.userType || !req.body.companyId || !req.body.companyType) { return; }

      if (!req.body.email || !req.body.mobile || !req.body.service || !req.body.availablity) { return; }

      // This is the tempory password assigned to the user
      let password = Math.floor(100000 + Math.random() * 900000);

      var newUser = new User({
        fname: fname,
        lname: lname,
        email: email,
        userType: userType,
        mobile: mobile,
        password: password,
        company: company,
        companyType: companyType,
        service: service,
        availablity: availablity
      });

      newUser.save(function (err, item) {

        if (item) {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: {
              userType: item.userType,
              isActive: item.isActive,
              isApproved: item.isApproved,
              company: item.company,
              companyType: item.companyType,
              password: password,
              fname: item.fname,
              lname: item.lname,
              email: item.email,
              mobile: item.mobile,
              profileCreatedAt: item.profileCreatedAt,
              userId: item.userId,
              service: item.service,
              availablity: item.availablity,
              _id: item._id,

            }

          });
        }
      });

      // res.json({
      //    request: req.body
      // })


    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }

  },

  /* 
      This function is written to add company in the system
  */
  addCompany: (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      // Here, we are receiving the User Name
      let companyName = req.body.companyName ? req.body.companyName : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Company Name",
      });

      // Here, Circular json avoided
      if (!req.body.companyName) { return; }

      let companyId = AutogenerateIdcontroller.autogenerateId("COMP");

      var newCompany = new CompanyDetail({
        companyName: companyName,
        companyId: companyId,
      });

      newCompany.save(function (err, item) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;
        }

        if (item) {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: {
              _id: item._id,
              companyName: item.companyName,
              companyId: item.companyId,
              date: item.date,
              status: item.status
            }
          });
        }

      });

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
      This function is written to add operator
  */
  addOperatorForCompany: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }

  },



}