//=================================================
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const Conference = require("../models/conference");
const CompanyDetail = require("../models/companyDetail");


const Company = require("../models/company");
const Guest = require("../models/guest");
const User = require("../models/user");
const Invitation = require("../models/invitation");
const FAQ = require("../models/faq");
const TAC = require("../models/tac");
const PrivacyPolicy = require("../models/privacyPolicy");



// const bcrypt = require("bcrypt");
const SALT_WORK_FACTOR = 10;
const Mailercontroller = require("../../../services/mailer");

const errorMsgJSON = require("../../../services/errors.json");
const config = require("../config/globals/keyfile");
const AutogenerateIdcontroller = require('../../../services/AutogenerateId');
const Base64ImgUploadmodule = require('../../../services/Base64ImgUploadmodule');
const OperatorManagementBusiness = require("../BusinessLogic/operatorManagementBusiness");

const ContentBusiness = require("../BusinessLogic/contentBusniess");

var fileupload = require('express-fileupload');

const fs = require('fs');
const AWS = require('aws-sdk');


//=================================================
var OpenTok = require('opentok'),
  opentok = new OpenTok(config.tokbox.apiKey, config.tokbox.apiSecret);

//================================
// Import events module
var events = require('events');

//===========================================

module.exports = {


  /*
      This Terms And Condition is written to delete 
      Policy
  */
  deletePrivacyPolicy: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      let policyId = req.body.policyId ? req.body.policyId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Policy Id",
      });

      if (!req.body.policyId) { return; }

      PrivacyPolicy.find({ '_id': policyId }, function (err, response) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
          return;
        }

        if (response.length < 1) {
          res.json({
            isError: false,
            message: "No Data Found",
            statuscode: 204,
            details: null,
          });
          return;
        }

        PrivacyPolicy.deleteOne({ '_id': policyId }, function (err, response) {

          if (err) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            });
            return;
          }
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: null
          });
        }) // END PrivacyPolicy.deleteOne({'_id': tacId}, function (err, response) {
      })  // END PrivacyPolicy.findOne({'id': tacId}, function (err, response) {         

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
      This api is written to update an Policy on the basis of Id
  */
  updatePrivacyPolicyById: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      let policyId = req.body.policyId ? req.body.policyId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Policy Id",
      });

      let policy = req.body.policy ? req.body.policy : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Policy",
      });

      if (!req.body.policyId || !req.body.policy) { return; }

      PrivacyPolicy.updateOne({ '_id': policyId },
        { $set: { 'policy': policy } },
        function (err, response) {

          if (err) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            });
            return;
          }

          if (response.nModified == 1) {

            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: response
            });

          } else {

            res.json({
              isError: false,
              message: "Some Problem Occur in Updation",
              statuscode: 204,
              details: null
            });
          }
        })

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
      This function is used to fetch all the Privacy Policies
  */
  getPrivacyPolicy: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      PrivacyPolicy.find({}, function (err, response) {

        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;
        }

        if (response.length < 1) {
          res.json({
            isError: false,
            message: "Data Not Found",
            statuscode: 204,
            details: null
          });

          return;
        } else {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: response
          });

          return;
        }

      })

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },


  /*
      This function is written to add privacy policy
  */
  insertPrivacyPolicy: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      let policy = req.body.policy ? req.body.policy : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - policies",
      });

      if (!req.body.policy) { return; }

      if (policy.length === 0) {
        res.json({
          isError: true,
          message: "Policies are not sent to insert",
          statuscode: 204,
          details: null
        });
        return;
      }


      PrivacyPolicy.insertMany(policy, function (err, response) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: response
          })

          return;
        }
        res.json({
          isError: false,
          message: errorMsgJSON[lang]["200"],
          statuscode: 200,
          details: response
        })

        return;
      })


    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
      This Terms And Condition is written to delete 
      Terms And Condition
  */
  deleteTAC: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      let tacId = req.body.tacId ? req.body.tacId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Id of Terms And Conditions",
      });

      if (!req.body.tacId) { return; }

      TAC.find({ '_id': tacId }, function (err, response) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
          return;
        }

        if (response.length < 1) {
          res.json({
            isError: false,
            message: "No Data Found",
            statuscode: 204,
            details: null,
          });
          return;
        }

        TAC.deleteOne({ '_id': tacId }, function (err, response) {

          if (err) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            });
            return;
          }
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: null
          });
        }) // END TAC.deleteOne({'_id': tacId}, function (err, response) {
      })  // END TAC.findOne({'id': tacId}, function (err, response) {         

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
      This api is written to update an Terms and Condition on the basis of Id
  */
  updateTacById: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      let tacId = req.body.tacId ? req.body.tacId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Id of Terms and Condition",
      });

      let termsAndCondition = req.body.termsAndCondition ? req.body.termsAndCondition : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Terms And Condition",
      });

      if (!req.body.tacId || !req.body.termsAndCondition) { return; }

      TAC.updateOne({ '_id': tacId }, { $set: { 'termsAndCondition': termsAndCondition } },
        function (err, response) {

          if (err) {
            res.json({
              isError: true,
              message: "NO Data Was Sent For Update",
              statuscode: 204,
              details: null
            });
            return;
          }

          if (response.nModified == 1) {

            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: response
            });

          } else {

            res.json({
              isError: false,
              message: "Some Problem Occur in Updation",
              statuscode: 204,
              details: null
            });
          }
        })

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
      This function is used to fetch all the Terms And Conditions
  */
  getTAC: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      TAC.find({}, function (err, response) {

        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;
        }

        if (response.length < 1) {
          res.json({
            isError: false,
            message: "Data Not Found",
            statuscode: 204,
            details: null
          });

          return;
        } else {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: response
          });

          return;
        }

      })

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
      This function is written to add Terms And Conditions
  */
  insertTAC: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      let tac = req.body.tac ? req.body.tac : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " -Frequently Asked Questions",
      });

      if (!req.body.tac) { return; }

      if (tac.length === 0) {
        res.json({
          isError: true,
          message: "Terms and conditions are not sent to insert",
          statuscode: 204,
          details: null
        });
        return;
      }


      TAC.insertMany(tac, function (err, response) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: response
          })

          return;
        }
        res.json({
          isError: false,
          message: errorMsgJSON[lang]["200"],
          statuscode: 200,
          details: response
        })

        return;
      })


    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
      This FAQ is written to delete FAQ
  */
  deleteFAQ: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      let faqId = req.body.faqId ? req.body.faqId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Id of Frequently Asked Questions",
      });

      if (!req.body.faqId) { return; }

      FAQ.find({ '_id': faqId }, function (err, response) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
          return;
        }

        if (response.length < 1) {
          res.json({
            isError: false,
            message: "No Data Found",
            statuscode: 204,
            details: null,
          });
          return;
        }

        FAQ.deleteOne({ '_id': faqId }, function (err, response) {

          if (err) {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            });
            return;
          }
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: null
          });
        })  // END FAQ.deleteOne({'_id': faqId}, function (err, response) {
      })  // END FAQ.find({'_id': faqId}, function (err, response) {  

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
      This api is written to update an faq on the basis of Id
  */
  updateFaqById: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      let faqId = req.body.faqId ? req.body.faqId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - Id of Frequently Asked Questions",
      });

      let answer = req.body.answer ? req.body.answer : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - answer",
      });

      if (!req.body.faqId || !req.body.answer) { return; }

      FAQ.updateOne({ '_id': faqId }, { $set: { 'answer': answer } },
        function (err, response) {

          if (err) {
            res.json({
              isError: true,
              message: "NO Data Was Sent For Update",
              statuscode: 204,
              details: null
            });
            return;
          }

          if (response.nModified == 1) {

            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: response
            });

          } else {

            res.json({
              isError: false,
              message: "Some Problem Occur in Updation",
              statuscode: 204,
              details: null
            });
          }
        })

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },



  /*
      This function is used to fetch all the FAQs
  */
  getFAQ: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      FAQ.find({}, function (err, response) {

        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;
        }

        if (response.length < 1) {
          res.json({
            isError: false,
            message: "Data Not Found",
            statuscode: 204,
            details: null
          });

          return;
        } else {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: response
          });

          return;
        }

      })

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
      This function is used to insert Faq
  */
  insertFAQ: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      let faq = req.body.faq ? req.body.faq : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " -Frequently Asked Questions",
      });

      if (!req.body.faq) { return; }

      if (faq.length === 0) {
        res.json({
          isError: true,
          message: "Frequently asked questions are not sent",
          statuscode: 204,
          details: null
        });
        return;
      }


      FAQ.insertMany(faq, function (err, response) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: response
          })

          return;
        }
        res.json({
          isError: false,
          message: errorMsgJSON[lang]["200"],
          statuscode: 200,
          details: response
        })

        return;
      })


    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
     Here, we are updating the contact us details
  */
  updateContactUsForMioAdmin: async (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";
    try {

      // Here, we are receiving the cimpany Id of the company
      let companyId = req.body.companyId ? req.body.companyId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " -Company Id",
      });

      let contactNo1 = req.body.contactNo1 ? req.body.contactNo1 : "";

      let contactNo2 = req.body.contactNo2 ? req.body.contactNo2 : "";

      let mailId1 = req.body.mailId1 ? req.body.mailId1 : "";

      let mailId2 = req.body.mailId2 ? req.body.mailId2 : "";

      // Here, Circular json avoided
      if (!req.body.companyId) { return; }

      let fetchContactUsDetailStatus = await OperatorManagementBusiness.fetchContactUsDetailStatus(companyId);


      if (fetchContactUsDetailStatus.isError == true) {
        res.json({
          isError: true,
          message: "Some Error Occur",
          statuscode: 404,
          details: null
        });

        return;
      }

      if (fetchContactUsDetailStatus.isUserExist == false) {
        res.json({
          isError: true,
          message: "No User Exisr",
          statuscode: 204,
          details: null
        });

        return;
      }


      let updateWith = {};
      let contactUs = {};

      if (contactNo1 != "") {
        contactUs.contactNo1 = contactNo1;
      } else {
        contactUs.contactNo1 = fetchContactUsDetailStatus.userDetail[0].contactUs.contactNo1;
      }

      if (contactNo2 != "") {
        contactUs.contactNo2 = contactNo2;
      } else {
        contactUs.contactNo2 = fetchContactUsDetailStatus.userDetail[0].contactUs.contactNo2;
      }

      if (mailId1 != "") {
        contactUs.mailId1 = mailId1;
      } else {
        contactUs.mailId1 = fetchContactUsDetailStatus.userDetail[0].contactUs.mailId1;
      }

      if (mailId2 != "") {
        contactUs.mailId2 = mailId2;
      } else {
        contactUs.mailId2 = fetchContactUsDetailStatus.userDetail[0].contactUs.mailId2;
      }

      updateWith = {
        "contactUs": contactUs
      }

      let updateWhere = {
        "_id": companyId,
      }

      if (Object.keys(updateWith).length === 0) {
        res.json({
          isError: true,
          message: "NO Data Was Sent For Update",
          statuscode: 204,
          details: null
        });
        return;
      }



      CompanyDetail.updateOne(updateWhere, updateWith,
        function (err, response) {

          if (err) {
            res.json({
              isError: true,
              message: "NO Data Was Sent For Update",
              statuscode: 204,
              details: null
            });
            return;
          }

          if (response.nModified == 1) {

            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: response
            });

          } else {

            res.json({
              isError: false,
              message: "Some Problem Occur in Updation",
              statuscode: 204,
              details: null
            });
          }

        }) // END CompanyDetail.updateOne(updateWhere,updateWith, 

    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },

  /*
      This api is written to update the mio admin details
  */
  updateMioAdminDetail: (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      // Here, we are receiving the company Id 
      let companyId = req.body.companyId ? req.body.companyId : res.json({
        isError: true,
        message: errorMsgJSON[lang]["303"] + " - conference Id",
      });

      let createdBy = req.body.createdBy ? req.body.createdBy : "";

      let companyName = req.body.companyName ? req.body.companyName : "";

      let mobile = req.body.mobile ? req.body.mobile : "";

      let countryCode = req.body.countryCode ? req.body.countryCode : "";

      let address = req.body.address ? req.body.address : "";

      let detail1 = req.body.detail1 ? req.body.detail1 : "";

      let detail2 = req.body.detail2 ? req.body.detail2 : "";

      // Here, Circular json avoided
      if (!req.body.companyId) { return; }

      let setValue = {};
      if (createdBy != "") {
        setValue.createdBy = req.body.createdBy;
      }

      if (companyName != "") {
        setValue.companyName = req.body.companyName;
      }

      if (mobile != "") {
        setValue.mobile = req.body.mobile;
      }

      if (countryCode != "") {
        setValue.countryCode = req.body.countryCode;
      }

      if (address != "") {
        setValue.address = req.body.address;
      }

      if (detail1 != "") {
        setValue.detail1 = req.body.detail1;
      }

      if (detail2 != "") {
        setValue.detail2 = req.body.detail2;
      }

      let query = { _id: companyId };

      if (Object.keys(setValue).length === 0) {
        res.json({
          isError: true,
          message: "NO Data Was Sent For Update",
          statuscode: 204,
          details: null
        });
        return;
      }


      let updatevalueswith = { $set: setValue };

      CompanyDetail.updateOne(query, updatevalueswith, function (err, response) {

        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;
        }

        if (response.nModified == 1) {

          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: response
          });

        } else {

          res.json({
            isError: false,
            message: "Some Problem Occur in Updation",
            statuscode: 204,
            details: null
          });
        }

      })


    } catch (error) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },
  //====================================================================
  

  //   for getting details of MIO company--  //
  getMioAdminDetail: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    try {
      
      let companyId = req.body.companyId;

      // if (!req.body.companyId) { return; }
      
      if (companyId != '') {
        
        let getCompanyDetailsStatus = await ContentBusiness.getCompanyDetails(companyId);
        if(getCompanyDetailsStatus.isError){
      
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });
          return ;

        }else{
          if(!getCompanyDetailsStatus.isUserExist){

            res.json({
              isError: false,
              message: errorMsgJSON[lang]["204"],
              statuscode: 204,
              details: null
            });
            return ;

          }else{
            let result = getCompanyDetailsStatus.details;
            
            res.json({
              isError: false,
              message: errorMsgJSON[lang]["200"],
              statuscode: 200,
              details: result
            });
            return ;

          }
        }


      } else {

        let userType = 'ADMIN';
        let getCompanyIdOfMIOStatus = await ContentBusiness.getCompanyIdOfMIO(userType);
        let derivedCompanyId = '';
        
        if(getCompanyIdOfMIOStatus.isError){
          
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["404"],
            statuscode: 404,
            details: null
          });

          return;

        }else{

          if(!getCompanyIdOfMIOStatus.isUserExist){
            
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["404"],
              statuscode: 404,
              details: null
            });
  
            return;
          }else{
            let response = getCompanyIdOfMIOStatus.companyId;

            if(response.length== 0 || !response[0].companyId){
              res.json({
                isError: true,
                message: errorMsgJSON[lang]["404"],
                statuscode: 404,
                details: null
              });
            }else{
              derivedCompanyId = response.length>0 && response[0].companyId;
              // console.log("derived companyId==", derivedCompanyId);

              let getCompanyDetailsStatus = await ContentBusiness.getCompanyDetails(derivedCompanyId);
              if(getCompanyDetailsStatus.isError){
                
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["404"],
                  statuscode: 404,
                  details: null
                });
                return ;

              }else{
                if(!getCompanyDetailsStatus.isUserExist){

                  res.json({
                    isError: false,
                    message: errorMsgJSON[lang]["204"],
                    statuscode: 204,
                    details: null
                  });
                  return ;

                }else{
                  let result = getCompanyDetailsStatus.details;
                  
                  res.json({
                    isError: false,
                    message: errorMsgJSON[lang]["200"],
                    statuscode: 200,
                    details: result
                  });
                  return ;

                }
              }

            }

          }

        }

        
      }


    } catch (error) {
      // console.log("catch-----");
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        message: "Error",
        statuscode: 404,
        details: null
      });
    }
  },



}