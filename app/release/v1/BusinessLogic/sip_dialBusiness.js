//=================================================
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const Conference = require("../models/conference");
const SipCall = require("../models/sipCall");
const Guest = require("../models/guest");
const User = require("../models/user");
const Invitation = require("../models/invitation");
// const bcrypt = require("bcrypt");
const SALT_WORK_FACTOR = 10;
const MailServices = require("../../../services/mailer");
const logger = require("../../../services/logger");

const errorMsgJSON = require("../../../services/errors.json");
const config = require("../config/globals/keyfile");
const AutogenerateIdcontroller = require('../../../services/AutogenerateId');
const Base64ImgUploadmodule = require('../../../services/Base64ImgUploadmodule');

const ConferenceBusiness = require("../BusinessLogic/conferenceBusiness");


var fileupload = require('express-fileupload');

const fs = require('fs');
const AWS = require('aws-sdk');


//=================================================
var OpenTok = require('opentok'),
  opentok = new OpenTok(config.tokbox.apiKey, config.tokbox.apiSecret);

//================================
// Import events module
var events = require('events');

//===========================================

// this function is used to place a call to the user
exports.dialOpentok = async (sessionId, sipToken, sipUri, senderUri) => {	
	return new Promise(function(resolve, reject){
		opentok.dial(sessionId, sipToken, sipUri, {  // start dial function
                    from: senderUri
            },  function (err, sipCall) {// when error occur

	        if (err) {  // When Error Occur
	            resolve({   
	                "ErrorStatus": true,
	            });
	        } else {
	            resolve({   
	                "ErrorStatus": false,
	                "sipCall": sipCall
	            });
	        }
        }) // End dialOpentok Function  
    }) // END Promise	
}

// This function is written add the details of sip call in database
exports.addSipCallDetail = async (toMobile, fromMobile, dialOpentokStatus) => {
    return new Promise(function(resolve, reject){
    	let sipCall = new SipCall({
            toMobile: toMobile,
            fromMobile: fromMobile,
            sipCallId: dialOpentokStatus.sipCall.id,
            sessionId: dialOpentokStatus.sipCall.sessionId, 
            projectId: dialOpentokStatus.sipCall.projectId, 
            connectionId: dialOpentokStatus.sipCall.connectionId, 
            streamId: dialOpentokStatus.sipCall.streamId, 
            createdAt: dialOpentokStatus.sipCall.createdAt,
            updatedAt: dialOpentokStatus.sipCall.updatedAt               
        });

        sipCall.save(function(err, item) {
            if (err) {  // When Error Occur
                resolve({   
                    "ErrorStatus": true,
                });
            } else {
                resolve({   
                    "ErrorStatus": false,
                    "sipCall": item
                });
            }
        });           
    }) // End of Promise 
} // End addSipCallDetail 

// This function is used to check whether the conference is active or not
exports.checkConferenceIsActive = async (sessionId) => {

  return new Promise(function(resolve, reject){

        let aggrQuery = [
            {
                $match: {
                    'sessionId': sessionId,   
                }
            },
            {
                $project: {
                    "hostUserId": 1,
                    "companyId": 1,
                    "isActive": 1
                    
                }
            },
            
        ];

        Conference.aggregate(aggrQuery, function (err, responseConference) {
              if (err) {  // When Error Occur
                  resolve({   
                      "ErrorStatus": true,
                  });
              } else {

                  if (responseConference.length > 0) { // When User Exist With THis Number
                      resolve({   
                          "ErrorStatus": false,
                          "isConferenceExist": true,
                          "responseConference": responseConference
                      });
                  }

                  if (responseConference.length == 0) { // When User Exist With THis Number
                      resolve({   
                          "ErrorStatus": false,
                          "isConferenceExist": false,
                      });
                  }
                  
              }


        });  
              
  }) // End of Promise 
} // End addSipCallDetail 

// This function is written add the details of sip call in database
exports.checkParticipantExist = async (sessionId, toMobile) => {

  return new Promise(function(resolve, reject){

        let aggrQuery = [
            {
                $match: {
                    'sessionId': sessionId,   
                }
            },
            {
                $project: {
                    "hostUserId": 1,
                    "participants": 1
                }
            },
            { $unwind: "$participants" },

            {
                '$match': { 'participants.contactNumber': toMobile.toString() }
            },
        ];

        Conference.aggregate(aggrQuery, function (err, responseConference) {
              if (err) {  // When Error Occur
                  resolve({   
                      "ErrorStatus": true,
                  });
              } else {

                  if (responseConference.length > 0) { // When User Exist With THis Number
                      resolve({   
                          "ErrorStatus": false,
                          "isUserExist": true,
                          "responseConference": responseConference
                      });
                  }

                  if (responseConference.length == 0) { // When User Exist With THis Number
                      resolve({   
                          "ErrorStatus": false,
                          "isUserExist": false,
                      });
                  }
                  
              }


        });  
              
  }) // End of Promise 
} // End addSipCallDetail 


// This function is written update the details of participant in conference
exports.updateParticipantDetail = async (updateWith, updateWhere) => {
    return new Promise(function(resolve, reject) {
        Conference.findOneAndUpdate(updateWhere, updateWith,
            function (updatederror, updatedresponse) {
                if (updatederror) {
                    resolve({   
                        "ErrorStatus": true,
                    });
                }
                if (updatedresponse) {
                    resolve({   
                        "ErrorStatus": false,
                        "updatedresponse": updatedresponse
                    });
                }
        })        
  }) // End of Promise 
} // End addSipCallDetail  

// This function is written fetch the details of participant in conference
exports.fetchParticipantDetail = async (aggrQuery) => {
    return new Promise(function(resolve, reject) {
        Conference.aggregate(aggrQuery, function (err, response) {
            if (err) {
                resolve({   
                    "ErrorStatus": true,
                });
            } else {
                resolve({   
                    "ErrorStatus": false,
                    "response": response
                });
            }
        })    
  }) // End of Promise 
} // End fetchParticipantDetail 


// This function is written fetch the details of participant in conference
exports.addParticipantDetail = async (insertquery, sessionId) => {
// async function addParticipantDetail(insertquery) { 
	console.log("ppppppppppp");
    return new Promise(function(resolve, reject) {

        Conference.findOneAndUpdate({ 'sessionId': sessionId },
            { $push: { "participants": insertquery } },
                function (updatederror, updatedresponse) {
		            if (updatederror) {
		                resolve({   
		                    "ErrorStatus": true,
		                });
		            } else {
		                resolve({   
		                    "ErrorStatus": false,
		                });
		            }
        })    
    }) // End of Promise 
} // End addParticipantDetail 


// this function is used to check whether the mobile number exist in the participant 
exports.checkParticipantInConference = async (conferenceId, toMobile) => {

    return new Promise(function(resolve, reject){
        let aggrQuery = [
            { '$match': { '_id': mongoose.Types.ObjectId(conferenceId) } },
            {
                $project: {
                    "conferenceTitle": 1,
                    "sessionId": 1,
                    "participants": 1
                }
            },

            { $unwind: "$participants" },
            {
              '$match': {
                'participants.contactNumber': toMobile.toString()
              }
            },
        ];

        // Here, we are fetching conference details 
        Conference.aggregate(aggrQuery, function (fetchConferenceErr, fetchConferenceResponse) { // start fetch conference
            if (fetchConferenceErr) { // When some error occur
                resolve({
                    "isError": true,
                });
            }

		    if (fetchConferenceResponse) { // START fetchConferenceResponse

		        if (fetchConferenceResponse.length == 0) { // When no participant available on this number
		            resolve({
		                "isError": false,
		                "isParticipantExist": false,
		            });
		        } // END fetchConferenceResponse.length == 0

		        if (fetchConferenceResponse.length > 0) { // When participant available on this number
		            let sessionId = fetchConferenceResponse[0].sessionId ? fetchConferenceResponse[0].sessionId : "";
		            let connectionId = fetchConferenceResponse[0].participants.connectionId ? fetchConferenceResponse[0].participants.connectionId : "";

		            resolve({
		                "isError": false,
		                "isParticipantExist": true,
		                "sessionId": sessionId,
		                "connectionId": connectionId,
		            });

		        } // END fetchConferenceResponse.length > 0  
		    } // END fetchConferenceResponse
	    })
    })    
} // start of checkParticipantInConference

// this function is used to disconnect the call
exports.disconnectCall = async (sessionId, connectionId) => {  
    return new Promise(function(resolve, reject){
        opentok.forceDisconnect(sessionId, connectionId, function (err) { 
            if (err) { // When some error occur
                resolve({
                    "isError": true,
                });
            } else {
                resolve({
                    "isError": false
                });
            }              
        })
    }) // END  Promise   
} // start of disconnectCall

// this function is used to used to update the field updated at sipCall collection
exports.updateSipCall = async (sessionId, connectionId) => {  
    return new Promise(function(resolve, reject){
        let updatedAt = Date.now();
        let updateWith = {
          "updatedAt": updatedAt.toString(),
        }

        let updateWhere = {
          "sessionId": sessionId,
          "connectionId": connectionId,
        }

        SipCall.updateOne(updateWhere, updateWith, 
          function (updatederror, updatedresponse) {

              if (updatederror) {
                  resolve({
                    "isError": true,
                  });
              } else {
                  resolve({
                    "isError": false,
                  });
              }
        })    
  }) // END  Promise   
} // start of updateSipCall



  // sipDialEndForEndConf: async (conferenceId, toMobile) => {
  exports.sipDialEndForEndConf = async (conferenceId, toMobile) => {
     
          //start executing the operation for each number
          let checkParticipantInConferenceStatus = await this.checkParticipantInConference(conferenceId, toMobile);
          // console.log(checkParticipantInConferenceStatus);


          if (checkParticipantInConferenceStatus.isError == true) { // When Error in fetching the participant
              let updateConferenceStatus = {
                  "isError": true
              } 

              return updateConferenceStatus;  
          }


          if (checkParticipantInConferenceStatus.isParticipantExist == false) {
              let updateConferenceStatus = {
                 "isError": false,
                 "isParticipantExist": false
              } 
              
              return updateConferenceStatus;
          }

          let sessionId = checkParticipantInConferenceStatus.sessionId;
          let connectionId = checkParticipantInConferenceStatus.connectionId;
          

          // Here, we are disconnecting the call
          let disconnectCallStatus = await this.disconnectCall(sessionId, connectionId);

          if (disconnectCallStatus.isError == true) { // Some error in disconnecting the call
              let updateConferenceStatus = {
                 "isError": true,
                 "isParticipantExist": true,
                 "message": "Either the call already disconnected or Some error occur in disconnecting"
              } 

              return updateConferenceStatus;
          }

          // Here, we are update the updateAt status in sipcall connection
          let updateSipCallStatus = await this.updateSipCall(sessionId, connectionId);

          if (updateSipCallStatus.isError == true) {
              let updateConferenceStatus = {
                 "isError": true,
                 "isParticipantExist": false,
                 "message": "Call Disconnected but Sip Call not updated"
              } 

              return updateConferenceStatus;
          } else {
              let updateConferenceStatus = {
                 "isError": true,
                 "isParticipantExist": false,
                 "message": "Call Disconnected Successfully"
              } 

              return updateConferenceStatus;
          }
          
  } // END sipDialEndForEndConf



