const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const User = require("../models/user");
const CompanyDetail = require("../models/companyDetail");
const Guest = require("../models/guest");
// const bcrypt = require("bcrypt");
const SALT_WORK_FACTOR = 10;
const Mailercontroller = require("../../../services/mailer");
const errorMsgJSON = require("../../../services/errors.json");
const config = require("../config/globals/keyfile");
const AutogenerateIdcontroller = require('../../../services/AutogenerateId');


//===========================================

// this function is required to fetch the details of a company on the basis of id
exports.fetchCompanyDetail = async (companyId) => {
    	
  
	return new Promise(function(resolve, reject){
		
       
        let aggrQuery = [
          {
            '$match': { 
              '_id': mongoose.Types.ObjectId(companyId),
              "status": "ACTIVE",
            }  
          }, 

          {
            $project: {
                '_id': 0,
                "status":1,
                "mobile":1,
                "countryCode":1,
            }
          },
           
        ];
        
        CompanyDetail.aggregate(aggrQuery, function (err, response) { // start of fetch function
        	console.log('Company Id==>'+companyId); 
        	console.log('Res==>'+response);
        	
        	if (err) {  // When Error Occur
	            resolve({   
	                "isError": true,
	            });
	        } else {
	        	if (response.length < 1) {
                    resolve({   
		                "isError": false,
		                "CompanyExist": false
		            }); 
	        	} else {
	        		resolve({   
		                "isError": false,
		                "CompanyExist": true,
		                "companyDetail": response
		            }); 
	        	}
	            
	        }
        }) // END CompanyDetail.aggregate(aggrQuery, function (err, response) {
    	
    }) // END Promise
    
}

// this function is required to check Whether the guest exist or not
exports.insertGuestUser = async (userType,name,emailAddress,phoneNumber,countryCode) => {
    let guestUserId = AutogenerateIdcontroller.autogenerateId("MGUSR");

	return new Promise(function(resolve, reject){
        let lastName = '';
        var newGuest = new Guest({
            userType:userType,
            firstName:name,
            lastName: lastName,
            email:emailAddress,
            mobile: phoneNumber, 
            countryCode:countryCode,
            userId:guestUserId,
        });

        newGuest.save(function(err, guest) {	
        	if (err) {  // When Error Occur
	            resolve({   
	                "isError": true,
	            });
	        } else {
	        	resolve({   
	                "isError": false,
	                "guest": guest
	            });  
	        }
        });	
    }) // END Promise	
}

// this function is required to check Whether the guest exist or not
exports.checkGuestExistance = async (email) => {	
	return new Promise(function(resolve, reject){
        
        let aggrQuery = [
            {
	            '$match': { 
	              'email': email,
	            }  
            },   
        ];

        Guest.aggregate(aggrQuery, function (err, response) { // start of fetch function
        	if (err) {  // When Error Occur
	            resolve({   
	                "isError": true,
	            });
	        } else {
	        	if (response.length < 1) {
                    resolve({   
		                "isError": false,
		                "isGuestExist": false
		            }); 
	        	} else {
	        		resolve({   
		                "isError": false,
		                "isGuestExist": true
		            }); 
	        	}      
	        }
        }) // END User.aggregate(aggrQuery, function (fetcherr, fetchresponse) {		
    }) // END Promise	
}

// this function is required to check Whether the address book exist or not
exports.checkExistanceAddressBook = async (emailAddress, userId) => {	
	return new Promise(function(resolve, reject){
        
        let aggrQuery = [
          {
            '$match': { 
              'userId': userId,
            }  
          }, 

          {
            $project: {
                '_id': 0,
                "addressBook":1
            }
          },
          { $unwind : "$addressBook" }, 
          {
            $match: { 'addressBook.emailAddress': emailAddress}  
          }   
        ];

        User.aggregate(aggrQuery, function (err, response) { // start of fetch function
        	if (err) {  // When Error Occur
	            resolve({   
	                "ErrorStatus": true,
	            });
	        } else {
	        	if (response.length < 1) {
                    resolve({   
		                "ErrorStatus": false,
		                "ContactExist": false
		            }); 
	        	} else {
	        		resolve({   
		                "ErrorStatus": false,
		                "ContactExist": true
		            }); 
	        	}
	            
	        }


        }) // END User.aggregate(aggrQuery, function (fetcherr, fetchresponse) {
		
    }) // END Promise	
}

//====================================================

// this function is required to insert the address book to a user
exports.addAddressBookToUser = async (arrAddressBook, userId) => {	
	return new Promise(function(resolve, reject){
		
		let insertquery={
          "name":arrAddressBook.name,
          "phoneNumber":arrAddressBook.phoneNumber,
          "emailAddress":arrAddressBook.emailAddress,
          "userType":arrAddressBook.userType,
          "countryCode":arrAddressBook.countryCode,
          "contactUserId":arrAddressBook.contactUserId
        }
        
		
		User.updateOne({ 'userId': userId }, 
			{ $push: { "addressBook": insertquery } }, 
			function (err, response) {

			if (err) {
	            resolve({   
	                "ErrorStatus": true,
	            });
	        } else {
	            if (response.nModified == 1) {
	                resolve({   
		                "ErrorStatus": false,
		                "isInserted": true,
		            });
	            } else {
	            	resolve({   
		                "ErrorStatus": false,
		                "isInserted": false,
		            });      
                }
            }
	    })			
                      

    }) // END Promise	
}

//====================================================