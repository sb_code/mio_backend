const mongoose = require("mongoose");
const Conference = require("../models/conference");
const User = require("../models/user");
const CompanyDetail = require("../models/companyDetail");

const Sip_dialBusiness = require("./sip_dialBusiness");


// this function is written to remove the user from a conference, assigned as operator
exports.removeUserToConference = async (userId, conferenceId) => {
	
	return new Promise(function(resolve, reject){        
		
		let updateWhere = {
			'_id': conferenceId
		}
        
        Conference.update(
		    updateWhere, 
		    {   $pull: 
		    	{ 
		    		"operator": { "userId": userId },
		    		"participants": { "userId": userId } 
		    	} 
		    },
		    function (err, response) {
		    	console.log("err==>"+err);
			    if (err) {
			        resolve({
			            "isError": true,
			        });
		        }
		        else {
		            resolve({
			            "isError": false,
			        });             
		        } 	
		}) // END  CompanyDetail.update(        
    }) // END Promise	
}


// this function is written to insert the user to a conference as operator
exports.insertUserToConference = async (userId, conferenceId, userDetail) => {
	
	return new Promise(function(resolve, reject){        
		let insertOperator = {
			"userId": userId,
		}

		let insertParticipant = {
	        'name': userDetail.name,
	        'emailAddress': userDetail.email,
	        'companyId': userDetail.companyId,
	        'countryCode': userDetail.countryCode,
	        'contactNumber': userDetail.contactNumber,
	        'userId': userId,
	        'userType': 'USER',
	    }

		let updateWhere = {
			'_id': conferenceId
		}
        
        Conference.update(
		    updateWhere, 
		    { 
		    	$push: 
		    	{   
		    		"operator": insertOperator,
		    	    "participants": insertParticipant 
		    	}	    	
		    },
		    function (err, response) {
		    	console.log("err==>"+err);
			    if (err) {
		          resolve({
		            "isError": true,
		          });
		        }
		        else {
		  
                    if (response.nModified == 1) {
                        resolve({
			                "isError": false,
			                "idUpdated": true,
			                "response": response
			            });
                    } else {
                    	resolve({
			                "isError": false,
			                "idUpdated": false,
			                "response": response
			            });
                    }               
		        } 	
		}) // END  CompanyDetail.update(        
    }) // END Promise	
}

// this function is written to change the isassigned status of the user
exports.unsetUserAssign = async (userId, conferenceId) => {
	
	return new Promise(function(resolve, reject){
	        
		let updateWhere = {
			'userId': userId
		}
        
        User.update(
		    updateWhere,
		    { $set: { isAssigned: false, assignedToConference: '' } },
		    function (err, response) {
		    	console.log("err==>"+err);
			    if (err) {
		            resolve({
		               "isError": true,
		            });
		        }
		        else {
		            resolve({
		               "isError": false,
		            });             
		        } 	
		}) // END  CompanyDetail.update(
	        
    }) // END Promise	
}



// this function is written to change the isassigned status of the user
exports.setUserAssign = async (userId, conferenceId) => {
	
	return new Promise(function(resolve, reject){
	        
		let updateWhere = {
			'userId': userId
		}
        
        User.update(
		    updateWhere,
		    { $set: { isAssigned: true, assignedToConference: conferenceId } },
		    function (err, response) {
		    	console.log("err==>"+err);
			    if (err) {
		          resolve({
		            "isError": true,
		          });
		        }
		        else {
		  
                    if (response.nModified == 1) {
                        resolve({
			                "isError": false,
			                "idUpdated": true,
			                "response": response
			            });
                    } else {
                    	resolve({
			                "isError": false,
			                "idUpdated": false,
			                "response": response
			            });
                    }               
		        } 	
		}) // END  CompanyDetail.update(
	        
    }) // END Promise	
}



// this function is written to fetch the user details
exports.fetchUserDetail = async (userId) => {
	
	return new Promise(function(resolve, reject){
	        
    	let aggrQuery = [
            { 
                '$match': { 
                	'userId': userId,
                	'isActive': true
                }
            },

            {
	            $project: {
	              '_id': 0,
	              'isActive': 1,
	              'userType': 1,
	              'isAssigned': 1,
	              'companyId': 1,
	              'userId': 1,
	              'fname': 1,
	              'lname': 1,
	              'email': 1,
	              'mobile': 1,
	              'countryCode': 1,
	              'name': { $concat: [ "$fname", " ", "$lname" ] }
	            }
	        },         
        ];

        User.aggregate(aggrQuery, function (err, response) {

	        if (err) {
	          resolve({
	            "isError": true,
	          });
	        }
	        else {

	        	if (response.length < 1) {
	        		resolve({
		               "isError": false,
		               "isUserExist": false
		            });
	        	}

	        	if (response.length > 0) {
	        		resolve({
		               "isError": false,
		               "isUserExist": true,
		               "userDetail": response
		            });
	        	}
	               
	        }
        }) //END User.aggregate
	        
    }) // END Promise	
}

// this function is written to un assign the user from a company
exports.unAssignUserToCompany = async (userId, companyId, conferenceId) => {
	let endTime = new Date().getTime();		
	return new Promise(function(resolve, reject){
		let updateWith = {
	        "operator.$.status": "WITH_DRAWN",
	        "operator.$.endTime": endTime,
	    }

	    let updateWhere = {
	        "_id": companyId,
	        "operator.userId": userId,
	    }

        CompanyDetail.updateOne(
        	updateWhere, 
        	updateWith, 
        	function (err, response) {
		    	console.log("err==>"+err);
			    if (err) {
		            resolve({
		              "isError": true,
		            });
		        }
		        else {
		            resolve({
		                "isError": false,
		            });                               
		        } 	
		}) // END  CompanyDetail.update(
	        
    }) // END Promise	
}

// this function is written to assign the user to a company
exports.assignUserToCompany = async (userId, companyId, conferenceId) => {
	let startTime = new Date().getTime();
	
		
	return new Promise(function(resolve, reject){
	        
    	let insertOperator = {
			"userId": userId,
			"conferenceId": conferenceId,
			"startTime": startTime,
			"assignToCompany": "SELF",
			"endTime": '',
			"status": 'ASSIGNED'
		}

		let updateWhere = {
			'_id': companyId
		}
        
        CompanyDetail.update(
		    updateWhere, 
		    { $push: { "operator": insertOperator } },
		    function (err, response) {
		    	console.log("err==>"+err);
			    if (err) {
		          resolve({
		            "isError": true,
		          });
		        }
		        else {
		  
                    if (response.nModified == 1) {
                        resolve({
			                "isError": false,
			                "idUpdated": true,
			                "response": response
			            });
                    } else {
                    	resolve({
			                "isError": false,
			                "idUpdated": false,
			                "response": response
			            });
                    }               
		        } 	
		}) // END  CompanyDetail.update(
	        
    }) // END Promise	
}

// this function is written to update assignment details of the user to a company
exports.updateAssignmentDetailToUserCompany = async (userId, companyId, conferenceId, companyIdofUser) => {
	let endTime = new Date().getTime();
	
		
	return new Promise(function(resolve, reject){
	        
    	let updateWith = {
	        "operator.$.status": "WITH_DRAWN",
	        "operator.$.endTime": endTime,
	    }

	    let updateWhere = {
	        "_id": companyIdofUser,
	        "operator.userId": userId,
	    }

        CompanyDetail.updateOne(
        	updateWhere, 
        	updateWith, 
        	function (err, response) {
		    	console.log("err==>"+err);
			    if (err) {
		            resolve({
		              "isError": true,
		            });
		        }
		        else {
		            resolve({
		                "isError": false,
		            });                               
		        } 	
		}) // END  CompanyDetail.update(
	        
    }) // END Promise	
}

// this function is written to assign the user to a company
exports.insertAssignmentDetailToUserCompany = async (userId, companyId, conferenceId, companyIdofUser) => {
	let startTime = new Date().getTime();
	
		
	return new Promise(function(resolve, reject){
	        
    	let insertOperator = {
			"userId": userId,
			"conferenceId": conferenceId,
			"startTime": startTime,
			"assignToCompany": companyId,
			"endTime": '',
			"status": 'ASSIGNED'
		}

		let updateWhere = {
			'_id': companyIdofUser
		}
        
        CompanyDetail.update(
		    updateWhere, 
		    { $push: { "operator": insertOperator } },
		    function (err, response) {
		    	console.log("err==>"+err);
			    if (err) {
		          resolve({
		            "isError": true,
		          });
		        }
		        else {
		  
                    if (response.nModified == 1) {
                        resolve({
			                "isError": false,
			                "idUpdated": true,
			                "response": response
			            });
                    } else {
                    	resolve({
			                "isError": false,
			                "idUpdated": false,
			                "response": response
			            });
                    }               
		        } 	
		}) // END  CompanyDetail.update(
	        
    }) // END Promise	
}

//=================================================================================

// this function is written to assign the user to a company
exports.fetchContactUsDetailStatus = async (companyId) => {
		
	return new Promise(function(resolve, reject){
		let aggrQuery = [
			{
				$match: {
					'_id': mongoose.Types.ObjectId(companyId),
				},
			},

			{
	            $project: {
	              '_id': 0,
	              'status': 1,
	              'contactUs': 1,             
	            }
	        },     
		];

		CompanyDetail.aggregate(aggrQuery,
			function (err, response) {
				if (err) {
					resolve({
						"isError": true,
					});
				} else {
					if (response.length < 1) {
						resolve({
							"isError": false,
							"isUserExist": false,
							"userDetail": response
						});
					} else {
						resolve({
							"isError": false,
							"isUserExist": true,
							"userDetail": response
						});
					}

				}
			});
	}) // END Promise  
	       
}

