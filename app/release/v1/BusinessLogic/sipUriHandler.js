
const uriConfig = (toContactNumber, provider) => {

  // List of SIP providerds URI
  let outboundProviderUri = {
    "VEZETI": `sip:${toContactNumber}@178.62.27.239`,
    "DAISY": `sip:+${toContactNumber}@31.13.156.183`
  }
  return outboundProviderUri[provider];
};

const getOutboundOption = (fromContactNumber, provider) => {
  // List of SIP providerds Option
  let outboundProviderOption = {
    "VEZETI": { from: `${fromContactNumber}@example.com` },
    "DAISY": { from: `${fromContactNumber}@example.com` }
  }
  return outboundProviderOption[provider];
}

const getProvider = (countryCode) => {
  // List of SIP providers
  let sipProviders = {
    "234": "VEZETI",
    "OTHER": "DAISY"
  }
  if (Object.keys(sipProviders).includes(countryCode)) {
    return sipProviders[countryCode];
  } else {
    return sipProviders["OTHER"];
  }
}

module.exports = {
  getOutboundConfig: ({ toCountryCode, toMobile, fromCountryCode, fromMobile }) => {
    let sipConfig = {
      sipUri: uriConfig(`${toCountryCode}${toMobile}`, getProvider(toCountryCode)),
      option: getOutboundOption(`${fromCountryCode}${fromMobile}`, getProvider(toCountryCode)),
    }
    return sipConfig;
  }
}