const mongoose = require("mongoose");
const Conference = require("../models/conference");
const ConferenceDuration = require("../models/conferenceDuration");
const SipCall = require("../models/sipCall");
const Invoice = require("../models/invoice");
const CompanyDetail = require("../models/companyDetail");
const WebCall = require("../models/webCall");
const Sip_dialBusiness = require("./sip_dialBusiness");
const Archives = require("../models/archives");
var Request = require('request');





// This Function is written to fetch the exchange rate
exports.getExchangeRate = async (currencyTo) => {
	
	return new Promise(function (resolve, reject) {

		if (currencyTo == 'NGN') {

			// When CutrrencyTo is NGN		
			Request.get({
				"url": `http://apilayer.net/api/live?access_key=77aa84ff99bdf3e882f4dde85af80591&currencies=NGN&source=USD&format=1`,           
	        }, (error, response, body) => {
	            if (error) {
	                resolve({
						"isError": true,
					});
	            } else {
	            	resolve({
						"isError": false,
						"response": response.body
					});
	            }
	            
	        });

		} else {

			// When CutrrencyTo is GBP, USD, EUR		
			Request.get({
	            "url": `https://api.exchangeratesapi.io/latest?base=USD&symbols=`+currencyTo+``,           
	        }, (error, response, body) => {
	            if (error) {
	                resolve({
						"isError": true,
					});
	            } else {
	            	resolve({
						"isError": false,
						"response": response.body
					});
	            }
	            
	        });

		}   
    }); // END return new Promise(function (resolve, reject) {
}



// This Function is written to fetch all conference in the basis of companyId
exports.fetchAllConferenceDuration = async (companyId, months, year) => {
	return new Promise(function (resolve, reject) {

		let aggrQuery = [
			{
				$match: {
					'companyId': companyId,
					'conferenceStatus': "ENDED",					
				},
			},

			{
				$project: {
					"_id": 1,
                    "companyId": 1,
			        "date": 1,
			        "month": { $month: "$date" },
			        "year": { $year: "$date" }
					
				}
			},		
			{
				$match: {
					'month': { $in: months },
					'year': year,		
				},
			},	   
		];

		ConferenceDuration.aggregate(aggrQuery,
			function (err, response) {
				if (err) {
					resolve({
						"isError": true,
					});
				} else {
					if (response.length < 1) {
						resolve({
							"isError": false,
							"isConfExist": false,
							"conferenceDurationDetail": response
						});
					} else {
						resolve({
							"isError": false,
							"isConfDurationExist": true,
							"conferenceDurationDetail": response
						});
					}

				}
			});
	}) // END Promise  
}

// This Function is written to fetch all conference in the basis of companyId
exports.fetchAllConference = async (companyId, months, year) => {
	return new Promise(function (resolve, reject) {

		let aggrQuery = [
			{
				$match: {
					'companyId': companyId,
					'isDeleted': false,					
				},
			},

			{
				$project: {
					"_id": 1,
                    "companyId": 1,
					"sessionId": 1,
					"callType": 1,
					"hostUserId": 1,
					"conferenceTitle": 1,
			        "date": 1,
			        "month": { $month: "$date" },
			        "year": { $year: "$date" }
					
				}
			},		
			{
				$match: {
					'month': { $in: months },
					'year': year,		
				},
			},	   
		];

		Conference.aggregate(aggrQuery,
			function (err, response) {
				if (err) {
					resolve({
						"isError": true,
					});
				} else {
					if (response.length < 1) {
						resolve({
							"isError": false,
							"isConfExist": false,
							"conferenceDetail": response
						});
					} else {
						resolve({
							"isError": false,
							"isConfExist": true,
							"conferenceDetail": response
						});
					}

				}
			});
	}) // END Promise  
}


// This Function is written to fetch all sip call details
exports.fetchAllArchiveDetailForDetail = async (companyId, months, year) => {
	return new Promise(function (resolve, reject) {

		let aggrQuery = [
			{
				$match: {
					'companyId': companyId,
					'isDeleted': false,					
				},
			},

			{
				$project: {
					"_id": 1,
                    "companyId": 1,
					"sessionId": 1,
					"callType": 1,
					"hostUserId": 1,
					"conferenceTitle": 1,
					
				}
			},

			
			{
			    $lookup: {
			        from: 'conferencedurations',
			        localField: 'sessionId',
			        foreignField: 'sessionId',
			        as: 'confDuration'
			    }
		    },

		    {   $unwind:"$confDuration" },

		    {
				$match: {
					'confDuration.conferenceStatus': 'ENDED',		
				},
			},

		    {
				$project: {
                    "companyId": 1,
					"sessionId": 1,
					"callType": 1,
					"hostUserId": 1,
					"conferenceTitle": 1,
					"confDuration._id": 1,
					"confDuration.date": 1,
					"confDuration.endedAt": 1,
					"confDuration.createdAt": 1,
					"confDuration.updatedAt": 1,

				}
			},
			
			{
			    $lookup: {
			        from: 'archives',
			        localField: 'confDuration._id',
			        foreignField: 'conferenceDurationId',
			        as: 'archivesDetail'
			    }
		    },

		    {   $unwind:"$archivesDetail" },

		    {
				$project: {
                    "companyId": 1,
					"sessionId": 1,
					"callType": 1,
					"hostUserId": 1,
					"conferenceTitle": 1,
					"confDuration._id": 1,
					"confDuration.date": 1,
					"confDuration.endedAt": 1,
					"confDuration.createdAt": 1,
					"confDuration.updatedAt": 1,
					"archivesDetail._id": 1,
					"archivesDetail.updatedAt": 1,
					"archivesDetail.createdAt": 1,
					"archivesDetail.status": 1,
					"archivesDetail.name": 1,
					"archivesDetail.duration": 1,
					"month": { $month: "$archivesDetail.createdAt" },
					"year": { $year: "$archivesDetail.createdAt" }

				}
			},

			{
				$match: {
					'archivesDetail.status': 'stopped',
					'month': { $in: months },
					'year': year,		
				},
			},

		   
		];

		Conference.aggregate(aggrQuery,
			function (err, response) {
				if (err) {
					resolve({
						"isError": true,
					});
				} else {
					if (response.length < 1) {
						resolve({
							"isError": false,
							"isConfExist": false,
							"conferenceDetail": response
						});
					} else {
						resolve({
							"isError": false,
							"isConfExist": true,
							"conferenceDetail": response
						});
					}

				}
			});
	}) // END Promise  
}


// This Function is written to fetch all sip call details
exports.fetchAllWebCallDetailForDetail = async (companyId, months, year) => {
	return new Promise(function (resolve, reject) {

		let aggrQuery = [
			{
				$match: {
					'companyId': companyId,
					'isDeleted': false,					
				},
			},

			{
				$project: {
					"_id": 1,
                    "companyId": 1,
					"sessionId": 1,
					"callType": 1,
					"hostUserId": 1,
					"conferenceTitle": 1,
					
				}
			},

			
			{
			    $lookup: {
			        from: 'conferencedurations',
			        localField: 'sessionId',
			        foreignField: 'sessionId',
			        as: 'confDuration'
			    }
		    },

		    {   $unwind:"$confDuration" },

		    {
				$match: {
					'confDuration.conferenceStatus': 'ENDED',		
				},
			},

		    {
				$project: {
                    "companyId": 1,
					"sessionId": 1,
					"callType": 1,
					"hostUserId": 1,
					"conferenceTitle": 1,
					"confDuration._id": 1,
					"confDuration.date": 1,
					"confDuration.endedAt": 1,
					"confDuration.createdAt": 1,
					"confDuration.updatedAt": 1,

				}
			},
			
			{
			    $lookup: {
			        from: 'webcalls',
			        localField: 'confDuration._id',
			        foreignField: 'conferenceDurationId',
			        as: 'webCallDetail'
			    }
		    },

		    {   $unwind:"$webCallDetail" },


		    {
				$project: {
                    "companyId": 1,
					"sessionId": 1,
					"callType": 1,
					"hostUserId": 1,
					"conferenceTitle": 1,
					"confDuration._id": 1,
					"confDuration.date": 1,
					"confDuration.endedAt": 1,
					"confDuration.createdAt": 1,
					"confDuration.updatedAt": 1,
					"webCallDetail._id": 1,
					"webCallDetail.date": 1,
					"webCallDetail.updatedAt": 1,
					"webCallDetail.createdAt": 1,
					"webCallDetail.conferenceStatus": 1,
					"webCallDetail.connectionId": 1,
					"webCallDetail.streamId": 1,
					"month": { $month: "$webCallDetail.date" },
					"year": { $year: "$webCallDetail.date" }

				}
			},

			{
				$match: {
					'webCallDetail.conferenceStatus': 'ENDED',
					'month': { $in: months },
					'year': year,		
				},
			},

		   
		];

		Conference.aggregate(aggrQuery,
			function (err, response) {
				if (err) {
					resolve({
						"isError": true,
					});
				} else {
					if (response.length < 1) {
						resolve({
							"isError": false,
							"isConfExist": false,
							"conferenceDetail": response
						});
					} else {
						resolve({
							"isError": false,
							"isConfExist": true,
							"conferenceDetail": response
						});
					}

				}
			});
	}) // END Promise  
}

// This Function is written to fetch all sip call details
exports.fetchAllSipCallDetailForDetail = async (companyId, months, year) => {
	return new Promise(function (resolve, reject) {

		let aggrQuery = [
			{
				$match: {
					'companyId': companyId,
					'isDeleted': false,					
				},
			},

			{
				$project: {
                    "companyId": 1,
					"sessionId": 1,
					"callType": 1,
					"hostUserId": 1,
					"conferenceTitle": 1,
				}
			},

			
			{
			    $lookup: {
			        from: 'conferencedurations',
			        localField: 'sessionId',
			        foreignField: 'sessionId',
			        as: 'confDuration'
			    }
		    },

		    {   $unwind:"$confDuration" },

		    {
				$match: {
					'confDuration.conferenceStatus': 'ENDED',		
				},
			},

		    {
				$project: {
                    "companyId": 1,
					"sessionId": 1,
					"callType": 1,
					"hostUserId": 1,
					"conferenceTitle": 1,
					"confDuration._id": 1,
					"confDuration.date": 1,
					"confDuration.endedAt": 1,
					"confDuration.createdAt": 1,
					"confDuration.updatedAt": 1,

				}
			},

			{
			    $lookup: {
			        from: 'sipcalls',			        
			        localField: 'confDuration._id',
			        foreignField: 'conferenceDurationId',
			        as: 'sipCallDetail'
			    }
		    },

		    {   $unwind:"$sipCallDetail" },


		    {
				$project: {
                    "companyId": 1,
					"sessionId": 1,
					"callType": 1,
					"hostUserId": 1,
					"conferenceTitle": 1,
					"confDuration._id": 1,
					"confDuration.date": 1,
					"confDuration.endedAt": 1,
					"confDuration.createdAt": 1,
					"confDuration.updatedAt": 1,
					"sipCallDetail._id": 1,
					"sipCallDetail.toMobile": 1,
					"sipCallDetail.countryCode": 1,
					"sipCallDetail.date": 1,
					"sipCallDetail.updatedAt": 1,
					"sipCallDetail.createdAt": 1,
					"sipCallDetail.callStatus": 1,
					"sipCallDetail.connectionId": 1,
					"sipCallDetail.streamId": 1,
					"month": { $month: "$sipCallDetail.date" },
					"year": { $year: "$sipCallDetail.date" }

				}
			},

			{
				$match: {
					'sipCallDetail.callStatus': 'ENDED',
					'month': { $in: months },
					'year': year,		
				},
			},

		   
		];

		Conference.aggregate(aggrQuery,
			function (err, response) {
				if (err) {
					resolve({
						"isError": true,
					});
				} else {
					if (response.length < 1) {
						resolve({
							"isError": false,
							"isConfExist": false,
							"conferenceDetail": response
						});
					} else {
						resolve({
							"isError": false,
							"isConfExist": true,
							"conferenceDetail": response
						});
					}

				}
			});
	}) // END Promise  
}

// this function is used to fetch the details of the invoice for particular month on basis of company
exports.fetchInvoiceDetail = async (companyId, month, year) => {
	return new Promise(function (resolve, reject) {

		let aggrQuery = [
			{
				$match: {
					'companyId': companyId,
					'month': month.toString(),
					'year': year.toString(),
				},
			},
		];

		Invoice.aggregate(aggrQuery,
			function (err, response) {
				if (err) {
					resolve({
						"isError": true,
					});
				} else {
					if (response.length < 1) {
						resolve({
							"isError": false,
							"isInvoicedExist": false,
							"invoiceDetail": response
						});
					} else {
						resolve({
							"isError": false,
							"isInvoicedExist": true,
							"invoiceDetail": response
						});
					}

				}
			});
	}) // END Promise  
}

// this function is used to fetch the details of the company
exports.insertInvoiceDetail = async (companyId, month, year, serviceDetail) => {
	return new Promise(function (resolve, reject) {

		let invoice1 = new Invoice({
			companyId: companyId,
			month: month,
			year: year,
			services: serviceDetail
		});

		invoice1.save(function (err, response) {
			if (err) {
				resolve({
					"isError": true,
				});
			} else {
				resolve({
					"isError": false,
					"response": response,
				});
			}
		});
	}) // END Promise  
}
//============================================================================

// this function is used to fetch the details of the company
exports.fetchCompanyDetail = async (companyId, months = [], year = 2020) => {
	return new Promise(function (resolve, reject) {

		let aggrQuery = [
			{
				$match: {
					'_id': mongoose.Types.ObjectId(companyId),
					'status': 'ACTIVE',
				},

			},

			{
				$project: {
					"services": 1,
                    "companyId": 1,
                    "vat": 1,
					"companyName": 1,
					"createdAt": 1,
					"updatedAt": 1,
					"currency": 1
				}
			},

		];

		CompanyDetail.aggregate(aggrQuery,
			function (err, response) {
				// console.log('response', response);

				if (err) {
					resolve({
						"isError": true,
					});
				} else {
					if (response.length < 1) {
						resolve({
							"isError": false,
							"iscompanyExist": false
						});
					} else {
						resolve({
							"isError": false,
							"iscompanyExist": true,
							"companyDetail": response
						});
					}

				}
			});
	}) // END Promise  
}
//============================================================================


// this function is used to fetch duration of the conference for Archives
exports.fetchArchivesForCompany = async (companyId, months = [], year = 2020) => {
	return new Promise(function (resolve, reject) {
		let aggrQuery = [
			{
				$match: {
					'companyId': companyId,
					'status': 'stopped',
				},

			},

			{
				$project: {
					'_id': 0,
					"name": 1,
					"duration": 1,
					"createdAt": 1,
					"updatedAt": 1,
					"status": 1,
					"month": { $month: "$createdAt" },
					"year": { $year: "$createdAt" }
				}
			},
			{
				$match: {
					'month': { $in: months },
					'year': year,
				},
			},

		];

		Archives.aggregate(aggrQuery,
			function (err, response) {
				if (err) {
					resolve({
						"isError": true,
					});
				} else {
					if (response.length < 1) {
						resolve({
							"isError": false,
							"isConfDurationExist": false
						});
					} else {
						resolve({
							"isError": false,
							"isConfDurationExist": true,
							"confDuration": response
						});
					}

				}
			});
	}) // END Promise  
}
//============================================================================
// this function is used to fetch duration of the conference for Sip call
exports.fetchSipCallForCompanyOrderByConfDuration = async (companyId, months = [], year = 2020) => {
	return new Promise(function (resolve, reject) {
		let aggrQuery = [
			{
				$match: {
					'companyId': companyId,
					'callStatus': 'ENDED',
				},

			},

			

			{
				$project: {
					'_id': 0,
					"createdAt": 1,
					"updatedAt": 1,
					"callStatus": 1,
					"sessionId": 1,
					"date": 1,
					"conferenceDurationId": 1,
					"month": { $month: "$date" },
					"year": { $year: "$date" }
				}
			},

			{
			      $lookup:
			         {
			            from: "conferences",
			            localField: "sessionId",
			            foreignField: "sessionId",
			            as: "sipCallDetail"
			        }
			},

			{
				$project: {
					
					"createdAt": 1,
					"updatedAt": 1,
					"callStatus": 1,
					"sessionId": 1,
					"date": 1,
					"conferenceDurationId": 1,
					"month": { $month: "$date" },
					"year": { $year: "$date" },
					"sipCallDetail._id": 1
				}
			},
            { 
            	$sort : { 
            		"sipCallDetail._id" : 1, 
            		"conferenceDurationId": 1 
            	} 
            },

			{
				$match: {
					'month': { $in: months },
					'year': year,
				},
			},

		];

		SipCall.aggregate(aggrQuery,
			function (err, response) {
				if (err) {
					resolve({
						"isError": true,
					});
				} else {
					if (response.length < 1) {
						resolve({
							"isError": false,
							"isConfDurationExist": false
						});
					} else {
						resolve({
							"isError": false,
							"isConfDurationExist": true,
							"sipCallList": response
						});
					}

				}

			});

	}) // END Promise  
}

//============================================================================

// this function is used to fetch duration of the conference for Sip call
exports.fetchSipCallForCompany = async (companyId, months = [], year = 2020) => {
	return new Promise(function (resolve, reject) {
		let aggrQuery = [
			{
				$match: {
					'companyId': companyId,
					'callStatus': 'ENDED',
				},

			},

			{
				$project: {
					'_id': 0,
					"createdAt": 1,
					"updatedAt": 1,
					"callStatus": 1,
					"date": 1,
					"month": { $month: "$date" },
					"year": { $year: "$date" }
				}
			},
			{
				$match: {
					'month': { $in: months },
					'year': year,
				},
			},

		];

		SipCall.aggregate(aggrQuery,
			function (err, response) {
				if (err) {
					resolve({
						"isError": true,
					});
				} else {
					if (response.length < 1) {
						resolve({
							"isError": false,
							"isConfDurationExist": false
						});
					} else {
						resolve({
							"isError": false,
							"isConfDurationExist": true,
							"confDuration": response
						});
					}

				}

			});

	}) // END Promise  
}
//============================================================================
// this function is used to fetch duration of the conference for Web call
exports.fetchWebCallForCompanyOrderByConf = async (companyId, months = [], year = 2020) => {
	return new Promise(function (resolve, reject) {
		let aggrQuery = [
			{
				$match: {
					'companyId': companyId,
					'conferenceStatus': 'ENDED',
				},

			},

			{
				$project: {
					'_id': 0,
					"startedAt": 1,
					"endedAt": 1,
					"conferenceId": 1,
					"conferenceDurationId": 1,
					"conferenceStatus": 1,
					"date": 1,
					"month": { $month: "$date" },
					"year": { $year: "$date" }
				}
			},
			{ 
            	$sort : { 
            		"conferenceId" : 1, 
            		"conferenceDurationId": 1 
            	} 
            },
			{
				$match: {
					'month': { $in: months },
					'year': year,
				},
			},

		];

		WebCall.aggregate(aggrQuery,
			function (err, response) {
				if (err) {
					resolve({
						"isError": true,
					});
				} else {
					if (response.length < 1) {
						resolve({
							"isError": false,
							"isConfDurationExist": false
						});
					} else {
						resolve({
							"isError": false,
							"isConfDurationExist": true,
							"webCallList": response
						});
					}

				}

			});

	}) // END Promise  
}


//=============================================================================
// this function is used to fetch duration of the conference for Web call
exports.fetchWebCallForCompany = async (companyId, months = [], year = 2020) => {
	return new Promise(function (resolve, reject) {
		let aggrQuery = [
			{
				$match: {
					'companyId': companyId,
					'conferenceStatus': 'ENDED',
				},

			},

			{
				$project: {
					'_id': 0,
					"startedAt": 1,
					"endedAt": 1,
					"conferenceStatus": 1,
					"date": 1,
					"month": { $month: "$date" },
					"year": { $year: "$date" }
				}
			},
			{
				$match: {
					'month': { $in: months },
					'year': year,
				},
			},

		];

		WebCall.aggregate(aggrQuery,
			function (err, response) {
				if (err) {
					resolve({
						"isError": true,
					});
				} else {
					if (response.length < 1) {
						resolve({
							"isError": false,
							"isConfDurationExist": false
						});
					} else {
						resolve({
							"isError": false,
							"isConfDurationExist": true,
							"confDuration": response
						});
					}

				}

			});

	}) // END Promise  
}
//============================================================================


// this function is written to insert the duration detail of a particular Web Call of a conference
exports.insertWebCallDetail = async (conferenceId, sessionId, conferenceDurationId, streamId, connectionId, companyId, userId) => {
	let startedAt = new Date().getTime();

	return new Promise(function (resolve, reject) {

		let webCall = new WebCall({
			conferenceId: conferenceId,
			sessionId: sessionId,
			startedAt: startedAt,
			conferenceDurationId: mongoose.Types.ObjectId(conferenceDurationId),
			companyId: companyId,
			streamId: streamId,
			userId : userId,
			connectionId: connectionId
		});

		webCall.save(function (err, item) {
			if (err) {  // When Error Occur
				resolve({
					"isError": true,
				});
			} else {
				resolve({
					"isError": false,
					"webCall": item
				});
			}
		}); // END conferenceDuration.save

	}) // END Promise  
}


// this function is written to insert the duration detail of conference
exports.insertConferenceDetail = async (conferenceId, sessionId, companyId) => {
	let startedAt = new Date().getTime();


	return new Promise(function (resolve, reject) {

		let conferenceDuration = new ConferenceDuration({
			conferenceId: conferenceId,
			sessionId: sessionId,
			companyId: companyId,
			startedAt: startedAt
		});

		conferenceDuration.save(function (err, item) {
			if (err) {  // When Error Occur
				resolve({
					"isError": true,
				});
			} else {
				resolve({
					"isError": false,
					"confDuration": item
				});
			}
		}); // END conferenceDuration.save

	}) // END Promise	
}

// this function is written to update the Sip call detail of conference
exports.updateSipCallForConf = async (conferenceDurationId) => {
	let updatedAt = new Date().getTime();
	console.log('updateWebCall ===>' + conferenceDurationId);
	return new Promise(function (resolve, reject) {
		let updateWith = {
			"updatedAt": updatedAt,
			"callStatus": "ENDED"
		}

		let updateWhere = {
			"conferenceDurationId": conferenceDurationId,
			"callStatus": "STARTED",
		}

		SipCall.updateMany(updateWhere, updateWith,
			function (updatederror, updatedresponse) {

				if (updatederror) {
					resolve({
						"isError": true,
					});
				} else {
					resolve({
						"isError": false,
					});
				}
			})

	}) // END Promise  
}


// this function is written to update the Web call detail of conference
exports.updateWebCall = async (conferenceId, conferenceDurationId) => {
	let endedAt = new Date().getTime();
	console.log('updateWebCall ===>' + conferenceId);
	return new Promise(function (resolve, reject) {
		let updateWith = {
			"endedAt": endedAt,
			"conferenceStatus": "ENDED"
		}

		let updateWhere = {
			"conferenceId": conferenceId,
			"conferenceDurationId": conferenceDurationId,
			"conferenceStatus": "STARTED",
		}

		WebCall.updateMany(updateWhere, updateWith,
			// WebCall.update(updateWhere, updateWith, 
			function (updatederror, updatedresponse) {

				if (updatederror) {
					resolve({
						"isError": true,
					});
				} else {
					resolve({
						"isError": false,
					});
				}
			})

	}) // END Promise  
}


// this function is written to update the duration detail of conference
exports.updateConferenceDuration = async (conferenceId, conferenceDurationId) => {
	let endedAt = new Date().getTime();
	console.log('updateConferenceDuration ===>' + conferenceDurationId);
	return new Promise(function (resolve, reject) {
		let updateWith = {
			"endedAt": endedAt,
			"conferenceStatus": "ENDED"
		}

		let updateWhere = {
			"conferenceId": conferenceId,
			"_id": conferenceDurationId,
			"conferenceStatus": "STARTED",
		}

		ConferenceDuration.update(updateWhere, updateWith,
			function (updatederror, updatedresponse) {

				if (updatederror) {
					resolve({
						"isError": true,
					});
				} else {
					resolve({
						"isError": false,
					});
				}
			})

	}) // END Promise	
}

// this function is used to fetch duration of the conference for sip call
exports.fetchConferenceDurationForSipCall = async (sessionId) => {

	return new Promise(function (resolve, reject) {
		let aggrQuery = [
			{
				$match: {
					'sessionId': sessionId,
					'conferenceStatus': 'STARTED',
				},

			},

			{ $sort: { _id: -1 } },

			{ $limit: 1 },

			{
				$project: {
					'_id': 1,
					"conferenceId": 1,
					"companyId": 1,
					"sessionId": 1,
					"date": 1
				}
			},
		];

		ConferenceDuration.aggregate(aggrQuery,
			function (err, response) {
				if (err) {
					resolve({
						"isError": true,
					});
				} else {
					if (response.length < 1) {
						resolve({
							"isError": false,
							"isConfDurationExist": false
						});
					} else {
						resolve({
							"isError": false,
							"isConfDurationExist": true,
							"confDuration": response
						});
					}

				}

			});

	}) // END Promise  
}


// this function is used to fetch duration of the conference
exports.fetchConferenceDuration = async (conferenceId) => {

	return new Promise(function (resolve, reject) {
		let aggrQuery = [
			{
				$match: {
					'conferenceId': conferenceId,
					'conferenceStatus': 'ENDED',
				}
			},
			{
				$project: {
					'_id': 0,
					"conferenceId": 1,
					"sessionId": 1,
					"endedAt": 1,
					"startedAt": 1,
				}
			},

		];

		ConferenceDuration.aggregate(aggrQuery,
			function (err, response) {
				if (err) {
					resolve({
						"isError": true,
					});
				} else {
					if (response.length < 1) {
						resolve({
							"isError": false,
							"isConfDurationExist": false
						});
					} else {
						resolve({
							"isError": false,
							"isConfDurationExist": true,
							"confDuration": response
						});
					}

				}

			});

	}) // END Promise  
}


// this function is used to fetch duration of the conference
exports.getDurationForSipForMobile = async (conferenceId, toNumber) => {

	return new Promise(function (resolve, reject) {
		let aggrQuery = [
			{
				$match: {
					'conferenceId': conferenceId,
					'conferenceStatus': 'ENDED',
				}
			},
			{
				$project: {
					'_id': 0,
					"conferenceId": 1,
					"sessionId": 1,
					"endedAt": 1,
					"startedAt": 1,
				}
			},

		];

		ConferenceDuration.aggregate(aggrQuery,
			function (err, response) {
				if (err) {
					resolve({
						"isError": true,
					});
				} else {
					if (response.length < 1) {
						resolve({
							"isError": false,
							"isConfDurationExist": false
						});
					} else {
						resolve({
							"isError": false,
							"isConfDurationExist": true,
							"confDuration": response
						});
					}

				}

			});

	}) // END Promise  
}





