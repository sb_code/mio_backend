const config = require("../config/globals/keyfile");

const paystack = require("paystack")(config.PAYSTACK.secretKey);
const stripe = require("stripe")(config.STRIPE.secretKey);

const CompanyDetail = require("../models/companyDetail");
const Transaction = require("../models/transaction");
const Invoice = require("../models/invoice");
const User = require("../models/user");


module.exports = {
  /**
   * Create new customer in paystack and save in db
   */
  createCustomerPaystack: async (fname, lname, email, userId, companyId) => {
    return new Promise((resolve, reject) => {
      paystack.customer.create({
        first_name: fname,
        last_name: lname,
        email: email,
        metadata: { userId, companyId }
      }).then(function (error, body) {
        if (error) {
          console.log('error', error);
          reject(error);
        } else {
          console.log('body', body);
          let query = { _id: companyId };
          let updatevalueswith = { $set: { paystackCustomerId: body.data.customer_code } };
          CompanyDetail.updateOne(query, updatevalueswith, (err, response) => {
            if (err) {
              console.log('err === ', err);
              resolve({ err });
            } else {
              resolve(body);
            }
          });
        }
      });
    });
  },

  /**
   * Create new customer in stripe and save in db
   */
  createCustomerStripe: async (email, userId, companyId) => {
    return new Promise((resolve, reject) => {
      stripe.customers.create({
        // source: token,
        description: 'Create new customer',
        email: email,
        metadata: { userId, companyId }
      }, (err, customer) => {
        if (customer) {
          console.log(customer);
          let query = { _id: companyId };
          let updatevalueswith = { $set: { stripeCustomerId: customer.id } };
          CompanyDetail.updateOne(query, updatevalueswith, (err, response) => {
            if (err) {
              console.log('err === ', err);
              resolve({ err });
            } else {
              resolve(customer);
            }
          });
        } else {
          console.log(err);
          reject(err);
        }
      });
    });
  },

  /**
   * Attaches a Source object to a Customer 
   * Only make stripe payment return charge object
   */
  makeStripePayment: async (description, token, currency, amount, customerId) => {
    return new Promise((resolve, reject) => {

      // attach source
      stripe.customers.createSource(
        customerId, { source: token, }, (err, source) => {
          if (source) {
            // create payment
            stripe.charges.create({
              description, source: source.id, currency, amount, 'customer': customerId
            }, (err, charge) => {
              if (charge) {
                resolve(charge);
              } else {
                // console.log('err1 === ', err);
                reject();
              }
              // detuch source
              stripe.customers.deleteSource(
                customerId,
                source.id,
                function (err, confirmation) {
                  // asynchronously called
                }
              );
            });
          } else {
            // console.log('err2 === ', err);
            reject();
          }
        }
      );
    });
  },

  /**
   * Save transaction
   */
  saveTransaction: async ({ userId, companyId, companyType, referenceCode, amount, currency, description, paymentGateway, paymentGatewayData, paymentMode }) => {
    return new Promise((resolve, reject) => {
      var newTransaction = new Transaction({
        userId,
        companyId,
        companyType,
        referenceCode,
        amount,
        currency,
        paymentGateway,
        paymentGatewayData,
        paymentMode,
        description,
      });

      newTransaction.save(function (err, transaction) {
        if (err) {
          reject(err);
        } else {
          resolve(transaction);
        }
      });
    });
  },

  /**
   * Paid Invoice
   */
  paidInvoice: async ({ invoiceId, transactionId, paymentMode }) => {
    return new Promise((resolve, reject) => {
      let updateWhere = {
        "_id": invoiceId,
      };

      let updateWith = {
        isPaid: true,
        transactionId,
        paymentMode,
      };

      Invoice.updateOne(updateWhere,
        { "$set": updateWith },
        function (updatederror, updatedresponse) {
          if (updatederror) {
            reject({
              "ErrorStatus": true,
            });
          }

          if (updatedresponse) {
            resolve({
              "ErrorStatus": false,
              "updatedresponse": updatedresponse
            });
          }
        });
    });
  },

  recgargeWallet: async (companyId, amount) => {
    return new Promise((resolve, reject) => {
      console.log(companyId, amount);

      CompanyDetail.find({ _id: companyId }, (err, response) => {
        if (err) {
          console.log('err', err);
          reject(err);
        } else {
          let details = response && response.length > 0 ? response[0] : null;
          if (details) {
            let walletBalence = details.walletBalence ? Number(details.walletBalence) : 0;
            walletBalence += Number(amount) / 100;
            let updateWith = {
              "walletBalence": walletBalence,
            }

            let updateWhere = {
              _id: companyId,
            }
            console.log('walletBalence', walletBalence);

            CompanyDetail.updateOne(updateWhere, { "$set": updateWith }, function (updatederror, updatedresponse) {
              if (updatederror) {
                console.log('walletBalence save');

                resolve();
              } else {
                reject();
              }
            });
          } else {
            reject();
          }
        }
      });
    });
  }
}