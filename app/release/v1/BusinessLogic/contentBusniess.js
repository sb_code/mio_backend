const mongoose = require("mongoose");
const Conference = require("../models/conference");
const User = require("../models/user");
const CompanyDetail = require("../models/companyDetail");
const FAQ = require("../models/faq");
const TAC = require("../models/tac");
const PrivacyPolicy = require("../models/privacyPolicy");

exports.getCompanyIdOfMIO = (userType) => {
    
    return new Promise((resolve, reject) => {
        let aggrQuery = [
            {
                $match: {
                    'userType': userType
                }
            },
            {
                $project: {
                    '_id': 0,
                    'companyId': 1
                }
            }
        ];
        
        User.aggregate(aggrQuery, function(err, response) {
            
            if (err) {
                resolve({
                    "isError": true
                })
            } else {
                if (response.length == 0) {
                    resolve({
                        "isError": false,
                        "isUserExist": false
                    })
                } else if (response.length > 0) {
                    
                    resolve({
                        "isError": false,
                        "isUserExist": true,
                        "companyId": response
                    })
                }
            }

        })
    })

}



exports.getCompanyDetails = (companyId) => {

    return new Promise((resolve, reject)=>{

        let aggrQuery = [];

        if(companyId == ''){
            resolve({
                "isError": true
            })
        }else{

            aggrQuery = [
                {
                    $match: {
                        '_id': mongoose.Types.ObjectId(companyId)
                    }
                }
            ];

            CompanyDetail.aggregate(aggrQuery, (err, response)=> {

                if(err){
                    resolve({
                        "isError": true
                    });
                }else{
                    if (response.length == 0) {
                        resolve({
                            "isError": false,
                            "isUserExist": false
                        })
                    } else if (response.length > 0) {
                        resolve({
                            "isError": false,
                            "isUserExist": true,
                            "details": response
                        })
                    }
                }

            })

        }

    })

}