const mongoose = require("mongoose");
const Conference = require("../models/conference");
const User = require("../models/user");
const Sip_dialBusiness = require("./sip_dialBusiness");

// this function is written to fetch all the call details of user for particular month
exports.fetchWebCallDetailForUser = async (operatorId, months, year, companyId) => {
	
	return new Promise(function(resolve, reject){
		let aggQuery= [];

		if(companyId != ''){
			
			aggQuery = [

				{ 
					$match: { 
						'_id': mongoose.Types.ObjectId(operatorId),
						'companyId':  mongoose.Types.ObjectId(companyId)
					} 
				},
				
				{
					$project: {
						'_id': 0,
						"userId": 1
					}
				},
				
				{
					$lookup: {
						from: "webcalls",
						localField: "userId",
						foreignField: "userId",
						as: "response"
					}
				},
				
				{   $unwind:"$response" },

				{
					$project: {
						'response._id': 1,
						'response.conferenceId': 1,
						'response.sessionId': 1,
						'response.streamId': 1,
						'response.connectionId': 1,
						'response.startedAt': 1,
						'response.endedAt': 1,
						'response.createdAt': 1,
						'response.conferenceStatus': 1,
						'year':  { $year: "$response.createdAt" },
						'month': { $month: "$response.createdAt" }
					}
				}, 
           
				{
					$match: {
						'response.conferenceStatus': "ENDED",
						'month': { $in: months },
						'year': year,		
					},
				}
			
			]

		}else{
			
			aggQuery = [

				{ 
					$match: { 
						'_id': mongoose.Types.ObjectId(operatorId)
					} 
				},
				{
					$project: {
						'_id': 0,
						"userId": 1
					}																												
				},
				{
					$lookup: {
						from: "webcalls",
						localField: "userId",
						foreignField: "userId",
						as: "response"
					}
				},
				
				{   $unwind:"$response" },

				{
					$project: {
						'response._id': 1,
						'response.conferenceId': 1,
						'response.sessionId': 1,
						'response.streamId': 1,
						'response.connectionId': 1,
						'response.startedAt': 1,
						'response.endedAt': 1,
						'response.createdAt': 1,
						'response.conferenceStatus': 1,
						'year':  { $year: "$response.createdAt" },
						'month': { $month: "$response.createdAt" }
					}
				}, 

				{
					$match: {
						'response.conferenceStatus': "ENDED",
						'month': { $in: months },
						'year': year,		
					},
				}  
			]
		}
		
        User.aggregate(aggQuery, function(err, response){

        	if (err) {
				resolve({
					"isError": true,
				});
	        }
	        else {

	        	if (response.length < 1) {
					// console.log("user don't exist");
	        		resolve({
		               "isError": false,
		               "isUserExist": false
		            });
	        	}

	        	if (response.length > 0) {
	        		resolve({
		               "isError": false,
		               "isUserExist": true,
		               "webCallDetails": response
		            });
	        	}
	               
	        }

        })		        
    }) // END Promise	
}



// this function is written to check whether the conference is active or not
exports.checkConferenceActive = async (conferenceId) => {

	return new Promise(function (resolve, reject) {

		let aggrQuery = [
			{
				'$match': {
					'_id': mongoose.Types.ObjectId(conferenceId),
				}
			},
			{
				$project: {
					'_id': 0,
					'isActive': 1,
					'hostUserId': 1,
					'conferenceTitle': 1,
					"companyId": 1,
					'sessionId': 1
				}
			},
		];

		Conference.aggregate(aggrQuery, function (err, response) {

			if (err) {
				resolve({
					"isError": true,
				});
			}
			else {

				if (response.length < 1) {
					resolve({
						"isError": false,
						"isConferenceExist": false
					});
				}

				if (response.length > 0) {
					resolve({
						"isError": false,
						"isConferenceExist": true,
						"conferenceDetail": response
					});
				}

			}
		}) //END Conference.aggregate

	}) // END Promise	
}


// this function is written to remove the user from the conference
exports.updateConference = async (conferenceId, userId) => {

	return new Promise(function (resolve, reject) {

		let updateWith = {
			"participants.$.isStarted": false,
		}

		let updateWhere = {
			"_id": conferenceId,
			"participants.userId": userId,
		}

		Conference.updateOne(updateWhere, updateWith, function (updatederror, updatedresponse) {

			if (updatederror) {
				resolve({
					"isError": true,
				});
			}
			else {
				if (updatedresponse.nModified == 1) {
					resolve({
						"isError": false,
						"isModified": true
					});
				}
				else {
					resolve({
						"isError": false,
						"isModified": false
					});
				}
			}
		}) //END Conference.updateOne

	}) // END Promise	
}

// this function is written to remove the user from the conference
exports.updateConferenceForDialedUser = async (conferenceId, contactNumber) => {

	return new Promise(function (resolve, reject) {

		let updateWith = {
			"participants.$.isStarted": false,
		}

		let updateWhere = {
			"_id": conferenceId,
			"participants.contactNumber": contactNumber,
		}

		Conference.updateOne(updateWhere, updateWith, function (updatederror, updatedresponse) {

			if (updatederror) {
				resolve({
					"isError": true,
				});
			}
			else {
				if (updatedresponse.nModified == 1) {
					resolve({
						"isError": false,
						"isModified": true
					});
				}
				else {
					resolve({
						"isError": false,
						"isModified": false
					});
				}
			}
		}) //END Conference.updateOne

	}) // END Promise	
}

// this function is used to check wheather the user exist in the conference
exports.checkUserExistance = async (conferenceId) => {

	return new Promise(function (resolve, reject) {

		let aggrQuery = [
			{
				'$match': { '_id': mongoose.Types.ObjectId(conferenceId) }
			},
			{
				$project: {
					'_id': 0,
					"hostUserId": 1,
					"isActive": 1,
					"participants": 1
				}
			},
			{ $unwind: "$participants" },

			{
				'$match': { 'participants.isStarted': true }
			},
		];

		Conference.aggregate(aggrQuery, function (err, response) {

			if (err) {
				resolve({
					"isError": true,
				});
			}
			else {

				resolve({
					"isError": false,
					"response": response
				});

			}
		}) //END Conference.aggregate



	}) // END Promise	
}


// this function is used to deactivate conference
exports.deactivateConference = async (conferenceId) => {

	return new Promise(function (resolve, reject) {
		let updateWith = {
			"isActive": false,
		}

		let updateWhere = {
			"_id": conferenceId,
		}

		Conference.updateOne(updateWhere, updateWith, function (updatederror, updatedresponse) {
			if (updatederror) {
				resolve({
					"isError": true,
				});
			}
			else {
				if (updatedresponse.nModified == 1) {
					resolve({
						"isError": false,
						"isModified": true
					});
				}
				else {
					resolve({
						"isError": false,
						"isModified": false
					});
				}
			}
		}) //END Conference.updateOne


	}) // END Promise	
}

/* 
      This function is written to end the phonic conference for 
      a particular participant
  */

exports.sipDialEndForEndConf = async (conferenceId, toMobile) => {

	return new Promise(function (resolve, reject) {
		// start executing the operation for each number
		// let checkParticipantInConferenceStatus = await Sip_dialBusiness.checkParticipantInConference(conferenceId, toMobile);

		//       if (checkParticipantInConferenceStatus.isError == true) { // When Error in fetching the participant
		//           resolve({
		//             "isError": true,
		//           });

		//           return;
		//       }

		//       if (checkParticipantInConferenceStatus.isParticipantExist == false) {
		//           resolve({
		//             "isError": false,
		//             "isParticipantExist": false
		//           });

		//           return;
		//       }

		//       let sessionId = checkParticipantInConferenceStatus.sessionId;
		//       let connectionId = checkParticipantInConferenceStatus.connectionId;

		//       // Here, we are disconnecting the call
		//       let disconnectCallStatus = await Sip_dialBusiness.disconnectCall(sessionId, connectionId);

		//       if (disconnectCallStatus.isError == true) { // Some error in disconnecting the call
		//           resolve({
		//             "isError": true,
		//           });

		//           return;
		//       }

		//       // Here, we are update the updateAt status in sipcall connection
		//       let updateSipCallStatus = await Sip_dialBusiness.updateSipCall(sessionId, connectionId);

		//       if (updateSipCallStatus.isError == true) {
		//           resolve({
		//             "isError": true,
		//           });

		//           return;
		//       }

		//       resolve({
		//         "isError": true,
		//         "isParticipantExist": false,
		//         "message": "Call Disconnected Successfully"
		//       });

	}) // END Promise            
} // END sipDialEndForEndConf


/**
 * Get Opentok token role 'subscriber','publisher','moderator'
 * Subscribers can only subscribe to streams
 * Publishers can subscribe and publish streams to the session, and they can use the signaling API.
 * Moderators have the privileges of publishers and, in addition, they can also force other users to disconnect from the session or to cease publishing. The default role (if no value is passed) is publisher.
 */
exports.getOpentokTokenRole = async (sessionId, userId = '', tokenFor = 'web') => {
	return new Promise((resolv, reject) => {
		Conference.find({ sessionId }, (error, response) => {
			if (!error) {
				if (response && response.length > 0) {
					if ((tokenFor == 'web' && userId && response[0].callType == "LECTURE" && response[0].hostUserId != userId) || (tokenFor == 'sip' && response[0].callType == "LECTURE")) {
						resolv('subscriber');
					} else {
						resolv('publisher');
					}
				} else {
					resolv('publisher');
				}
			} else {
				resolv('publisher');
			}
		});
	});
}







