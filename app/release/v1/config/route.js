const express = require('express');
const router = express.Router();

const mongoose = require('mongoose');


// server connection 
// mongoose.connect('mongodb+srv://miouser:1q1q1q1@mio-tgaxo.mongodb.net/mio?retryWrites=true&w=majority', {
//   useNewUrlParser: true,
//   useUnifiedTopology: true
// })

// localhost connection 
mongoose.connect('mongodb://localhost:27017/mio', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

mongoose.connection.on('connected', function () {
  console.log(' Mongoose default connection open to ' + "mongodb://localhost:27017/mio");
});

// If the connection throws an error
mongoose.connection.on('error', function (err) {
  console.log('Mongoose default connection error: ' + err);
});
// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
  console.log('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection 
process.on('SIGINT', function () {
  mongoose.connection.close(function () {
    console.log('Mongoose default connection disconnected through app termination');
    process.exit(0);
  });
});



//aws connection
// mongoose.connect('mongodb://username:password@ip_address:port/db_name',{
//     useNewUrlParser:true
// })


const userRoute = require('./endpoints/user');
const adminRoute = require('./endpoints/admin');
const conferenceRoute = require('./endpoints/conference');
const userManagementRoute = require('./endpoints/user_management');
const operatorManagementRoute = require('./endpoints/operator_management');
const contentManagementRoute = require('./endpoints/content_management');
const sipDialRoute = require('./endpoints/sip_dial');
const priceManagementRoute = require('./endpoints/price_management');
const baseServiceRoute = require('./endpoints/baseService');
const paymentRoute = require('./endpoints/payment');

const testRouter = require('./endpoints/testModule');

/**
 *  ROUTES -  ROOT LEVEL
 */
router.use("/user", userRoute)
router.use("/admin", adminRoute)
router.use("/conference", conferenceRoute)
router.use("/user-management", userManagementRoute)
router.use("/sip-dial", sipDialRoute)
router.use("/price-management", priceManagementRoute)
router.use("/services", baseServiceRoute)
router.use("/operator-management", operatorManagementRoute)
router.use("/content-management", contentManagementRoute)
router.use("/payment", paymentRoute)
router.use("/test", testRouter)



module.exports = router
