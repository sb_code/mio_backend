const express = require('express');
const router = express.Router();

const testCtrl = require('../../controllers/test')
router.use(express.json());
router.use(express.urlencoded({ extended: true }));

router.post('/upload-file-s3', testCtrl.uploadFileS3);

router.post('/pdf-generate', testCtrl.pdfGenerate);

router.post('/start-sip-vezeti', testCtrl.startSipVezeti);

router.post('/stop-sip-vezeti', testCtrl.stopSipVezeti);

module.exports = router;