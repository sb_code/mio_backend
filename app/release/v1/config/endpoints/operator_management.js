const express = require('express');
const router = express.Router();
const operatorManagementCtrl = require('../../controllers/operator_management');
const checkAuth = require('../../middleware/check-auth');
router.use(express.json());
router.use(express.urlencoded({ extended: true }));

router.post('/add-company', operatorManagementCtrl.addCompany);
router.post('/add-operator', operatorManagementCtrl.addOperator);
router.post('/assign-operator', operatorManagementCtrl.assignOperator);
router.post('/assign-operator-to-company', operatorManagementCtrl.assignOperatorToCompany);
router.post('/un-assign-operator-from-company', operatorManagementCtrl.unAssignOperatorFromCompany);

router.post('/fetch-all-admin-operator', operatorManagementCtrl.fetchAllAdminOperator);

module.exports = router;