const express = require('express');
const router = express.Router();
const userManagementCtrl = require('../../controllers/user_management');
const checkAuth = require('../../middleware/check-auth');
router.use(express.json());
router.use(express.urlencoded({ extended: true }))

router.post('/add-new-user', userManagementCtrl.addNewUser);
router.post('/edit-user', userManagementCtrl.editUser);
router.post('/get-user-company-basis', userManagementCtrl.getUserCompanyBasis);
router.post('/delete-user-id-basis', userManagementCtrl.deleteUserIdBasis);
router.post('/search-user-name-basis', userManagementCtrl.searchUserNameBasis);
router.post('/get-user-usertype-basis', userManagementCtrl.getUserUsertypeBasis);
router.post('/change-user-status', userManagementCtrl.changeUserStatus);



module.exports = router;