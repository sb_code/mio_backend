const express = require('express');
const router = express.Router();
const baseServiceCtrl = require('../../controllers/baseService');
const checkAuth = require('../../middleware/check-auth');
router.use(express.json());
router.use(express.urlencoded({ extended: true }))

router.post('/add-services', baseServiceCtrl.addNewServices);
router.post('/add-services-config', baseServiceCtrl.addServicesConfig);
router.post('/get-base-services', baseServiceCtrl.getBaseServices);
router.post('/edit-base-services', baseServiceCtrl.editBaseService);
router.post('/edit-service-config', baseServiceCtrl.editServiceConfig);
router.post('/delete-base-services', baseServiceCtrl.deleteBaseServices);
router.post('/delete-services-config', baseServiceCtrl.deleteServicesConfig);
router.post('/search-base-services', baseServiceCtrl.searchBaseServices);

router.post('/add-base-services-to-company', baseServiceCtrl.addBaseServicesToCompany);
// router.post('/get-company-service', baseServiceCtrl.getCompanyService);
// router.post('/edit-company-service', baseServiceCtrl.editCompanyService);

module.exports = router;
