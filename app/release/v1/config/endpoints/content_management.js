const express = require('express');
const router = express.Router();
const contentManagementCtrl = require('../../controllers/content_management');
const checkAuth = require('../../middleware/check-auth');
router.use(express.json());
router.use(express.urlencoded({ extended: true }));

router.post('/get-mio-admin-detail', contentManagementCtrl.getMioAdminDetail);
router.post('/update-mio-admin-detail', contentManagementCtrl.updateMioAdminDetail);
router.post('/update-contactUs-for-mio-admin', contentManagementCtrl.updateContactUsForMioAdmin);
router.post('/insert-faq', contentManagementCtrl.insertFAQ);
router.post('/get-faq', contentManagementCtrl.getFAQ);
router.post('/update-faq-by-id', contentManagementCtrl.updateFaqById);
router.post('/delete-faq', contentManagementCtrl.deleteFAQ);

router.post('/insert-tac', contentManagementCtrl.insertTAC);
router.post('/get-tac', contentManagementCtrl.getTAC);
router.post('/update-tac-by-id', contentManagementCtrl.updateTacById);
router.post('/delete-tac', contentManagementCtrl.deleteTAC);

router.post('/insert-privacy-policy', contentManagementCtrl.insertPrivacyPolicy);
router.post('/get-privacy-policy', contentManagementCtrl.getPrivacyPolicy);
router.post('/update-privacy-policy-by-id', contentManagementCtrl.updatePrivacyPolicyById);
router.post('/delete-privacy-policy', contentManagementCtrl.deletePrivacyPolicy);

module.exports = router;