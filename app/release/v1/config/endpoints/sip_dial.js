const express = require('express');
const router = express.Router();
const sipDailCtrl = require('../../controllers/sip_dial');
const checkAuth = require('../../middleware/check-auth');
router.use(express.json());
router.use(express.urlencoded({ extended: true }))

router.post('/sip-dial-out-multiple', sipDailCtrl.sipDialOutMultiple);
router.post('/sip-dial-out', sipDailCtrl.sipDialOut);
router.post('/sip-dial-end', sipDailCtrl.sipDialEnd);




module.exports = router;