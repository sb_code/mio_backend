const express = require('express');
const router = express.Router();
const adminCtrl = require('../../controllers/admin');
const checkAuth = require('../../middleware/check-auth');
router.use(express.json());
router.use(express.urlencoded({extended:true}))



router.post('/get_user_list', adminCtrl.getUserList);
router.post('/approve_user', adminCtrl.approveUser);
router.post('/remove_user', adminCtrl.removeUser);
router.post('/find_user', adminCtrl.findUser);
router.post('/get_user_details', adminCtrl.getUserDetails);
router.post('/add_edit_user', adminCtrl.addEditUser);



module.exports = router;