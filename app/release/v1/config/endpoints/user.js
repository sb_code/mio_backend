const express = require('express');
const router = express.Router();
const userCtrl = require('../../controllers/users');
const checkAuth = require('../../middleware/check-auth');
router.use(express.json());
router.use(express.urlencoded({ extended: true }))



router.post('/sign_up', userCtrl.signUp);
router.post('/sign_in', userCtrl.signIn);
router.post('/sign_out', userCtrl.signOut);
router.post('/forgot_password', userCtrl.forgotPassword);
router.post('/reset_password', userCtrl.resetPassword);
router.post('/get_user_by_id', userCtrl.getUserById);
router.post('/update_user_details', userCtrl.updateUserDetails);
router.post('/serach_user_details', userCtrl.searchUserDetails);
router.post('/add_contact', userCtrl.addAddressBook);
router.post('/get_contact_list', userCtrl.getContactList);
router.post('/get_contact_details_by_id', userCtrl.getContactDetailsById);
router.post('/update_contact_details', userCtrl.updateContactDetails);
router.post('/delete_contact_details', userCtrl.deleteContactDetails);
router.post('/search_contact_details', userCtrl.searchContactDetails);
router.post('/search_contact_details_by_email', userCtrl.searchContactDetailsByEmail);

router.post('/get-user-by-email', userCtrl.getUserByemail);
router.post('/add-contact-multiple', userCtrl.addAddressBookMultiple);
router.post('/get-companies', userCtrl.getCompanies);
router.post('/get-company-by-id', userCtrl.getCompanyById);
router.post('/update-company-details', userCtrl.updateCompanyDetails);
router.post('/company-upgrade-request', userCtrl.companyUpgradeRequest);
router.post('/upgrade-request-list', userCtrl.upgradeRequestList);
router.post('/upgrade-request-action', userCtrl.upgradeRequestAction);
router.post('/search-companies', userCtrl.searchCompanies);
router.post('/change-company-status', userCtrl.changeCompanyStatus);
// router.post('/conference-wise-company', userCtrl.conferenceWiseCompany);



// router.post('/join-conference-with-code', userCtrl.joinConferenceWithCode);



module.exports = router;