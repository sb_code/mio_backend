const express = require('express');
const router = express.Router();
const priceManagementCtrl = require('../../controllers/price_management');
const checkAuth = require('../../middleware/check-auth');
router.use(express.json());
router.use(express.urlencoded({ extended: true }))

router.post('/calculate-price-for-conference', priceManagementCtrl.calculatePriceForConference);
router.post('/calculate-price-for-company', priceManagementCtrl.calculatePriceForCompany);
router.post('/create-invoice', priceManagementCtrl.createInvoice);
router.post('/fetch-invoice-companyId', priceManagementCtrl.fetchInvoiceByCompanyId);
router.post('/fetch-invoice-By-Id', priceManagementCtrl.fetchInvoiceById);
router.post('/send-invoice-By-mail', priceManagementCtrl.sendInvoiceByMail);
router.post('/edit-invoice-By-Id', priceManagementCtrl.editInvoiceById);
router.post('/get-transaction-list-By-CompanyId', priceManagementCtrl.getTransactionListByCompanyId);
router.post('/fetch-conference-activity-details', priceManagementCtrl.fetchConferenceActivityDetails);
router.post('/fetch-call-usage-detail-of-conference', priceManagementCtrl.fetchCallUsageDetailOfConference);

module.exports = router;