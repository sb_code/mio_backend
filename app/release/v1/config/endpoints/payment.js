const express = require('express');
const router = express.Router();

const paymentCtrl = require('../../controllers/payment')
router.use(express.json());
router.use(express.urlencoded({ extended: true }));

router.post('/create-customer', paymentCtrl.createCustomer);

router.post('/stripe-payment', paymentCtrl.stripePayment);

router.post('/paystack-payment', paymentCtrl.paystackPayment);

router.post('/paypal-payment', paymentCtrl.paypalPayment);

router.post('/manual-payment', paymentCtrl.manualPayment);

module.exports = router;
