const express = require('express');
const router = express.Router();
const conferenceCtrl = require('../../controllers/conference');
const checkAuth = require('../../middleware/check-auth');
router.use(express.json());
router.use(express.urlencoded({ extended: true }))

router.post('/create-conference', conferenceCtrl.createConference);
router.post('/host-conference', conferenceCtrl.hostConference);
router.post('/start-broadcast', conferenceCtrl.startBroadcast);
router.post('/stop-broadcast', conferenceCtrl.stopBroadcast);
router.post('/join-conference', conferenceCtrl.joinConference);

router.post('/list-conference', conferenceCtrl.listConference);
router.post('/list-conference-company', conferenceCtrl.listConferenceCompany);
router.post('/delete-conference', conferenceCtrl.deleteConference);

router.post('/remove-participant', conferenceCtrl.removeParticipant);
router.post('/add-participant', conferenceCtrl.addParticipant);

router.post('/get-invitation-link', conferenceCtrl.getInvitationLink);
router.post('/join-conference-with-code', conferenceCtrl.joinConferenceWithCode);

router.post('/login-as-guest', conferenceCtrl.loginAsGuest);
router.post('/sip-dial-out', conferenceCtrl.sipDialOut);
router.post('/sip-dial-end', conferenceCtrl.sipDialEnd);

router.post('/start-conference', conferenceCtrl.startConference);
router.post('/end-conference', conferenceCtrl.endConference);
router.post('/sip-dial-status', conferenceCtrl.sipDialStatus);
router.post('/get-participant-details', conferenceCtrl.getParticipantDetails);
router.post('/fetch-conference-by-id', conferenceCtrl.fetchConferenceById);

router.post('/upload-participant-file', conferenceCtrl.uploadParticipantFile);

router.post('/dialout-for-dialin', conferenceCtrl.dialOutForDialin);
router.post('/dialout-hang-up', conferenceCtrl.dialOutHangUp);
router.post('/nexmo-event-to-mio', conferenceCtrl.nexmoEvent);
router.get('/nexmo-answer-to-mio', conferenceCtrl.nexmoAnswer);
router.get('/nexmo-fallback-to-mio', conferenceCtrl.nexmoFallback);
router.post('/nexmo-dtmf', conferenceCtrl.nexmoDtmf);
router.get('/test-mail', conferenceCtrl.testMail);

router.post('/fetch-inactive-conference', conferenceCtrl.fetchConferenceForHostUser);
router.post('/fetch-active-conference', conferenceCtrl.fetchConferenceOfParticipants);

router.post('/start-archive', conferenceCtrl.startArchive);
router.post('/stop-archive', conferenceCtrl.stopArchive);
router.post('/get-archives', conferenceCtrl.getArchives);
router.post('/delete-archive', conferenceCtrl.deleteArchive);
router.post('/get-archive-list', conferenceCtrl.getArchiveList);
router.post('/is-host-join', conferenceCtrl.isHostJoin);


router.post('/get-users-webcall-duration', conferenceCtrl.webCallDuration);


module.exports = router;