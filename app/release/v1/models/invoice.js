const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const autoId = require('../../../services/AutogenerateId');

const invoiceSchema = new mongoose.Schema({


    _id: mongoose.Schema.Types.ObjectId,

    companyId: {
        type: String,
        default: ''
    },

    month: {
        type: String,
        default: ''
    },

    year: {
        type: String,
        default: ''
    },

    date: {
        type: Date,
        default: Date.now
    },

    isPaid: {
        type: Boolean,
        enum: [true, false],
        default: false
    },

    isPublished: {
        type: Boolean,
        enum: [true, false],
        default: false
    },

    transactionId: {
        type: String
    },

    paymentMode: {
        type: String
    },

    services: [{
        serviceId: String,
        serviceKey: String,
        serviceName: String,
        pricePerMinute: String,
        totalDuration: String,
        totalAmount: String,
    }]


},
    {
        timestamps: true,
        versionKey: false
    })



invoiceSchema.pre('save', async function (next) {
    var invoice = this;
    if (this.isNew) {
        try {
            invoice._id = new mongoose.Types.ObjectId();
            return next();
        } catch (error) {
            return next(error);
        }

    } else {
        return next();
    }
})


const Invoice = mongoose.model("Invoice", invoiceSchema)

module.exports = Invoice