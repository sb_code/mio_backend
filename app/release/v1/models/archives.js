const mongoose = require('mongoose');
// const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const autoId = require('../../../services/AutogenerateId');

const archivesSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,

    archiveId: {
        type: String
    },

    status: {
        type: String
    },
    name: {
        type: String
    },
    sessionId: {
        type: String
    },
    startedAt: {
        type: Number
    },
    size: {
        type: Number
    },
    duration: {
        type: Number
    },
    outputMode: {
        type: String
    },
    hasAudio: {
        type: Boolean
    },
    hasVideo: {
        type: Boolean
    },
    lastUpdate: {
        type: String
    },
    event: {
        type: String
    },
    url: {
        type: String
    },
    conferenceId: {
        type: String
    },
    
    companyId: {
        type: String,
        default : ''
    },

    conferenceDurationId: {
        type: mongoose.Schema.Types.ObjectId,
        // type: String,
        // default : ''
    },
},
    {
        timestamps: true, // add created_at , updated_at at the time of insert/update
        versionKey: false
    })

archivesSchema.pre('save', async function (next) {

    var archives = this;
    if (this.isModified('password') || this.isNew) {

        try {
            archives._id = new mongoose.Types.ObjectId();
            // let usrTypeBlock = 'ARCH'
            // archives.companyId = await autoId.autogenerateId(usrTypeBlock);
            return next();
        } catch (error) {
            return next(error);
        }

    } else {
        return next();
    }
})

const Archives = mongoose.model("Archives", archivesSchema)

module.exports = Archives