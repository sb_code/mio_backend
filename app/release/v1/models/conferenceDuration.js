const mongoose = require('mongoose');
// const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const autoId = require('../../../services/AutogenerateId');

const conferenceDurationSchema = new mongoose.Schema({
    
    _id : mongoose.Schema.Types.ObjectId,
    
    
    conferenceId : {
        type : String
    },

    sessionId : {
        type : String
    },

    companyId: {
        type: String,
        default : ''
    },

    startedAt: {
        type: String
    },

    endedAt: {
        type: String,
        default : ''
    },

    conferenceStatus: {
        type: String,
        enum : ['STARTED','ENDED'],
        default : 'STARTED'
    },
    
    date: {
        type: Date,
        default: Date.now
    }

},
{
    timestamps: true, // add created_at , updated_at at the time of insert/update
    versionKey: false 
})




conferenceDurationSchema.pre('save', async function (next) {
    var conferenceDuration = this;
    if (this.isNew) {
        try {            
            conferenceDuration._id = new mongoose.Types.ObjectId();
           
        } catch (error) {
            return next(error);
        }

    } else {
        return next();
    }
})


const conferenceDuration = mongoose.model("conferenceDuration", conferenceDurationSchema)

module.exports = conferenceDuration