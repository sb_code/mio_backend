const mongoose = require('mongoose');

transactionSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  userId: {
    type: String,
  },
  companyId: {
    type: String,
  },
  companyType: {
    type: String,
    enum: ['STARTUP', 'ENTERPRISE']
  },
  referenceCode: {
    type: String,
  },
  amount: {
    type: Number
  },
  currency: {
    type: String,
    enum: ['USD', 'GBP', 'NGN', 'EUR']
  },
  description: {
    type: String
  },
  paymentGateway: {
    type: String,
    enum: ['PAYSTACK', 'STRIPE', 'PAYPAL', 'MANUAL']
  },
  paymentMode: {
    type: String,
    enum: ["ONLINE", "OFFLINE"]
  },
  paymentGatewayData: {},

},
  {
    timestamps: true, // add created_at , updated_at at the time of insert/update
    versionKey: false
  })

transactionSchema.pre('save', async function (next) {
  var transaction = this;
  if (this.isNew) {
    try {
      transaction._id = new mongoose.Types.ObjectId();
      return next();
    } catch (error) {
      return next(error);
    }
  } else {
    return next();
  }
})

const Transaction = mongoose.model("Transaction", transactionSchema)

module.exports = Transaction