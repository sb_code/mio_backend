const mongoose = require('mongoose');
// const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const autoId = require('../../../services/AutogenerateId');

const aclSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    aclId : {
        type : String
    },        
    userType : {
        type : String,
        enum : ['ADMIN','ADMIN-OPERATOR','COMPANY-OPERATOR','ENTERPRISE','STARTER','PARTICIPANT'],
    },    
    pageAccess : [],
    componentAccess : [],
},
{
    timestamps: true, // add created_at , updated_at at the time of insert/update
    versionKey: false 
})


const ACL = mongoose.model("ACL", aclSchema)

module.exports = ACL