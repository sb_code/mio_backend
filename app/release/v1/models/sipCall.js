const mongoose = require('mongoose');
// const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const autoId = require('../../../services/AutogenerateId');

const sipCallSchema = new mongoose.Schema({
	
    _id : mongoose.Schema.Types.ObjectId,
    
    toMobile : {
        type : String
    },

    countryCode : {
        type : String
    },

    fromMobile : {
        type : String,
        default : ''
    },

    companyId: {
        type: String,
        default : ''
    },

    sessionId : {
        type : String,
        default : ''
    },

    sipCallId : {
        type : String,
        default : ''
    },

    connectionId: {
        type: String,
        default : ''
    },

    streamId: {
        type: String,
        default : ''
    },

    createdAt: {
        type: String,
        default : ''
    },

    updatedAt: {
        type: String
    },

    callStatus: {
        type: String,
        enum : ['STARTED','ENDED'],
        default : 'STARTED'
    },

    conferenceDurationId : {
        type: mongoose.Schema.Types.ObjectId,
        // type : String,
        // default : ''
    },

    
    date: {
        type: Date,
        default: Date.now
    }

},
{
    timestamps: true, // add created_at , updated_at at the time of insert/update
    versionKey: false 
})




sipCallSchema.pre('save', async function (next) {
    var sipCall = this;
    if (this.isNew) {
        try {            
            sipCall._id = new mongoose.Types.ObjectId();
           
        } catch (error) {
            return next(error);
        }

    } else {
        return next();
    }
})


const sipCall = mongoose.model("sipCall", sipCallSchema)

module.exports = sipCall