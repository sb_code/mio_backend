const mongoose = require('mongoose');
// const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const autoId = require('../../../services/AutogenerateId');

const baseServicesSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,

  services: [
    {
      serviceKey: {
        enum: ["DIALIN", "DIALOUT", "ARCHIVE", "WEB"],
        type: String
      },

      serviceName: {
        type: String
      },

      costPerMinute: {
        type: Number
      },
      pricePerMinute: {
        type: Number
      },
      multiplier: {
        type: Number
      },
      createdBy: {
        type: String
      },
    }
  ],

  serviceConfig: [
    {
      companyType: {
        enum: ["FREE_TYPE", "STARTUP", "ENTERPRISE"],
        type: String
      },

      trialPeriod: {
        type: Number
      },

      numberOfSeats: {
        type: Number
      },

      pricePerSeat: {
        type: Number
      },

      defaultCurrency: {
        enum: ["GBP", "USD", "NAIRA", "EURO"],
        type: String
      },

      defaultVat: {
        type: Number
      }

    }
  ],
},
  {
    timestamps: true, // add created_at , updated_at at the time of insert/update
    versionKey: false
  })

baseServicesSchema.pre('save', async function (next) {

  var archives = this;
  if (this.isModified('password') || this.isNew) {

    try {
      archives._id = new mongoose.Types.ObjectId();
      // let usrTypeBlock = 'ARCH'
      // archives.companyId = await autoId.autogenerateId(usrTypeBlock);
      return next();
    } catch (error) {
      return next(error);
    }

  } else {
    return next();
  }
})

const BaseServices = mongoose.model("BaseServices", baseServicesSchema)

module.exports = BaseServices