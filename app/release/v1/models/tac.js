const mongoose = require('mongoose');
// const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const autoId = require('../../../services/AutogenerateId');

const tacSchema = new mongoose.Schema({

	
    _id : mongoose.Schema.Types.ObjectId,

    termsAndCondition : {
        type : String,
        default : ''
    },

},
{
    timestamps: true, // add created_at , updated_at at the time of insert/update
    versionKey: false 
})



tacSchema.pre('save', async function (next) {
    var tac = this;
    if (this.isNew) {
        try {            
            tac._id = new mongoose.Types.ObjectId();
            // amenity.amenityId = await autoId.autogenerateId('AME');
            return next();
        } catch (error) {
            return next(error);
        }

    } else {
        return next();
    }
})


const tac = mongoose.model("tac", tacSchema)

module.exports = tac