const mongoose = require('mongoose');
// const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const autoId = require('../../../services/AutogenerateId');

const faqSchema = new mongoose.Schema({

	
    _id : mongoose.Schema.Types.ObjectId,

    question : {
        type : String,
        default : ''
    },

    answer: {
        type: String,
        default : ''
    },
   
},
{
    timestamps: true, // add created_at , updated_at at the time of insert/update
    versionKey: false 
})



faqSchema.pre('save', async function (next) {
    var faq = this;
    if (this.isNew) {
        try {            
            faq._id = new mongoose.Types.ObjectId();
            // amenity.amenityId = await autoId.autogenerateId('AME');
            return next();
        } catch (error) {
            return next(error);
        }

    } else {
        return next();
    }
})


const faq = mongoose.model("faq", faqSchema)

module.exports = faq