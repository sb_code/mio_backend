const mongoose = require('mongoose');
// const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const autoId = require('../../../services/AutogenerateId');

const privacyPolicySchema = new mongoose.Schema({

	
    _id : mongoose.Schema.Types.ObjectId,

    policy : {
        type : String,
        default : ''
    },

},
{
    timestamps: true, // add created_at , updated_at at the time of insert/update
    versionKey: false 
})



privacyPolicySchema.pre('save', async function (next) {
    var privacyPolicy = this;
    if (this.isNew) {
        try {            
            privacyPolicy._id = new mongoose.Types.ObjectId();
            // amenity.amenityId = await autoId.autogenerateId('AME');
            return next();
        } catch (error) {
            return next(error);
        }

    } else {
        return next();
    }
})


const privacyPolicy = mongoose.model("privacyPolicy", privacyPolicySchema)

module.exports = privacyPolicy