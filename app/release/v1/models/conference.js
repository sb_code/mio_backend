const mongoose = require('mongoose');
// const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const autoId = require('../../../services/AutogenerateId');

const conferenceSchema = new mongoose.Schema({

	
    _id : mongoose.Schema.Types.ObjectId,
    conferenceTitle : {
        type : String
    },
    sessionId: {
        type: String
    },
    broadcastId: {
        type: String
    },
    hostUserId: {
        type: String
    },
    companyId: {
        type: String
    },
    companyId: {
        type: String,
        default : ''
    },
    invitationCode: {
        type: String
    },
    callInitiateBy: {
        type: String
    },
    dtmf: {
        type: String,
    },
    //---------------------------------------------------
    callType : {
        type : String,
        enum : ['LECTURE','CONFERENCE'],
        default : 'CONFERENCE'
    },

    recordingPreference : {
        type : String,
        enum : ['AUTOMATICALLY_INITIATED','AVAILABLE_TO_ORGANISER','NOT_ALLOWED'],
        default : 'NOT_ALLOWED'
    },

    fileShare : {
        type : Boolean,
        enum : [true,false],
        default : false
    },

    operator: [{
        userId: String,
        default: ''
    }],

    allowParticipantToShareScreen : {
        type : Boolean,
        enum : [true,false],
        default : true
    },

    acceptInboundCall : {
        type : Boolean,
        enum : [true,false],
        default : false
    },

    //---------------------------------------------------
    inboundConnectionId: {
        type: String,
        default: 'NA'
    },

    isDeleted : {
        type : Boolean,
        enum : [true,false],
        default : false
    },

    isActive : {
        type : Boolean,
        enum : [true,false],
        default : false
    },

    //===============================
    date: {
        type: Date,
        default: Date.now
    },
   
    timeOut : {
        hours:Number,
        minutes:Number,
        seconds:Number,
    },
    
    duration:{
        hours:Number,
        minutes:Number,
        seconds:Number,
    },   
    status: {
        type: String
    },
    maxNoOfParticipants: {
        type: Number
    },

    participants:[{
        name:String,
        emailAddress:String,
        companyId:String,
        countryCode:String,
        contactNumber:String,
        token:String,
        userId:String,
        connectionId:{
            type: String,
            default : 'NA'
        },
        event:{
            type: String,
            default : 'NA'
        },
        state:{
            type: String,
            default : 'NA'
        },
        reason_code:{
            type: String,
            default : 'NA'
        },
        reason_message:{
            type: String,
            default : 'NA'
        },
        streamId:{
            type: String,
            default : 'NA'
        },
        createdAt:{
            type: String,
            default: (new Date()).getTime()
        },
        userType : {
            type : String,
            enum : ['GUEST','USER','DIALEDUSER'],
            default : 'USER'
        },
        isStarted : {
            type : Boolean,
            enum : [true,false],
            default : false
        },
    }]

},
{
    timestamps: true, // add created_at , updated_at at the time of insert/update
    versionKey: false 
})

conferenceSchema.pre('save', async function (next) {
    var conference = this;
    if (this.isNew) {
        try {            
            conference._id = new mongoose.Types.ObjectId();
            // amenity.amenityId = await autoId.autogenerateId('AME');
            return next();
        } catch (error) {
            return next(error);
        }

    } else {
        return next();
    }
})

const Conference = mongoose.model("Conference", conferenceSchema)

module.exports = Conference