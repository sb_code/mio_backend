const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const autoId = require('../../../services/AutogenerateId');

const userSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    userId: {
        type: String
    },
    fname: {
        type: String
    },
    lname: {
        type: String
    },

    countryCode: {
        type: String
    },

    email: {
        type: String,
        lowercase: true,
        trim: true,
        match: [/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/, 'Please fill valid email address'],
        validate: {
            validator: function () {
                return new Promise((res, rej) => {
                    User.findOne({ email: this.email, _id: { $ne: this._id } })
                        .then(data => {
                            if (data) {
                                res(false)
                            } else {
                                res(true)
                            }
                        })
                        .catch(err => {
                            res(false)
                        })
                })
            }, message: 'Email Already Taken'
        }
    },
    mobile: {
        type: String,
        get: v => parseInt(v),
        trim: true,
        //match : [/^(\+\d{1,3}[- ]?)?\d{10}$/,'Please fill valid mobile number'],
        match: [/^[0-9]*$/, 'Please fill valid mobile number'],
        // validate: {
        //     validator: function () {
        //         return new Promise((res, rej) => {
        //             User.findOne({ mobile: this.mobile, _id: { $ne: this._id } })
        //                 .then(data => {
        //                     if (data) {
        //                         res(false)
        //                     } else {
        //                         res(true)
        //                     }
        //                 })
        //                 .catch(err => {
        //                     res(false)
        //                 })
        //         })
        //     }, message: 'Mobile Already Taken'
        // }
    },
    password: {
        type: String,
        required: true
    },
    otp: {
        type: Number
    },
    userType: {
        type: String,
        enum: ['ADMIN', 'ADMIN_OPERATOR', 'COMPANY_ADMIN', 'COMPANY_OPERATOR', 'COMPANY_USER', 'PARTICIPANT'],
        default: 'COMPANY_ADMIN'
    },
    isActive: {
        type: Boolean,
        enum: [true, false],
        default: true
    },

    isApproved: {
        type: Boolean,
        enum: [true, false],
        default: false
    },

    isAssigned: { // This field is written to tho show that if the user assigned to a company as operator
        type: Boolean,
        enum: [true, false],
        default: false
    },

    assignedToConference: {
        type: String,
        default: ''
    },

    service: {
        type: String,
        default: 'NA'
    },

    availablity: {
        type: String,
        default: 'NA'
    },

    isLoggedIn: {
        type: Boolean,
        enum: [true, false],
        default: false
    },
    profileCreatedAt: {
        type: Date,
        default: Date.now
    },
    userImage: String,

    // companyName : {
    //     type : String,
    //     default : ''
    // },

    pricePerMinute : {
        type : Number,
        default : 0
    },

    companyId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'companyDetail',
        required: true
    },

    createdBy: {
        type: String,
        default: ''
    },


    // company:[{
    //     companyName : String,
    //     companyType : {
    //         type : String,
    //         enum : ['FREE_TYPE','STARTUP','ENTERPRISE',''],
    //         default : ''
    //     },       
    // }],

    // unique participation code < fixed length >
    upc: {
        type: String,
        default: 'XXX-XXX-XXX'
    },
    detailsOne: {
        type: String,
        default: ""
    },
    detailsTwo: {
        type: String,
        default: ""
    },
    addressBook: [{
        addressBookId: String,
        contactUserId: String,
        name: String,
        phoneNumber: String,
        emailAddress: String,
        userType: {
            type: String,
            enum: ['ADMIN', 'ADMIN_OPERATOR', 'COMPANY_ADMIN', 'COMPANY_OPERATOR', 'COMPANY_USER', 'PARTICIPANT', 'GUEST'],
            default: 'COMPANY_USER'
        },
        countryCode: String,
    }]
},
    {
        timestamps: true, // add created_at , updated_at at the time of insert/update
        versionKey: false
    })

userSchema.pre('save', async function (next) {

    var user = this;
    if (this.isModified('password') || this.isNew) {

        try {
            var salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
            var hashed_password = await bcrypt.hash(user.password, salt)
            user.password = hashed_password;
            user._id = new mongoose.Types.ObjectId();
            // let usrTypeBlock = this.userType == 'USER'?'USER':(this.userType=='ADMIN-OPERATOR'?'CMGR':'ADMN')
            // let usrTypeBlock = 'MUSR'
            // user.userId = await autoId.autogenerateId(usrTypeBlock);

            return next();
        } catch (error) {
            return next(error);
        }

    } else {
        return next();
    }
})

userSchema.methods.comparePassword = function (pw, cb) {
    bcrypt.compare(pw, this.password, function (err, isMatched) {
        if (err) return cb({ "err from here": err })
        cb(null, isMatched)
    })
}


const User = mongoose.model("User", userSchema)

module.exports = User