const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const autoId = require('../../../services/AutogenerateId');

const invitationSchema = new mongoose.Schema({

	
    _id : mongoose.Schema.Types.ObjectId,
    conferenceTitle : {
        type : String
    },
    conferenceId : {
        type : String
    },
    invitationCode: {
        type: String
    },
    uniqueCode: {
        type: String
    },

    email: {
        type: String
    },

    generatedLink: {
        type: String
    },

    // email : { 
    //     type: String, 
    //     lowercase: true, 
    //     trim: true,
    //     match : [/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,'Please fill valid email address'],
    //     validate: {
    //         validator: function() {
    //           return new Promise((res, rej) =>{
    //             User.findOne({email: this.email,_id: {$ne: this._id}})
    //                 .then(data => {
    //                     if(data) {
    //                         res(false)
    //                     } else {
    //                         res(true)
    //                     }
    //                 })
    //                 .catch(err => {
    //                     res(false)
    //                 })
    //           })
    //         }, message: 'Email Already Taken'
    //       } 
    // },

    date: {
        type: Date,
        default: Date.now
    }


},
{
    timestamps: true, // add created_at , updated_at at the time of insert/update
    versionKey: false 
})

// conferenceSchema.pre('save', async function (next) {

//     var user = this;
//     if (this.isModified('password') || this.isNew) {

//         try {
//             var salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
//             var hashed_password = await bcrypt.hash(user.password, salt)
//             user.password = hashed_password;
//             user._id = new mongoose.Types.ObjectId();
//             // let usrTypeBlock = this.userType == 'USER'?'USER':(this.userType=='ADMIN-OPERATOR'?'CMGR':'ADMN')
//             let usrTypeBlock = 'MUSR'
//             user.userId = await autoId.autogenerateId(usrTypeBlock);
//             return next();
//         } catch (error) {
//             return next(error);
//         }

//     } else {
//         return next();
//     }
// })

// userSchema.methods.comparePassword = function (pw, cb) {
//     bcrypt.compare(pw, this.password, function (err, isMatched) {
//         if (err) return cb({"err from here":err})
//         cb(null, isMatched)
//     })
// }


invitationSchema.pre('save', async function (next) {
    var invitation = this;
    if (this.isNew) {
        try {            
            invitation._id = new mongoose.Types.ObjectId();
            // amenity.amenityId = await autoId.autogenerateId('AME');
            return next();
        } catch (error) {
            return next(error);
        }

    } else {
        return next();
    }
})


const Invitation = mongoose.model("Invitation", invitationSchema)

module.exports = Invitation