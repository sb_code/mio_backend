const mongoose = require('mongoose');
// const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const autoId = require('../../../services/AutogenerateId');

const guestSchema = new mongoose.Schema({

	
    _id : mongoose.Schema.Types.ObjectId,
    firstName : {
        type : String
    },
    lastName: {
        type: String
    },
    
    email : { 
        type: String, 
        lowercase: true, 
        trim: true,
        match : [/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,'Please fill valid email address']
    },
    mobile : { 
        type: String,
        get : v => parseInt(v),
        trim: true,
        // match : [/^(\+\d{1,3}[- ]?)?\d{10}$/,'Please fill valid mobile number'],
        match : [/^[0-9]*$/,'Please fill valid mobile number'],
    },

    countryCode: {
        type: String
    },
    
    userType : {
        type : String,
        //enum : ['ADMIN','ADMIN-OPERATOR','COMPANY-OPERATOR','ENTERPRISE','STARTER','PARTICIPANT'],
        default : 'GUEST'
    },
    profileCreatedAt: {
        type: Date,
        default: Date.now
    },
    userId: {
        type: String
    },
 

},
{
    timestamps: true, // add created_at , updated_at at the time of insert/update
    versionKey: false 
})



guestSchema.pre('save', async function (next) {
    var guest = this;
    if (this.isNew) {
        try {            
            guest._id = new mongoose.Types.ObjectId();
            // amenity.amenityId = await autoId.autogenerateId('AME');
            return next();
        } catch (error) {
            return next(error);
        }

    } else {
        return next();
    }
})


const Guest = mongoose.model("Guest", guestSchema)

module.exports = Guest