const mongoose = require('mongoose');
// const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const autoId = require('../../../services/AutogenerateId');

const companyDetailSchema = new mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,

	companyId: {
		type: String
	},

	createdBy: {
		type: String
	},

	companyName: {
		type: String
	},

	walletBalence: {
		type: String,
		default: ''
	},

	sipProvider: {
		type: String,
		default: ''
	},

	paymentGateway: {
		type: String,
		default: ''
	},

	companyType: {
		type: String,
		enum: ['FREE_TYPE', 'STARTUP', 'ENTERPRISE', ''],
		default: ''
	},

	companyCreatedAt: {
		type: Date,
		default: Date.now
	},

	noOfSeats: {
		type: Number,
		default: 0
	},

	pricePerSeat: {
		type: Number,
		default: 0
	},

	currency: {
		type: String,
		enum: ['GBP', 'USD', 'NAIRA', 'EURO'],
		default: 'USD'
	},

	vat: {
		type: Number,
		default: 0
	},

	trialPeriod: {
		type: Number,
		default: 0
	},

	status: {
		type: String,
		enum: ['ACTIVE', 'IN_ACTIVE'],
		default: 'ACTIVE'
	},
	//===============
	address: {
		type: String,
		default: ''
	},

	detail1: {
		type: String,
		default: ''
	},

	detail2: {
		type: String,
		default: ''
	},

	//==========
	mobile: {
		type: String,
		default: ""
	},

	countryCode: {
		type: String,
		default: ""
	},

	contactUs: {

		contactNo1: {
			type: String,
			default: ''
		},
		contactNo2: {
			type: String,
			default: ''
		},

		mailId1: {
			type: String,
			default: ''
		},

		mailId2: {
			type: String,
			default: ''
		},


	},
	//================

	operator: [{
		userId: String,
		conferenceId: String,
		startTime: {
			type: String,
			default: ''
		},
		endTime: {
			type: String,
			default: ''
		},

		assignToCompany: {
			type: String,
			default: 'SELF'
		},

		status: {
			type: String,
			enum: ['ASSIGNED', 'WITH_DRAWN', 'NOT_ASSIGNED'],
			default: 'ASSIGNED'
		},
	}],

	services: {
		type: []
	},

	paystackCustomerId: {
		type: String,
	},

	stripeCustomerId: {
		type: String,
	},
	upgradeRequestId: {
		type: String
	}
},
	{
		timestamps: true, // add created_at , updated_at at the time of insert/update
		versionKey: false
	})

companyDetailSchema.pre('save', async function (next) {

	var companyDetail = this;
	if (this.isModified('password') || this.isNew) {

		try {
			companyDetail._id = new mongoose.Types.ObjectId();
			let usrTypeBlock = 'COMP'
			companyDetail.companyId = await autoId.autogenerateId(usrTypeBlock);
			return next();
		} catch (error) {
			return next(error);
		}

	} else {
		return next();
	}
})

const CompanyDetail = mongoose.model("CompanyDetail", companyDetailSchema)

module.exports = CompanyDetail