const mongoose = require('mongoose');
// const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const autoId = require('../../../services/AutogenerateId');

const companySchema = new mongoose.Schema({

	
    _id : mongoose.Schema.Types.ObjectId,
    companyName : {
        type : String
    },
    
    companyId: {
        type: String
    },
   
    date: {
        type: Date,
        default: Date.now
    },
   
       
    status: {
        type: String,
        enum : ['ACTIVE','IN_ACTIVE'],
        default : 'ACTIVE'
    },

    operator:[{
        userId:String,
        startDate: {
            type: String,
            default: '0000000000000'
        },
        endDate: {
            type: String,
            default: '0000000000000'
        },
        status : {
            type : Number,
            enum : [1000,2000,3000],
            default : 1000
        },
    }]
    
    
},
{
    timestamps: true, // add created_at , updated_at at the time of insert/update
    versionKey: false 
})

companySchema.pre('save', async function (next) {
    var company = this;
    if (this.isNew) {
        try {            
            company._id = new mongoose.Types.ObjectId();
            // amenity.amenityId = await autoId.autogenerateId('AME');
            return next();
        } catch (error) {
            return next(error);
        }

    } else {
        return next();
    }
})


const Company = mongoose.model("Company", companySchema)

module.exports = Company