const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const autoId = require('../../../services/AutogenerateId');

const upgradeRequestSchema = new mongoose.Schema({


	_id: mongoose.Schema.Types.ObjectId,

	companyId: {
		type: String,
		default: ''
	},

	companyName: {
		type: String,
		default: ''
	},

	requestedBy: {
		type: String,
	},

	currentCompanyType: {
		type: String,
		default: ''
	},

	changeToCompanyType: {
		type: String,
		default: ''
	},

	changeType: {
		type: String,
		enum: ['UPGRADE', 'DOWNGRADE']
	},

	status: {
		type: String,
		enum: ['APPLIED', 'ACCEPTED', 'REJECTED'],
		default: 'APPLIED'
	},

},
	{
		timestamps: true,
		versionKey: false
	})



upgradeRequestSchema.pre('save', async function (next) {
	var upgradeRequest = this;
	if (this.isNew) {
		try {
			upgradeRequest._id = new mongoose.Types.ObjectId();
			return next();
		} catch (error) {
			return next(error);
		}

	} else {
		return next();
	}
})


const UpgradeRequest = mongoose.model("UpgradeRequest", upgradeRequestSchema)

module.exports = UpgradeRequest