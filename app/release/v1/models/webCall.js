const mongoose = require('mongoose');
// const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const autoId = require('../../../services/AutogenerateId');

const webCallSchema = new mongoose.Schema({
	
    _id : mongoose.Schema.Types.ObjectId,   

    conferenceId : {
        type : String,
        default : ''
    },

    companyId: {
        type: String,
        default : ''
    },

    userId: {
        type: String,
        default : ''
    },

    conferenceDurationId : {
        type: mongoose.Schema.Types.ObjectId,
        //type : String,
        // default : ''
    },

    sessionId : {
        type : String,
        default : ''
    },

    streamId : {
        type : String,
        default : ''
    },

    connectionId: {
        type: String,
        default : ''
    },

   
    startedAt: {
        type: String,
        default : ''
    },

    endedAt: {
        type: String,
        default : ''
    },

    conferenceStatus: {
        type: String,
        enum : ['STARTED','ENDED'],
        default : 'STARTED'
    },
    
    date: {
        type: Date,
        default: Date.now
    }

},
{
    timestamps: true, // add created_at , updated_at at the time of insert/update
    versionKey: false 
})




webCallSchema.pre('save', async function (next) {
    var webCall = this;
    if (this.isNew) {
        try {            
            webCall._id = new mongoose.Types.ObjectId();
           
        } catch (error) {
            return next(error);
        }

    } else {
        return next();
    }
})


const webCall = mongoose.model("webCall", webCallSchema)

module.exports = webCall