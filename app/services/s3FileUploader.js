const config = require("../release/v1/config/globals/keyfile");
const fs = require('fs');
const AWS = require('aws-sdk');
module.exports = {
  upload: (fileToUpload, bucketName, fileName) => new Promise((resolve, reject) => {
    try {

      if (!fileToUpload || !bucketName || !fileName) { reject(); return; }

      let AWS_ACCESS_KEY = config.aws_s3.accessKeyId;
      let AWS_SECRET_ACCESS_KEY = config.aws_s3.secretAccessKey;
      /*
            // name of the file to be uploaded
            let fileNameOriginal = file.name;
      
            // deriving a file name which is to be uploaded
            var arrfileName = fileNameOriginal.split(".");
            var fileExtension = arrfileName.reverse()[0];
            arrfileName.reverse(); // reversing an array
            arrfileName.pop(); // deleting an array from an array
            var date = new Date();
            var timestamp = date.getTime();
      
            var fileName = arrfileName.join();
            fileName = fileName + '-' + timestamp + '.' + fileExtension;
      
     */
      // local location where the file has to be uploaded
      // let fileToUpload = __dirname + '/uploads/' + fileName;

      let fileUrl = `https://${bucketName}.s3.us-east-2.amazonaws.com/${fileName}`;
      //let fileUrl = config.aws_s3.aws_url+bucketName+"/"+fileName;

      const s3 = new AWS.S3({
        accessKeyId: AWS_ACCESS_KEY,
        secretAccessKey: AWS_SECRET_ACCESS_KEY
      });

      // Import events module
      var events = require('events');

      // Create an eventEmitter object
      var eventEmitter = new events.EventEmitter();

      // Create an event handler as follows
      var upload_file_s3_bucket = function upload_file_s3_bucket() { // Start of creation of event handeler
        const fileContent22 = fs.readFileSync(fileToUpload);

        const params = {
          Bucket: bucketName, // pass your bucket name
          Key: fileName,
          ACL: 'public-read',
          Body: fileContent22
        };
        s3.upload(params, function (err, data) {
          if (err) {
            console.log("upload error");
            reject(err);
          }
          if (data) {
            fs.unlink(fileToUpload, function (err, response) {
              if (err) {
                console.log("upload error");
                reject(err);
                return;
              } else {
                resolve(fileUrl);
              }

            });

          }

        });

      } // Start of creation of event handeler

      // Bind the connection event with the handler
      eventEmitter.on('upload_file_s3_bucket', upload_file_s3_bucket);
      eventEmitter.emit('upload_file_s3_bucket');

      // uploading the files
      /*file.mv(fileToUpload, function (err, response) { // Start of File upload in local storage
        if (err) { // When Error Occur
          reject();
          return;
        } else {
          // Fire the data_received event 
          eventEmitter.emit('upload_file_s3_bucket');
        }
      }); // End of File upload in local storage        
*/
    } catch (error) {
      reject(error);
    }
  }),
} 