const pdf = require('html-pdf');
const path = require('path');
const os = require('os');

const s3FileUploader = require('./s3FileUploader');
const config = require("../release/v1/config/globals/keyfile");

module.exports = {
  generatePdf: async (htmlString, fileName) => new Promise((resolve, reject) => {

    const options = {
      "format": "A4",
      "orientation": "portrait",
      "footer": {
        "height": "12mm",
        // "contents": {
        //   default:
        //   `<div style="background-color: rgb(0, 48, 87);color: #fff;height:200px;display: table;position: absolute;height: 100%;width: 100%;">
        //   <div style="margin-top: 8px; display: table-cell; vertical-align: middle;">

        //   <div style="margin-left: auto; margin-right: auto;float: left; position: relative; width: 100%; color: #fff;">
        //   <span style=''>Property Report for <span className="highlight-area">${request.body.address}, <span style={{ textTransform: 'capitalize' }}>New York</span>, NY 85523</span>
        //   </div>
        //   <div style="margin-left: auto; margin-right: auto;float: right; position: relative; width: 100%; color: #fff;">
        //   <span style=''>Page <strong >{{page}}</strong> of <strong>{{pages}}</strong></span>
        //   </div>
        //   </div>
        //   </div>`,
        // }
      }
    }

    const localPDFFile = path.join(os.tmpdir(), 'localPDFFile.pdf');
    console.log('log0');
    console.log(localPDFFile);
    // const html = handlebars.compile(htmlString)(user);
    console.log("template compiled with user data", htmlString);

    pdf.create(htmlString, options).toFile(localPDFFile, (err, res) => {
      console.log('log1');
      console.log(localPDFFile);
      if (err) {
        console.log('log2');
        console.log(err);
        reject(err);
        return;
      }
      console.log("pdf created locally");
      s3FileUploader.upload(localPDFFile, config.aws_s3.invoiceBucketName, fileName).then((url) => {
        resolve(url);
        return;
      }).catch(error => {
        console.log('log3');
        console.log(error);
        reject(error);
        return;
      });
    });

  })
}