const nodemailer = require('nodemailer');

module.exports = {
  viaGmail: (data, callback) => {

    var transporter = nodemailer.createTransport({
      service: 'gmail',
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: 'miowy77@gmail.com',
        pass: 'Password@123'
      }
    });

    const mailoption = {
      from: 'Mio Support<miowy77@gmail.com>',
      to: data.receiver,
      subject: data.subject,
      // text: 'Hello,',
      html: data.message,
      attachments: data.attachments,
    };

    transporter.sendMail(mailoption, (err, info) => {
      if (err) {
        console.log(err)
        callback(err, null)
      } else {
        callback(null, info)
      }
    });

  }


}