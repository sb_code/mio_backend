const Conference = require("../release/v1/models/conference");
const mongoose = require('mongoose');
// const Autoidmodule= {
//     autogenerateId: (request)=>{        
//         var milliseconds = new Date().getTime();
//         const finalId=request+milliseconds+Math.floor(10000 + Math.random() * 90000).toString();
//         return finalId;
//     },    
// }

// module.exports = Autoidmodule;




        

const Autoidmodule= {
    autogenerateId:(request)=>{
        console.log(request)
        var milliseconds = new Date().getTime();
        const finalId=request+milliseconds;
        return finalId;

    },


    autogenerateOTP:async () => {
        let sixDigitOTP = Math.floor(Math.random() * 899999 + 100000);

        async function checkExistanceOTP(sixDigitOTP){
              
            return new Promise(function(resolve, reject){

			    let aggrQuery = [
			        {
			            $match: {
			                'dtmf': sixDigitOTP,
			                'isDeleted': false
			            } 
			        },    
			    ];

	            Conference.aggregate(aggrQuery, function (err, responseConference) {
                    console.log(responseConference);
	                if (err) {
	                    resolve({   
	                          "ErrorStatus": true,     
	                    });
	                } else {
	                    if (responseConference.length > 0) {
	                        resolve({
	                            "isOTPExist": true,
	                            "ErrorStatus": false,
	                        });
	                    } else {
	                        resolve({
	                            "isOTPExist": false,
	                            "ErrorStatus": false,
	                            "sixDigitOTP": sixDigitOTP      
	                        });
	                    }                  
	                }
	            })  
	        })  

        }

        let sixDigitOTPStatus = await checkExistanceOTP(sixDigitOTP);

        return sixDigitOTPStatus;
    },

    

    
}

module.exports = Autoidmodule;